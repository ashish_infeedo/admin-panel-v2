REACT_APP_STAGE=ai GENERATE_SOURCEMAP=false yarn build
aws s3 sync build/static/js s3://beta.admin.ai.amber.infeedo.com/static/js --region us-east-1
aws s3 sync build/static/css s3://beta.admin.ai.amber.infeedo.com/static/css --region us-east-1
aws s3 sync build s3://beta.admin.ai.amber.infeedo.com --region us-east-1