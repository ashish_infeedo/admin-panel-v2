const fs = require('fs');
const path = require('path');
const file = fs.readFileSync(path.resolve('') + '/build/index.html');
const newfile = file.toString().replace(/.js"/g, '.js.gz"').replace(/.css"/g, '.css.gz"');
fs.writeFileSync(path.resolve('') + '/build/index.html', newfile);