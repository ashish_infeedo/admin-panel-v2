REACT_APP_STAGE=stage GENERATE_SOURCEMAP=false yarn build 
aws s3 sync build/static/js s3://beta.admin.fidento.com/static/js --region us-east-1  
aws s3 sync build/static/css s3://beta.admin.fidento.com/static/css --region us-east-1  
aws s3 sync build s3://beta.admin.fidento.com --region us-east-1