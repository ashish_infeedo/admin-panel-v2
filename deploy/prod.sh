# REACT_APP_STAGE=secure GENERATE_SOURCEMAP=false yarn build-prod 
# aws s3 sync build/static/js s3://beta.admin.secure.amber.infeedo.com/static/js --region us-east-1 --content-encoding=gzip 
# aws s3 sync build/static/css s3://beta.admin.secure.amber.infeedo.com/static/css --region us-east-1 --content-encoding=gzip 
# aws s3 sync build s3://beta.admin.secure.amber.infeedo.com --region us-east-1

REACT_APP_STAGE=prod GENERATE_SOURCEMAP=false yarn build-prod 
aws s3 sync build/static/js s3://beta.admin.amber.infeedo.com/static/js --region us-east-1  
aws s3 sync build/static/css s3://beta.admin.amber.infeedo.com/static/css --region us-east-1  
aws s3 sync build s3://beta.admin.amber.infeedo.com --region us-east-1