REACT_APP_STAGE=crash GENERATE_SOURCEMAP=false yarn build-prod 
aws s3 sync build/static/js s3://beta.admin.crash.amber.infeedo.com/static/js --region us-east-1 --content-encoding=gzip 
aws s3 sync build/static/css s3://beta.admin.crash.amber.infeedo.com/static/css --region us-east-1 --content-encoding=gzip 
aws s3 sync build s3://beta.admin.crash.amber.infeedo.com --region us-east-1
