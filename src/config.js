const env = {
  dev: {
    "env": "dev",
    "url": "https://api.dev.amber.infeedo.com/",
    "us-east-1" : "",
    "eu-west-1" : "",
    "ap-south-1" : ""
  },
  local: {
    "env": "local",
    "url": "https://api.amber.fidento.com/",
    "us-east-1" : "",
    "eu-west-1" : "",
    "ap-south-1" : "",
    "pe_url": "http://localhost:3001/"
  },
  crash: {
    "env": "crash",
    "url": "https://api.crash.amber.infeedo.com/",
    "us-east-1" : "",
    "eu-west-1" : "",
    "ap-south-1" : ""
  },
  dev2: {
    "env": "dev2",
    "url": "https://api.dev2.amber.infeedo.com/",
    "us-east-1" : "",
    "eu-west-1" : "",
    "ap-south-1" : ""
  },
  stage: {
    "env": "stage",
    "url": "https://api.amber.fidento.com/",
    "us-east-1" : "https://api.amber.fidento.com",
    "eu-west-1" : "https://api.eu.amber.fidento.com",
    "ap-south-1" : ""
  },
  ai: {
    "env": "ai",
    "url": "https://api.ai.amber.infeedo.com/",
    "us-east-1" : "",
    "eu-west-1" : "",
    "ap-south-1" : ""
  },
  secure: {
    "env": "secure",
    "url": "https://api-us-secure.infeedo.com/",
    "us-east-1" : "https://api-us-secure.infeedo.com/",
    "eu-west-1" : "https://api-eu-secure.infeedo.com/",
    "ap-south-1" : ""
  },
  prod: {
    "env": "prod",
    "url": "https://api1.amber.infeedo.com/",
    "socket_url": "https://api1.amber.infeedo.com",
    "us-east-1" : "https://api.amber.infeedo.com/",
    "eu-west-1" : " https://api-eu.infeedo.com/",
    "ap-south-1" : "https://api-ap.infeedo.com/"
  }
}

// Default to dev if not set
const config = env[process.env.REACT_APP_STAGE];

export default {
  // Add common config values here
  ...config
};