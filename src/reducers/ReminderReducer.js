import { STORE_REMINDERS } from '../actions/Types';

const initialState = {
    reminders: []
}

export default function(state = initialState, action) {
    switch(action.type){
        case STORE_REMINDERS: 
            return { 
                ...state, 
                reminders: action.payload
            }
        default: 
            return state
    }
}