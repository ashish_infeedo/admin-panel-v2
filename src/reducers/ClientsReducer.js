import { 
    STORE_CLIENTS,
    STORE_CLIENTS_LIST, 
    STORE_CLIENT_DETAILS, 
    CLEAR_CLIENT_DETAILS, 
    UPDATE_CLIENT_DETAILS, 
    STORE_BILLING_CONTACTS, 
    STORE_ADMINS
} from '../actions/Types';

const initialState = {
    clients: [],
    admins: [],
    clients_list:[],
    client_details: {},
    billing_contacts: {},
    cohorts: [],
    total_clients: null,
    total_pages: null,
    page_size: 0
}

const formatClients =  (clients) => {
    const clients_list = [{key: 0, value:"All companies"}];
    clients.forEach((company, index) => {
        clients_list.push({key:index+1, value:company.title});
    });
    return clients_list
}

const setClientDetails = (payload, client_details) => {
    if(payload.title) {
        client_details.title = payload.title;
    }
    if(payload.email_domain) {
        client_details.email_domain = payload.email_domain;
    }
    return client_details;
}

export default function(state = initialState, action) {
    switch(action.type){
        case STORE_CLIENTS: 
            return { 
                ...state, 
                clients: action.payload.data,
                total_clients: action.payload.total_clients,
                total_pages: action.payload.total_pages,
                page_size: action.payload.page_size
            }
        case STORE_CLIENTS_LIST:
            return {
                ...state,
                clients_list: formatClients(action.payload.data)
            }
        case STORE_CLIENT_DETAILS:
            return {
                ...state,
                client_details: action.payload
            }
        case CLEAR_CLIENT_DETAILS: 
            return {
                ...state,
                client_details: {}
            }
        case UPDATE_CLIENT_DETAILS:
            return {
                ...state,
                client_details: setClientDetails(action.payload, state.client_details)
            }
        case STORE_BILLING_CONTACTS:
            return {
                ...state,
                billing_contacts: action.payload
            }
        case STORE_ADMINS:
            return {
                ...state,
                admins: action.payload
            }
        default: 
            return state
    }
}