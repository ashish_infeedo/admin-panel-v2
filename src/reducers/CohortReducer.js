import { STORE_COHORTS } from '../actions/Types';

const initialState = {
    cohorts: []
}

export default function(state = initialState, action) {
    switch(action.type){
        case STORE_COHORTS: 
            return { 
                ...state, 
                cohorts: action.payload
            }
        default: 
            return state
    }
}