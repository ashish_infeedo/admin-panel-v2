import { combineReducers } from 'redux';
import ClientReducer from './ClientsReducer';
import GodReducer from './GodReducer';
import WildReducer from './WildReducer';
import ResponseReducer from './ResponseReducer';
import ReminderReducer from './ReminderReducer';
import ReachoutReducer from './ReachoutReducer';
import CohortReducer from './CohortReducer';
import BreadBoardReducer from './BreadBoardReducer';
import AccessReducer from './AccessReducer';

export default combineReducers({
    client: ClientReducer,
    god: GodReducer,
    wild: WildReducer,
    response: ResponseReducer,
    reminder: ReminderReducer,
    reachout: ReachoutReducer,
    cohort: CohortReducer,
    breadboard: BreadBoardReducer,
    access: AccessReducer
})