import { 
    SAVE_ACCESS_EMAILS,
    SAVE_ACCESS_FILE_URL,
    SAVE_DEMOGRAPHIC_ACCESS,
    STORE_ADMINS,
    STORE_ACCESS_GROUPS } from '../actions/Types';

const initialState = {
    access_file_url: "",
    access_emails: [],
    demographic_access_data : [],
    admin_list: [],
    user_permisssions: [],
    access_groups: []
}

export default function(state = initialState, action) {
    switch(action.type){
        case SAVE_ACCESS_EMAILS: 
            return {
                ...state,
                access_emails: action.payload,
            };
        case STORE_ACCESS_GROUPS: 
            return {
                ...state,
                access_groups: action.payload,
            };
        case SAVE_ACCESS_FILE_URL: 
            return {
                ...state,
                access_file_url: action.payload,
            };
        case SAVE_DEMOGRAPHIC_ACCESS: 
            return {
                ...state,
                demographic_access_data: action.payload,
            };
        case STORE_ADMINS: 
            return {
                ...state,
                admin_list: action.payload,
            };
        default: 
            return state;
    }
}