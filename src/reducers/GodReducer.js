import { STORE_GOD_REPORTS } from '../actions/Types';

const initialState = {
    reports: [],
    total_count: null,
    total_pages: null,
    page_size: 0
}

export default function(state = initialState, action) {
    switch(action.type){
        case STORE_GOD_REPORTS: 
            return { 
                ...state, 
                reports: action.payload.data,
                total_count: action.payload.total_count,
                total_pages: action.payload.total_pages,
                page_size: action.payload.page_size 
            }
        default: 
            return state
    }
}