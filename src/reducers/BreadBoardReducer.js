import { BUCKETS_PATCHED } from '../actions/Types';

const initialState = {
    refresh_count: 0
}

export default function(state = initialState, action) {
    switch(action.type){
        case BUCKETS_PATCHED: 
            return { 
                ...state, 
                refresh_count: (state.refresh_count + 1)
            }
        default: 
            return state
    }
}