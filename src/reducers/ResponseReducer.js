import { STORE_RESPONSES } from '../actions/Types';

const initialState = {
    responses: []
}

export default function(state = initialState, action) {
    switch(action.type){
        case STORE_RESPONSES: 
            return { 
                ...state, 
                responses: action.payload
            }
        default: 
            return state
    }
}