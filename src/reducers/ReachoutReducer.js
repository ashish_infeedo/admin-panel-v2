import { STORE_REACHOUTS } from '../actions/Types';

const initialState = {
    reachouts: []
}

export default function(state = initialState, action) {
    switch(action.type){
        case STORE_REACHOUTS: 
            return { 
                ...state, 
                reachouts: action.payload
            }
        default: 
            return state
    }
}