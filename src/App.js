import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import {NotificationContainer} from 'react-notifications';
import { Provider } from 'react-redux';
import store from './store';

// CSS
// import './App.css';
import 'bulma/css/bulma.min.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'react-notifications/lib/notifications.css';
import '../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

// Components
import ProtectedRoute from './helper/ProtectedRoute';
import NotFound from './components/static/NotFound';
import Login from './components/layout/Login';
import AutoLogin from './components/layout/AutoLogin';
import Client from './components/clients/Clients';
import ClientDetails from './components/clients/ClientDetails';
import Header from './components/layout/Header';
import GodMode from './components/godmode/GodMode';
import WildDomain from './components/wild/WildDomain';
import Response from './components/response/Response';
import Email from './components/email/Email';
import Reminder from './components/reminder/Reminder';
import Reachout from './components/reachout/Reachout';
import UserDetails from './components/Users/UserDetails';
import ActionPlanning from './components/actionplanning/ActionPlanning';
import GlobalBanner from './components/banner_config/GlobalBanner';

class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <Router>
          <div className="App">
              <Route path="/admin-panel" component={Header} />
              <div className="column is-11-desktop is-11-widescreen is-11-fullhd margin-auto">
              <div className="app-wrapper prevent-scroll-wrapper">
              <Switch>
                <Route exact path="/" component={Login} />
                <Route exact path="/autologin/:token/:client_id" component={AutoLogin}/>
                <ProtectedRoute exact path="/admin-panel/clients" component={Client} />
                <ProtectedRoute exact path="/admin-panel/clients/:client_id/:selected_tab" component={ClientDetails} />
                <ProtectedRoute exact path="/admin-panel/god-mode" component={GodMode} />
                <ProtectedRoute exact path="/admin-panel/email/:mode" component={Email} />
                <ProtectedRoute exact path="/admin-panel/wild-domain" component={WildDomain} />
                <ProtectedRoute exact path="/admin-panel/responses" component={Response} />
                <ProtectedRoute exact path="/admin-panel/reminders" component={Reminder} />
                <ProtectedRoute exact path="/admin-panel/reachouts" component={Reachout} />
                <ProtectedRoute exact path="/admin-panel/action-planning" component={ActionPlanning} />
                <ProtectedRoute exact path="/admin-panel/global-banner" component={GlobalBanner} />
                <ProtectedRoute exact path="/admin-panel/clients/:client_id/:selected_tab/:user_id" component={UserDetails} />
                <Route component={NotFound} />
              </Switch>
              </div>
              </div>
            </div>
        </Router>
        <NotificationContainer/>
      </Provider>
    );
  }
}

export default App;
