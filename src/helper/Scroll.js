export const ScrollToTop = () => {
    setTimeout(() => { document.body.scrollTop = document.documentElement.scrollTop = 0; }, 500);
}