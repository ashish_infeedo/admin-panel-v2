import axios from 'axios';
import config from '../config';

const _axios = axios.create({
    baseURL: config.url,
});

_axios.interceptors.request.use((config_axios) => {
    if(sessionStorage.token){
        config_axios.headers.Authorization = `Bearer ${sessionStorage.token}`;
    }
    const current_region_url = sessionStorage.current_region_url
    if(current_region_url && config[current_region_url]){
        config_axios.baseURL = config[current_region_url];
    } else {
        config_axios.baseURL = config.url;
    }
    return config_axios;
}, (error) => {
    return Promise.reject(error);
});

_axios.interceptors.response.use((response) => {
    return response;
}, (error) => {
    const {status} = error.response;
    const { url } = error.response.config;
    if (status === 401 && !url.includes('authenticate')) {
        sessionStorage.clear();
        window.location.href = '/';
    }
    return Promise.reject(error);
});
        
export default _axios;