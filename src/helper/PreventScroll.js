let scrollPosition = 0;

export const resetScrollPosition = () => {
    scrollPosition = 0;
}

export const disableBodyScroll = () => {
    scrollPosition = 0;
    scrollPosition = window.pageYOffset;
    document.body.classList.add("body-no-scroll");
}

export const enableBodyScroll = () => {
    document.body.classList.remove("body-no-scroll");
    window.scrollTo(0, scrollPosition);
}