import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classnames from 'classnames';
// actions
import {getWildDomains, saveWildDomains} from '../../actions/WildActions';
// components
import TextInputgroup from '../generic/TextInputGroup';

class WildDomain extends Component {
  state = {
    loading: true,
    saving: false,
    domain: '',
    all_domains: [],
    all_domains_copy: [],
    disable_add: true,
    disable_save: true
}

  componentDidMount() {
    this.getWildDomains();
  }

  getWildDomains = async () => {
    const response = await this.props.getWildDomains();
    this.setState({loading:false});
    this.setState({all_domains: response.data.split(','), all_domains_copy: [...(response.data.split(','))]})
  }

  onSave = async () => {
    const { all_domains } = this.state; 
    this.setState({saving: true});
    await this.props.saveWildDomains(all_domains.join())
    this.setState({saving: false, disable_save: true});
  }

  onDelete = (index) => {
    const { all_domains } = this.state;
    delete all_domains.splice(index, 1);
    this.setState({all_domains: all_domains}, () => this.enableDisableButtons());
  }

  onAdd = (e) => {
    e.preventDefault();
    const { all_domains, domain } = this.state;
    all_domains.unshift(domain);
    this.setState({all_domains: all_domains, domain: ''}, () => this.enableDisableButtons());
  }

  onChange = e => this.setState({ [e.target.name]: e.target.value }, () => this.enableDisableButtons());

  enableDisableButtons = () => {
    const {all_domains_copy, all_domains, domain} = this.state;
    if(all_domains_copy.toString() !== all_domains.toString()){
      this.setState({disable_save: false});
    } else {
      this.setState({disable_save: true});
    }

    if(domain.length){
      this.setState({disable_add: false});
    } else{
      this.setState({disable_add: true});
    }
  }

  render() {
    const { loading, saving, domain, all_domains, disable_add, disable_save } = this.state;
    return (
        <div>
            <form onSubmit={this.onAdd} className="columns">
                <div className="column is-8">
                    <TextInputgroup 
                    name="domain"
                    placeholder="Add Domain"
                    value={domain}
                    type="text"
                    onChange={this.onChange}
                    icon="fas fa-at"
                    />
                </div>
                <div className="column is-2 pd-up-20 has-text-right">
                    <button type="submit" className="button is-primary is-fullwidth" disabled={disable_add}>
                        <span className="icon"><i className="fas fa-plus"></i></span>
                        <span>Add</span>
                    </button>
                </div>
                <div className="column is-2 pd-up-20 has-text-right">
                    <button onClick={this.onSave} type="button" className={classnames('button is-success is-fullwidth',{'is-loading': saving})} disabled={disable_save}>
                        <span className="icon"><i className="fas fa-save"></i></span>
                        <span>Save</span>
                    </button>
                </div>
            </form>
            {!loading ? all_domains.map((key, index) => (
                <span key={index} className="tag is-medium mg-5">
                {key}
                <button onClick={this.onDelete.bind(this, index)} className="delete is-small"></button>
                </span>
            )) : <p className="no-data has-text-centered">Loading Data...</p>}
        </div>
    )
  }
}

// PropTypes
// ---------
WildDomain.propTypes = {
    wild_domains: PropTypes.object.isRequired,
    getWildDomains: PropTypes.func.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    // wild_domains: state.wild.wild_domains,
})

export default connect(mapStateToProps, {getWildDomains, saveWildDomains})(WildDomain);