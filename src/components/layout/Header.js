import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { CSVLink } from 'react-csv';
// actions
import { logout } from '../../actions/LoginActions';
import {getChatFeedback} from '../../actions/ResponseActions';
// assets
import amber_brown from '../../assets/images/amber_brown.png';

class Header extends Component {
  state = {
    is_active: false,
    profile: {},
    current_path: '',
    exportData : "",
    inProgress : false
  }
  csvLink = React.createRef()

  componentDidMount() {
    if(sessionStorage.profile) {
      this.setState({profile: JSON.parse(sessionStorage.profile)});
      this.setActiveTab();
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      this.setState({is_active: false});
      this.setActiveTab();
    }
  }

  setActiveTab = () => {
    const { pathname } = this.props.location;
    this.setState({current_path: pathname});
  }

  toggleNav = () => {
    const { is_active } = this.state;
    this.setState({is_active: !is_active})
  }

  logout = () => {
    this.props.history.push('/');
    this.props.logout();
  }

  downloadChatFeedback = async() =>{
    this.setState({
      inProgress : true
    })
    
    const response = await this.props.getChatFeedback();
    this.setState({
      exportData : response.data,
      inProgress : false
    }, () => {
      this.csvLink.current.link.click()
    })
  }

  render() {
    const { is_active, profile, current_path , inProgress} = this.state;
    return (
    <div>
      <div>
        <nav className="navbar" role="navigation" aria-label="main navigation">
          <div className="navbar-brand">
              <Link to="/admin-panel/clients" className="navbar-item">
                <img src={amber_brown} width="100" height="28" alt="App logo"/>
              </Link>
              
              <a role="button" onClick={this.toggleNav} className={classnames('navbar-burger burger',{'is-active': is_active})} aria-label="menu" aria-expanded="false" data-target="navbarBasic">
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
              </a>
          </div>

          <div id="navbarBasic" className={classnames('navbar-menu',{'is-active absolute-navMenu': is_active})}>
              <div className="navbar-end">
              <Link to="/admin-panel/clients" className={classnames('navbar-item', {'active-tab': current_path.includes('/admin-panel/clients')})}>
                  <i className="fas fa-user-circle nav-icon"></i>
                  Clients
              </Link>
              {profile.id === 2 ? <Link to="/admin-panel/god-mode" className={classnames('navbar-item', {'active-tab': current_path === '/admin-panel/god-mode'})}>
                  <i className="fas fa-eye nav-icon"></i>
                  God Mode
              </Link>: null}
              <Link to="/admin-panel/email/demo" className={classnames('navbar-item', {'active-tab': current_path.includes('/admin-panel/email')})}>
                  <i className="fas fa-envelope nav-icon"></i>
                  Email
              </Link>
              <Link to="/admin-panel/wild-domain" className={classnames('navbar-item', {'active-tab': current_path === '/admin-panel/wild-domain'})}>
                  <i className="fas fa-paw nav-icon"></i>
                  Wild Domain
              </Link>
              <Link to="/admin-panel/reachouts" className={classnames('navbar-item', {'active-tab': current_path === '/admin-panel/reachouts'})}>
                  <i className="fas fa-comment-dots nav-icon"></i>
                  Reachout
              </Link>
              <Link to="/admin-panel/responses" className={classnames('navbar-item', {'active-tab': current_path === '/admin-panel/responses'})}>
                  <i className="fas fa-comments nav-icon"></i>
                  Responses
              </Link>
              <Link to="/admin-panel/reminders" className={classnames('navbar-item', {'active-tab': current_path === '/admin-panel/reminders'})}>
                  <i className="fas fa-clock nav-icon"></i>
                  Reminders
              </Link>
              <Link to="/admin-panel/action-planning" className={classnames('navbar-item', {'active-tab': current_path.includes('/admin-panel/action-planning')})}>
                  <i className="fas fa-newspaper nav-icon"></i>
                  APF
              </Link>
              <Link to="/admin-panel/global-banner" className={classnames('navbar-item', {'active-tab': current_path.includes('/admin-panel/action-planning')})}>
                  <i className="fas fa-bullhorn nav-icon"></i>
                  Global Banner
              </Link>

              <div className="navbar-item has-dropdown is-hoverable">
                  <a className={classnames("navbar-link", {'hidden': is_active})}>
                      <i className="fas fa-cog"></i>
                  </a>

                  <div className="navbar-dropdown is-right">
                     <span className="navbar-item">
                        <b>{profile.name}</b>
                    </span>
                    {profile.id === 2 ?<span className="navbar-item navbar-item-hov">
                       <div onClick= {this.downloadChatFeedback}> Download Chat Feedback </div>
                       <CSVLink filename="data.csv" data={this.state.exportData} ref={this.csvLink}></CSVLink>
                    </span> : null}
                    <a onClick={this.logout} className="navbar-item">
                        Log out
                    </a>
                  </div>
                </div>
              </div>
          </div>
        </nav>
      </div>
      { inProgress !== false 
          ? <div title="Chat Feedback download is in progress" className="feedback-download-spinner"> 
              <i className="fas fa-spinner fa-spin spinner-right-margin"></i>  
              <span className="feedback-download-text"> Chat Feedback download is in progress. </span>
            </div>
          : null
      }
    </div>
    )
  }
}

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  getChatFeedback : PropTypes.func.isRequired
};

export default connect(null, {logout , getChatFeedback})(Header);