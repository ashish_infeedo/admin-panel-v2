import React, { Component } from 'react';
import jwt_decode from 'jwt-decode';

export default class AutoLogin extends Component {
  
  // component mounts
  // ----------------
  componentDidMount(){
      const {token, client_id} = this.props.match.params
      if(token){
        sessionStorage.token = token
        sessionStorage.setItem('profile', JSON.stringify(jwt_decode(token)));
        this.props.history.push(`/admin-panel/clients/${client_id}/access`);
      }
  }

  // render UI
  // ---------
  render() {
    return (
      <div>
        <p className="has-text-centered">loading...</p>
      </div>
    )
  }
}
