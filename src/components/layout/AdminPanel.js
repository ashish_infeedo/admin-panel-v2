import React from 'react';
import {Route} from 'react-router-dom';
// components
import Header from './Header';
import Clients from '../clients/Clients';

const AdminPanel = ({ match }) => (
    <div>
        <Header/>
        <Route exact path={`${match.url}/clients`} component={Clients}/>
    </div>
) 


export default AdminPanel;