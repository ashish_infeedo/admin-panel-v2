import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import map from 'lodash.map';
// components
import TextInputGroup from '../generic/TextInputGroup';
import Dropdown from '../generic/Dropdown'
// actions
import { verifyLogin } from '../../actions/LoginActions';
// assets
import amber_brown from '../../assets/images/amber_brown.png';

class Login extends Component {
    state = {
        email: '',
        password: '',
        loading: false,
        regions : [{key: "us-east-1", value: "Rest of the World (us-east-1)"},
                    {key: "eu-west-1", value: "Europe (eu-west-1)"},
                    {key : 'ap-south-1', value : "India"}],
        selected_region : {key: "us-east-1", value: "Rest of the World (us-east-1)"},
        default_region : {key: "us-east-1", value: "Rest of the World (us-east-1)"},
    };

    componentDidMount(){
        if(sessionStorage.token) {
            this.props.history.push('/admin-panel/clients')
        }
    }

    keyCharAt(key, i) {
        return key.charCodeAt(Math.floor(i % key.length));
    }

    xor_encrypt(key, data) {
        return map(data, (c, i) => {
            return c.charCodeAt(0) ^ this.keyCharAt(key, i);
        });
    }

    onSubmit = async (e) => {
        e.preventDefault();
        this.setState({loading: true})
        const { email, password, selected_region, default_region } = this.state;
        if(selected_region.value !== default_region.value) {
            if(sessionStorage.current_region_url) {
                sessionStorage.current_region_url = selected_region.key;
            }
            else {
                sessionStorage.setItem("current_region_url" , selected_region.key);
            }
        }
        else{
            sessionStorage.removeItem("current_region_url");   
        }
        
        let encrypted_password = this.xor_encrypt('rkjbflaejrbgbjefaljkbewf', password);
        const payload = {email: email, password: encrypted_password};
        await this.props.verifyLogin(payload)
        this.setState({loading: false})
        this.props.history.push('/admin-panel/clients')
    }
  
    onChange = e => this.setState({ [e.target.name]: e.target.value });

    select_region = (selected_region) => {
        this.setState({
            selected_region
        })
    }
  
    render() {
        const {email, password, loading, regions, selected_region} = this.state;

        return (
            <div className="login">
                <div className="columns is-marginless">
                    <div className="column is-4 is-offset-4">
                        <div className="login_form">
                            <figure className="logo has-text-centered">
                                <img src={amber_brown} width="180px" alt="App Logo"></img>
                            </figure><br/>
                            <h5 className="title is-5">Admin Panel Login</h5>
                            <div>
                                <div className="login-form">
                                    <form onSubmit={this.onSubmit}>
                                        <Dropdown dropdown_items={regions} selected_item={selected_region} onClick={this.select_region}/>
                                        <TextInputGroup
                                            name="email"
                                            placeholder="Enter Email"
                                            value={email}
                                            type="email"
                                            onChange={this.onChange}
                                            icon="fas fa-envelope"
                                            auto_focus={true}
                                        />
                                        <TextInputGroup
                                            name="password"
                                            placeholder="Enter Password"
                                            value={password}
                                            type="password"
                                            onChange={this.onChange}
                                            icon="fas fa-lock"
                                        />
                                        <div className="field">
                                            <p className="control">
                                            <button type="submit" className={classnames('button is-primary is-fullwidth',{'is-loading': loading})}>
                                                Login
                                            </button>
                                            </p>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

Login.propTypes = {
    verifyLogin: PropTypes.func.isRequired
};

export default connect(null, {verifyLogin})(Login);