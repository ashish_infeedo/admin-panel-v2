import React from 'react';
import PropTypes from 'prop-types';

const Dropdown = ({
  selected_item,
  dropdown_items,
  onClick,
  identity,
  classes,
  dropdown_classes,
  loading
}) => {
  return (
    <div className={`dropdown is-hoverable ${dropdown_classes}`}>
        <div className={`dropdown-trigger ${dropdown_classes}`}>
            <button type="button" className={`button ${classes}`} aria-haspopup="true" aria-controls="dropdown-menu4">
            <span>{loading ? 'loading...' : (selected_item[identity? identity: 'value'] ? selected_item[identity? identity: 'value'] : 'No Selection')}</span>
            <span className="icon is-small">
                <i className="fas fa-angle-down" aria-hidden="true"></i>
            </span>
            </button>
        </div>
        {
          dropdown_classes.search('is-disabled') > -1?
           '':
           <div className="dropdown-menu" id="dropdown-menu4" role="menu">
            <div className="dropdown-content dropdown-fix-height">
            {dropdown_items.length ? dropdown_items.map((item, index) => (
                <a key={index} onClick={onClick.bind(this, item)} className="dropdown-item">
                    {item[identity? identity: 'value']}
                </a>
            ))
            : <a className="dropdown-item">
                  No Data
              </a>
            }
            </div>
        </div>
        }
    </div>
  );
};

Dropdown.propTypes = {
  selected_item: PropTypes.object.isRequired,
  dropdown_items: PropTypes.array.isRequired,
  onClick: PropTypes.func.isRequired,
  identity: PropTypes.string,
  dropdown_classes: PropTypes.string,
  loading: PropTypes.bool,
};

Dropdown.defaultProps = {
    selected_item: 0,
    dropdown_classes: '',
    loading: false
};

export default Dropdown;