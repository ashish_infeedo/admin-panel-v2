import React, { Component } from 'react';
import TextInputgroup from '../TextInputGroup';

class SelectDropdown extends Component {
    render() {
        const {access_data, access_key, show_title, extra_info} = this.props;
        return (
            <div className="pd-10">
            {show_title ? <p className="is-size-7 space-dd"><b>{access_data.title} ({access_data.options.length})</b></p> : null}
              <div className="dropdown is-hoverable">
                    <div className="dropdown-trigger">
                        <button className="button is-size-7 width-180 justify-space-between" aria-haspopup="true" aria-controls="dropdown-menu2">
                        {access_data.data_loaded ? <span>({access_data.selected_count}) Selected</span> : <span>loading...</span>}
                        <span className="icon is-small">
                            <i className="fas fa-angle-down" aria-hidden="true"></i>
                        </span>
                        </button>
                    </div>
                    <div className="dropdown-menu width-280 no-margin" id="dropdown-menu2" role="menu">
                        <div className="dropdown-content fixed-height-dd">
                            <div className="dropdown-item">
                            <TextInputgroup
                                name={access_key}
                                placeholder="search"
                                value={access_data.search_text}
                                type="text"
                                extra_info={extra_info}
                                onChange={this.props.onSearch}
                                icon="fas fa-search"
                                is_small={true}
                            />
                            </div>
                            {access_data.search_options.length ? access_data.search_options.slice(0, access_data.show_only).map((option, index) => (
                                <React.Fragment key={index}>
                                <div className="dropdown-item">
                                    <label className="checkbox">
                                        <input type="checkbox" checked={access_data.selected_options[option.title] ? access_data.selected_options[option.title] : false} onChange={this.props.selectOption.bind(this, access_key, option.title, false, extra_info)}/>
                                        &nbsp;{option.title}
                                    </label>
                                </div>
                                {index+1 === access_data.search_options.length || index+1 === access_data.show_only ? null : <hr className="dropdown-divider"/>}
                                </React.Fragment>
                            )):
                                <div className="dropdown-item">
                                    <p className="has-text-centered">No Results</p>
                                </div> 
                            }
                            {access_data.show_only < access_data.search_options.length ? 
                                <React.Fragment>
                                    <hr className="dropdown-divider"/>
                                    <div className="dropdown-item">
                                        <p className="has-text-centered">
                                            <span className="text-underline blue-text-color pointer font-14" onClick={this.props.loadMore.bind(this, access_key, extra_info)}>
                                                show more
                                            </span>
                                        </p>
                                    </div>
                                </React.Fragment>
                            : null}
                        </div>
                    </div>  
                </div>
            </div>
        )
    }
}

export default SelectDropdown;
