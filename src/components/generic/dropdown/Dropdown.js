import React from 'react';
import Select from 'react-select';

const Dropdown = ({
    options,
    defaultValue,
    isMulti,
    handleChange,
    type,
    classes
  }) => {
    return (
        <div className={classes}>
            <Select
                defaultValue={defaultValue}
                isMulti = {isMulti}
                options={options}
                classNamePrefix="select"
                onChange={handleChange.bind(this, type)}
            />
        </div>
    )
};

export default Dropdown;

