import React from 'react';
import PropTypes from 'prop-types';

const Pagination = ({
  total_pages,
  current_page,
  onPrevious,
  onNext,
  jumpToLast
}) => {
  return (
    <nav className="pagination is-small is-centered is-rounded" role="navigation" aria-label="pagination">
        <button onClick={onPrevious} className="pagination-previous" disabled={current_page === 1}>Previous</button>
        <button onClick={onNext} className="pagination-next" disabled={current_page === total_pages || total_pages === 0}>Next</button>
        {total_pages === 0 
        ? null 
        : 
        <ul className="pagination-list">
          <li><button className="pagination-link is-current" aria-current="page">{current_page}</button></li>
          <li><b> of </b></li>
          <li><button onClick={jumpToLast} disabled={current_page === total_pages} className="pagination-link">{total_pages}</button></li>
        </ul>}
    </nav>
  );
};

Pagination.propTypes = {
  total_pages: PropTypes.number,
  current_page: PropTypes.number.isRequired,
  onPrevious: PropTypes.func.isRequired,
  onNext: PropTypes.func.isRequired,
  jumpToLast: PropTypes.func.isRequired
};

Pagination.defaultProps = {
    current_page: 1,
    total_pages: 1
};

export default Pagination;