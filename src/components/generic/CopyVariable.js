import React, { Component } from 'react'

export default class componentName extends Component {

    // Copy Text
    // ---------
    copyText = (id) => {
        const copyText = document.querySelector(`#${id}`);
        copyText.select();
        document.execCommand("copy");
    }

    render() {
        const { variables } = this.props;
        return (
            <div className="column is-3">
                <article className="message is-info">
                <div className="message-body">
                <p><b className="v-middle">Copy Variables</b><br/><br/></p>
                    {variables.map((variable, index) => (
                        <React.Fragment key={index}>
                            <p className="is-size-7 has-text-weight-bold">{variable.description}</p>
                            <div className="field has-addons is-small varibale-box">
                                <div className="control">
                                    <input className="input is-small" type="text" id={variable.id} value={variable.value} readOnly/>
                                </div>
                                <div className="control" onClick={this.copyText.bind(this, variable.id)}>
                                    <a type="cancel" className="button is-small is-info">
                                    <i className="fas fa-copy"></i>
                                    </a>
                                </div>
                            </div>
                        </React.Fragment>
                    ))
                    }
                </div>
                </article>
            </div>
        )
    }
}
