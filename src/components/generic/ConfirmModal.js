import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import parse from 'html-react-parser';

const ConfirmModal = ({
  title,
  text,
  confirm_text,
  onConfirm,
  onClose,
  is_active,
  doing
}) => {
  return (
    <div className={classnames('modal',{'is-active': is_active})}>
        <div className="modal-background" onClick={onClose}></div>
        <div className="modal-card">
            <header className="modal-card-head">
            <p className="modal-card-title is-size-6 has-text-weight-bold">{title}</p>
            <button onClick={onClose} className="delete" aria-label="close"></button>
            </header>
            <section className="modal-card-body">
            {parse(text)}
            </section>
            <footer className="modal-card-foot">
                <button onClick={onClose} className="button">Cancel</button>
                <button onClick={onConfirm} className={classnames('button is-success',{'is-loading': doing})}>{confirm_text}</button>
            </footer>
        </div>
    </div>
  );
};

ConfirmModal.propTypes = {
  text: PropTypes.string,
  title: PropTypes.string,
  confirm_text: PropTypes.string,
  doing: PropTypes.bool,
  is_active: PropTypes.bool.isRequired,
  onConfirm: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

ConfirmModal.defaultProps = {
    title: 'Confirm',
    text: 'Are you sure ?',
    confirm_text: 'Yes',
    is_active: false,
    doing: false
};

export default ConfirmModal;