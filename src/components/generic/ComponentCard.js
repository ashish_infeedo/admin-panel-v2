import React from 'react';
import classnames from 'classnames';

const ComponentCard = (WrappedComponent, title, index, number_color_class) => {
  class HOC extends React.Component {

    state = {
        is_open: false
    }

    componentDidUpdate(prevProps){
        const { disabled } = this.props;
        if(disabled !== prevProps.disabled && disabled === true){
            this.setState({is_open: false});
        }
    }

    toggleOpen = () => {
        const { is_open } = this.state;
        const { disabled } = this.props;
        if(disabled){
            return;
        }
        this.setState({is_open: !is_open})
    }

    render() {
        const { is_open } = this.state;
        const { disabled, dynamic_title, dynamic_index, action_buttons, return_value } = this.props;
        return (
            <div className="card">
            <header className={classnames('card-header is-hoverable', {'disable-accordian': disabled})}>
                <p onClick={this.toggleOpen} className="card-header-title is-marginless">
                    <span className={`tag is-rounded ${(number_color_class ? number_color_class : 'is-link')}`}>{(dynamic_index ? dynamic_index : index)}</span>&nbsp; {(dynamic_title && dynamic_title.length ? dynamic_title : title)}
                </p>
                {
                    action_buttons && action_buttons.length 
                    ? 
                        <span className="buttons pd-up-5 is-marginless">
                            {
                                action_buttons.map((button, index) => (
                                    <button key={index} onClick={() => {button.function_call(return_value)}} className={`button is-small is-rounded is-outlined ${button.color_class}`} disabled={button.disabled}>
                                        <span>{button.title}</span>
                                    </button>
                                ))
                            }
                        </span>
                    : null
                }
                <span className="icon mg-10">
                    {is_open ? <i className="fas fa-angle-down" aria-hidden="true"></i> :  <i className="fas fa-angle-right" aria-hidden="true"></i>} 
                </span>
            </header>
            {is_open? 
            <div className="card-content">
                <div className="content">
                    <WrappedComponent {...this.props}/>
                </div>
            </div>
            : null}
        </div>
        );
    }
  }
    
  return HOC;
};

export default ComponentCard;