import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const TextInputGroup = ({
  label,
  name,
  value,
  placeholder,
  type,
  onChange,
  error,
  is_small,
  auto_focus,
  icon,
  required,
  disabled,
  extra_info
}) => {
  return (
    <div className="field">
        <label className="label" htmlFor={name}>{label}</label>
        <p className="control has-icons-left has-icons-right">
        <input 
            className={classnames("input", {'is-small': is_small})}
            type={type} 
            placeholder={placeholder}
            name={name} 
            value={value}
            onChange={(e) => onChange(e, extra_info)}
            required={required} 
            autoFocus={auto_focus}
            disabled={disabled}/>
        <span className="icon is-small is-left">
            <i className={icon}></i>
        </span>
        <span className="help is-danger">{error}</span>
        </p>
    </div>
  );
};

TextInputGroup.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  value: PropTypes.any.isRequired,
  type: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  error: PropTypes.string,
  is_small: PropTypes.bool,
  auto_focus: PropTypes.bool,
  icon: PropTypes.string,
  required: PropTypes.bool,
};

TextInputGroup.defaultProps = {
  type: 'text',
  is_small: false,
  auto_focus: false,
  required: true
};

export default TextInputGroup;
