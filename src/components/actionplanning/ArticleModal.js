import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const ArticleModal = ({
  onClose,
  article,
  is_active,
  edit_mode,
  onChange,
  onSubmit,
  elements,
  onSelectElements,
  onSelectCategory,
  saving,
  link_meta_data,
  onFetchData
}) => {
  return (
      <div className={classnames('modal', { 'is-active': is_active })}>
          <div className="modal-background"></div>
          <div className="modal-card">
              <header className="modal-card-head">
                  <p className="modal-card-title">Add Article</p>
              </header>
              <section className="modal-card-body">
                  <form onSubmit={onSubmit}>
                      <div className="field">
                          <label className="label">Link</label>
                          <div className="control">
                              <input onChange={onChange} value={(article ? article.link : '')} className="input" type="text" name="link" placeholder="Enter Link" required />
                              <button onClick={onFetchData.bind(this, article)} type="button" className="button mg-top-10 is-info is-small is-outlined">
                                  <span>Fetch Data</span>
                              </button>
                          </div>
                      </div>
                      {link_meta_data &&
                          <div>
                              <div className="field">
                                  <label className="label">Title</label>
                                  <div className="control">
                                      <input value={(article ? article.title : '')} name="title" className="input" type="text" placeholder="Enter Title" disabled required />
                                  </div>
                              </div>
                              <div className="field">
                                  <label className="label">Categories</label>
                                  <div className="buttons">
                                      <button type="button" onClick={onSelectCategory.bind(this, 'Informational')} className={classnames('button is-small', { 'is-info': article.categories.includes('Informational') })}>Informational</button>
                                      <button type="button" onClick={onSelectCategory.bind(this, 'Actionable')} className={classnames('button is-small', { 'is-info': article.categories.includes('Actionable') })}>Actionable</button>
                                      <button type="button" onClick={onSelectCategory.bind(this, 'Recommended')} className={classnames('button is-small', { 'is-info': article.categories.includes('Recommended') })}>Recommended</button>
                                  </div>
                              </div>

                              <div className="field">
                                  <label className="label">Summary</label>
                                  <div className="control">
                                      <textarea className="textarea" value={(article ? article.summary : '')} name="summary" placeholder="Enter Article Summary" disabled required></textarea>
                                  </div>
                              </div>

                              <div className="field">
                                  <label className="label">Elements</label>
                                  <div className="buttons">
                                      {
                                          elements.map(element => (
                                              <button key={element.id} onClick={onSelectElements.bind(this, element.id)} type="button" className={classnames('button is-small', { 'is-info': article.elements_id.includes(element.id) })}>{element.name}</button>
                                          ))
                                      }
                                      {/* <button type="button" className="button is-small">Actionable</button>
                                        <button type="button" className="button is-small">Recommended</button> */}
                                  </div>
                              </div>

                          </div>
                      }
                      <hr />
                      <div className="buttons is-right">
                          <button type="button" className="button" onClick={onClose}>Cancel</button>
                          <button type="submit" disabled={!link_meta_data} className={classnames('button is-success', { 'is-loading': saving })}>{edit_mode ? 'Update' : 'Save'} Article</button>
                      </div>
                  </form>
              </section>
          </div>
      </div>
  );
};

ArticleModal.propTypes = {
  edit_mode: PropTypes.bool.isRequired,
  article: PropTypes.object.isRequired,
  is_active: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  elements: PropTypes.array.isRequired,
  onSelectElements: PropTypes.func.isRequired,
  onSelectCategory: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  saving: PropTypes.bool.isRequired,
  onFetchData: PropTypes.bool.isRequired
};

ArticleModal.defaultProps = {
    is_active: false
};

export default ArticleModal;