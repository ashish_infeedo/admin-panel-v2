import React from 'react';
import PropTypes from 'prop-types';


const ArticleCard = ({
    article,
    onEdit,
    onDelete
  }) => {
return (
    <div className="columns">
        <div className="card column apf-card">
            <div className="content padding-10">
                <div className="columns">
                    <h6 className="column is-8 has-text-grey-dark">{article.title}</h6>
                    <div className="column is-4">
                        <div className="buttons is-right">
                            {/* <button onClick={onEdit.bind(this, article)} type="button" className="button is-info is-small is-outlined">
                                <span className="icon"><i className="fas fa-edit"></i></span>
                                <span>Edit</span>
                            </button> */}
                            <button onClick={onDelete.bind(this, article)} type="button" className="button is-danger is-small is-outlined">
                                <span className="icon"><i className="fas fa-trash-alt"></i></span>
                                <span>Delete</span>
                            </button>
                        </div>
                    </div>
                </div>
                <p className="is-size-7 has-text-grey">
                    {article.upvotes} upvotes, {article.downvotes} downvotes
                </p>
                <p className="font-size-14">
                    {article.summary}
                </p>
                <hr/>
                <div className="columns">
                    <div className="no-padding-margin-bottom column is-10 tags are-medium">
                        <span className="is-size-7"><b>Elements: &nbsp;&nbsp;</b></span>
                        {
                            article.elements_name ? article.elements_name.map(element =>  (
                                <span key={element} className="tag is-grey-dark is-normal">{element}</span>
                            )) : null
                        }
                    </div>
                    <p className="no-padding-margin-bottom column font-size-14 has-text-right">
                        <a href={article.link} className="anchor-blue" target="_blank" rel="noopener noreferrer">
                            <span>
                                Read full article
                            </span>
                        </a>
                    </p>
                </div>
                <div className="tags are-medium">
                    <span className="is-size-7"><b>Categories: &nbsp;&nbsp;</b></span>
                    {
                        article.categories ? article.categories.map(category =>  (
                            <span key={category} className="tag is-warning is-normal">{category}</span>
                        )) : null
                    }
                </div>
            </div>
        </div>
    </div>
);
};

// Porptypes
// ---------
ArticleCard.propTypes = {
    article: PropTypes.object.isRequired,
    onEdit: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired
};

export default ArticleCard;
