import React, { Component } from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { NotificationManager } from 'react-notifications';
// actions
import {getDrivers, getElements, createArticle, getAllArticles, editArticle, deleteArticle, getLinkMetaData } from '../../actions/APFActions';
// components
import ArticleModal from './ArticleModal';
import ArticleCard from './ArticleCard';
import ConfirmModal from '../generic/ConfirmModal';

class ActionPlanning extends Component {
    
    state = {
        drivers: [],
        all_articles: [],
        selected_driver: {
            name: '',
            id: null,
            tag_line: ''
        },
        article_modal_open: false,
        edit_mode: false,
        selected_article: {
            article_id: null,
            title: "",
            summary: "",
            categories: [],
            link: '',
            driver: null,
            elements_id: []
        },
        elements: [],
        saving: false,
        loading_drivers: false,
        loading_elements: false,
        loading_articles: true,
        deleting: false,
        delete_modal_active: false,
        link_meta_data: false
    }

    componentDidMount(){
        this.getDrivers();
    }

    getDrivers = async () => {
        this.setState({loading_drivers: true});
        const response = await this.props.getDrivers();
        if(response && response.status === 200){
            const drivers = response.data.data;
            this.setState({drivers, selected_driver: (drivers.length ? drivers[0] : {})}, () => {
                this.getElements();
            });
        }
        this.setState({loading_drivers: false});
    }

    getElements = async () => {
        const { selected_driver } = this.state;
        if(!selected_driver.id){
            return;
        }
        this.setState({loading_elements: true});
        const response = await this.props.getElements(selected_driver.id);
        if(response && response.status === 200){
            const elements = response.data.data
            this.setState({elements: (elements.length ? elements : [])}, () => this.getAllArticles());
        }
        this.setState({loading_elements: false});
    }

    getAllArticles = async () => {
        const { selected_driver } = this.state;
        if(!selected_driver.id){
            return;
        }
        this.setState({loading_articles: true});
        const response = await this.props.getAllArticles(selected_driver.id);
        if(response && response.status === 200){
            const all_articles = response.data.data;
            this.setState({all_articles: (all_articles.length ? all_articles : [])});
        }
        this.setState({loading_articles: false});
    }

    transformArticles = (articles) => {
        const { elements } = this.state;
        const all_articles = [];
        elements.forEach(element => {
            const element_data = {element : element.name, articles: []};
            articles.forEach(article => {
                if(article.elements_name.includes(element.name)){
                    element_data.articles.push(article);
                }
            })
            all_articles.push(element_data);
        });
        return all_articles;
    }

    selectTab = (selected_driver) => {
        this.setState({selected_driver, loading_articles: true}, () => {
            this.getElements();
        });
    }

    openArticleModal = () => {
        this.setState({article_modal_open: true});
    }

    resetSelecetdArticle = () => {
        const selected_article = {
            article_id: null,
            title: "",
            summary: "",
            categories: [],
            link: '',
            driver: null,
            elements_id: []
        };
        this.setState({selected_article});
    }

    closeArticleModal = () => {
        this.resetSelecetdArticle();
        this.setState({article_modal_open: false, edit_mode: false, link_meta_data: false});
    }

    onChange = e => {
        const { selected_article } = this.state;
        selected_article[e.target.name] = e.target.value
        this.setState({selected_article});
    };

    addUpdateArticle = async (event) => {
        event.preventDefault();
        const { selected_driver, selected_article, edit_mode } = this.state;
        if(selected_article.elements_id.length === 0){
            NotificationManager.info("Please Select atleast one element");
            return;
        }
        if(selected_article.categories.length === 0){
            NotificationManager.info("Please Select atleast one category");
            return;
        }
        this.setState({saving: true});
        if(!edit_mode){
            delete selected_article.article_id;
            delete selected_article.driver;
            const response = await this.props.createArticle(selected_driver.id, selected_article);
            if(response && response.status === 200){
                NotificationManager.success("Article Saved");
                this.closeArticleModal();
                this.getAllArticles();
            }
        } else {
            const article_id = selected_article.article_id;
            delete selected_article.article_id;
            delete selected_article.driver;
            const response = await this.props.editArticle(selected_driver.id, article_id, selected_article);
            if(response && response.status === 200){
                NotificationManager.success("Article Saved");
                this.closeArticleModal();
                this.getAllArticles();
            }
        }
        this.setState({saving: false,link_meta_data : false});
    }

    onDeleteArticle = async () => {
        const { selected_article, selected_driver } = this.state;
        this.setState({deleting: true});
        const response = await this.props.deleteArticle(selected_driver.id, selected_article.article_id);
        if(response && response.status === 200){
            NotificationManager.success("Article Deleted");
            this.getAllArticles();
            this.resetSelecetdArticle();
            this.closeConfirmModal();
        }
        this.setState({deleting: false});
    }

    onSelectElements = (element_id) => {
        const { selected_article } = this.state;
        if(selected_article.elements_id.includes(element_id)){
            selected_article.elements_id.splice(selected_article.elements_id.indexOf(element_id), 1);
        } else {
            selected_article.elements_id.push(element_id);
        }
        selected_article.elements_id.sort();
        this.setState({selected_article});
    }

    onSelectCategory = (category) => {
        const { selected_article } = this.state;
        if(selected_article.categories.includes(category)){
            selected_article.categories.splice(selected_article.categories.indexOf(category), 1);
        } else {
            selected_article.categories.push(category);
        }
        selected_article.categories.sort();
        this.setState({selected_article});
    }

    onEdit = (selected_article) => {
        this.setState({selected_article: JSON.parse(JSON.stringify(selected_article)), edit_mode: true}, this.openArticleModal());
    }

    onDelete = (selected_article) => {
        this.setState({selected_article, delete_modal_active: true});
    }

    onFetchData = async () => {
        const { selected_article } = this.state;
        const link_data = {
            link: selected_article.link
        }
        const response = await this.props.getLinkMetaData(link_data);

        if (response && response.status === 200) {
            const link_metadata = response.data.data;
            selected_article.title = link_metadata.title;
            selected_article.summary = link_metadata.summary;
            this.setState({
                selected_article,
                link_meta_data: true
            })
        }
    }

    closeConfirmModal = () => {
        this.setState({delete_modal_active: false});
    }

    render() {
        const { drivers, selected_driver, article_modal_open, edit_mode, selected_article, elements, saving, all_articles, loading_articles, deleting, delete_modal_active, link_meta_data } = this.state;
        return (
            <div className="content">
                <h3>Action Planning Framework</h3>
                <div className="tabs">
                    <ul className="is-marginless">
                        {
                            drivers.map((driver, index) => (
                                <li onClick={this.selectTab.bind(this, driver)} key={index} className={classnames('', {'is-active': driver.name === selected_driver.name})}>
                                    <a>{driver.name}</a>
                                </li>
                            ))
                        }
                    </ul>
                </div>
                <div className="buttons is-right">
                    <button type="button" className="button is-success" onClick={this.openArticleModal}>
                        <span className="icon"><i className="fas fa-plus"></i></span>
                        <span>Add New Article</span>
                    </button>
                </div>
                <div className="padding-15">
                { all_articles.length && !loading_articles ? 
                    (all_articles.map((article, index) => (
                        <div key={index} className="apf-card-wrapper">
                            <ArticleCard article={article} key={index} onEdit={this.onEdit} onDelete={this.onDelete}/>
                        </div>
                    )))
                    : <p className="has-text-centered">{!loading_articles ? 'No Articles added': 'loading...'}</p>
                }
                </div>
                <ArticleModal 
                    is_active={article_modal_open} 
                    edit_mode={edit_mode}
                    onClose={this.closeArticleModal} 
                    article={selected_article}
                    onChange={this.onChange}
                    onSubmit={this.addUpdateArticle}
                    onFetchData = {this.onFetchData}
                    elements={elements}
                    onSelectElements={this.onSelectElements}
                    onSelectCategory={this.onSelectCategory}
                    saving={saving}
                    link_meta_data = {link_meta_data}
                    />
                <ConfirmModal
                    text="Are you sure you want to delete?"
                    title="Delete Article"
                    confirm_text="Yes"
                    doing={deleting}
                    is_active={delete_modal_active}
                    onConfirm={this.onDeleteArticle}
                    onClose={this.closeConfirmModal}
                />
            </div>
        )
    }
}


ActionPlanning.propTypes = {
    getDrivers: PropTypes.func.isRequired,
    getElements: PropTypes.func.isRequired,
    createArticle: PropTypes.func.isRequired,
    getAllArticles: PropTypes.func.isRequired,
    deleteArticle: PropTypes.func.isRequired,
    editArticle: PropTypes.func.isRequired,
    getLinkMetaData: PropTypes.func.isRequired
}

export default connect(null, {getDrivers, getElements, createArticle, getAllArticles, editArticle, deleteArticle, getLinkMetaData})(ActionPlanning);