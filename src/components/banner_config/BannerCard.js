import React, { Component } from "react";
import DateTimePicker from "react-datetime-picker";
import Switch from "react-switch";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import classnames from "classnames";
import { EditorState, convertToRaw, ContentState } from "draft-js";
import PropTypes from "prop-types";

export default class BannerCard extends Component {
  // initial state
  //----------------------------------------------------
  state = {
    editorState: EditorState.createEmpty(),
    toolbar: { options: ["inline" , "link"] },
    data: {},
    original_copy_data : {},
    btn_loading: false,
    selected_color: "Red",
    all_color: [
      { key: 1, value: "Red" },
      { key: 2, value: "Yellow" },
      { key: 3, value: "Green" },
    ],
    all_pages: {
      "all" : "All",
      "people" : "People",
      "response" : "Response",
      "highlights" : "Highlights",
      "engagement" : "Engagement",
      "textsens" : "Textsens",
      "attrition" : "Attrition",
      "driver-element-analysis" : "Driver Element",
      "saved-views" : "Saved Views",
      "action-planning" : "Action Planning",
      "anonymous-messages" : "Anonymous Messages"
    },
    priority_level: [1, 2, 3],
  };

  // component mount
  //----------------------------------------------------
  componentDidMount() {
    this.setBannerData();
  }

  //component update (update data is previous data is not same)
  //----------------------------------------------------
  componentDidUpdate(prevProps) {
    const { data } = this.props;
    if (JSON.stringify(data) !== JSON.stringify(prevProps.data)) {
      this.setBannerData();
    }
  }

  // setting banner data for card
  //----------------------------------------------------
  setBannerData = () => {
    const { data } = this.props;
    const banner_data = JSON.parse(JSON.stringify(data));
    if (banner_data.start_time && banner_data.end_time) {
      banner_data.start_time = banner_data.start_time * 1000;
      banner_data.end_time = banner_data.end_time * 1000;
    }
    if(banner_data.content.length !== 0)
    {
      banner_data.content = (decodeURIComponent(escape(window.atob(banner_data.content))));
    }
    this.setState({ data: banner_data,  original_copy_data : JSON.stringify(banner_data)});
    if (data.color_code) {
      if (data.color_code === 3) {
        this.setState({ selected_color: "Green" });
      } else if (data.color_code === 2) {
        this.setState({ selected_color: "Yellow" });
      } else {
        this.setState({ selected_color: "Red" });
      }
    }
    if (banner_data.content.length !== 0) {
      this.createEditorState(banner_data.content);
    }
    if (banner_data.content.length === 0) {
      this.setState({ editorState: EditorState.createEmpty() });
    }
  };

  // editor state
  //----------------------------------------------------
  createEditorState = content => {
    const contentBlock = htmlToDraft(content);
    if (contentBlock) {
      const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
      const editorState = EditorState.createWithContent(contentState);
      this.setState({ editorState });
    }
  };

  // editor state change
  //----------------------------------------------------
  onEditorStateChange = editorState => {
    let { data } = this.state;
    this.setState({
      editorState,
    });
    data.content = draftToHtml(convertToRaw(editorState.getCurrentContent()));
    this.setState(data);
  };

  // chnange banner start date
  //----------------------------------------------------
  bannerStart = date => {
    const { data } = this.state;
    if (date !== null) {
      data.start_time = date.valueOf();
    } else {
      data.start_time = null;
    }
    this.setState({ data });
  };

  // chnage banner end date
  //----------------------------------------------------
  bannerEnd = date => {
    const { data } = this.state;
    if (date !== null) {
      data.end_time = date.valueOf();
    } else {
      data.end_time = null;
    }
    this.setState({ data });
  };

  // update banner on click of update btn
  //----------------------------------------------------
  updateBanner = async () => {
    const { updateBannerContent } = this.props;
    const { data } = this.state;
    this.setState({ btn_loading: true, original_copy_data : JSON.stringify(data)});
    let temp_data = JSON.parse(JSON.stringify(data));
    temp_data.content = btoa(unescape(encodeURIComponent(temp_data.content)));
    temp_data.end_time = temp_data.end_time / 1000;
    temp_data.start_time = temp_data.start_time / 1000;
    await updateBannerContent(temp_data);
    this.setState({ btn_loading: false });
  };

  // change active status
  //----------------------------------------------------
  changeActiveStatus = status => {
    let { data } = this.state;
    data.status = status;
    this.setState({ data });
  };

  //delete banner on click of delete btn
  //----------------------------------------------------
  deleteBanner = async () => {
    const { data } = this.state;
    const { index } = this.props;
    this.setState({ btn_loading: true });
    await this.props.deleteBannerById(data, index);
    this.setState({ btn_loading: false });
  };

  // change banner color(dropdown)
  //----------------------------------------------------
  changeColor = color => {
    const { data } = this.state;
    data.color_code = color.key;
    this.setState({ data, selected_color: color.value });
  };

  // change priority level
  //----------------------------------------------------
  changePriority = priority => {
    const { data } = this.state;
    data.priority = priority;
    this.setState({ data });
  };

  changePage = (page) => {
    if(page.length){
      const {data} = this.state;
      data.page_name = page;
      this.setState({ data });
    }
  }

  // render fn
  //----------------------------------------------------
  render() {
    const { data, toolbar, editorState, btn_loading, selected_color, all_color, priority_level, original_copy_data , all_pages} = this.state;
    return (
      <div>
        <div className="card banner-padding mg-top-bottom-10">
          <div className="flex-touch align-center">
            <div className="banner-items-padding ">
              <label className="label ">Banner Start Date</label>
              <DateTimePicker onChange={this.bannerStart} value={data.start_time ? new Date(data.start_time) : null} />
            </div>
            <div>
              <i className="fas fa-exchange-alt fa-lg is-hidden-mobile"></i>
            </div>
            <div className="banner-items-padding ">
              <label className="label ">Banner End Date</label>
              <DateTimePicker onChange={this.bannerEnd} value={data.end_time ? new Date(data.end_time) : null} name="end_time" />
            </div>
            <div className="banner-items-padding ">
              <label className="label ">Priority</label>
              <div className={`dropdown is-hoverable`}>
                <div className="dropdown-trigger">
                  <button type="button" className="button banner-dropdown-box" aria-haspopup="true" aria-controls="dropdown-menu4">
                    <span>{data.priority ? `Level ${data.priority}` : ""}</span>
                    <span className="icon is-small">
                      <i className="fas fa-angle-down" aria-hidden="true"></i>
                    </span>
                  </button>
                </div>
                <div className="dropdown-menu min-width-unset" id="dropdown-menu4" role="menu">
                  <div className="dropdown-content dropdown-fix-height">
                    {priority_level.length ? (
                      priority_level.map((item, index) => (
                        <a key={index} onClick={this.changePriority.bind(this, item)} className="dropdown-item ">
                          {`Level ${item}`}
                        </a>
                      ))
                    ) : (
                      <a className="dropdown-item">No Data</a>
                    )}
                  </div>
                </div>
              </div>
            </div>
            <div className="banner-items-padding ">
              <label className="label ">Color</label>
              <div className={`dropdown is-hoverable `}>
                <div className="dropdown-trigger">
                  <button type="button" className="button banner-dropdown-box" aria-haspopup="true" aria-controls="dropdown-menu4">
                    <span className={`  banner-color-item ${selected_color}-banner-dot`}>{selected_color}</span>
                    <span className="icon is-small">
                      <i className="fas fa-angle-down" aria-hidden="true"></i>
                    </span>
                  </button>
                </div>
                <div className="dropdown-menu min-width-unset" id="dropdown-menu4" role="menu">
                  <div className="dropdown-content dropdown-fix-height">
                    {all_color.length ? (
                      all_color.map((item, index) => (
                        <a key={index} onClick={this.changeColor.bind(this, item)} className={`dropdown-item  banner-color-item ${item.value}-banner-dot`}>
                          {item.value}
                        </a>
                      ))
                    ) : (
                      <a className="dropdown-item">No Data</a>
                    )}
                  </div>
                </div>
              </div>
            </div>
            <div className="banner-items-padding ">
              <label className="label ">Page</label>
              <div className={`dropdown is-hoverable`}>
                <div className="dropdown-trigger">
                  <button type="button" className="button banner-dropdown-box" aria-haspopup="true" aria-controls="dropdown-menu4">
                    <span>{data.page_name && all_pages[`${data.page_name}`] ? all_pages[`${data.page_name}`] : ""}</span>
                    <span className="icon is-small">
                      <i className="fas fa-angle-down" aria-hidden="true"></i>
                    </span>
                  </button>
                </div>
                <div className="dropdown-menu min-width-unset" id="dropdown-menu4" role="menu">
                  <div className="dropdown-content dropdown-fix-height">
                    {Object.keys(all_pages).length ? (
                      Object.entries(all_pages).map(([key,value], index) => (
                        <a key={key} onClick={this.changePage.bind(this, key)} className="dropdown-item ">
                          {value}
                        </a>
                      ))
                    ) : (
                      <a className="dropdown-item">No Data</a>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <br />
          <div className="banner-items-padding ">
            <Editor toolbar={toolbar} editorState={editorState} wrapperClassName="demo-wrapper" editorClassName="demo-editor" onEditorStateChange={this.onEditorStateChange} stripPastedStyles={true} />
          </div>
          <div className="flex-touch ">
            <div className="display-flex banner-items-padding align-center">
              <label className="label marginright-md no-padding-margin-bottom ">Active</label>
              <Switch className="status" onChange={this.changeActiveStatus} checked={data.status ? true : false} disabled={false} uncheckedIcon={false} checkedIcon={false} onColor={"#23d160"} height={15} width={35} />
            </div>
            <div className="buttons banner-update-btns">
              <div className="banner-items-padding">
                <button onClick={this.updateBanner} disabled = {original_copy_data === JSON.stringify(data)} className={classnames("button is-link is-small is-outlined banner-items-padding ", { "is-loading": btn_loading })}>
                  {data.id !== undefined ? "Update" : "Add"}
                </button>
                <button onClick={this.deleteBanner} className={classnames("button is-danger is-small  is-outlined ", { "is-loading": btn_loading })}>
                  Delete
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// props type
//---------------------------------------------------------------------------------------------------------------------------
BannerCard.propTypes = {
  data: PropTypes.object.isRequired,
  updateBannerContent: PropTypes.func.isRequired,
  deleteBannerById: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
};
