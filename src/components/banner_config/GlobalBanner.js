import React, { Component } from "react";
import BannerCard from "./BannerCard";
import { connect } from "react-redux";
import { getBanners, updateBanners, createBanners, deleteExistingBanner } from "../../actions/BannerActions";
import PropTypes from "prop-types";

class GlobalBanner extends Component {
  state = {
    initial_value: {
      content: "",
      end_time: null,
      priority: 3,
      start_time: null,
      status: true,
      color_code: 1,
      page_name : "all"
    },
    banner_data: [],
    loading: true,
    global_id: 0
  };

  // setting id for global banner i.e 0
  //----------------------------------------------------------------
  componentDidMount() {
    this.getBannerData();
  }

  // setting the previous banners data
  //----------------------------------------------------------------
  getBannerData = async () => {
    const { global_id, initial_value } = this.state;
    const banner_data = await this.props.getBanners(global_id);
    if (banner_data && banner_data.length) {
      this.setState({ banner_data: banner_data });
    } else {
      this.setState({ banner_data: [initial_value] });
    }
    this.setState({ loading: false });
  };

  //adding the new banner card with default values
  //----------------------------------------------------------------
  addBanner = () => {
    const { banner_data, initial_value } = this.state;
    let temp_data = JSON.parse(JSON.stringify(banner_data));
    temp_data.unshift(initial_value);
    this.setState({ banner_data: temp_data });
  };

  //update banner content
  //----------------------------------------------------------------
  updateBannerContent = async content => {
    const { global_id } = this.state;
    const { updateBanners, createBanners } = this.props;
    if (content.id !== undefined) {
      await updateBanners(global_id, content.id, content);
    } else {
      const response = await createBanners(global_id, content);
      if (response && response.status === 200) {
        this.setState({ loading: true }, () => {
          this.getBannerData();
        });
      }
    }
  };

  // delete banner by id
  //----------------------------------------------------------------
  deleteBannerById = async (data, index) => {
    const { global_id, banner_data } = this.state;
    const { deleteExistingBanner } = this.props;
    if (data.id !== undefined) {
      const response = await deleteExistingBanner(global_id, data.id);
      if (response && response.status === 200) {
        this.setState({ loading: true }, () => {
          this.getBannerData();
        });
      }
    } else {
      banner_data.splice(index, 1);
      this.setState({ banner_data });
    }
  };

  // render fuction
  //----------------------------------------------------------------
  render() {
    const { banner_data, loading } = this.state;
    return (
      <div>
        {loading ? (
          <p className="no-data has-text-centered">Loading Data...</p>
        ) : (
          <div>
              <button className="button is-success add-banner" onClick={this.addBanner}>
                Add A Banner
              </button>
            <br />
            <div className="mg-top-40">{banner_data.length ? banner_data.map((banner, key) => <BannerCard key={key} data={banner} updateBannerContent={this.updateBannerContent} deleteBannerById={this.deleteBannerById} index={key} />) : null}</div>
          </div>
        )}
      </div>
    );
  }
}

// props type
//----------------------------------------------------------------
GlobalBanner.propTypes = {
  client_details: PropTypes.object.isRequired,
};

// map redux state to props
//----------------------------------------------------------------
const mapStateToProps = state => ({
  client_details: state.client.client_details,
});

export default connect(mapStateToProps, { getBanners, updateBanners, createBanners, deleteExistingBanner })(GlobalBanner);
