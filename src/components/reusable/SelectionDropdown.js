import React, { Component } from 'react';
import TextInputgroup from '../generic/TextInputGroup';
import PropTypes from 'prop-types';

class SelectionDropdown extends Component {
    state = {
        original_data_array: [],
        data_array: [],
        search_key: '',
    };

    componentDidMount() {
        this.setData();
    }

    componentDidUpdate(prevProps, prevState) {
        const { values_array, value } = this.props;
        const { search_key } = this.state;
        if (JSON.stringify(values_array) !== JSON.stringify(prevProps.values_array)) {
            this.setData();
        }
        if (prevState.search_key !== search_key) {
            this.updateValues();
        }
    }

    // set date locally
    setData = () => {
        const { values_array } = this.props;
        if (values_array.length) {
            this.setState({
                original_data_array: JSON.parse(JSON.stringify(values_array)),
                data_array: JSON.parse(JSON.stringify(values_array)),
            });
        }
    };

    //update search key
    updateSearchKey = e => {
        this.setState({ search_key: e.target.value });
    };

    //update value on basis of search
    updateValues = () => {
        const { original_data_array, search_key } = this.state;
        let temp_array = [];
        original_data_array.forEach((value, index) => {
            if (value && value.title && value.title.toLowerCase().includes(search_key)) {
                temp_array.push(value);
            }
        });
        this.setState({ data_array: temp_array });
    };

    //when an opdation is selected callback trigger
    optionSelect = option => {
        this.props.onOptionSelect(option);
    };

    //render
    render() {
        const { enable_search, value, label, min_width, height } = this.props;
        const { search_key, data_array } = this.state;
        return (
            <div>
                {label.length ? (
                    <div className="mg-bottom-10">
                        <label className="label">{label}</label>
                    </div>
                ) : null}
                <div className="dropdown is-hoverable">
                    <div className="dropdown-trigger">
                        <button
                            className="button is-size-7 flex-left"
                            style={{ minWidth: `${min_width}px` }}
                            aria-haspopup="true"
                            aria-controls="dropdown-menu2">
                            {value.length ? <span>{value}</span> : ''}
                            <span className="icon is-small dropdown-icon">
                                <i className="fas fa-angle-down " aria-hidden="true"></i>
                            </span>
                        </button>
                    </div>
                    <div className="dropdown-menu" id="dropdown-menu2" role="menu">
                        <div className="dropdown-content overflow_auto" style={{ minWidth: `${min_width}px`, height: `${height}px` }}>
                            {enable_search ? (
                                <div className="dropdown-item">
                                    <TextInputgroup
                                        name={'search_box'}
                                        placeholder="search"
                                        value={search_key}
                                        type="text"
                                        onChange={this.updateSearchKey}
                                        icon="fas fa-search"
                                        is_small={true}
                                    />
                                </div>
                            ) : null}
                            {data_array.length ? (
                                data_array.map((option, index) => (
                                    <React.Fragment key={index}>
                                        <div className="dropdown-item" onClick={this.optionSelect.bind(this, option)}>
                                            <label className="checkbox">{option.title}</label>
                                        </div>
                                    </React.Fragment>
                                ))
                            ) : (
                                <div className="dropdown-item">
                                    <p className="has-text-centered">No Results</p>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

SelectionDropdown.defaultProps = {
    values_array: [],
    default_data: [],
    value: '',
    label: '',
    min_width: '120',
    height: '120',
};

// props type
//------------------------------------------------------------------------
SelectionDropdown.propTypes = {
    values_array: PropTypes.array.isRequired,
};

export default SelectionDropdown;
