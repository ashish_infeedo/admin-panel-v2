import React, { Component } from 'react';
import DatePicker from 'react-date-picker';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';
// components
import TextInputgroup from '../generic/TextInputGroup';
// actions
import { getReminders } from '../../actions/ReminderActions';

class Reminder extends Component {

    state = {
        date: moment().startOf('day').toDate(),
        search: '',
        all_reminders: [],
        loading: true
    }

    componentDidMount() {
        const { date } = this.state;
        this.getReminders(date);
    }

    getReminders = async (date) => {
        const unix = this.getUnixStamp(date);
        await this.props.getReminders(unix);
        this.setState({all_reminders: this.props.reminders});
        this.setState({loading: false});
    }

    getUnixStamp = (date) => {
        return moment(date).unix();
    }

    // Search
    // ------
    onSearch = e => {
        this.setState({ [e.target.name]: e.target.value });
        const search = e.target.value;
        if(search.length > 0) {
            const results = this.props.reminders.filter(response => response.title.toLowerCase().includes(search.toLowerCase()));
            this.setState({all_reminders: results});
        } else if(!search.length) {
            this.setState({all_reminders: this.props.reminders});
        }
    };
    
    // On change
    // ---------
    onChange = (date) => {
        this.setState({ date: date });
        this.getReminders(date);
    }

    render() {
        const { search, all_reminders, loading } = this.state;
        return (
        <div>
            <div className="columns">
                <div className="column is-10">
                    <TextInputgroup
                        name="search"
                        placeholder="Search Client"
                        value={search}
                        type="text"
                        onChange={this.onSearch}
                        icon="fas fa-search"
                    />
                </div>
                <div className="column pd-up-20">
                <DatePicker onChange={this.onChange} value={this.state.date} /> 
                </div>
            </div>
            <div className="card">
                <header className="card-header">
                    <p className="card-header-title">
                    Reminders
                    </p>
                </header>
                <div className="card-content is-paddingless">
                    <div className="content">
                        <div className="custom-table">
                            <table className="table is-striped">
                                <thead>
                                    <tr>
                                    <th>S.No.</th>
                                    <th>Client</th>
                                    <th>To Start Reminders</th>
                                    <th>Dropped Reminders</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {!loading ? all_reminders.map((reminder, index) => (
                                <tr key={index}>
                                    <td>{index + 1}</td>
                                    <td><b>{reminder.title}</b></td>
                                    <td>{reminder.to_start_reminders}</td>
                                    <td>{reminder.dropped_reminders}</td>
                                </tr>
                                )): null}
                                </tbody>
                            </table>
                            {(all_reminders.length === 0 && !loading) ? <p className="no-data has-text-centered">No Data</p> : null}
                            {loading ? <p className="no-data has-text-centered">Loading Data...</p> : null}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}
// PropTypes
// ---------
Response.propTypes = {
    reminders: PropTypes.array.isRequired,
    getReminder: PropTypes.func.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    reminders: state.reminder.reminders,
})

export default connect(mapStateToProps, {getReminders})(Reminder);