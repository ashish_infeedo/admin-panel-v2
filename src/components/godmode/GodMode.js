import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';
import classnames from 'classnames';
// components
import Pagination from '../generic/Pagination';
import TextInputgroup from '../generic/TextInputGroup';
import Dropdown from '../generic/Dropdown';
import ChatView from './ChatView';
// actions
import { getGodReports, getChat } from '../../actions/GodActions';
import { getClientsList } from '../../actions/ClientsActions';
import { ScrollToTop } from '../../helper/Scroll';


class GodMode extends Component {

    state ={
        current_page: 1,
        search: '',
        response: '',
        company:'',
        response_states: [
            {
                key: 0,
                value: 'All Responses'
            },
            {
                key: 1,
                value: 'To Start'
            },
            {
                key: 2,
                value: 'In Progress'
            },
            {
                key: 3,
                value: 'Completed'
            },
            {
                key: 4,
                value: 'Deleted'
            }
        ],
        selected_response: {key: 0,value: 'All Responses'},
        selected_company: {key: 0, value: 'All Companies'},
        loading: true,
        getting_chat: false,
        // ChatView
        name: '',
        is_active: false,
        transcript: [],
        session_id: null,
        profile: {}
    }

    componentDidMount() {
        const {current_page, response, search, company} = this.state;
        this.getGodReports(current_page, response, search, company);
        this.props.getClientsList();
        if(sessionStorage.profile) {
            const profile = JSON.parse(sessionStorage.profile);
            this.setState({profile});
            if(profile.id !== 2){
                this.props.history.push(`/admin-panel/clients`);
            }
        } else {
            this.props.history.push(`/admin-panel/clients`);
        }
    }

    // Get God Reports 
    // ---------------
    getGodReports = async (page, response, search, company) => {
        await this.props.getGodReports(page, response, search, company);
        this.setState({loading: false});
    }

    // Pagination Methods
    // ------------------
    onNext = () => {
        let { current_page, search, company, response } = this.state;
        current_page += 1; 
        this.setState({current_page: current_page});
        this.getGodReports(current_page, response, search, company);
        ScrollToTop();
    }

    onPrevious = () => {
        let { current_page, search, company, response } = this.state;
        current_page -= 1;
        this.setState({current_page: current_page});
        this.getGodReports(current_page, response, search, company);
        ScrollToTop();
    }

    jumpToLast = () => {
        const { total_pages } = this.props;
        this.setState({current_page: total_pages});
        this.getGodReports(total_pages, '', '', '');
        ScrollToTop();
    }

    // Search
    // ------
    onSearch = e => {
        const { response, company } = this.state;
        this.setState({ [e.target.name]: e.target.value });
        const search = e.target.value;
        if(search.length > 1) {
            this.setState({current_page: 1});
            this.getGodReports(1, response, search, company);
        } else if(!search.length) {
            this.setState({current_page: 1});
            this.getGodReports(1, response, '', company);
        }
    };

    // Select Response type
    // --------------------
    selectResponse = (selected) => {

        const { search, company} = this.state;
        this.setState({selected_response: selected})
        this.setState({response: selected.key})

        if(selected.key !== 0) {
            this.getGodReports(1, selected.key, search, company);
        } else {
            this.getGodReports(1, '', search, company);
            this.setState({response:''});
        }
    }

    // Select Company
    // --------------
    selectCompany = (company) => {
        const { search, response } = this.state;
        this.setState({selected_company: company});
        this.setState({company: company.value});
        if(company.key !== 0) {
            this.getGodReports(1, response, search, company.value);
        } else {
            this.setState({company: ''});
            this.getGodReports(1, response, search, '');
        }
    }

    // Transcript Modal
    // -----------------
    onClose = () => {
        this.setState({is_active: false, transcript: []});
    }

    onOpen = async (report) => {
        this.setState({getting_chat: true, name: report.name, session_id: report.session_id})
        this.looseFocus();
        const response = await this.props.getChat(report.session_id);
        this.setState({transcript: response ,is_active: true, getting_chat: false});
    }

    looseFocus = () => {
        document.activeElement.blur();
    }

    // Render
    // ------
    render() {
        const { reports, total_pages, clients_list, page_size } = this.props;
        const { loading, getting_chat, current_page, search, selected_response, selected_company, response_states, name, is_active, transcript, session_id } = this.state;
        return (
        <div>
            <div className="columns">
                <div className="column is-8">
                    <TextInputgroup
                        name="search"
                        placeholder="Search Employee by Name or Email"
                        value={search}
                        type="text"
                        onChange={this.onSearch}
                        icon="fas fa-search"
                    />
                </div>
                <div className="column is-2 pd-up-20">
                    <Dropdown selected_item={selected_response} dropdown_items={response_states} onClick={this.selectResponse}/>
                </div>
                <div className="column is-2 pd-up-20">
                    <Dropdown selected_item={selected_company} dropdown_items={clients_list} onClick={this.selectCompany}/>
                </div>
            </div>
            <div className="card">
                <header className="card-header">
                    <p className="card-header-title">
                    God Mode
                    </p>
                </header>
                <div className="card-content is-paddingless">
                    <div className="content">
                        <div className="custom-table">
                            <table className="table is-striped">
                                <thead>
                                    <tr>
                                    <th>S.No.</th>
                                    <th>Name</th>
                                    <th>Trigger Date</th>
                                    <th>Answer Date</th>
                                    <th>Touchpoint</th>
                                    <th>Reminders</th>
                                    <th>Response</th>
                                    <th>View Chat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {!loading ? reports.map((report, index) => (
                                <tr key={index}>
                                    <td>{index + 1 + (current_page - 1) * page_size}</td>
                                    <td><b>{report.name}</b><br/> <span className="is-size-7">{report.email}</span></td>
                                    <td>{moment(report.trigger_date * 1000).format("DD MMM YY")}</td>
                                    { report.completion_date ? <td>{moment(report.completion_date * 1000).format("DD MMM YY")}</td> : <td>NA</td>}
                                    <td>{report.duration} {report.unit}</td>
                                    <td>{report.reminder_to_start} | {report.reminder_incomplete}</td>
                                    <td>{report.survey_status}</td>
                                    <td>
                                        <div className="buttons">
                                            <button onClick={this.onOpen.bind(this, report)} 
                                                    className={classnames('button is-info is-small is-rounded is-outlined',{'is-loading': (getting_chat && session_id === report.session_id)})} 
                                                    disabled={report.survey_status !== 'In Progress' && report.survey_status !== 'Completed'}>
                                                <span>View Chat</span>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                )): null}
                                </tbody>
                            </table>
                            {(reports.length === 0 && !loading) ? <p className="no-data has-text-centered">No Data</p> : null}
                            {loading ? <p className="no-data has-text-centered">Loading Data...</p> : null}
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            {loading
                ? null 
                : <Pagination onNext={this.onNext} onPrevious={this.onPrevious} jumpToLast={this.jumpToLast} current_page={current_page} total_pages={total_pages}/>
            }
            <ChatView name={name} is_active={is_active} transcript={transcript} onClose={this.onClose}/>
        </div>
        )
    }
}

// PropTypes
// ---------
GodMode.propTypes = {
    reports: PropTypes.array.isRequired,
    total_pages: PropTypes.number,
    getGodReports: PropTypes.func.isRequired,
    getClientsList: PropTypes.func.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    reports: state.god.reports,
    total_pages: state.god.total_pages,
    clients_list: state.client.clients_list,
    page_size: state.client.page_size
})

export default connect(mapStateToProps, {getGodReports, getClientsList, getChat})(GodMode);