import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const ChatView = ({
  name,
  is_active,
  transcript,
  onClose,
}) => {
  return (
    <div className={classnames('modal',{'is-active': is_active})}>
    <div className="modal-background"></div>
        <div className="modal-card">
            <header className="modal-card-head">
            <p className="modal-card-title">Transcript of {name}</p>
            <button onClick={onClose} className="delete" aria-label="close"></button>
            </header>
            {transcript.length ? <section className="modal-card-body is-paddingless">
            <div className="chat">
            {transcript.map((msg, index) => (
                <div key={index} className={classnames('messages', {'amber': msg.left === 1, 'yours': msg.left === 0})}>
                <div className="chat-message last">
                {msg.contents.map((content, index) => (
                    <div key={index}>
                        <div className="mg-tb-10">
                        {content.type === 'string' ? content.content : null}
                        {content.type === 'image' ? <img width="40px" src={`../assets/images/${content.content}`} alt="icon"/> : null}
                        {content.type === 'button' ? <button className="button is-info is-small">{content.content}</button> : null}
                        {content.type === 'radio' ?  content.content : null}
                        {content.type === 'radio' ?  
                        <div className="mg-tb-10">
                            {content.options.map((option, index) => 
                                <span key={index}>
                                    {option.type === 'image' ? <img width="40px" src={`../assets/images/${option.content}`} alt="icon"/> : null}
                                </span>
                            )}
                        </div> 
                        : null}
                        </div>
                    </div>
                ))}
                </div>
                </div>
            ))}
            </div>
            </section> : null }
        </div>
    </div>
  );
};

ChatView.propTypes = {
  name: PropTypes.string,
  transcript: PropTypes.array.isRequired,
  is_active: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired
};

ChatView.defaultProps = {
  name: 'NA'
};

export default ChatView;
