import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { NotificationManager } from 'react-notifications';
import classnames from 'classnames';
// components
import TextInputgroup from '../../generic/TextInputGroup';
import Dropdown from '../../generic/Dropdown';
// actions
import { getSurveys } from '../../../actions/ClientsActions';
import { sendDemoEmail } from '../../../actions/EmailActions';

class DemoEmail extends Component {
  
  state = {
      emails: '',
      emails_array: [],
      client_id: 8,
      total_emails: 0,
      loading: false,
      sending: false,
      stages: [],
      selected_stage: {key: null, value: 'Not Selected'},
      survey_id: null,
      disable_send: true
  }

  componentDidMount() {
      this.setState({loading: true});
      this.getSurveys();
  }

  getSurveys = async () => {
    const { client_id } = this.state;
    const response = await this.props.getSurveys(client_id);
    const stages = [];
    response.forEach((stage, index) => {
        if(stage.status === 0){
            stages.push({key: stage.survey_id, value: `${stage.duration} ${stage.unit}`})
        }
    })
    this.setState({loading: false, stages: stages});
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
    const emails = e.target.value;
    if(emails.length > 0) {
        let emails_array = emails.split(',').map(email => email.trim());;
        if (emails_array[emails_array.length - 1] === "") {
            emails_array.pop();
        }
        const count = emails_array.length;
        this.setState({total_emails: count, emails_array: emails_array}, () =>  this.enableDisableSend());
    } else {
        this.setState({total_emails: 0, emails_array: []}, () =>  this.enableDisableSend());
    }
  }

  selectStage = (stage) => {
    this.setState({selected_stage: stage}, () =>  this.enableDisableSend());
  }

  enableDisableSend = () => {
      const {total_emails, selected_stage} = this.state;
      if(total_emails > 0 && selected_stage.key !== null){
        this.setState({disable_send: false});
      } else {
        this.setState({disable_send: true});
      }
  }

  onSubmit = async (e) => {
    e.preventDefault();
    const { emails_array, selected_stage } = this.state;
    const survey_id = selected_stage.key;
    if(survey_id !== null) {
        this.setState({sending: true});
        await this.props.sendDemoEmail(survey_id, emails_array);
        this.setState({sending: false, emails: '', total_emails: 0, selected_stage: {key: null, value: 'Not Selected'}}, () =>  this.enableDisableSend());
    } else {
        NotificationManager.warning('Please Select a stage');
    }
  }

  render() {
    const { emails, stages, sending, selected_stage, total_emails, disable_send } = this.state;
    return (
      <div>
        <div>
        <article className="message">
            <div className="message-body">
            This section will help you to send chats to prospective clients for giving them feeler about our product. Go get'em tiger!
            </div>
        </article>
        <br/>
        </div>
        <form onSubmit={this.onSubmit}>
        <div className="columns">
            <div className="column is-10">
                <TextInputgroup
                    name="emails"
                    placeholder="Enter comma seperated emails"
                    value={emails}
                    type="text"
                    onChange={this.onChange}
                    icon="fas fa-envelope"
                />
            </div>
            <div className="control column is-2 pd-up-20">
                <Dropdown selected_item={selected_stage} dropdown_items={stages} onClick={this.selectStage}/>
            </div>
        </div>
        <div className="control">
            <button type="submit" className={classnames('button is-success',{'is-loading': sending})} disabled={disable_send}>
            <span className="icon">
                <i className="fas fa-paper-plane"></i>
            </span>
            <span>Send to ({total_emails})</span>
            </button>
        </div>
        </form>
      </div>
    )
  }
}

// PropTypes
// ---------
DemoEmail.propTypes = {
    getSurveys: PropTypes.func.isRequired,
    sendDemoEmail: PropTypes.func.isRequired
}

export default connect(null, {getSurveys, sendDemoEmail})(DemoEmail);