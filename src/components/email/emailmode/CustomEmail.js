import React, { Component } from 'react';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
// import htmlToDraft from 'html-to-draftjs';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { NotificationManager } from 'react-notifications';
import classnames from 'classnames';
// components
import TextInputgroup from '../../generic/TextInputGroup';
import Dropdown from '../../generic/Dropdown';
// actions
import { getCustomEmailSenders, sendCustomEmails, uploadSignature } from '../../../actions/EmailActions';

class CustomEmail extends Component {

    state = {
        editorState: EditorState.createEmpty(),
        email_ids: '',
        selected_client_id: {key: 0, value: 'Not Selected'},
        selected_chat_status: {key: 0, value: 'Not Selected'},
        selected_sender: {key: null, value: 'Not Selected'},
        exclude: '',
        subject: '',
        receiver: 'people',
        senders: [],
        chat_status: [
            {key: 0, value: 'All'},
            {key: 1, value: 'Yet To Start'},
            {key: 2, value: 'In Progress'},
            {key: 3, value: 'Completed'}
        ],
        client_ids: [],
        editor_content: '',
        sending: false,
        uploading: false,
        url: '',
        disable_send: false
    }

    componentDidMount() {
        // const html = '<p>Hey this <strong>editor</strong> rocks 😀</p>';
        // const contentBlock = htmlToDraft(html);
        // if (contentBlock) {
        //     const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
        //     const editorState = EditorState.createWithContent(contentState);
        //     this.setState({editorState, editorState});
        // }
        this.getCustomEmailSenders();
        
    }

    // get Custom Email Senders
    // ------------------------
    getCustomEmailSenders = async () => {
        const response = await this.props.getCustomEmailSenders();
        let type = '';
        const unique = {};
        response.forEach((sender) => {
            if(sender.role === 1) {
                type = 'Group';
            } else if(sender.role === 2) {
                type = 'Cohort';
            }
            unique[sender.client_id] = sender.client_name;
            sender.key = sender.client_id;
            sender.value = sender.user_name + ' - ' + type;
        })
        this.setState({senders: response});
        this.makeUniqueClients(unique);
    }

    // make unique client names
    // ------------------------
    makeUniqueClients = (clients) => {
        const client_ids = [];
        Object.entries(clients).forEach(
            ([key, value]) => client_ids.push({key: parseInt(key), value: value})
        );
        this.setState({client_ids: client_ids});
    }

    // Select Email Sender
    // ------------------
    selectSender = (sender) => {
        this.setState({selected_sender: sender})
    }

    // Select Reciever as custom emails or group
    // -----------------------------------------
    selectReceiver = (e) => {
        const receiver = e.target.value
        this.setState({receiver: receiver});
        if(receiver === 'people') {
            this.setState({
                selected_client_id: {key: 0, value: 'Not Selected'},
                selected_chat_status: {key: 0, value: 'Not Selected'},
                exclude: ''
            })
        } else if(receiver === 'group') {
            this.setState({email_ids: ''})
        }
    }

    // select chat status
    // ------------------
    selectChatStatus = (status) => {
        this.setState({selected_chat_status: status});
    }

    // select Client
    // -------------
    selectClient = (client) => {
        this.setState({selected_client_id: client});
    }

    // On editor state change
    // ----------------------
    onEditorStateChange = (editorState) => {
        this.setState({
            editorState,
            editor_content: draftToHtml(convertToRaw(editorState.getCurrentContent()))
        });
    }

    // On Change text inputs
    // ---------------------
    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    // Send Custom emails
    // ------------------
    sendCustomEmail = async () => {
        const { editor_content, subject,  email_ids, selected_sender, selected_chat_status, selected_client_id, exclude } =this.state;
        this.setState({sending: true});
        const payload = {
            chat_status: selected_chat_status.key,
            client_id: selected_client_id.key,
            content: editor_content,
            exclude: exclude,
            from: selected_sender.user_name,
            subject: subject,
            to: email_ids
        }
        await this.props.sendCustomEmails(payload);
        this.resetState();
        this.setState({sending: false});
    }

    // Reset Email page State
    // ----------------------
    resetState = () => {
        this.setState({
            editorState: EditorState.createEmpty(),
            subject: '',
            email_ids: '',
            selected_client_id: {key: 0, value: 'Not Selected'},
            selected_chat_status: {key: 0, value: 'Not Selected'},
            selected_sender: {key: null, value: 'Not Selected'},
            exclude: ''
        })
    }

    // Validate Email form
    // -------------------
    validateEmailForm = (e) => {
        e.preventDefault();
        const { editor_content, subject,  email_ids, selected_sender, selected_chat_status, selected_client_id, editorState, receiver } = this.state;
        if( selected_sender.key === null) {
            NotificationManager.warning(`Please select a sender`);
            return;
        }
        if(editor_content === '' || editorState.getCurrentContent().getPlainText().trim().length === 0){
            NotificationManager.warning(`Please add content to email`);
            return;
        } 
        if(subject === '') {
            NotificationManager.warning(`Please add subject`);
            return;
        }
        if(receiver === 'people') {
            if(email_ids === '') {
                NotificationManager.warning(`Please add emails`);
                return;
            }
        }  
        if(receiver === 'group') {
            if(selected_client_id.key === 0) {
                NotificationManager.warning(`Please Select Company`);
                return;
            }
            if(selected_chat_status.value === 'Not Selected') {
                NotificationManager.warning(`Please Select Chat Progress`);
                return;
            }
        }
        this.sendCustomEmail();
    }

    // Copy Text
    // ---------
    copyText = (id) => {
        const copyText = document.querySelector(`#${id}`);
        copyText.select();
        document.execCommand("copy");
    }

    handleFiles = async (e) => {
        const files = Array.from(e.target.files)
        this.setState({uploading: true});
        const formData = new FormData();
        formData.append('image', files[0]);
        const url = await this.props.uploadSignature(formData);
        this.setState({uploading: false, url: url.url});
    }

    // Render HTML
    // -----------
    render() {
        const { editorState, contentState, email_ids, subject, receiver, selected_sender, selected_chat_status, selected_client_id, senders, chat_status, client_ids, sending, uploading, exclude, url, disable_send} = this.state;
        return (
        <div>
            <article className="message">
                <div className="message-body">
                Use this section when there is an urgent need to send an email to employees in a company. <br/><b>Example:</b> Vedanta Fiasco. Speak to VP in doubt.
                </div>
            </article>
            <br/>
            <form onSubmit={this.validateEmailForm}>
            <div className="columns">
            {/* Email Fields */}
            <div className="column is-9">
                <div className="control" onChange={this.selectReceiver}>
                <b>Who will receive this? &nbsp;&nbsp;</b>
                  <label className="radio">
                    <input type="radio" name="people" value="people" checked={ receiver === 'people' } readOnly/>
                     &nbsp;Selective People
                  </label>&nbsp;&nbsp;
                  <label className="radio">
                    <input type="radio" value="group" name="group" checked={ receiver === 'group' } readOnly/>
                    &nbsp;Group
                  </label>
                </div><br/>
                <div>
                    <b className="v-middle">Select Sender &nbsp;&nbsp;</b>
                    <Dropdown selected_item={selected_sender} dropdown_items={senders} onClick={this.selectSender}/>
                </div>
                <br/>
                {receiver === 'people' ? <div className="mg-ud-5">
                    <TextInputgroup
                        name="email_ids"
                        placeholder="Enter comma seperated emails"
                        value={email_ids}
                        type="text"
                        onChange={this.onChange}
                        icon="fas fa-envelope"
                    />
                </div> : null}
                {receiver === 'group' ? <div>
                    <b className="v-middle">Select Company &nbsp;&nbsp;</b>
                    <Dropdown selected_item={selected_client_id} dropdown_items={client_ids} onClick={this.selectClient}/>
                    &nbsp;&nbsp;<b className="v-middle">Chat Progress &nbsp;&nbsp;</b>
                    <Dropdown selected_item={selected_chat_status} dropdown_items={chat_status} onClick={this.selectChatStatus}/>
                </div> : null}
                {receiver === 'group' ? <div className="mg-ud-15">
                    <TextInputgroup
                        name="exclude"
                        placeholder="Enter comma seperated people to exclude"
                        value={exclude}
                        type="text"
                        onChange={this.onChange}
                        icon="fas fa-times-circle"
                        required={false}
                    />
                </div> : null}
                <div className="mg-ud-15">
                    <TextInputgroup
                        name="subject"
                        placeholder="Enter subject"
                        value={subject}
                        type="text"
                        onChange={this.onChange}
                        icon="fas fa-align-justify"
                    />
                </div>
            </div>
            {/* Variables */}
            <div className="column is-3">
                    <article className="message is-info">
                    <div className="message-body">
                    <p><b className="v-middle">Copy Variables</b><br/><br/></p>
                    <div className="field has-addons is-small">
                        <div className="control">
                            <input className="input is-small" type="text" id="firstname" value="{{{firstname}}}" readOnly/>
                        </div>
                        <div className="control" onClick={this.copyText.bind(this, 'firstname')}>
                            <a type="cancel" className="button is-small is-info">
                            <i className="fas fa-copy"></i>
                            </a>
                        </div>
                    </div>
                    <div className="field has-addons is-small">
                        <div className="control">
                            <input className="input is-small" type="text" id="stage" value="{{{stage}}}" readOnly/>
                        </div>
                        <div className="control" onClick={this.copyText.bind(this, 'stage')}>
                            <a className="button is-small is-info">
                            <i className="fas fa-copy"></i>
                            </a>
                        </div>
                    </div>
                    <div className="field has-addons is-small">
                        <div className="control">
                            <input className="input is-small" id="companyname" type="text" value="{{{companyname}}}" readOnly/>
                        </div>
                        <div className="control" onClick={this.copyText.bind(this, 'companyname')}>
                            <a className="button is-small is-info">
                            <i className="fas fa-copy"></i>
                            </a>
                        </div>
                    </div>
                    </div>
                    </article>
                </div>
            </div>
            <div className="is-light">
            <Editor
                initialContentState={contentState}
                editorState={editorState}
                wrapperClassName="demo-wrapper"
                editorClassName="demo-editor"
                onEditorStateChange={this.onEditorStateChange}
            />
            </div>
            <br/>
            <div className="columns">
                <div className="control column is-6">
                    <button type="submit" className={classnames('button is-success', {'is-loading': sending})} disabled={disable_send}>
                    <span className="icon">
                        <i className="fas fa-paper-plane"></i>
                    </span>
                    <span>Send</span>
                    </button>
                </div>
                <div className="file column is-6">
                <label className="file-label is-pulled-right">
                    <input className="file-input" type="file" accept="image/*" name="signature" onChange={this.handleFiles}/>
                    <span className="file-cta">
                    <span className="file-icon">
                        <i className="fas fa-upload"></i>
                    </span>
                    { uploading ?
                    <span className="file-label">
                        Uploading...
                    </span>
                    : <span className="file-label">
                        Upload Signature
                    </span>}
                    </span>
                </label>
                </div>
            </div>
            </form>
            <p className="help mg-10 has-text-right">{ url ? 'Copy the image URL from below' : 'Note: Keep the dimension below 700 X 500' }</p> 
            { url ? <div className="columns column">
                <div className="card column is-offset-10 is-2 is-paddingless is-pulled-right">
                    <div className="card-image">
                        <figure className="image">
                        <img src={url} alt="Placeholder image" className="sign-img"/>
                        </figure>
                    </div>
                    <div className="card-content">
                        <div className="field has-addons is-small">
                            <div className="control">
                                <input className="input is-small" id="signatureurl" type="text" value={url} readOnly/>
                            </div>
                            <div className="control" onClick={this.copyText.bind(this, 'signatureurl')}>
                                <a className="button is-small is-info">
                                <i className="fas fa-copy"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> : null}
            <br/><br/><br/><br/>
        </div>
        )
    }
}

// PropTypes
// ---------
CustomEmail.propTypes = {
    getCustomEmailSenders: PropTypes.func.isRequired,
}

export default connect(null, {getCustomEmailSenders, sendCustomEmails, uploadSignature})(CustomEmail);