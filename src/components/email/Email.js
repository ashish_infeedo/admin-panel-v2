import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {Route} from 'react-router-dom';
import classnames from 'classnames';
// components
import CustomEmail from './emailmode/CustomEmail';
import DemoEmail from './emailmode/DemoEmail';



export default class Email extends Component {
    state = {
       selected_tab: 'demo'
    }

    static getDerivedStateFromProps(props) { 
        return {
            selected_tab: props.match.params.mode
        }
    }

    changeTab = (tab) => {
        this.setState({selected_tab: tab});
    }

    render() {
        const { selected_tab } = this.state;
        return (
        <div>
            <div className="tabs">
                <ul>
                    <li onClick={this.changeTab.bind(this, 'demo')} className={classnames({'is-active': selected_tab === 'demo' })}><Link to={`/admin-panel/email/demo`}>Send Demo Email</Link></li>
                    <li onClick={this.changeTab.bind(this, 'custom')} className={classnames({'is-active': selected_tab === 'custom' })}><Link to={`/admin-panel/email/custom`}>Send Custom Email</Link></li>
                </ul>
            </div>
            <div>
             <Route  exact path={`/admin-panel/email/demo`} component={DemoEmail}/>
             <Route  exact path={`/admin-panel/email/custom`} component={CustomEmail}/>
            </div>
        </div>
        )
    }
}
