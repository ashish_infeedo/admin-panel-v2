import React, { Component } from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
import PropTypes from 'prop-types'
import DatePicker from 'react-date-picker';
import { NotificationManager } from 'react-notifications';
import moment from 'moment';
import {languages} from '../../constants'

// components
import TextInputGroup from '../generic/TextInputGroup';
import Dropdown from '../generic/Dropdown';
import ConfirmModal from '../generic/ConfirmModal';

// actions
import { getDepartments, getLocations, getCohorts } from '../../actions/ClientsActions';
import { getUserDetails, uploadUserProfileImage, removeUserProfileImage, updateUserProfile } from '../../actions/UserActions';

class UserDetails extends Component {

    state = {
        client_id: '',
        user_id: '',
        loading: true,
        user_details: {},
        original_details: {},
        gender_options: [
            { key: 1, value: 'Male' },
            { key: 2, value: 'Female' },
            { key: 3, value: 'Transgender' },
        ],
        marital_status_options: [
            { key: 1, value: 'Single' },
            { key: 2, value: 'Married' },
            { key: 3, value: 'Unmarried' },
            { key: 4, value: 'Separated' },
            { key: 5, value: 'Divorced' },
            { key: 6, value: 'Common law' },
            { key: 7, value: 'Widowed' },
            { key: 8, value: 'Domestic partnership' },
        ],
        languages: languages,
        selected_gender: { key: 0, value: 'loading...' },
        selected_marital_status: { key: 0, value: 'loading...' },
        current_department: { key: 0, value: 'loading...' },
        current_location: { key: 0, value: 'loading...' },
        current_cohort: { key: 0, value: 'loading...' },
        preset_language: { key: 0, value: 'loading...' },
        departments: [],
        locations: [],
        cohorts: [],
        show_advanced: false,
        show_dates: false,
        show_basic_details: true,
        is_form_dirty: false,
        uploading_image: false,
        confirmation_model: {
            title: 'Remove User Image!',
            text: 'Are you sure you want to remove image?',
            confirm_text: 'Yes',
            is_active: false
        },
        profile_updation_confirmation_model: {
            title: 'Update User Profile!',
            text: 'Are you sure you want update profile? There is no rollback after this!',
            confirm_text: 'Yes',
            is_active: false
        },
        change_set: {}
    }

    componentDidMount() {
        this.getUsersPageData()
    }

    componentWillUnmount() {
        // this.props.clearUserDetails();
    }

    componentDidUpdate(prevProps, nextProps) {
        // if (prevProps.match.params.user_id !==  nextProps.user_id) {
        //     this.getUsersPageData();
        // }
    }

    getUsersPageData = async() => {
        await this.getURLParameters();
        await this.getDepartments();
        await this.getLocations();
        await this.getCohorts();
        await this.getUserDetails();
    }

    getDepartments = async () => {
        const { client_id } = this.props.match.params;
        const response = await this.props.getDepartments(client_id);
        this.setState({ departments: response.map((response, index) => ({ key: response.id, value: response.title })) });
    }
    getLocations = async () => {
        const { client_id } = this.props.match.params;
        const response = await this.props.getLocations(client_id);
        this.setState({ locations: response.map((response, index) => ({ key: response.id, value: response.title })) });
    }
    getCohorts = async () => {
        const { client_id } = this.props.match.params;
        const response = await this.props.getCohorts(client_id);
        this.setState({ cohorts: response.map((response, index) => ({ key: response.id, value: response.title })) });
    }

    getUserDetails = async () => {
        const { client_id, user_id } = this.props.match.params;
        var response = await this.props.getUserDetails(client_id, user_id);
        response.data.birthday = response.data.birthday ? new Date(response.data.birthday * 1000) : null;
        response.data.leaving_date = response.data.leaving_date ? new Date(response.data.leaving_date * 1000) : null;
        response.data.joining = response.data.joining ? new Date(response.data.joining * 1000) : null;
        response.data.resignation_date = response.data.resignation_date ? new Date(response.data.resignation_date * 1000) : null;
        response.data.wedding = response.data.wedding ? new Date(response.data.wedding * 1000) : null;
        response.data.doa = response.data.doa ? new Date(response.data.doa * 1000) : null;
        this.setState({ loading: false, user_details: response.data, original_details: { ...response.data } });
        this.setDropdowns(response.data);
    }

    getURLParameters() {
        const { client_id, user_id } = this.props.match.params;
        this.setState({ client_id: client_id, user_id: user_id });
    }

    selectGender = (gender) => {
        const {user_details, change_set} = this.state
        user_details.gender = gender.value;
        if(gender.value !== null) {
            change_set.gender = gender.value;
        }
        this.setState({
            selected_gender: gender ? gender : null,
            user_details: user_details,
            change_set
        }, () => this.checkIsFormDirty())
    }

    selectMaritalStatus = (marital_status) => {
        const {user_details, change_set} = this.state
        user_details.marital_status = marital_status.value
        if(marital_status.value !== null) {
            change_set.marital_status = marital_status.value 
        }
        this.setState({
            selected_marital_status: marital_status ? marital_status : null,
            user_details: user_details,
            change_set
        }, () => this.checkIsFormDirty())
    }
    selectLocation = (location) => {
        let location_name = this.findDemographic(location, 'location');
        this.setState({
            current_location: location.value = location_name
        })
    }
    selectLocation = (location) => {
        const {user_details, change_set} = this.state;
        user_details.location_id = location.key;
        change_set.location_id = location.key;
        this.setDemographicName(location.key, 'location')
        this.setState({user_details : user_details, change_set}, () => this.checkIsFormDirty())
    }
    selectDepartment = (department) => {
        const {user_details, change_set} = this.state;
        user_details.department_id = department.key
        change_set.demographic_id = department.key
        this.setDemographicName(department.key, 'department')
        this.setState({user_details : user_details, change_set}, () => this.checkIsFormDirty())
    }
    selectCohort = (cohort) => {
        const {user_details, change_set} = this.state;
        user_details.current_cohort_id = cohort.key
        change_set.current_cohort_id = cohort.key
        this.setDemographicName(cohort.key, 'cohort')
        this.setState({user_details : user_details, change_set}, () => this.checkIsFormDirty())
    }
    
    selectLanguage = (language) => {
        let {user_details, change_set} = this.state;
        user_details.preset_language = language.key
        change_set.preset_language = language.key
        let selected_language = language
        this.setState({user_details, preset_language: selected_language, change_set}, () => this.checkIsFormDirty())
    }

    setDemographicName = (demographic_id, demographic) => {
        if (demographic === 'location') {
            var loc = this.findDemographicName(demographic_id, demographic);
            this.setState({
                current_location: { key: demographic_id, value: loc }
            })
        } else if (demographic === 'department') {
            var dep = this.findDemographicName(demographic_id, demographic);
            this.setState({
                current_department: { key: demographic_id, value: dep }
            })
        } else if (demographic === 'cohort') {
            var cohort = this.findDemographicName(demographic_id, demographic);
            this.setState({
                current_cohort: { key: demographic_id, value: cohort }
            })
        } else if (demographic === 'language') {
            var language = this.findDemographicName(demographic_id, demographic)
            this.setState({preset_language: {key: demographic_id, value: language}})
        }
    }

    findDemographicName = (demographic_id, demographic) => {
        var demo = ''
        if (demographic === 'location') {
            this.state.locations.forEach(function (loc) {
                if (demographic_id === loc.key) {
                    demo = loc.value
                } else {
                    return 'N/A'
                }
            })
        } else if (demographic === 'department') {
            this.state.departments.forEach(function (dep) {
                if (demographic_id === dep.key) {
                    demo = dep.value
                } else {
                    return 'N/A'
                }
            })
        } else if (demographic === 'cohort') {
            this.state.cohorts.forEach(function (cohort) {
                if (demographic_id === cohort.key) {
                    demo = cohort.value
                } else {
                    return 'N/A'
                }
            })
        } else if (demographic === 'language') {
            this.state.languages.forEach((lang) => {
                if(demographic_id === lang.key) {
                    demo = lang.value
                } else {
                    return 'N/A'
                }
            })
        }
        return demo
    }

    checkIsFormDirty = () => {
        if (JSON.stringify(this.state.user_details) === JSON.stringify(this.state.original_details)) {
            this.setState({
                is_form_dirty: false
            })
        } else {
            this.setState({
                is_form_dirty: true
            })
        }
    }

    onChange = (e) => {
        let att = [e.target.name]
        let value = e.target.value
        let { user_details, change_set } = this.state;
        user_details[att] = value;
        change_set[att] = value;
        this.setState({ user_details, change_set }, () => this.checkIsFormDirty())
    };

    handleFiles = async (e) => {
        const { client_id } = this.props.match.params;
        const files = Array.from(e.target.files)
        this.setState({ uploading_image: true });
        const formData = new FormData();
        formData.append('jpg', files[0]);
        const response = await this.props.uploadUserProfileImage(client_id, formData);
        if (response.status === 200) {
            NotificationManager.info('Image is uploaded successfully, Please refresh to verify')
        }
        this.setState({ uploading_image: false });
    }

    openConfirmationModal = () => {
        const { confirmation_model, user_details } = this.state;
        confirmation_model.is_active = true;
        confirmation_model.title = `Remove ${user_details.name}'s Image!`
        this.setState({ confirmation_model: confirmation_model })
    }
    closeConfirmationModal = (e) => {
        const { confirmation_model } = this.state;
        confirmation_model.title = `Remove users Image!`
        confirmation_model.is_active = false;
        this.setState({ confirmation_model: confirmation_model })
    }

    removeUserImage = () => {
        const { client_id, user_id } = this.props.match.params;
        const {user_details} = this.state
        this.props.removeUserProfileImage(client_id, user_id);
        NotificationManager.info(`${user_details.name}'s image is restored to default`)
        this.closeConfirmationModal()
    }

    onDateChange = (type, e) => {
        if (e) {
            this.handleDateChange(type, moment(e).unix())
        } else {
            this.handleDateChange(type, null)
        }
    }
    
    handleDateChange = (type, date) => {
        const {user_details, change_set} = this.state;
        user_details[type] = date ? new Date(date * 1000) : null
        change_set[type] = date ? date : null
        this.setState({user_details: user_details, change_set}, () => this.checkIsFormDirty())
    }
    
    updateRadioOptions = (e) => {
        const {user_details, change_set} = this.state;
        let name = e.target.name;
        let value = e.target.value
        user_details[name] = parseInt(value)
        change_set[name] = parseInt(value)
        this.setState({user_details : user_details, change_set}, () => this.checkIsFormDirty())
    }

    updateProfileConfimation = () => {
        const {profile_updation_confirmation_model} = this.state;
        profile_updation_confirmation_model.is_active = true
        this.setState({profile_updation_confirmation_model: profile_updation_confirmation_model})
    }

    closeProfileConfirmationModal = (e) => {
        const { profile_updation_confirmation_model } = this.state;
        profile_updation_confirmation_model.title = `Remove users Image!`
        profile_updation_confirmation_model.is_active = false;
        this.setState({ profile_updation_confirmation_model: profile_updation_confirmation_model })
    }

    updateUserProfile = async () => {
        const { client_id, user_id } = this.props.match.params;
        const response = await this.props.updateUserProfile(client_id, user_id, this.state.change_set);
        if (response.status === 200) {
            NotificationManager.info('User profile is updated')
            this.closeProfileConfirmationModal()
            this.setState({original_details: {...this.state.user_details}}, () => this.checkIsFormDirty())
        } else {
            NotificationManager.info(response.data)
        }
    }

    setDropdowns = () => {
        const {user_details} = this.state
        this.selectGender({ key: 0, value: user_details.gender })
        this.selectMaritalStatus({ key: 0, value: user_details.marital_status })
        this.setDemographicName(user_details.location_id, 'location')
        this.setDemographicName(user_details.department_id, 'department')
        this.setDemographicName(user_details.current_cohort_id, 'cohort')
        this.setDemographicName(user_details.current_language_id, 'language')
    }

    goBack = () => {
        const { client_id, user_id } = this.props.match.params;
        this.props.history.push(`/admin-panel/clients/${client_id}/users`)
    }

    resetProfile = () => {
        this.setState({user_details: JSON.parse(JSON.stringify(this.state.original_details)), change_set: {}}, () => {
            this.checkIsFormDirty()
            this.setDropdowns()
        })
    }

    render() {
        const { user_details, selected_gender, gender_options, marital_status_options, selected_marital_status, loading, departments, locations, cohorts, current_cohort, current_department, current_location, show_advanced, show_dates, show_basic_details, is_form_dirty, uploading_image, confirmation_model, profile_updation_confirmation_model, preset_language, languages, original_details } = this.state;
        return (
            loading ? <h2 className="has-text-weight-semibold is-size-5 has-text-centered mg-top-40"> Loading Profile...
                </h2> : <div className="user-profile columns">
                    <div className="column is-3 flex-wrap border-right dashed">
                    <button onClick={this.goBack} className="button is-info is-inverted margineft-xs" >
                        <i className={classnames('fas fa-arrow-left marginright-xs ')}></i>
                        Go Back
                    </button>
                        <div className="column is-12  has-text-centered   image-conatiner padding-right-25 mg-right-20">
                            <figure className="image is-128x128">
                                <img alt="user-profile" className="is-rounded" src={user_details.pic_url} />
                            </figure>
                            
                        </div>
                        <div className="column is-12 has-text-centered">
                            <h2 className="has-text-weight-bold is-size-5 marginbottom-sm">
                                {original_details.name}'s Profile
                            </h2>
                            <p className="is-size-7 has-text-weight-bold marginbottom-md">
                                {
                                    user_details.role === 0 ? `inFeedo's Super Admin` :
                                    (user_details.primary_admin === user_details.id && user_details.role == 1) ? `Primary Admin`:
                                    (user_details.role === 1 && user_details.primary_admin !== user_details.id) ? `Admin`:
                                    user_details.role === 2 ? 'IAM Admin' :
                                    user_details.role === 3 ? 'Member' : ''
                                }  
                            </p>
                            <p className="margintop-sm">
                                <span className="file is-small marginright-xs">
                                    <label className="file-label ">
                                        <input className="file-input" type="file" accept="image/jpg" name="signature" onChange={this.handleFiles} />

                                        {!uploading_image ? <span className="file-cta">
                                            <span className="file-label">
                                                Upload Image
                                            </span></span> :
                                            <a className="button is-loading is-small">
                                                Uploading...
                                        </a>}

                                    </label>
                                </span> | <a className="button is-danger is-outlined is-small marginleft-xs" onClick={this.openConfirmationModal}>Reset Image</a>
                            </p>
                            
                            <p className="control margintop-md paddingleft-md has-text-centered">
                                <button className="button is-bordered marginright-md is-small" disabled={!is_form_dirty} onClick={this.resetProfile}>
                                    Reset
                                </button> 
                                <button className="button is-link is-small" disabled={!is_form_dirty} onClick={this.updateProfileConfimation}>
                                    Update Profile
                                </button> 
                            </p>
                            
                        </div>
                    </div>
                    <div className="columns column is-9">
                        <div className="column paddingleft-lg user-details">
                                <h2 className="has-text-weight-semibold is-size-5 has-text-left marginbottom-md border-bottom"> 
                                    <span className="border-bottom bold  paddingbottom-xs">
                                        Basic Details
                                    </span>
                                </h2>
                                <div className="basic-details padding-md marginbottom-xlg card">
                                    <div className="columns">
                                        <div className="column">
                                            <label className="label">
                                                Name
                                            </label>
                                            <TextInputGroup
                                                name="name"
                                                placeholder="Name"
                                                value={user_details.name || ''}
                                                type="text"
                                                onChange={this.onChange}
                                                icon="fas fa-user"
                                                auto_focus={true}
                                            />
                                        </div>
                                        <div className="column">
                                            <label className="label">
                                                Email ID
                                            </label>
                                            <TextInputGroup
                                                name="email"
                                                placeholder="Email ID"
                                                value={user_details.email || ''}
                                                type="email"
                                                onChange={this.onChange}
                                                icon="fas fa-envelope"
                                                disabled={true}
                                            />
                                        </div>
                                        <div className="column">
                                            <label className="label">
                                                Personal Email ID
                                            </label>
                                            <TextInputGroup
                                                name="personal_email"
                                                placeholder="Personal Email"
                                                value={user_details.personal_email || ''}
                                                type="text"
                                                onChange={this.onChange}
                                                icon="fas fa-user-tag"
                                            />
                                        </div>
                                    </div>
                                    <div className="columns">
                                        <div className="column">
                                            <label className="label">
                                                Location
                                            </label>
                                            <Dropdown dropdown_classes={'is-fullwidth'} classes={'justify-space-between is-fullwidth'} selected_item={current_location} dropdown_items={locations} onClick={this.selectLocation} />
                                        </div>
                                        <div className="column">
                                            <label className="label">
                                                Designation
                                            </label>
                                            <TextInputGroup
                                                name="designation"
                                                placeholder="Designation"
                                                value={user_details.designation || ''}
                                                type="text"
                                                onChange={this.onChange}
                                                icon="fas fa-user-tag"
                                            />
                                        </div>
                                        <div className="column">
                                            <label className="label">
                                                Function
                                            </label>
                                            <TextInputGroup
                                                name="function"
                                                placeholder="Function"
                                                value={user_details.function || ''}
                                                type="text"
                                                onChange={this.onChange}
                                                icon="fas fa-user-tag"
                                            />
                                        </div>
                                    </div>
                                    <div className="columns">
                                        <div className="column">
                                            <label className="label">
                                                Given Name
                                            </label>
                                            <TextInputGroup
                                                name="given_name"
                                                placeholder="Given Name"
                                                value={user_details.given_name || ''}
                                                type="text"
                                                onChange={this.onChange}
                                                icon="fas fa-user"
                                            />
                                        </div>
                                        <div className="column">
                                            <label className="label">
                                                Phone
                                            </label>
                                            <TextInputGroup
                                                name="phone"
                                                placeholder="Phone"
                                                value={user_details.phone || ''}
                                                type="text"
                                                onChange={this.onChange}
                                                icon="fas fa-user-tag"
                                            />
                                        </div>
                                        <div className="column">
                                            <label className="label">
                                                Sub Function
                                            </label>
                                            <TextInputGroup
                                                name="sub_function"
                                                placeholder="Sub Function"
                                                value={user_details.sub_function || ''}
                                                type="text"
                                                onChange={this.onChange}
                                                icon="fas fa-user-tag"
                                                disabled={true}
                                            />
                                        </div>

                                    </div>
                                    <div className="columns">
                                        <div className="column">
                                            <label className="label">
                                                Gender
                                            </label>
                                            <Dropdown dropdown_classes={'is-fullwidth'} classes={'justify-space-between is-fullwidth'} selected_item={selected_gender} dropdown_items={gender_options} onClick={this.selectGender} />
                                        </div>
                                        <div className="column">
                                            <label className="label">
                                                Cohort
                                            </label>
                                            <Dropdown dropdown_classes={'is-fullwidth'} classes={'justify-space-between is-fullwidth'} selected_item={current_cohort} dropdown_items={cohorts} onClick={this.selectCohort} />
                                        </div>
                                        <div className="column">
                                            <label className="label">
                                                Marital Status
                                            </label>
                                            <Dropdown dropdown_classes={'is-fullwidth'} classes={'justify-space-between is-fullwidth'} selected_item={selected_marital_status} dropdown_items={marital_status_options} onClick={this.selectMaritalStatus} />
                                        </div>
                                    </div>
                                    
                                    <div className="columns">
                                        <div className="column is-4">
                                            <label className="label">
                                                Department
                                            </label>
                                            <Dropdown dropdown_classes={'is-fullwidth'} classes={'justify-space-between is-fullwidth'} selected_item={current_department} dropdown_items={departments} onClick={this.selectDepartment} />
                                        </div>
                                        {
                                            user_details.role !== 3 ?
                                                <div className="column">
                                                <label className="label">
                                                    Launch SPOC
                                                </label>
                                                <div className="control margintop-sm paddingtop-xs" onChange={this.updateRadioOptions.bind(this)}>
                                                    <label className="radio">
                                                        <input disabled={!user_details.show_is_spoc_toggle} readOnly value="1" type="radio" name="is_spoc" checked={user_details.is_spoc} />
                                                        &nbsp;Yes
                                                    </label>
                                                    <label className="radio">
                                                        <input disabled={!user_details.show_is_spoc_toggle} readOnly value="0" type="radio" name="is_spoc" checked={!user_details.is_spoc} />
                                                        &nbsp;No
                                                    </label>
                                                </div>
                                                {
                                                    !user_details.show_is_spoc_toggle && user_details.is_spoc ?
                                                    <small className="has-text-success is-size-7 line-height-17"><i className="fas fa-info-circle marginright-xs"></i> CSAT feedback received from user</small> :
                                                    user_details.show_is_spoc_toggle && !user_details.is_spoc ?
                                                    <small className="has-text-danger is-size-7 line-height-17">
                                                        <i className="fas fa-info-circle marginright-xs"></i> 
                                                        CSAT feedback modal on dashboard turned off.
                                                    </small> : user_details.show_is_spoc_toggle && user_details.is_spoc?
                                                    <small className="has-text-success is-size-7 line-height-17">
                                                        <i className="fas fa-info-circle marginright-xs"></i> CSAT feedback modal on dashboard turned on.
                                                    </small> : !user_details.show_is_spoc_toggle && !user_details.is_spoc ?
                                                    <small className="has-text-danger is-size-7 line-height-17">
                                                        <i className="fas fa-info-circle marginright-xs"></i> CSAT feedback modal active only for 2 weeks after launch.
                                                    </small> : ''
                                                }
                                            </div>
                                        : null
                                        }
                                        <div className={classnames('column is-4', {'is-hidden': !user_details.been_multilingual})}>
                                            <label className="label">
                                                Preset Language
                                            </label>
                                            <Dropdown classes={'justify-space-between is-fullwidth'} dropdown_classes={!user_details.multilingual_enabled ? 'is-disabled is-fullwidth' : ' is-fullwidth'} selected_item={preset_language} dropdown_items={languages} onClick={this.selectLanguage} />
                                        </div>

                                    </div>
                                </div>
                                
                                
                            <h2 className="has-text-weight-semibold is-size-5 has-text-left marginbottom-md border-bottom"> 
                                <span className="border-bottom bold  paddingbottom-xs">
                                    Dates
                                </span>
                            </h2>
                            <div className="dates padding-md marginbottom-xlg card">
                                <div className="columns">
                                    <div className="column">
                                        <label className="label">
                                            Date Of Joining (DOJ)
                                </label>
                                        <DatePicker name="joining" onChange={this.onDateChange.bind(this, 'joining')} value={user_details.joining} />
                                    </div>
                                    <div className="column">
                                        <label className="label">
                                            Date Of Birth (DOB)
                                </label>
                                        <DatePicker name="birthday" onChange={this.onDateChange.bind(this,'birthday')} value={user_details.birthday} />
                                    </div>
                                    <div className="column">
                                        <label className="label">
                                            Date Of Wedding (DOW)
                                </label>
                                        <DatePicker name="wedding" onChange={this.onDateChange.bind(this,'wedding')} value={user_details.wedding} />
                                    </div>
                                    </div>
                                <div className="columns">
                                    <div className="column">
                                        <label className="label">
                                            Date of Appraisal (DOA)
                                </label>
                                        <DatePicker name="doa" onChange={this.onDateChange.bind(this,'doa')} value={user_details.doa} />
                                    </div>
                                    <div className="column">
                                        <label className="label">
                                            Date of Resignation (DOR)
                                </label>
                                        <DatePicker name="resignation_date" onChange={this.onDateChange.bind(this,'resignation_date')} value={user_details.resignation_date} />
                                    </div>
                                    <div className="column">
                                        <label className="label">
                                            Date of Leaving (DOL)
                                </label>
                                        <DatePicker name="leaving_date" onChange={this.onDateChange.bind(this,'leaving_date')} value={user_details.leaving_date} />
                                    </div>
                                </div>
                            </div>

                            
                            <h2 className="has-text-weight-semibold is-size-5 has-text-left marginbottom-md border-bottom"> 
                                <span className="border-bottom bold  paddingbottom-xs">
                                    Advanced Detailes
                                </span>
                            </h2>
                            <div className="advanced padding-md card">
                                <div className="columns">
                                <div className="column">
                                    <label className="label">
                                        Employee Code
                                    </label>
                                    <TextInputGroup
                                        name="employee_code"
                                        placeholder="Employee Code"
                                        value={user_details.employee_code || ''}
                                        type="text"
                                        onChange={this.onChange}
                                        icon="fas fa-user-tag"
                                        disabled={true}
                                    />
                                </div>
                                <div className="column">
                                    <label className="label">
                                        Employee Grade
                                    </label>
                                    <TextInputGroup
                                        name="employee_grade"
                                        placeholder="Employee Grade"
                                        value={user_details.employee_grade || ''}
                                        type="text"
                                        onChange={this.onChange}
                                        icon="fas fa-user-tag"
                                    />
                                </div>
                                <div className="column">
                                    <label className="label">
                                        Business
                                    </label>
                                    <TextInputGroup
                                        name="business"
                                        placeholder="Business"
                                        value={user_details.business || ''}
                                        type="text"
                                        onChange={this.onChange}
                                        icon="fas fa-user-tag"
                                    />
                                </div>
                            </div>
                                <div className="columns">
                                    <div className="column">
                                        <label className="label">
                                            City
                                        </label>
                                        <TextInputGroup
                                            name="city"
                                            placeholder="City"
                                            value={user_details.city || ''}
                                            type="text"
                                            onChange={this.onChange}
                                            icon="fas fa-city"
                                            disabled={true}
                                        />
                                    </div>

                                    <div className="column">
                                        <label className="label">
                                            Country
                                        </label>
                                        <TextInputGroup
                                            name="country"
                                            placeholder="Country"
                                            value={user_details.country || ''}
                                            type="text"
                                            onChange={this.onChange}
                                            icon="fas fa-globe"
                                            disabled={true}
                                        />
                                    </div>
                                    <div className="column">
                                        <label className="label">
                                            Vertical
                                        </label>
                                        <TextInputGroup
                                            name="vertical"
                                            placeholder="Vertical"
                                            value={user_details.vertical || ''}
                                            type="text"
                                            onChange={this.onChange}
                                            icon="fas fa-user-tag"
                                            disabled={true}
                                        />
                                    </div>
                                </div>
                                <div className="columns">
                                    <div className="column">
                                        <label className="label">
                                            Team
                                        </label>
                                        <TextInputGroup
                                            name="team"
                                            placeholder="Team"
                                            value={user_details.team || ''}
                                            type="text"
                                            onChange={this.onChange}
                                            icon="fas fa-user-tag"
                                        />
                                    </div>
                                    <div className="column">
                                        <label className="label">
                                            Custom Company
                                        </label>
                                        <TextInputGroup
                                            name="custom_company"
                                            placeholder="Custom Company"
                                            value={user_details.custom_company || ''}
                                            type="text"
                                            onChange={this.onChange}
                                            icon="fas fa-user-tag"
                                        />
                                    </div>
                                    <div className="column">
                                        <label className="label">
                                            Division
                                        </label>
                                        <TextInputGroup
                                            name="division"
                                            placeholder="Division"
                                            value={user_details.division || ''}
                                            type="text"
                                            onChange={this.onChange}
                                            icon="fas fa-user-tag"
                                            disabled={true}
                                        />
                                    </div>
                                </div>
                                <div className="columns">
                                    <div className="column">
                                        <label className="label">
                                            Email Sender
                                        </label>
                                        <TextInputGroup
                                            name="email_sender"
                                            placeholder="Email Sender"
                                            value={user_details.email_sender || ''}
                                            type="text"
                                            onChange={this.onChange}
                                            icon="fas fa-user-tag"
                                        />
                                    </div>
                                    <div className="column">
                                        <label className="label">
                                            Email Sender Designation
                                        </label>
                                        <TextInputGroup
                                            name="email_sender_designation"
                                            placeholder="Email Sender Designation"
                                            value={user_details.email_sender_designation || ''}
                                            type="text"
                                            onChange={this.onChange}
                                            icon="fas fa-user-tag"
                                        />
                                    </div>
                                    <div className="column">
                                        <label className="label">
                                            HRBP
                                        </label>
                                        <TextInputGroup
                                            name="hrbp"
                                            placeholder="HRBP"
                                            value={user_details.hrbp || ''}
                                            type="text"
                                            onChange={this.onChange}
                                            icon="fas fa-user-tag"
                                        />
                                    </div>
                                </div>
                                <div className="columns">
                                    <div className="column">
                                        <label className="label">
                                            Is CXO
                                        </label>
                                        <div className="control margintop-sm paddingtop-xs" onChange={this.updateRadioOptions.bind(this)}>
                                            <label className="radio">
                                                <input readOnly value="1" type="radio" name="is_cxo" checked={user_details.is_cxo} />
                                                &nbsp;Yes
                                            </label>
                                            <label className="radio">
                                                <input readOnly value="0" type="radio" name="is_cxo" checked={!user_details.is_cxo} />
                                                &nbsp;No
                                            </label>
                                        </div>
                                    </div>
                                    <div className="column">
                                        <label className="label">
                                            L_Team
                                        </label>
                                        <TextInputGroup
                                            name="l_team"
                                            placeholder="L_Team"
                                            value={user_details.l_team || ''}
                                            type="text"
                                            onChange={this.onChange}
                                            icon="fas fa-user-tag"
                                        />
                                    </div>
                                    <div className="column">
                                        <label className="label">
                                            Nationality
                                        </label>
                                        <TextInputGroup
                                            name="nationality"
                                            placeholder="Nationality"
                                            value={user_details.nationality || ''}
                                            type="text"
                                            onChange={this.onChange}
                                            icon="fas fa-user-tag"
                                            disabled={true}
                                        />
                                    </div>
                                </div>
                                <div className="columns">
                                    <div className="column">
                                        <label className="label">
                                            Office Phone
                                        </label>
                                        <TextInputGroup
                                            name="office_phone"
                                            placeholder="Office Phone"
                                            value={user_details.office_phone || ''}
                                            type="text"
                                            onChange={this.onChange}
                                            icon="fas fa-user-tag"
                                        />
                                    </div>
                                    <div className="column">
                                        <label className="label">
                                            Region
                                        </label>
                                        <TextInputGroup
                                            name="region"
                                            placeholder="Region"
                                            value={user_details.region || ''}
                                            type="text"
                                            onChange={this.onChange}
                                            icon="fas fa-user-tag"
                                            disabled={true}
                                        />
                                    </div>
                                    <div className="column">
                                        <label className="label">
                                            Sub Department
                                        </label>
                                        <TextInputGroup
                                            name="sub_department"
                                            placeholder="Sub Department"
                                            value={user_details.sub_department || ''}
                                            type="text"
                                            onChange={this.onChange}
                                            icon="fas fa-user-tag"
                                            disabled={true}
                                        />
                                    </div>
                                </div>
                                <div className="columns">
                                    <div className="column">
                                        <label className="label">
                                            Unsubscribe Celebration Email
                                        </label>
                                        <div className="control margintop-sm paddingtop-xs" onChange={this.updateRadioOptions.bind(this)}>
                                            <label className="radio">
                                                <input readOnly value="1" type="radio" name="unsubscribe_celebration_email" checked={user_details.unsubscribe_celebration_email} />
                                                &nbsp;Yes
                                            </label>
                                            <label className="radio">
                                                <input readOnly value="0" type="radio" name="unsubscribe_celebration_email" checked={!user_details.unsubscribe_celebration_email} />
                                                &nbsp;No
                                            </label>
                                        </div>
                                    </div>
                                    <div className="column">
                                        <label className="label">
                                            Unsubscribe Fortnightly Email
                                        </label>
                                        <div className="control margintop-sm paddingtop-xs" onChange={this.updateRadioOptions.bind(this)}>
                                            <label className="radio">
                                                <input readOnly value="1" type="radio" name="unsubscribe_fortnightly" checked={user_details.unsubscribe_fortnightly} />
                                                &nbsp;Yes
                                            </label>
                                            <label className="radio">
                                                <input readOnly type="radio" value="0" name="unsubscribe_fortnightly" checked={!user_details.unsubscribe_fortnightly} />
                                                &nbsp;No
                                            </label>
                                        </div>
                                    </div>
                                    <div className="column">
                                        <label className="label">
                                            Unsubscribe daily Email
                                        </label>
                                        <div className="control margintop-sm paddingtop-xs" onChange={this.updateRadioOptions.bind(this)}>
                                            <label className="radio">
                                                <input readOnly value="1" type="radio" name="unsubscribe_daily" checked={user_details.unsubscribe_daily} />
                                                &nbsp;Yes 
                                            </label>
                                            <label className="radio">
                                                <input readOnly value="0" type="radio" name="unsubscribe_daily" checked={!user_details.unsubscribe_daily} />
                                                &nbsp;No
                                            </label>
                                        </div>
                                    </div>
                                </div> 
                                    <div className="columns">
                                        <div className="column">
                                            <label className="label">
                                                WhatsApp Notifications
                                            </label>
                                            <p>
                                                {
                                                    user_details.whatsapp_notification === 0 ? 'DEFAULT' :
                                                    user_details.whatsapp_notification === 1 ? 'OPT-IN' :
                                                    user_details.whatsapp_notification === 2 ? 'OPT-OUT' : '' 
                                                }
                                            </p>
                                        </div>
                                        <div className="column">
                                            <label className="label">
                                                Minimum Responses on Dashboard
                                            </label>
                                            <TextInputGroup
                                                name="min_threshold"
                                                placeholder="Min Threshold"
                                                value={user_details.min_threshold || ''}
                                                type="text"
                                                onChange={this.onChange}
                                                icon="fas fa-user-tag"
                                                disabled={false}
                                            />
                                        </div>
                                        <div className="column">
                                            <label className="label">
                                                Work Experience
                                            </label>
                                            <TextInputGroup
                                                name="work_experience"
                                                placeholder="Work Experience"
                                                value={user_details.work_experience || ''}
                                                type="text"
                                                onChange={this.onChange}
                                                icon="fas fa-user-tag"
                                                disabled={true}
                                            />
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <ConfirmModal
                        title={confirmation_model.title}
                        text={confirmation_model.text}
                        confirm_text={confirmation_model.confirm_text}
                        onConfirm={this.removeUserImage}
                        onClose={this.closeConfirmationModal}
                        is_active={confirmation_model.is_active} />
                    <ConfirmModal
                        title={profile_updation_confirmation_model.title}
                        text={profile_updation_confirmation_model.text}
                        confirm_text={profile_updation_confirmation_model.confirm_text}
                        onConfirm={this.updateUserProfile}
                        onClose={this.closeProfileConfirmationModal.bind(this)}
                        is_active={profile_updation_confirmation_model.is_active} />
                </div>
        )
    }
}


// PropTypes
// ---------
UserDetails.propTypes = {
    getUserDetails: PropTypes.func.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    user_details: state.client.user_details

})

export default connect(mapStateToProps, { getUserDetails, getDepartments, getLocations, getCohorts, uploadUserProfileImage, removeUserProfileImage, updateUserProfile })(UserDetails);