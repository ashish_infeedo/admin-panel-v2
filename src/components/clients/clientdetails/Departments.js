import React, { Component } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';
// components
import TextInputgroup from '../../generic/TextInputGroup';
import ConfirmModal from '../../generic/ConfirmModal';
import {looseFocus} from '../../../helper/LooseFocus';
// actions
import { getDepartments, addDepartments, deleteDepartment, editDepartment } from '../../../actions/ClientsActions';

class Departments extends Component {

  state = {
    client_id: null,
    search: '',
    departments: [],
    original_departments: [],
    new_departments: '',
    edit_department_id: null,
    edit_department_text: '',
    total_new_departments: 0,
    selected_department: {},
    delete_modal: {
      title: 'Delete Department',
      text: 'Are you sure you want to delete?',
      confirm_text: 'Yes',
      is_active: false
    },
    adding: false,
    loading: false,
    deleting: false,
    editing: false,
    disbable_add: true
  }
  
  componentDidMount(){
    const { client_id } = this.props.match.params;
    this.setState({client_id: client_id});
    this.setState({loading: true});
    this.getDepartments();
  }

  getDepartments = async () => {
    const { client_id } = this.props.match.params;
    const response = await this.props.getDepartments(client_id);
    this.setState({departments: response, original_departments: response, loading: false});
  }

  editDepartent = async (departent) => {
    const id = departent.id;
    const { edit_department_text } = this.state;
    looseFocus();
    this.setState({editing: true});
    await this.props.editDepartment(id, edit_department_text);
    this.getDepartments();
    this.setState({edit_department_text: '', edit_department_id: null});
    this.setState({editing: false});
    looseFocus();
  }

  cancelEdit = () => {
    this.setState({edit_department_text: '', edit_department_id: null});
    looseFocus();
  }

  selectEditDepartent = (departent) => {
    this.setState({edit_department_text: departent.title, edit_department_id: departent.id});
  }

  openDeleteModal = (departent) => {
    const { delete_modal } = this.state;
    delete_modal.title += ` ${departent.title}`;
    delete_modal.is_active = true;
    this.setState({delete_modal: delete_modal, selected_department: departent});
  }

  closeDeleteModal = () => {
    const { delete_modal } = this.state;
    delete_modal.is_active = false;
    delete_modal.title = 'Delete Department';
    this.setState({delete_modal: delete_modal});
  }

  onSubmit = async (e) => {
    e.preventDefault();
    const { client_id, new_departments } = this.state;
    this.setState({adding: true});
    await this.props.addDepartments(client_id, new_departments);
    this.getDepartments();
    this.setState({adding: false, new_departments: '', disbable_add: true, total_new_departments: 0});
  }

  onSearch = (e) => {
    this.setState({ [e.target.name]: e.target.value });
    const search = e.target.value;
    const { original_departments } = this.state;
    const departments = original_departments;
    if(search.length > 0) {
        const results = departments.filter(department => department.title.toLowerCase().includes(search.toLowerCase()));
        this.setState({departments: results});
    } else if(!search.length) {
        this.setState({departments: original_departments});
    }
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  onChangeDepartments = (e) => {
    this.onChange(e);
    const departments = e.target.value;
    if(departments.length > 0) {
      let departments_array = departments.split(',');
      if (departments_array[departments_array.length - 1] === "") {
        departments_array.pop();
      }
      const count = departments_array.length;
      this.setState({total_new_departments: count, disbable_add: false});
    } else {
      this.setState({total_new_departments: 0, disbable_add: true});
    }
  }

  looseFocus = () => {
    document.activeElement.blur();
  }

  confirmDelete = async () => {
    const { id } = this.state.selected_department;
    this.setState({deleting: true});
    await this.props.deleteDepartment(id);
    this.closeDeleteModal();
    this.getDepartments();
    this.setState({deleting: false});
  }

  openDeleteModal = (departent) => {
    const { delete_modal } = this.state;
    delete_modal.title += ` ${departent.title}`;
    delete_modal.is_active = true;
    this.setState({delete_modal: delete_modal, selected_department: departent});
  }

  closeDeleteModal = () => {
    const { delete_modal } = this.state;
    delete_modal.is_active = false;
    delete_modal.title = 'Delete Cohort';
    this.setState({delete_modal: delete_modal});
  }

  render() {
    const { search, adding, loading, editing, deleting, edit_department_id, edit_department_text, new_departments, total_new_departments, departments, delete_modal, disbable_add } = this.state;
    return (
      <div>
        <div className="columns">
          <div className="column is-3">
          <TextInputgroup
              name="search"
              placeholder="Search Department"
              value={search}
              type="text"
              onChange={this.onSearch}
              icon="fas fa-search"
          />
          </div>
          <div className="column is-8">
            <form onSubmit={this.onSubmit} className="columns">
              <div className="column is-10">
              <TextInputgroup
                  name="new_departments"
                  placeholder="Add Comma Separated Departments"
                  value={new_departments}
                  type="text"
                  onChange={this.onChangeDepartments}
                  icon="fas fa-building"
              />
              </div>
              <div className="column is-3 pd-up-20">
                <button type="submit" className={classnames('button is-success is-fullwidth',{'is-loading': adding})} disabled={disbable_add}>
                  <span className="icon">
                    <i className="fas fa-plus"></i>
                  </span>
                  <span>Add ({total_new_departments})</span>
                </button>
              </div>
            </form>
          </div>
        </div>
        <div className="card">
          <div className="card-content is-paddingless">
            <div className="content">
              <div className="custom-table">
                <table className="table is-striped">
                  <thead>
                      <tr>
                      <th>S.No.</th>
                      <th>Department</th>
                      <th>Created On</th>
                      <th>Actions</th>
                      </tr>
                  </thead>
                  <tbody>
                  {!loading ? departments.map((departent, index) => (
                    <tr key={index}>
                    <td>{index + 1}</td>
                    <td className={classnames({'is-paddingless': departent.id === edit_department_id})}>
                    { departent.id === edit_department_id ?
                    <TextInputgroup
                        name="edit_department_text"
                        placeholder=""
                        value={edit_department_text}
                        type="text"
                        onChange={this.onChange}
                        icon="fas fa-pen"
                        is_small={true}
                        auto_focus={true}
                    />
                    : departent.title
                    }
                    </td>
                    <td>{moment.unix(departent.created).format('DD/MM/YYYY')}</td>
                    <td>
                    { departent.id === edit_department_id ?
                    <div className="buttons">
                        <button onClick={this.editDepartent.bind(this, departent)} className={classnames('button is-success is-small is-rounded is-outlined',{'is-loading': editing})}>
                            <span>Save</span>
                        </button>
                        <button onClick={this.cancelEdit} className="button is-danger is-small is-rounded is-outlined">
                            <span>Cancel</span>
                        </button>
                    </div>
                    : 
                    <div className="buttons">
                        <button onClick={this.selectEditDepartent.bind(this, departent)} className="button is-info is-small is-rounded is-outlined">
                            <span>Edit</span>
                        </button>
                        <button onClick={this.openDeleteModal.bind(this, departent)} className="button is-danger is-small is-rounded is-outlined">
                            <span>Delete</span>
                        </button>
                    </div>
                    }
                    </td>
                    </tr>
                  )) : null}
                  </tbody>
                </table>
                {(departments.length === 0 && !loading) ? <p className="no-data has-text-centered">No Data</p> : null}
                {loading ? <p className="no-data has-text-centered">Loading Data...</p> : null}
              </div>
            </div>
          </div>
        </div>
        <ConfirmModal 
        title={delete_modal.title}
        text={delete_modal.text}
        confirm_text={delete_modal.confirm_text}
        onConfirm={this.confirmDelete}
        onClose={this.closeDeleteModal}
        is_active={delete_modal.is_active}
        doing={deleting}/>
      </div>
    )
  }
}

// PropTypes
// ---------
Departments.propTypes = {
  getDepartments: PropTypes.func.isRequired,
  addDepartments: PropTypes.func.isRequired,
  deleteDepartment: PropTypes.func.isRequired,
  editDepartment: PropTypes.func.isRequired
}

export default connect(null, {getDepartments, addDepartments, deleteDepartment, editDepartment})(Departments);