import React, { Component } from 'react'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import SummaryDetailsModal from './SummaryDetailsModal'
import classnames from 'classnames';
// actions
import { getLaunchCheckList, sendTriggerChat } from '../../../actions/ClientsActions';

class Summary extends Component {

  state = {
    client_id: null,
    launchCheckList: {},
    is_modal_active: false,
    header_text: "",
    sub_header_text: "",
    cardData: []
  }

  componentDidMount() {
    const { client_id } = this.props.match.params;
    this.setState({
      client_id: client_id
    })
    this.getLaunchCheckListDeatils(client_id)
  }

  componentDidUpdate() {
    const { client_id } = this.props.match.params;
    if (this.state.client_id !== client_id) {
      this.setState({
        client_id: client_id
      })
      this.getLaunchCheckListDeatils(client_id)
    }
  }

  getLaunchCheckListDeatils = async (client_id) => {
    const response = await this.props.getLaunchCheckList(client_id)
    this.setState({
      launchCheckList: response.data
    })
  }

  returnGroupElements = () => {
    const { launchCheckList } = this.state;
    let HTML = ''
    if (launchCheckList.groupEnabled === 1 || launchCheckList.groupEnabled === 2 || launchCheckList.groupEnabled === 3)
      HTML = <div className="color-green"> Group is enabled. </div>
    else if (launchCheckList.groupEnabled === 4)
      HTML = <div className="color-red"> Trial for {this.props.client_details.title} has ended. </div>
    else if (launchCheckList.groupEnabled === 5)
      HTML = <div className="color-red"> Subscription for {this.props.client_details.title} has ended.</div>
    else if (launchCheckList.groupEnabled === 6)
      HTML = <div className="color-red"> Group is disabled. </div>
    else
      HTML = null

    return HTML;
  }

  showDepartments = (event) => {
    let cardData = this.state.launchCheckList.employees_in_departments;
    this.showSummaryModal("Departments", cardData, "Number of employees per department")
  }

  showLocations = (event) => {
    let cardData = this.state.launchCheckList.employees_in_locations;
    this.showSummaryModal("Locations", cardData, "Number of employees per location")
  }


  showCohorts = (event) => {
    let cardData = this.state.launchCheckList.employees_in_cohorts;
    this.showSummaryModal("Cohorts", cardData, "Number of employees per cohort")
  }

  showSummaryModal = (text, cardData, subHeaderText) => {
    this.setState({
      is_modal_active: true,
      header_text: text,
      cardData: cardData,
      sub_header_text: subHeaderText
    })
  }

  hideSummaryModal = () => {
    this.setState({
      is_modal_active: false,
      header_text: "",
      cardData: [],
      sub_header_text: ""
    })
  }

  triggerchats = async () => {
    const { client_id } = this.state;
    await this.props.sendTriggerChat(client_id)
  }

  onModalClose = () => {
    this.hideSummaryModal()
  }

  render() {
    const { launchCheckList, is_modal_active, header_text, cardData, sub_header_text } = this.state;
    return (
      <div>
        <div className="summary-outer-div">
          <div className="columns is-12 pd-up-20">
            {launchCheckList.numberOfDepartments > 0 && launchCheckList.employees_in_departments.length > 0
              ? <div onClick={this.showDepartments} className="color-green" > Departments - {launchCheckList.numberOfDepartments} </div>
              : <div className="color-red" > Departments - {launchCheckList.numberOfDepartments} </div>
            }
          </div>
          <div className="columns is-12 pd-up-20">
            {launchCheckList.numberOfLocations > 0 && launchCheckList.employees_in_locations.length > 0
              ? <div onClick={this.showLocations} className="color-green" > Locations - {launchCheckList.numberOfLocations}</div>
              : <div className="color-red" > Locations - {launchCheckList.numberOfLocations}</div>
            }
          </div>
          <div className="columns is-12 pd-up-20">
            {launchCheckList.numberOfCohorts > 0 && launchCheckList.employees_in_cohorts.length > 0
              ? <div onClick={this.showCohorts} className="color-green" > Cohorts - {launchCheckList.numberOfCohorts}</div>
              : <div className="color-red" > Cohorts - {launchCheckList.numberOfCohorts}</div>
            }
          </div>
          <div className="columns is-12 pd-up-20">
            {launchCheckList.numberOfDefaultCohortStages > 0
              ? <div className="color-green" > Active Stages - {launchCheckList.numberOfDefaultCohortStages}</div>
              : <div className="color-red" > Active Stages - {launchCheckList.numberOfDefaultCohortStages}</div>
            }
          </div>
          <div className="columns is-12 pd-up-20 summary-header-text">
            <span> Users Summary </span>
          </div>
          <div className="columns is-12 pd-up-20">
            {launchCheckList.numberOfUsers > 0
              ? <div className="color-green" > All Users - {launchCheckList.numberOfUsers}</div>
              : <div className="color-red" > All Users - {launchCheckList.numberOfUsers}</div>
            }
          </div>
          <div className="columns is-12 pd-up-20">
            {launchCheckList.primaryAdminSet
              ? <div className="color-green" > Primary Admin - {launchCheckList.primaryAdminName}</div>
              : <div className="color-red" > Primary Admin is not set </div>
            }
          </div>
          {launchCheckList.number_of_whatsapp_opt_in || launchCheckList.number_of_whatsapp_opt_out ?
            <>
              <div className="columns is-12 pd-up-20 summary-header-text">
                <div> WhatsApp </div>
              </div>
              <div className="columns is-12 pd-up-20">
                <div className="color-green" > Opted-In Users - {launchCheckList.number_of_whatsapp_opt_in} </div>
                <div className="color-green" > Opted-Out Users - {launchCheckList.number_of_whatsapp_opt_out} </div>
              </div>
            </>
            : null
          }

          <div className="columns is-12 pd-up-20 summary-header-text">
            <div> Members: </div>
          </div>
          <div className="columns is-12 pd-up-20">
            {launchCheckList.numberOfNonAdminUsers > 0
              ? <div className="color-green" > Total Active Users - {launchCheckList.numberOfNonAdminUsers} </div>
              : <div className="color-red" > Active Users - {launchCheckList.numberOfNonAdminUsers} </div>
            }
          </div>
          <div className="columns is-12 pd-up-20">
            {launchCheckList.numberOfNonAdminUsersWithoutJoining > 0
              ? <div className="color-red" > Active Users (joining date not set) - {launchCheckList.numberOfNonAdminUsersWithoutJoining} </div>
              : null
            }
          </div>
          <div className="columns is-12 summary-border-text">
          </div>
          <div className="columns is-12 pd-up-20">
            {this.returnGroupElements()}
          </div>
          <div className="columns is-12 pd-up-20">
            {launchCheckList.exit_enable === 1
              ? <div className="color-green" > Exit Module is enabled </div>
              : <div className="color-red" > Exit Module is disabled </div>
            }
          </div>
          <div className="columns is-12 pd-up-20">
            {launchCheckList.inWildDomain
              ? <div className="color-red" > Group is in wild domain.</div>
              : <div className="color-green" > Group is not in wild domain.</div>
            }
          </div>
          <div className="columns is-12 pd-up-20">
            {launchCheckList.to_start_reminders_limit === 4
              ? <div className="color-green" > To start chat reminders limit is {launchCheckList.to_start_reminders_limit} </div>
              : <div className="color-yellow" > To start chat reminders limit is {launchCheckList.to_start_reminders_limit} </div>
            }
          </div>
          <div className="columns is-12 pd-up-20">
            {launchCheckList.incomplete_reminders_limit === 4
              ? <div className="color-green" > Incomplete chat reminders limit is {launchCheckList.incomplete_reminders_limit} </div>
              : <div className="color-yellow" > Incomplete chat reminders limit is {launchCheckList.incomplete_reminders_limit} </div>
            }
          </div>
          <div className="columns is-12 pd-up-20">
            <button type="submit" onClick={this.triggerchats} className={classnames('button is-link')}>
              <span>Trigger chats</span>
            </button>
          </div>
        </div>
        <SummaryDetailsModal is_active={is_modal_active} header_text={header_text} onClose={this.onModalClose} cardData={cardData} sub_header_text={sub_header_text} />
      </div>
    )
  }
}

Summary.propTypes = {
  getLaunchCheckList: PropTypes.func.isRequired,
  sendTriggerChat: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  client_details: state.client.client_details,
})

export default connect(mapStateToProps, { getLaunchCheckList, sendTriggerChat })(Summary);
