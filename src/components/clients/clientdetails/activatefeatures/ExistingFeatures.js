import React, { Component } from 'react';
import TextInputgroup from '../../../generic/TextInputGroup';
import { updateFeature, deleteExistingFeature } from '../../../../actions/ActivateFeatures';
import { connect } from 'react-redux';

class ExistingFeatures extends Component {
    state = {
        edit_mode: true,
        feature: {},
        cohort_title: '',
        orinal_value : null
    };

    componentDidMount() {
        this.setFeature();
    }

    componentDidUpdate(prevProps) {
        const { feature } = this.props;
        if (JSON.stringify(prevProps.feature) !== JSON.stringify(feature)) {
            this.setFeature();
        }
    }

    setFeature = () => {
        const { feature, cohort_title } = this.props;
        this.setState({ feature, cohort_title, orinal_value: feature.value ? JSON.parse(JSON.stringify(feature.value)) : null});
    };

    onChange = e => {
        const { feature } = this.state;
        feature['value'] = e.target.value;
        this.setState({ feature });
    };

    updateFeature = async () => {
        const { client_details, updateFeature } = this.props;
        const { feature } = this.state;
        let params = {};
        params.id = feature.id;
        params.value = feature.value;
        params.key = feature.key;
        this.setState({ loading: true });
        if (client_details.id) {
            const response = await updateFeature(client_details.id, params);
            if(response && response.status === 200)
            {
                this.setState({orinal_value : JSON.parse(JSON.stringify(feature.val))})
            }
        }
    };

    deleteFeature = async () => {
        const { client_details, deleteExistingFeature} = this.props;
        const { feature } = this.state;
        this.setState({ loading: true });
        if (client_details.id) {
            const response = await deleteExistingFeature(client_details.id, feature.id);
            if(response && response.status === 200)
            {
                this.props.callbackFn();
            }
        }
    };

    render() {
        const { feature, cohort_title, orinal_value} = this.state;
        console.log(feature);
        return (
            <div className="card card-margin columns">
                <div className="column is-3 align-center-center">
                    <span className = "text-centre">
                        <span className ="is-size-7"> Feature</span><br/><b>{feature.key ? feature.key : ''}</b>
                    </span>
                </div>
                <div className="column is-3 text-centre existing-feature-value">
                    <span className ="is-size-7">Value</span>
                    <TextInputgroup name="value"  value={feature.value} type="text" onChange={this.onChange} required={true} />
                </div>
                <div className="column is -4 align-center-left ">
                    <button className="button is-link is-outlined add-banner mg-5 " disabled = {orinal_value  === feature.value || feature.value === ''} onClick={this.updateFeature}>
                        Update
                    </button>
                    <button className="button is-danger  is-outlined add-banner mg-5" onClick={this.deleteFeature}>
                        Delete
                    </button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    client_details: state.client.client_details,
});

export default connect(mapStateToProps, { updateFeature, deleteExistingFeature })(ExistingFeatures);
