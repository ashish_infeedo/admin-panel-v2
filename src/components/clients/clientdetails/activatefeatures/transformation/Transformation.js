export const transformActiveFeaturesData = response => {
    let selected_cohort = {};
    let cohorts_array = [];
    let params_array = [];
    let selected_param = {};
    let original_params_array = [];
    let active_features = {};
    let cohort_map = {};

    if (response && response.cohort) {
        cohorts_array = response.cohort;
        response.cohort.forEach((cohort, index) => {
            if (cohort.id === -1) {
                selected_cohort = cohort;
            }
            cohort_map[cohort.id] = cohort.title;
        });
        if (Object.keys(selected_cohort).length === 0) {
            selected_cohort = cohorts_array[0];
        }
    }
    if (response && response.params && response.params.default) {
        response.params.default.forEach((param, index) => {
            let temp_object = {};
            temp_object.title = param.key;
            temp_object.id = param.id;
            params_array.push(temp_object);
            if (param.id === -1) {
                selected_param = param;
            }
        });
        if (Object.keys(selected_param).length === 0) {
            selected_param = response.params.default[0];
        }
        original_params_array = response.params.default;
        if (response.params && response.params.client) {
        let default_values = {};
        let total_values = response.params.client;
            if(total_values["-1"])
            {
                default_values[-1] = total_values["-1"]
                delete total_values["-1"]
            }
            active_features = default_values;
            active_features = {...active_features, ...total_values}

        }
    }
    return { selected_cohort, cohorts_array, params_array, selected_param, original_params_array, active_features, cohort_map };
};
