import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getAllFeatures, addFeature } from '../../../../actions/ActivateFeatures';
import SelectionDropdown from '../../../reusable/SelectionDropdown';
import { transformActiveFeaturesData } from './transformation/Transformation';
import TextInputgroup from '../../../generic/TextInputGroup';
import ExistingFeatures from './ExistingFeatures';

class ActivateFeatures extends Component {
    state = {
        cohorts_array: [],
        params_array: [],
        selected_cohort: {},
        selected_param: {},
        original_params_array: [],
        loading: true,
        variable_value: '',
        active_features: {},
        cohort_map: {},
    };

    componentDidMount() {
        this.getAllFeatures();
    }

    componentDidUpdate(prevProps) {
        const { client_details } = this.props;
        if (JSON.stringify(prevProps.client_details) !== JSON.stringify(client_details)) {
            this.getAllFeatures();
        }
    }

    // get all features and set them locally
    getAllFeatures = async () => {
        const { client_details, getAllFeatures } = this.props;
        if (client_details.id) {
            this.setState({ loading: true });
            const response = await getAllFeatures(client_details.id);
            if(response)
            {
                const transformed_data = await transformActiveFeaturesData(response);
                this.setState({
                    cohorts_array: transformed_data.cohorts_array,
                    params_array: transformed_data.params_array,
                    selected_cohort: transformed_data.selected_cohort,
                    selected_param: transformed_data.selected_param,
                    original_params_array: transformed_data.original_params_array,
                    active_features: transformed_data.active_features,
                    variable_value: transformed_data.selected_param.value,
                    cohort_map: transformed_data.cohort_map,
                });
            }
            this.setState({ loading: false });
        }
    };

    // when a cohort is selected
    selectCohort = option => {
        this.setState({ selected_cohort: option });
    };

    //when a param is selected
    selectParam = option => {
        const { original_params_array } = this.state;
        original_params_array.forEach((param, index) => {
            if (param.id === option.id) {
                this.setState({ selected_param: param, variable_value: param.value });
            }
        });
    };

    //when the value is changed
    variableChange = e => {
        this.setState({ variable_value: e.target.value });
    };

    //fn to add a feature
    addFeature = async () => {
        const { addFeature, client_details } = this.props;
        const { selected_param, selected_cohort, variable_value } = this.state;
        const param = {};
        param.id = selected_cohort.id;
        param.value = variable_value;
        param.key = selected_param.key;

        if (client_details.id && variable_value !== '') {
            const response = await addFeature(client_details.id, param);
            if (response && response.status === 200) {
                this.getAllFeatures();
            }
        }
    };

    render() {
        const { loading, cohorts_array, selected_cohort, params_array, selected_param, variable_value, active_features, cohort_map } =
            this.state;
        return (
            <div id = "activate-feature">
                {loading ? (
                    <p className="no-data has-text-centered">Loading Data...</p>
                ) : (
                    <div>
                        <div className="marginbottom-md">
                            <span className="is-size-4 marginbottom-xs"><b>Add Feature</b></span>
                            <br/>
                            <span> <strong>Note:</strong> For a certain feature first priority is given to a selected cohort, if the cohort isn't available for that feature than default cohort is selected.</span>
                        </div>
                        <div className="columns card padding-md card-margin">
                            <div className="column is-3">
                                <SelectionDropdown
                                    label={'Feature'}
                                    enable_search={true}
                                    values_array={params_array}
                                    value={selected_param.key}
                                    onOptionSelect={this.selectParam}
                                    min_width={'300'}
                                    height={'300'}
                                />
                            </div>
                            <div className="column is-3">
                                <div className="mg-bottom-10">
                                    <label className="label">Value</label>
                                </div>
                                <TextInputgroup
                                    name="variable_value"
                                    placeholder=""
                                    value={variable_value}
                                    type="text"
                                    onChange={this.variableChange}
                                    is_small={true}
                                    auto_focus={true}
                                    required={true}
                                />
                            </div>
                            <div className="column is-3">
                                <SelectionDropdown
                                    label={'Cohorts'}
                                    enable_search={true}
                                    values_array={cohorts_array}
                                    value={selected_cohort.title}
                                    onOptionSelect={this.selectCohort}
                                    min_width={'300'}
                                    height={'300'}
                                />
                            </div>
                            <div className="column is-3 align-center-left">
                                <button className="button is-success" disabled = {variable_value === ''} onClick={this.addFeature}>
                                    Save
                                </button>
                            </div>
                        </div>
                        <br/>
                        <hr />
                        {Object.keys(active_features).length ? (
                            <div>
                                <div className="marginbottom-md">
                                    <span className="is-size-4"><b>Existing Features</b></span>
                                </div>
                               
                                {Object.entries(active_features).map(([key, entries_array], index) =>
                                    entries_array.length 
                                        ?
                                        <div className = "marginbottom-lgr" > 
                                        <div className = "padding-left-5">
                                        <span><b>{cohort_map[key] ? cohort_map[key] : ''}</b></span>
                                    </div>
                                    { entries_array.map((data, index2) => (
                                              <ExistingFeatures
                                                  feature={data}
                                                  cohort_title={cohort_map[data.cohort_id] ? cohort_map[data.cohort_id] : ''}
                                                  callbackFn={this.getAllFeatures}
                                              />
                                          ))}
                                          {index < Object.entries(active_features).length -1 ? 
                                            <hr/> : null
                                          }
                                          
                                        </div>: null
                                )}
                            </div>
                        ) : null}
                    </div>
                )}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    client_details: state.client.client_details,
});

export default connect(mapStateToProps, { getAllFeatures, addFeature })(ActivateFeatures);
