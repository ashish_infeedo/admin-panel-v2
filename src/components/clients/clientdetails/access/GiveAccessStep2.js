import React, { Component } from 'react';
import classnames from 'classnames';
import Switch from "react-switch";
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { checkEqualObjects } from '../../../../helper/CheckEqualObjects';
// actons
import {getDepartments, getLocations, getCohorts, getSubDepartments, getBusiness, getFunctions, getManagers, getTeams, getHRBPs, getLteams, getAdminFeatures, checkUsersInIAM, giveAccess, makeAdmin, editAccess, setAdminFeatures, getCities, getCountry, getVertical, getEmployeeType, getSubFunction, getNationality, getRegions, getDivisions} from '../../../../actions/ClientsActions';
import { revokeSuperAdmin, revokeAdmin, getManagerEligibility, getDriverElements, setManagerAccess, getManagerAccess } from '../../../../actions/AccessActions';
// components
import SelectDropdown from './SelectDropdown';
import PreviewPermissions from './PreviewPermissions';

class GiveAccessStep2 extends Component {

    state = {
        selected_tab: 0, // 0 - full, 1 - restricted
        is_bat_enabled: false,
        is_bat_default_value: false,
        total_selected: 0,
        access_levels: {
            departments: {
                options: [],
                title: 'Departments',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                getMethod: this.props.getDepartments

            },
            sub_department: {
                options: [],
                title: 'Sub Departments',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                getMethod: this.props.getSubDepartments
            },
            locations: {
                options: [],
                title: 'Locations',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                getMethod: this.props.getLocations
            },
            cohorts: {
                options: [],
                title: 'Cohorts',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                getMethod: this.props.getCohorts
            },
            business: {
                options: [],
                title: 'Business',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                getMethod: this.props.getBusiness
            },
            functions: {
                options: [],
                title: 'Functions',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                getMethod: this.props.getFunctions
            },
            managers: {
                options: [],
                title: 'Managers',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                getMethod: this.props.getManagers
            },
            teams: {
                options: [],
                title: 'Teams',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                getMethod: this.props.getTeams
            },
            hrbp: {
                options: [],
                title: 'HRBPs',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                getMethod: this.props.getHRBPs
            },
            l_team: {
                options: [],
                title: 'L Teams',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                getMethod: this.props.getLteams
            },
            vertical: {
                options: [],
                title: 'Vertical',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                getMethod: this.props.getVertical
            },
            city:{
                options: [],
                title: 'City',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                getMethod: this.props.getCities
            },
            country:{
                options: [],
                title: 'Country',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                getMethod: this.props.getCountry
            },
            employee_type:{
                options: [],
                title: 'Employee Type',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                getMethod: this.props.getEmployeeType
            },
            sub_function:{
                options: [],
                title: 'Sub-Functions',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                getMethod: this.props.getSubFunction
            },
            nationality:{
                options: [],
                title: 'Nationality',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                getMethod: this.props.getNationality
            },
            region:{
                options: [],
                title: 'Regions',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                getMethod: this.props.getRegions
            },
            division:{
                options: [],
                title: 'Division',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                getMethod: this.props.getDivisions
            }

        },
        total_demographics_count: 18,
        features: {},
        no_users_in_iam: true,
        checking_iam: false,
        permissions: {},
        loading_count: 0,
        loading_features: false,
        eligibility: false,
        eligibility_msg: 'loading...',
        driver_elements: {},
        loading_driver_elements: false,
        expaned_driver: {},
        selected_driver_elements: {},
        driver_map:{},
        element_map:{},
        total_driver_element_selected_count: 0,
        initial_selected_sub_access: {},
        selected_sub_access: {}
    }

    componentDidMount() {
        const { is_edit, selected_member } = this.props;
        if(selected_member.manager_id === undefined){
            this.getAdminFeatures();
        }
        this.getAccessValues();
        this.getManagerEligibility();
        if(is_edit){
            this.getPermissionsFromAdmin();
        } else {
            this.changeTab(0);
        }
        if(is_edit && selected_member.manager_access_id){
            this.getManagerAccess();
        }
    }

    componentDidUpdate(prevProps){
        const {step_number} = this.props;
        if(step_number !== prevProps.step_number && step_number ===  2){
            this.setState({no_users_in_iam: true}, () => this.validateUsersInIAM());
        }
        if(step_number !== prevProps.step_number && step_number ===  3){
            this.giveAccess();
        }
    }

    getAccessValues = () => {
        const { access_levels } = this.state;
        const { client_id } = this.props;
        Object.entries(access_levels).forEach(
            async ([key, value]) => {
                const response = await value.getMethod(client_id);
                if(response){
                    access_levels[key].options = response;
                    access_levels[key].search_options = JSON.parse(JSON.stringify(response));
                    access_levels[key].data_loaded = true;
                }
                this.incrementLoadingCount();
                this.setState({access_levels});
            }
        );
    }

    incrementLoadingCount = () => {
        const { loading_count } = this.state;
        this.setState({loading_count: (loading_count + 1)});
    }

    getManagerEligibility = async () => {
        const { client_id, selected_member } = this.props;
        const response = await this.props.getManagerEligibility(client_id, selected_member.user_id);
        if(response){
            this.setState({eligibility: response.eligibility, eligibility_msg: response.message});
            if(response.eligibility){
                this.getDriverElements();
            }
        }
    }

    getManagerAccess = async () => {
        const { client_id, selected_member } = this.props;
        const response = await this.props.getManagerAccess(client_id, selected_member.manager_access_id);
        this.decodeManagerPayload(response.drivers_elements_ids);
    }

    getDriverElements = async () => {
        const { client_id } = this.props;
        this.setState({loading_driver_elements: true});
        const response = await this.props.getDriverElements(client_id);
        if(response){
            this.setState({driver_elements: response});
        }
        this.setState({loading_driver_elements: false});
    }

    getPermissionsFromAdmin = () => {
        const { selected_member } = this.props;
        if(selected_member.role ===  1){
            this.setState({selected_tab: 0});
        } else if(selected_member.manager_id){
            this.setState({selected_tab: 2});
        }else {
            this.setState({selected_tab: 1});
            Object.entries(selected_member.attributes).forEach(
                ([key, value]) => {
                    value.forEach((access) => {
                        this.selectOption(key, access, true);
                    });
                }
            )
        }
    }

    validateUsersInIAM = async () => {
        const { client_id } = this.props;
        const permissions = this.getSelectedPermissions();
        this.setState({checking_iam: true, permissions});
        const response = await this.props.checkUsersInIAM(client_id, {permissions})
        if(response === 0){
            this.setState({no_users_in_iam: true});
            this.props.disableNext(true);
        } else {
            this.setState({no_users_in_iam: false});
            this.props.disableNext(false);
        }
        this.setState({checking_iam: false});
    }

    getSelectedPermissions = () => {
        const {access_levels, selected_tab} = this.state;
        if(selected_tab === 0){
            return {};
        }
        const permissions = {}
        Object.entries(access_levels).forEach(
            ([key, value]) => {
                const arr = [];
                if(value.selected_count > 0){
                    Object.entries(value.selected_options).forEach(
                        ([key, value]) => {
                            if(value === true){
                                arr.push(key);
                            }
                        }
                    )
                }
                permissions[key] = arr;
            }
        )
        return permissions;
    }

    giveAccess = async () => {
        const { client_id, selected_member, is_edit, admin_type } = this.props;
        const { permissions, selected_tab, is_bat_enabled, features } = this.state;
        let id = null;
        let new_sub_admin_access_id = null;
        if(is_edit){
            id = (admin_type === 0 ? selected_member.user_id : selected_member.id)
        } else {
            id = selected_member.id
        }
        this.props.loadingNext(true);
        let response = null;
        if(selected_tab ===  2){
            const driver_elements = this.createManagerPayload();
            response = this.props.setManagerAccess(client_id, selected_member.user_id, driver_elements);
        } 
        else if(selected_tab  === 0 && !is_edit){
            response = await this.props.makeAdmin(id);
        } 
        else {
            if(is_edit){
                if(admin_type === 0 && admin_type !== selected_tab){
                    // revoke and then give
                    await this.props.revokeSuperAdmin(id);
                    response = await this.props.giveAccess(client_id, id, {permissions});
                } else if(admin_type === 1 && admin_type !== selected_tab){
                    await this.props.revokeAdmin(client_id, id);
                    response = await this.props.makeAdmin(selected_member.user_id);
                } else if( admin_type === 1 && admin_type === selected_tab){
                    response = await this.props.editAccess(client_id, id, {permissions, edit: true});
                }
            } else {
                response = await this.props.giveAccess(client_id, id, {permissions});
                new_sub_admin_access_id = response.access_id;
            }
        }
        if(selected_tab !== 2){
            response = await this.props.setAdminFeatures(client_id, selected_member.user_id, is_bat_enabled , (features.access_id ? features.access_id : new_sub_admin_access_id) );
        }
        this.props.loadingNext(false);
        if(response){
            this.props.getAdminsOrSubAdminsOrManagers();
            this.props.closeModal();
        }

    }

    getAdminFeatures = async () => {
        const { client_id, selected_member, is_edit } = this.props;
        let id = null;
        if(is_edit){
            id = selected_member.user_id;
        } else {
            id = selected_member.id
        }
        this.setState({loading_features: true});
        const response = await this.props.getAdminFeatures(client_id, id);
        if(is_edit){
            this.setState({is_bat_enabled: response.features.anonymous_bat, is_bat_default_value: response.features.anonymous_bat})
        }
        this.setState({features: response, loading_features: false});
    }


    changeTab = (selected_tab) => {
        const { is_edit, admin_type } = this.props;
        const { total_selected, is_bat_default_value } = this.state;
        let is_bat_enabled = false;
        if(!is_edit){
            if(selected_tab === 0){
                is_bat_enabled = true;
                this.props.disableNext(false);
            } 
            else if(selected_tab === 1){
                is_bat_enabled = false;
                if(total_selected <= 0){
                    this.props.disableNext(true);
                } else {
                    this.props.disableNext(false);
                }
            }
            else if(selected_tab === 2){
                this.props.disableNext(true);
            } 
        } else {
            if(selected_tab === 0){
                if(admin_type === 1){
                    is_bat_enabled = true;
                    this.props.disableNext(false);
                }
                if(admin_type === 0){
                    is_bat_enabled = is_bat_default_value;
                    this.props.disableNext(true);
                } 
            }
            // else if(selected_tab === 1){
            //     this.checkEnableNext();
            // }
            else if(selected_tab === 2){
                this.props.disableNext(true);
            } 
        }
        this.setState({selected_tab, is_bat_enabled});
    }

    toggleAnonymousBat = (checked) => {
        const { features, selected_tab } = this.state;
        const { is_edit } = this.props;
        if(is_edit){
            if(selected_tab === 0){
                if(features.anonymous_bat !== checked){
                    this.props.disableNext(false);
                } else {
                    this.props.disableNext(true);
                }
            }
        }
        this.setState({is_bat_enabled: checked}, () => this.checkRestrictedAccessEditEnable());
    }

    loadMore = (key) => {
        const { access_levels } = this.state;
        if(access_levels[key].show_only < access_levels[key].options.length){
            access_levels[key].show_only += 50;
            this.setState({access_levels});
        }
    }

    onSearch = (e) => {
        const {access_levels} = this.state;
        const key = e.target.name;
        access_levels[key].search_text = e.target.value;
        if(access_levels[key].search_text.length > 0) {
            const results = access_levels[key].options.filter(option => option.title.toLowerCase().includes(access_levels[key].search_text.toLowerCase()));
            access_levels[key].search_options = results;
        } else if(!access_levels[key].search_text.length) {
            access_levels[key].search_options = JSON.parse(JSON.stringify(access_levels[key].options))
        }
        access_levels[key].show_only = 50;
        this.setState({access_levels});
    }

    selectOption = (key, title, previous_selected = false) => {
        const {access_levels, total_selected, initial_selected_sub_access, selected_sub_access} = this.state;
        const state = (access_levels[key].selected_options[title] ? access_levels[key].selected_options[title] : false)
        // ----------------------------
        // to check enable disable next
        if(previous_selected){
            initial_selected_sub_access[`${key}:${title}`] = !state;
        }
        if(!state){
            selected_sub_access[`${key}:${title}`] = !state;
        } else {
            delete selected_sub_access[`${key}:${title}`];
        }
        // to check enable disable next end
        // --------------------------------
        access_levels[key].selected_options[title] = !state;
        if(state === false){
            access_levels[key].selected_count = (++access_levels[key].selected_count);
            this.setState({total_selected : (total_selected + 1)});
        } else if(state === true){
            access_levels[key].selected_count = (--access_levels[key].selected_count);
            this.setState({total_selected : (total_selected - 1)});
        }
        this.setState({access_levels, initial_selected_sub_access, selected_sub_access}, () => this.checkRestrictedAccessEditEnable());
    }

    checkRestrictedAccessEditEnable = () => {
        const { selected_tab ,initial_selected_sub_access, selected_sub_access, total_selected, is_bat_default_value, is_bat_enabled } = this.state;
        const { is_edit } = this.props;
        if(selected_tab !== 1){
            return false;
        }
        if(is_edit){
            if((!checkEqualObjects(initial_selected_sub_access, selected_sub_access)) || (checkEqualObjects(initial_selected_sub_access, selected_sub_access) && is_bat_default_value !== is_bat_enabled && total_selected > 0)){
                this.props.disableNext(false);
            } else {
                this.props.disableNext(true);
            }
        } else {
            if(total_selected > 0){
                this.props.disableNext(false);
            } else {
                this.props.disableNext(true);
            }
        }
    }
    
    // checkEnableNext = () => {
    //     const { total_selected } = this.state;
    //     const { is_edit } = this.props;
    //     // if(is_edit){
    //     //     // to do
    //     // } else {
    //         if(total_selected > 0){
    //             this.props.disableNext(false);
    //         } else {
    //             this.props.disableNext(true);
    //         }
    //     // }
    // }

    selectDriver = (drivers) => {
        const { selected_driver_elements } = this.state;
        let select_status = 0; // 0 - no effect, 1 - select_all,  2 - deselect all
        if(!selected_driver_elements[drivers[0].driver_id]){
            select_status = 0;
        } else {
            if(selected_driver_elements[drivers[0].driver_id].select_count === drivers.length){
                select_status = 2;
            } else {
                select_status = 1;
            }
        }
        drivers.forEach(element => {
            this.selectDeselectElement(element, select_status);
        });
    }

    selectDeselectElement = (element, select_status = 0) => {
        
        const { selected_driver_elements, driver_map, element_map } = this.state;

        let { total_driver_element_selected_count } = this.state;
        if(!selected_driver_elements[element.driver_id]){
            selected_driver_elements[element.driver_id] = {selected_elements: {}, select_count: 0};
            selected_driver_elements[element.driver_id].selected_elements[element.element_id] = true;
            selected_driver_elements[element.driver_id].select_count = selected_driver_elements[element.driver_id].select_count + 1;
            total_driver_element_selected_count = total_driver_element_selected_count + 1;
            driver_map[element.driver_id] = element.driver;
            element_map[element.element_id] = element.element;
        } else {
            if(select_status === 1 && !selected_driver_elements[element.driver_id].selected_elements[element.element_id]){
                selected_driver_elements[element.driver_id].selected_elements[element.element_id] = true;
                selected_driver_elements[element.driver_id].select_count = selected_driver_elements[element.driver_id].select_count + 1;
                total_driver_element_selected_count = total_driver_element_selected_count + 1;
                driver_map[element.driver_id] = element.driver;
                element_map[element.element_id] = element.element;
            } 
            else if(select_status === 2 && selected_driver_elements[element.driver_id].selected_elements[element.element_id]){
                selected_driver_elements[element.driver_id].selected_elements[element.element_id] = false;
                selected_driver_elements[element.driver_id].select_count = selected_driver_elements[element.driver_id].select_count - 1;
                total_driver_element_selected_count = total_driver_element_selected_count - 1;
            } else if(select_status === 0){
                if(selected_driver_elements[element.driver_id].selected_elements[element.element_id]){
                    selected_driver_elements[element.driver_id].selected_elements[element.element_id] = false;
                    selected_driver_elements[element.driver_id].select_count = selected_driver_elements[element.driver_id].select_count - 1;
                    total_driver_element_selected_count = total_driver_element_selected_count - 1;
                } else {
                    selected_driver_elements[element.driver_id].selected_elements[element.element_id] = true;
                    selected_driver_elements[element.driver_id].select_count = selected_driver_elements[element.driver_id].select_count + 1;
                    total_driver_element_selected_count = total_driver_element_selected_count + 1;
                    driver_map[element.driver_id] = element.driver;
                    element_map[element.element_id] = element.element;
                }
            }
        }
        this.setState({ selected_driver_elements, total_driver_element_selected_count }, () => this.checkEnableManagerNext());
    }

    expandCollapseDriver = (key) => {
        const { expaned_driver } = this.state;
        if(expaned_driver[key]){
            expaned_driver[key] = false;
        } else {
            expaned_driver[key] = true;
        }
        document.activeElement.blur();
        this.setState({expaned_driver});
    }

    checkEnableManagerNext = () => {
        const { total_driver_element_selected_count, eligibility } = this.state;
        if(total_driver_element_selected_count > 0 && eligibility){
            this.props.disableNext(false);
        } else{
            this.props.disableNext(true);
        }
    }

    createManagerPayload = () => {
        const { selected_driver_elements } = this.state;
        const driver_elements = [];
        Object.entries(selected_driver_elements).forEach(([key, value]) =>{
            let obj = {};
            let arr = [];
            obj.driver = key;
            Object.entries(value.selected_elements).forEach(([key, value]) =>{
                arr.push(key);
            })
            obj.elements =  arr;
            driver_elements.push(obj);
        })
        return driver_elements;
    }

    decodeManagerPayload = (payload) => {
        const { selected_driver_elements } = this.state;
        let { total_driver_element_selected_count } = this.state;
        const driver_elements = JSON.parse(payload);
        driver_elements.forEach(driver => {
            selected_driver_elements[driver.driver] = {selected_elements: {}, select_count: 0}
            driver.elements.forEach(element => {
                selected_driver_elements[driver.driver].selected_elements[element] = true;
                selected_driver_elements[driver.driver].select_count += 1;
                total_driver_element_selected_count += 1;
            })
        })
        this.setState({selected_driver_elements, total_driver_element_selected_count}, () => this.checkEnableManagerNext());
    }

    render() {
        const { selected_tab, is_bat_enabled, access_levels, no_users_in_iam, checking_iam, permissions, loading_count, loading_features, eligibility, eligibility_msg, driver_elements, loading_driver_elements, expaned_driver, selected_driver_elements, element_map, driver_map, total_demographics_count } = this.state;
        const {step_number, selected_member, is_edit} = this.props;
        return (
            <div>
                { step_number === 1 ? 
                <div>
                    <div className="tabs is-centered is-small is-toggle is-toggle-rounded mg-bottom-10">
                        <ul>
                            <li className={classnames('', {'is-active': selected_tab === 0})}>
                            <a onClick={this.changeTab.bind(this, 0)} className={classnames('', {'disable-anchor': is_edit && selected_tab ===  2})}>
                                <span className="icon is-small"><i className="fas fa-user-shield"></i></span>
                                <span>Full Access</span>
                            </a>
                            </li>
                            <li className={classnames('', {'is-active': selected_tab === 1})}>
                            <a onClick={this.changeTab.bind(this, 1)} className={classnames('', {'disable-anchor': is_edit && selected_tab ===  2})}>
                                <span className="icon is-small"><i className="fas fa-user-alt"></i></span>
                                <span>Restricted Access</span>
                            </a>
                            </li>
                            <li className={classnames('', {'is-active': selected_tab === 2})}>
                            <a onClick={this.changeTab.bind(this, 2)} className={classnames('', {'disable-anchor': is_edit && selected_tab !==  2})}>
                                <span className="icon is-small"><i className="fas fa-user-alt"></i></span>
                                <span>Manager Access</span>
                            </a>
                            </li>
                        </ul>
                    </div>
                    <p className="has-text-centered has-text-grey mg-bottom-12"><small>{selected_tab === 0 ? 'Recommended for CEO/CHRO' : (selected_tab === 1 ? 'Recommended for HR Leadership/HRBPs' : eligibility_msg)}</small></p>
                    {selected_tab === 1 ? <div className="card">
                        <header className="card-header">
                            <p className="card-header-title">
                            Access Level
                            </p>
                        </header>
                        <div className="card-content columns flex-wrap">
                            {loading_count === total_demographics_count ?
                            <div className="columns is-12 flex-wrap">
                            {Object.entries(access_levels).map(
                                ([key, value]) => (
                                    (value.options.length ? <div key={key} className="column is-6">
                                        <SelectDropdown access_data={value} access_key={key} onSearch={this.onSearch} loadMore={this.loadMore} selectOption={this.selectOption}/>
                                    </div> : null)
                                )
                            )}
                            </div>:
                            <p className="has-text-centered width-100"><span>Loading Data ...</span></p>}
                        </div>
                    </div> : null}
                    {selected_tab === 1 ? <br/> : null }
                    {selected_tab  !== 2 
                    ? <div className="card">
                        <header className="card-header">
                            <p className="card-header-title">
                            Features
                            </p>
                        </header>
                        {loading_features ? <p className="has-text-centered"><span><br/>Loading Data ...<br/><br/></span></p>
                            :<div className="card-content columns flex-wrap">
                            <div className="content column is-6 space-toggle">
                                <Switch onChange={this.toggleAnonymousBat.bind(this)} checked={is_bat_enabled}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={18} width={35}/>
                                <span className="is-size-7 toggle-text"><b>Anonymous Bat</b></span>
                            </div>
                            <div className="content column is-6 space-toggle">
                                <Switch onChange={()=>{}} checked={false} disabled={true}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={18} width={35}/>
                            <span className="is-size-7 toggle-text"><b>Manager Access</b><small> (coming soon)</small></span>
                            </div>
                            <div className="content column is-6 space-toggle">
                                <Switch onChange={()=>{}} checked={false} disabled={true}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={18} width={35}/>
                                <span className="is-size-7 toggle-text"><b>People to Meet</b><small> (coming soon)</small></span>
                            </div>
                            <div className="content column is-6 space-toggle">
                                <Switch onChange={()=>{}} checked={false} disabled={true}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={18} width={35}/>
                                <span className="is-size-7 toggle-text"><b>Chats</b><small> (coming soon)</small></span> 
                            </div>
                            <div className="content column is-6 space-toggle">
                                <Switch onChange={()=>{}} checked={false} disabled={true}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={18} width={35}/>
                                <span className="is-size-7 toggle-text"><b>PIIs</b><small> (coming soon)</small></span> 
                            </div>
                        </div>}
                    </div>
                    :(eligibility ? <div className="card">
                        <header className="card-header">
                            <p className="card-header-title">
                            Driver-Element List
                            {/* <button className="button is-pulled-right is-info is-inverted is-small">Select All</button> */}
                            </p>
                        </header>
                        <div className="card-content">
                        {loading_driver_elements ? <p className="has-text-centered width-100"><span>Loading Data ...</span></p> 
                        : <div className="columns flex-wrap">{Object.entries(driver_elements).map(
                                ([key, value]) => (
                                (value.length ? <div key={key} className="content column driver-ele-acc is-12">
                                    <label className="checkbox">
                                        <input 
                                        id={key}
                                        checked={selected_driver_elements[value[0].driver_id] ? selected_driver_elements[value[0].driver_id].select_count === value.length : false} 
                                        onChange={this.selectDriver.bind(this, value)} 
                                        type="checkbox"/>
                                        <span className="checkbox-title is-size-7"> {key} ({value.length})</span>
                                    </label>
                                    <button className="button is-info is-inverted is-small" onClick={this.expandCollapseDriver.bind(this, key)}>{expaned_driver[key] ? 'Collapse' : 'Expand'}</button>
                                    {expaned_driver[key] ? value.map((element, index) => (
                                        <div key={index} className="sub-drivers">
                                            <label className="checkbox">
                                                <input 
                                                id={index}
                                                checked={selected_driver_elements[element.driver_id] ? (selected_driver_elements[element.driver_id].selected_elements[element.element_id] ? selected_driver_elements[element.driver_id].selected_elements[element.element_id] : false) : false} 
                                                onChange={this.selectDeselectElement.bind(this, element, 0)} 
                                                type="checkbox"/>
                                                <span className="checkbox-title is-size-7"> {element.element}</span>
                                            </label>
                                        </div>
                                    ))
                                    : null}
                                </div> : null)
                        ))}</div>}
                        </div>
                    </div>: null)}
                    <br/>
                </div>
                :
                <div>
                    {no_users_in_iam ? <p className="has-text-centered pd-10">{checking_iam ? 'Checking users....': '0 Users in selected permissions. Go back and change the permissions'}</p> 
                    : <PreviewPermissions permissions={permissions} access_type={selected_tab} is_bat_enabled={is_bat_enabled} selected_member={selected_member} element_map={element_map} driver_map={driver_map} selected_driver_elements={selected_driver_elements}/> 
                    }
                </div>
                }
            </div>
        )
    }
}

// PropTypes
// ---------
GiveAccessStep2.propTypes = {
    getDepartments: PropTypes.func.isRequired,
    getBusiness: PropTypes.func.isRequired,
    getFunctions: PropTypes.func.isRequired,
    getManagers: PropTypes.func.isRequired,
    getTeams: PropTypes.func.isRequired,
    getHRBPs: PropTypes.func.isRequired,
    getLteams: PropTypes.func.isRequired,
    getSubDepartments: PropTypes.func.isRequired,
    getLocations: PropTypes.func.isRequired,
    getCohorts: PropTypes.func.isRequired,
    getAdminFeatures: PropTypes.func.isRequired,
    client_id: PropTypes.number.isRequired,
    selected_member: PropTypes.object.isRequired,
    step_number: PropTypes.number.isRequired,
    disableNext: PropTypes.func.isRequired,
    checkUsersInIAM: PropTypes.func.isRequired,
    giveAccess: PropTypes.func.isRequired,
    editAccess: PropTypes.func.isRequired,
    loadingNext: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
    getAdminsOrSubAdminsOrManagers: PropTypes.func.isRequired,
    makeAdmin: PropTypes.func.isRequired,
    revokeSuperAdmin: PropTypes.func.isRequired,
    admin_type: PropTypes.number.isRequired,
    revokeAdmin: PropTypes.func.isRequired,
    setAdminFeatures: PropTypes.func.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    
})
  
export default connect(mapStateToProps, {getDepartments, getLocations, getCohorts, getSubDepartments, getBusiness, getFunctions, getManagers, getTeams, getHRBPs, getLteams, getAdminFeatures, checkUsersInIAM, giveAccess, makeAdmin, editAccess, revokeSuperAdmin, revokeAdmin, setAdminFeatures, getManagerEligibility, getDriverElements, setManagerAccess, getManagerAccess, getCities, getCountry, getVertical, getEmployeeType, getSubFunction, getNationality, getRegions, getDivisions})(GiveAccessStep2);