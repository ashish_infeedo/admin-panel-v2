import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { withRouter } from 'react-router-dom';
import AddAdminContainer from './AddAdmin/AddAdminContainer';
import ViewAccessContainer from './AddAccessGroup/ViewAccessContainer';
import {getAccessGroups} from '../../../../actions/AccessActions';
import AdminTable from './AdminTable';


class Access extends Component {

  state = {
    selected_tab: 0,
    client_id: 0,
    access_groups: [],
    admin_creation_form_enabled : false,
    current_admin: {},
    is_edit: false
  }

  componentDidMount(){
    const { client_id } = this.props.match.params;
    this.setState({
      client_id: parseInt(client_id)
    }, () => {this.getAccessGroupsList(client_id)});
  }

  changeTab = (selected_tab) => {
    this.setState({
      selected_tab
    });
  }

  getAccessGroupsList = async() => {
    let response = await this.props.getAccessGroups(this.state.client_id);
    if (response &&  response.data && response.data.length) {
        this.setState({
          access_groups : response.data
        })
    }
  }

  toggleAddAdmin = _ => {
    const {admin_creation_form_enabled} = this.state;
    if(admin_creation_form_enabled){
      this.setState({
        current_admin: {},
        is_edit: false
      })
    }
      this.setState({
        admin_creation_form_enabled : !this.state.admin_creation_form_enabled
      })
  }

  setAdminForEdit = (current_admin) => {
    this.setState({
      current_admin,
      is_edit: true,
      admin_creation_form_enabled: true
    })
  }
 
  render() {
    const { selected_tab, access_groups, admin_creation_form_enabled, current_admin, is_edit } = this.state;
    const { client_id } = this.props.match.params;
    return (
      <div>
        <div className="tabs is-centered is-small is-toggle is-toggle-rounded mg-bottom-10">
            <ul>
                <li className={classnames('', {'is-active': selected_tab === 0})}>
                  <a onClick={this.changeTab.bind(this, 0)}>
                      <span className="icon is-small"><i className="fas fa-user-shield"></i></span>
                      <span>Admins</span>
                  </a>
                </li>
                <li className={classnames('', {'is-active': selected_tab === 1})}>
                  <a onClick={this.changeTab.bind(this, 1)}>
                      <span className="icon is-small"><i className="fas fa-user-alt"></i></span>
                      <span>Access Groups</span>
                  </a>
                </li>
            </ul>
        </div>
        {selected_tab === 0 ? 
          <div className="columns">
            <div className="column is-12">
              {
                admin_creation_form_enabled ?
                  <AddAdminContainer 
                      access_groups= {access_groups}
                      client_id={client_id}
                      toggleAddAdmin = {this.toggleAddAdmin}
                      current_admin={current_admin}
                      is_edit={is_edit}
                      getAccessGroupsList ={this.getAccessGroupsList}/> 
                :
                <AdminTable 
                  selected_tab={selected_tab} 
                  client_id={parseInt(client_id)}
                  toggleAddAdmin = {this.toggleAddAdmin}
                  setAdminForEdit={this.setAdminForEdit}/>
              }
            </div>
          </div> : null}
          {selected_tab === 1 ? 
          <div className="columns">
            <div className="column is-12">
                  <ViewAccessContainer 
                    access_groups= {access_groups}
                    client_id={client_id}
                    getAccessGroupsList = {this.getAccessGroupsList}/>
            </div>
          </div> : null}
      </div>
    )
  }
}

export default connect(null, {getAccessGroups})(withRouter(Access));