import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const GiveAccessSteps = ({
    step_number,
    is_edit,
    selected_member_email
}) => {
  return (
    <div className="steps">
        {!is_edit ? <div className={classnames('step-item', {'is-completed': step_number >= 0, 'is-success': step_number > 0})}>
            <div className="step-marker">
            { step_number > 0 ? <span className="icon">
                <i className="fa fa-check"></i>
            </span>: null }
            </div>
            <div className="step-details">
            <p className="step-title is-size-7">Step 1</p>
            <p className="is-size-7">{selected_member_email.length ? selected_member_email :'Add Email'}</p>
            </div>
        </div> : null}
        <div className={classnames('step-item', {'is-completed': step_number >= 1, 'is-success': step_number > 1})}>
            <div className="step-marker">
            { step_number > 1 ? <span className="icon">
                <i className="fa fa-check"></i>
            </span>: null }
            </div>
            <div className="step-details">
            <p className="step-title is-size-7">Step {is_edit ? '1' : '2'}</p>
            <p className="is-size-7">Choose Access Level</p>
            </div>
        </div>
        <div className={classnames('step-item', {'is-completed': step_number >= 2, 'is-success': step_number > 2})}>
            <div className="step-marker">
            { step_number > 2 ? <span className="icon">
                <i className="fa fa-check"></i>
            </span>: null }
            </div>
            <div className="step-details">
            <p className="step-title is-size-7">Step {is_edit ? '2' : '3'}</p>
            <p className="is-size-7">Confirm Access</p>
            </div>
        </div>
    </div>
  );
};

GiveAccessSteps.propTypes = {
  step_number: PropTypes.number.isRequired,
  is_edit: PropTypes.bool.isRequired,
  selected_member_email: PropTypes.string.isRequired
};

GiveAccessSteps.defaultProps = {
    step_number: 0,
    is_edit: false
};

export default GiveAccessSteps;