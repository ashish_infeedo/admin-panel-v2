import React, { Component } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { CSVLink } from 'react-csv';
import { disableBodyScroll, enableBodyScroll } from '../../../../helper/PreventScroll';
// actions
import {getAdmins, makePrimaryAdmin} from '../../../../actions/ClientsActions';
import {revokeAdmin, revokeSuperAdmin, sendResetPasswordEmail, deleteManagerAccess, deleteAdminAccess} from '../../../../actions/AccessActions';
// components
import ConfirmModal from '../../../../components/generic/ConfirmModal'; 
import AccessModal from './AccessModal';
import GiveAccessModal from './GiveAccessModal';
import TextInputgroup from '../../../generic/TextInputGroup';
import moment from 'moment';

class AdminTable extends Component {

    // initial state
    // -------------
    state = {
        loading_admins: false,
        all_admins: [],
        admin_modal: {
            title: 'Make Admin - ',
            text: 'Are you sure you want to change primary admin?',
            confirm_text: 'Yes',
            is_active: false
        },
        revoke_modal: {
            title: 'Revoke Admin - ',
            text: 'Are you sure you want to revoke access?',
            confirm_text: 'Yes',
            is_active: false
        },
        reset_modal: {
            title: 'Send Reset Email to - ',
            text: 'Are you sure you want to send reset password email?',
            confirm_text: 'Yes',
            is_active: false
        },
        access_modal_active: false,
        updating: false,
        revoking: false,
        resetting: false,
        selected_user: {},
        is_active_access: false,
        is_edit: false,
        exportData: '',
        search: '',
        delete_modal_active: false,
        current_admin_id: 0,
        deleting: false
    }

    csvLink = React.createRef()

    componentDidMount(){
        this.getAdmins(true);
    }

    // component updates
    // -----------------
    componentDidUpdate(prevProps){
        const { client_id, selected_tab } = this.props;
        if((client_id !== 0 && prevProps.client_id !== client_id) || prevProps.selected_tab !== selected_tab){
            this.getAdmins(true);
        }
    }

    // get admins
    // ----------------
    getAdmins = async (show_loading = true) => {
        const { client_id } = this.props;
        if(show_loading){
            this.setState({loading_admins: true});
        }
        const response = await this.props.getAdmins(client_id);
        this.setState({loading_admins: false});
        if(response){
            this.setState({all_admins: response, selected_admins_copy: JSON.parse(JSON.stringify(response))});
        }
    }

    revokeAdminAccess = async () => {
        const {client_id, selected_tab} = this.props;
        const {selected_user} = this.state;
        this.setState({revoking: true});
        const id = (selected_tab === 0 ? selected_user.user_id : selected_user.id)
        if(selected_tab === 0){
            await this.props.revokeSuperAdmin(id);
        } else if(selected_tab === 1){
            await this.props.revokeAdmin(client_id, id);
        } else if(selected_tab === 2){
            await this.props.deleteManagerAccess(client_id, selected_user.manager_access_id);
        }
        this.setState({revoking: false, selected_user: {}});
        this.closeRevokeModal();
        this.getAdmins(false)
    }

    makePrimaryAdmin = async () => {
        const { selected_user } = this.state;
        const { client_id } = this.props;
        this.setState({updating: true});
        await this.props.makePrimaryAdmin(client_id, selected_user.user_id);
        this.closeAdminModal();
        this.getAdmins(false);
        this.setState({updating: false, selected_user: {}});
    }

    sendResetPasswordEmail = async () => {
        const { selected_user } = this.state;
        this.setState({resetting: true});
        await this.props.sendResetPasswordEmail(selected_user.user_id, selected_user.name);
        this.setState({resetting: false, selected_user: {}});
        this.closeResetModal();
    }

    openRevokeModal = (user) => {
        const { revoke_modal } = this.state;
        disableBodyScroll();
        revoke_modal.title += ` ${user.name}`;
        revoke_modal.is_active = true;
        this.setState({revoke_modal: revoke_modal, selected_user: user});
    }

    closeRevokeModal = () => {
        const { revoke_modal } = this.state;
        enableBodyScroll();
        revoke_modal.is_active = false;
        revoke_modal.title = 'Revoke Admin - ';
        this.setState({revoke_modal: revoke_modal, selected_user: {}});
    }

    openAdminModal = (user) => {
        const { admin_modal } = this.state;
        disableBodyScroll();
        admin_modal.title += ` ${user.name}`;
        admin_modal.is_active = true;
        disableBodyScroll();
        this.setState({admin_modal: admin_modal, selected_user: user});
    }

    closeAdminModal = () => {
        const { admin_modal } = this.state;
        enableBodyScroll();
        admin_modal.is_active = false;
        admin_modal.title = 'Make Admin - ';
        enableBodyScroll();
        this.setState({admin_modal: admin_modal, selected_user: {}});
    }

    openResetModal = (user) => {
        const { reset_modal } = this.state;
        reset_modal.title += ` ${user.name}`;
        reset_modal.is_active = true;
        disableBodyScroll();
        this.setState({reset_modal: reset_modal, selected_user: user});
    }

    closeResetModal = () => {
        const { reset_modal } = this.state;
        reset_modal.is_active = false;
        reset_modal.title = 'Send Reset Email to Admin - ';
        enableBodyScroll();
        this.setState({reset_modal: reset_modal, selected_user: {}});
    }

    openDeleteModal = (current_admin_id) => {
        this.setState({
            current_admin_id,
            delete_modal_active : true
        });
    }

    closeDeleteModal = () => {
        this.setState({
            current_admin_id: 0,
            delete_modal_active : false
        });
    }

    openAccessModal = (user) => {
        disableBodyScroll();
        this.setState({access_modal_active: true, selected_user: user});
    }

    closeAccessModal = () => {
        enableBodyScroll();
        this.setState({access_modal_active: false, selected_user: {}});
    }

    openGiveAccessModal = (selected_user) => {
        disableBodyScroll();
        if(selected_user){
            this.setState({selected_user})
        }
        this.setState({is_active_access: true});
    }

    closeGiveAccessModal = () => {
        enableBodyScroll();
        this.setState({is_active_access: false, is_edit: false, selected_user: {}});
    }

    editPermissions = (selected_user) => {
        this.setState({selected_user, is_edit: true}, () => this.openGiveAccessModal());
    }

    onSearch = (e) => {
        this.setState({ [e.target.name]: e.target.value });
        const search = e.target.value;
        const { selected_admins_copy } = this.state;
        const search_data= JSON.parse(JSON.stringify(selected_admins_copy));
        if(search.length > 0) {
            const results = search_data.filter(admin => (admin.user_name.toLowerCase().includes(search.toLowerCase()) || admin.email.toLowerCase().includes(search.toLowerCase())));
            this.setState({all_admins: results});
        } else if(!search.length) {
            this.setState({all_admins: selected_admins_copy});
        }
      }

    downloadCSV = () => {
        const { all_admins } = this.state;
        all_admins.forEach(admin => {
            delete admin.attributes;
            delete admin.features;
            delete admin.disable_stages;
            delete admin.client_status;
            delete admin.reminder_enabled;
            delete admin.enable_anonymous_bat;
            delete admin.primary_admin;
            delete admin.drivers_elements_ids;
            delete admin.drivers_elements;
            delete admin.subordinates;
            if(admin.created){
                admin.created =  moment.unix(admin.created).format('DD MMMM YYYY');
            }
            if(admin.login_date){
                admin.login_date =  moment.unix(admin.login_date).format('DD MMMM YYYY');
            }
            if(admin.roles && admin.roles.length){
                admin.access_group = admin.roles[0].name;
            }
            delete admin.roles;
        })
        this.setState({
          exportData : all_admins
        }, () => {
          this.csvLink.current.link.click()
        })
    }

    toggleAddAdmin = _ => {
        this.props.toggleAddAdmin()
    }

    deleteAdmin = async () => {
        const {client_id} = this.props;
        const {current_admin_id} = this.state;
        const response = await this.props.deleteAdminAccess(current_admin_id, client_id);
        if(response && response.status === 200){
            this.getAdmins();
        }
        this.closeDeleteModal();
    }

    editAdmin = (current_admin) => {
        this.props.setAdminForEdit(current_admin);
    }

    // render UI
    // ---------
    render() {
        const { all_admins, loading_admins, admin_modal, revoke_modal, reset_modal, updating, revoking, resetting, access_modal_active, selected_user, is_active_access, is_edit, exportData, search, delete_modal_active, deleting } = this.state;
        const { selected_tab, client_id } = this.props;
        return (
            <div>
                <button onClick= {this.downloadCSV} className="button is-small" disabled={loading_admins || (all_admins.length === 0 && !loading_admins)}>
                    <span className="icon">
                        <i className="fas fa-download"></i>
                    </span>
                    <span>Download CSV</span>
                </button>
                <CSVLink filename={selected_tab === 0 ? 'admin_data.csv' : (selected_tab === 1 ? 'sub_admin_data.csv' : 'manager_data.csv')} data={exportData} ref={this.csvLink}></CSVLink>
                <button onClick={this.toggleAddAdmin} className="button is-success is-small is-pulled-right">
                    <span className="icon">
                        <i className="fas fa-plus"></i>
                    </span>
                    <span>Add Admin</span>
                </button>
                <br/><br/>
                <TextInputgroup
                    name="search"
                    placeholder="Search with name or email"
                    value={search}
                    type="text"
                    onChange={this.onSearch}
                    icon="fas fa-search"
                />
                <br/>
                <div className="card">
                    <div className="card-content is-paddingless">
                        <div className="content">
                            <div className="custom-table">
                                <table className="table is-striped is-fullwidth">
                                    <thead>
                                        <tr>
                                        <th>S.No.</th>
                                        <th>Name</th>
                                        <th>Access Group</th>
                                        <th>Access</th>
                                        {selected_tab === 0 ? <th>Type</th> : null}
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {!loading_admins && all_admins.length ? all_admins.sort((a, b) => b.created - a.created).map((admin, index) => (
                                    <tr key={index}>
                                    <td>{index+1}</td>
                                    <td><b>{admin.user_name}</b> <br/> <span className="is-size-7">{admin.email}</span></td>
                                    <td>{admin.roles && admin.roles.length ? admin.roles[0].name : 'NA'}</td>
                                    <td>
                                        <span className="cursor-pointer text-underline has-text-info is-size-7" onClick={this.editAdmin.bind(this, admin)}>
                                            Edit
                                        </span>
                                    </td>
                                    <td>
                                        <span className="cursor-pointer text-underline has-text-info is-size-7" onClick={this.openDeleteModal.bind(this, admin.id)}>
                                            Delete
                                        </span>
                                    </td>
                                    </tr>
                                    )): null}
                                    </tbody>
                                </table>
                                {(all_admins.length === 0 && !loading_admins) ? <p className="no-data has-text-centered">No Data</p> : null}
                                {loading_admins ? <p className="no-data has-text-centered">Loading Data...</p> : null}
                            </div>
                        </div>
                    </div>
                    <ConfirmModal 
                        title={admin_modal.title}
                        text={admin_modal.text}
                        confirm_text={admin_modal.confirm_text}
                        onConfirm={this.makePrimaryAdmin}
                        onClose={this.closeAdminModal}
                        is_active={admin_modal.is_active}
                        doing={updating}
                    />
                    <ConfirmModal 
                        title={revoke_modal.title}
                        text={revoke_modal.text}
                        confirm_text={revoke_modal.confirm_text}
                        onConfirm={this.revokeAdminAccess}
                        onClose={this.closeRevokeModal}
                        is_active={revoke_modal.is_active}
                        doing={revoking}
                    />
                    <ConfirmModal 
                        title={reset_modal.title}
                        text={reset_modal.text}
                        confirm_text={reset_modal.confirm_text}
                        onConfirm={this.sendResetPasswordEmail}
                        onClose={this.closeResetModal}
                        is_active={reset_modal.is_active}
                        doing={resetting}
                    />
                    <AccessModal
                        title={selected_user.name ? selected_user.name : ''}
                        onClose={this.closeAccessModal}
                        access={selected_user ? selected_user : {}}
                        is_active={access_modal_active}
                    />
                    <GiveAccessModal 
                        onClose={this.closeGiveAccessModal} 
                        is_active={is_active_access} 
                        client_id={client_id}
                        admin={selected_user ? selected_user : {}}
                        admin_type={selected_tab} //0 - admin 1 - subadmin
                        getAdminsOrSubAdminsOrManagers={this.getAdmins}
                        is_edit={is_edit}
                    />
                    {delete_modal_active ?
                        <ConfirmModal
                            text="Are you sure you want to delete?"
                            title="Delete Admin"
                            confirm_text="Yes"
                            doing={deleting}
                            is_active={delete_modal_active}
                            onConfirm={this.deleteAdmin}
                            onClose={this.closeDeleteModal}/> : null}
                </div>
            </div>
        )
    }
}

// PropTypes
// ---------
AdminTable.propTypes = {
    selected_tab: PropTypes.number.isRequired,
    getAdmins: PropTypes.func.isRequired,
    revokeSuperAdmin: PropTypes.func.isRequired,
    sendResetPasswordEmail: PropTypes.func.isRequired,
    revokeAdmin: PropTypes.func.isRequired,
    makePrimaryAdmin: PropTypes.func.isRequired,
    client_id: PropTypes.number.isRequired,
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    
})
  
export default connect(mapStateToProps, {getAdmins, makePrimaryAdmin, revokeAdmin, revokeSuperAdmin, sendResetPasswordEmail, deleteManagerAccess, deleteAdminAccess})(AdminTable);
