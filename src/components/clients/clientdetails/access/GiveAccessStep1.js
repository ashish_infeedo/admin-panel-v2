import React, { Component } from 'react';
import Autosuggest from 'react-autosuggest';
import PropTypes from 'prop-types';


export default class GiveAccessStep1 extends Component {

    state = {
        members :[],
        value: '',
        selected_value: '',
        suggestions: []
    }

    componentDidMount(){
        const { members } = this.props;
        this.props.selectMember({email: ''});
        if(members.length !== 0){
            this.setState({members});
        }
    }

    componentDidUpdate(prevProps){
        const { members } = this.props;
        if(members.length !== prevProps.members.length){
            this.setState({members});
        }
    }

    onChange = (event, { newValue }) => {
        this.setState({
            value: newValue
        }, () => this.enableDisableNext());
    };

    enableDisableNext = () => {
        const { value, selected_value} = this.state;
        if(value === selected_value && selected_value !== ''){
            this.props.disableNext(false);
        } else {
            this.props.disableNext(true);
        }
    }

    onSuggestionSelected = (e, { suggestion, suggestionValue }) => {
        this.props.selectMember(suggestion);
        this.setState({selected_value: suggestionValue}, () => this.enableDisableNext());
    }

    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: this.getSuggestions(value)
        });
    };
    
    // Autosuggest will call this function every time you need to clear suggestions.
    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    getSuggestions = value => {
        const {members} = this.state;
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;
        return inputLength === 0 ? [] : members.filter(member => member.email.toLowerCase().includes(inputValue.toLowerCase()));
        // members.filter(lang => lang.name.toLowerCase().slice(0, inputLength) === inputValue);
    };
      

    render() {
        const {suggestions, value, selected_value} = this.state;
        const inputProps = {
            placeholder: 'Enter Email to Invite',
            value,
            onChange: this.onChange
          };
        return (
            <div>
                <div className="autosuggest-access">
                {value === selected_value && selected_value !== '' ? <p className="has-text-right is-size-7 mg-10 no-margin-right">Selecetd (1)</p> : <p className="has-text-right is-size-7 mg-10 no-margin-right">Total Results: ({suggestions.length})</p>}
                <Autosuggest
                    suggestions={suggestions.slice(0, (suggestions.length < 300 ? suggestions.length : 300))}
                    onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                    onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                    getSuggestionValue={suggestion => suggestion.email}
                    onSuggestionSelected={this.onSuggestionSelected}
                    renderSuggestion={
                        suggestion => (
                            <div>
                            {suggestion.email}
                            </div>
                        )
                    }
                    inputProps={inputProps}
                />
                </div>
            </div>
        )
    }
}


GiveAccessStep1.propTypes = {
    members: PropTypes.array.isRequired,
    selectMember: PropTypes.func.isRequired,
    disableNext: PropTypes.func.isRequired
};
  