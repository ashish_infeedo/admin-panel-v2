import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import moment from 'moment';

const AccessModal = ({
  title,
  onClose,
  access,
  is_active,
}) => {
  return (
    <div className={classnames('modal',{'is-active': is_active})}>
        <div className="modal-background"></div>
        <div className="modal-card">
            <header className="modal-card-head">
            <p className="modal-card-title is-size-6 has-text-weight-bold">{title}</p>
            <button onClick={onClose} className="delete" aria-label="close"></button>
            </header>
            <section className="modal-card-body radius-access">
            <h3 className="has-text-weight-bold is-capitalized mg-10">Information</h3>
            <hr className="is-marginless"/>
            <div className="columns">
                <div className="column is-6">
                    <h3 className="has-text-weight-bold is-capitalized margin-subhead is-size-7">Adden on</h3>
                    <p className="access-p">{access.created ? moment.unix(access.created).format('DD MMMM YYYY') : 'Not Available'}</p>
                </div>
                <div className="column is-6">
                    <h3 className="has-text-weight-bold is-capitalized margin-subhead is-size-7">Last Login</h3>
                    <p className="access-p">{access.login_date ? moment.unix(access.login_date).format('DD MMMM YYYY') : 'Not Available'}</p>
                </div>
            </div>
            <hr className="is-marginless"/>
            <h3 className="has-text-weight-bold is-capitalized mg-10">Access Types</h3>
            <hr className="is-marginless"/>
            {access.manager_id === undefined ? null : <br/>}
            {access.attributes ? Object.entries(access.attributes).map(([key, value], index) => (
                (value.length ? 
                <div key={key}>
                    <h3 className="has-text-weight-bold is-capitalized margin-subhead is-size-7">{key}</h3>
                        <div className="tags is-marginless mg-10">
                            {value.map((access, index) => (
                                <span key={index} className="tag is-warning is-rounded mg-tag-access">{access}</span>
                            ))}
                        </div>
                </div>: null)
            )): (access.manager_id === undefined ? <div className="tags is-marginless mg-10">
                    <span className="tag is-primary is-rounded mg-10">Full Access</span>
                </div> : null)
            }
            {access.drivers_elements ?
            Object.entries(access.drivers_elements).map(([key, value], index) => ( 
                <div key={index}>
                    <h3 className="has-text-weight-bold is-capitalized mg-10 manager-heading">Driver: {key}</h3>
                    <div className="tags is-marginless mg-10">
                    <h3 className="has-text-weight-bold is-capitalized margin-subhead is-size-7">Elements:</h3>
                    {Object.entries(value).map(([key, value], index) => ( 
                        <span key={index} className="tag is-warning is-rounded mg-tag-access">{value.element}</span>
                    ))}
                     </div>
                </div> 
            ))
            : null}
            <br/>
            {access.manager_id === undefined ? <div>
            <hr className="is-marginless"/>
            <h3 className="has-text-weight-bold is-capitalized mg-10">Features</h3>
            <hr className="is-marginless"/>
            <div className="columns flex-wrap">
                <div className="column is-6 no-padding-bottom">
                    <h3 className="has-text-weight-bold is-capitalized margin-subhead is-size-7">Anonymous Bat</h3>
                    <p className="access-p">{access.features ? (access.features.anonymous_bat === true ? <span className="has-text-success">Enabled</span> : <span className="has-text-danger">Disbaled</span> ) : 'Not Available'}</p>
                </div>
                {/* <div className="column is-6 no-padding-bottom">
                    <h3 className="has-text-weight-bold is-capitalized margin-subhead is-size-7">Manager Access</h3>
                    <p className="access-p"><i>coming soon</i></p>
                </div>
                <div className="column is-6 no-padding-bottom">
                    <h3 className="has-text-weight-bold is-capitalized margin-subhead is-size-7">People to Meet</h3>
                    <p className="access-p"><i>coming soon</i></p>
                </div>
                <div className="column is-6 no-padding-bottom">
                    <h3 className="has-text-weight-bold is-capitalized margin-subhead is-size-7">Chats</h3>
                    <p className="access-p"><i>coming soon</i></p>
                </div>
                <div className="column is-6 no-padding-bottom">
                    <h3 className="has-text-weight-bold is-capitalized margin-subhead is-size-7">PIIs</h3>
                    <p className="access-p"><i>coming soon</i></p>
                </div> */}
            </div>
            </div>: null}
            <br/>
            </section>
        </div>
    </div>
  );
};

AccessModal.propTypes = {
  title: PropTypes.string.isRequired,
  access: PropTypes.object.isRequired,
  is_active: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired
};

AccessModal.defaultProps = {
    is_active: false,
    doing: false
};

export default AccessModal;