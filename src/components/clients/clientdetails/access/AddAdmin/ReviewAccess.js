import React, { Component } from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
import {saveAdminAccess, editAdminAccess} from '../../../../../actions/AccessActions';
import { NotificationManager } from 'react-notifications';
// actions
import {getAdmins} from '../../../../../actions/ClientsActions';

class ReviewAccess extends Component {
    state = {
        selected_access: [],
        current_access_group: {},
        features_list : {
            'PEOPLE_TO_MEET' : 'People to Meet (PTM)',
            'EXIT' : 'Exit Module',
            'CRISIS_MANAGEMENT' : 'Crisis Management',
            'DASHBOARD_SETUP' : 'Setup Page',
            'EMPLOYEE_CHATS' : 'Employee Chats',
            'UNIVERSAL_FILTERS' : 'Dashboard Filters',
            'PII' : "PII'S",
            'EXPORTS' : "Data Exports",
            'RESPONSE_RATE' : 'Response Rate',
            'ADMINS_CHAT_ACCESS' : 'Admin Chat Access',
            'ANONYMOUS_BAT' : 'Anonymous Messages',
            'MOMENTS_THAT_MATTER': 'Moments that Matter',
            'CONFIGURE_REPORT': 'Configure Report',
            'PULSE': 'Custom Check-In',
            'TEXT_SENS' : 'TextSens',
            'DRIVER_ELEMENT' : 'Drivers & Elements',
            'ENGAGEMENT_SCORE' : 'Engagement Score'
        },
        save_admin_in_progress: false
    }

    componentDidMount(){
        this.checkForAccessGroups();
    }

    componentDidUpdate(prevProps){
        const {selected_access_groups, demographic_access_data} = this.props;
        if(JSON.stringify(prevProps.selected_access_groups) !== JSON.stringify(selected_access_groups) || (JSON.stringify(prevProps.demographic_access_data) !== JSON.stringify(demographic_access_data))){
            this.checkForAccessGroups();
        }
        
    }

    checkForAccessGroups = () => {
        let {selected_access, current_access_group} = this.state; 
        const {selected_access_groups, access_groups} = this.props;
        let flag = false;
        let count = 1;
        access_groups.forEach(per => {
            if(selected_access_groups[parseInt(per.id)]){
                selected_access.push(per);
                flag = true;
                if(count === 1){
                    current_access_group = per
                    count++;
                }
            }
        });
        if(flag){
            this.setState({
                selected_access: JSON.parse(JSON.stringify(selected_access)),
                current_access_group
            })
        }
    }

    giveDemographicAccess = async () => {
        const {access_file_url, access_emails, demographic_access_data, is_edit, current_admin, client_id} = this.props;
        const {selected_access} = this.state;
        let emails = access_emails;
        let access_groups = [];
        this.setState({
            save_admin_in_progress: true
        })
        let default_view = 0;
        let set_default_view = 0;
        selected_access.forEach((access) => {
            let access_data = {};
            access_data.id = access.id;
            access_data.demographic_toggle = access.demographic_toggle;
            if(access.demographic_toggle){
                access_data.view_type = 3;
                default_view = 1;
            }
            else if(access.demographic_toggle === 0 && access.permissions.hierarchy_wise){
                access_data.view_type  = 4;
                default_view = 1;
            }
            else if(access.permissions && access.permissions.direct_level){
                access_data.view_type  = 1;
                if(!(access.permissions && access.permissions.skip_level)){
                    default_view = 1;
                }
            } 
            else if(access.permissions && access.permissions.skip_level){
                    access_data.view_type  = 2;
                    if(!(access.permissions && access.permissions.direct_level)){
                        default_view = 1;
                    }
            }
            else{

            }

            if(selected_access.length < 2 && default_view){
                set_default_view = 1;
            }
            access_data.total_views = access.total_views;
            if(access.demographic_toggle){
                access_data.demographic_access = JSON.parse(JSON.stringify(demographic_access_data));
            }
            else{
                access_data.demographic_access = [];
            }
            access_groups.push(access_data);
        })
        let response = {}
        if(is_edit){
            response = await this.props.editAdminAccess(current_admin.id, access_groups, set_default_view, client_id);
        }
        else {
            response = await this.props.saveAdminAccess(access_file_url, emails, access_groups, set_default_view, client_id);
        }

        if(response && response.status === 200){
            await this.props.getAdmins(client_id);
            if(is_edit){
                NotificationManager.success(`Saved successfully. Changes in access will only be reflected once you refresh.`);
            }
            else{
                NotificationManager.success(`Saved successfully.`);
            }
        }
        this.setState({
            save_admin_in_progress: false
        }, () => {
            this.props.toggleAddAdmin();
        })
    }

    changeTab = (current_access_group) => {
        this.setState({
            current_access_group
        })
    }
    render() {
        const {selected_access, current_access_group, features_list, save_admin_in_progress} = this.state;
        const {previous_step} = this.props;
        return (
            <div className="card full-height box-blue-border mg-top-15">
                <div className="card-content">
                    <div className="columns flex-wrap">
                        <div className="column is-12">
                            { selected_access.length > 0 ?
                                <div className="tabs demographics-list is-small is-toggle is-toggle-rounded">
                                    <ul className="const-tab-height-attrition">
                                    { selected_access.map((access) => (
                                        <li key={access.id} className={classnames({'is-active': current_access_group.id === access.id })} onClick={this.changeTab.bind(this, access)}><a>{access.name}</a></li>
                                    ))}
                                    </ul>
                                </div> : null}
                        </div>
                        <p className="has-text-grey-lighter is-size-7 is-uppercase padding-left-10 mg-top-30">Features:</p>
                        <div className="columns flex-wrap column is-12 mg-bottom-20">
                                { current_access_group && current_access_group.permissions ?
                                    Object.keys(current_access_group.permissions).map((per) => (
                                        Object.keys(current_access_group.permissions[per]).map((per_access) => (
                                            features_list[per_access] ?
                                                <div className="is-6 column columns no-paddingbottom no-marginbottom">
                                                    <h6 className="column no-paddingbottom is-8">{features_list[per_access]}</h6>
                                                    <span className={classnames('column is-4', {'has-text-success' :current_access_group.permissions[per][per_access], 'has-text-danger': !current_access_group.permissions[per][per_access] })}>{current_access_group.permissions[per][per_access] ? "Enabled" : "Disabled"}</span>
                                                </div> : null
                                        ))
                                    )) : null}
                            </div>
                        <div className="column is-12 no-padding-top no-padding-bottom">
                            <div className="">
                                <button id="ag-save-demographs" className={classnames('is-small button is-outlined padding-left-right-20')} onClick={this.props.updateStep.bind(this,previous_step)}>Previous</button>
                                <button id="ag-next-demographs" onClick={this.giveDemographicAccess} className={classnames('button is-small is-primary mg-left-10 padding-left-right-20', {'is-loading': save_admin_in_progress})}>Give Access</button>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    admin_list: state.access.admin_list,
    access_groups: state.access.access_groups,
    demographic_access_data: state.access.demographic_access_data,
    access_file_url: state.access.access_file_url,
    access_emails: state.access.access_emails
  })

  export default connect(mapStateToProps, {saveAdminAccess, editAdminAccess, getAdmins})(ReviewAccess)