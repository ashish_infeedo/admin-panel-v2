import React from 'react'
import classnames from 'classnames';

export default function SelectAccessGroup({
    access_groups,
    selected_groups,
    selectAccessGroup,
    updateStep,
    nextStep,
    selected_access_groups_id
}) {
    return (
        <div className="access-section section no-padding margintop-md marginbottom-md">
                        {/* <p className="has-text-weight-semibold is-5 marginbottom-sm">
                            <span className="serial-check checked">
                                <i className={classnames("fas fa-check")}></i>
                            </span>
                            Select Access Groups <a className="pointer is-link is-size-7">View</a>
                        </p> */}
                        <div className="card">
                            <div className="card-content">
                                <p className="has-text-weight-semibold is-6 level">
                                    {access_groups.length} Access Groups 
                                </p>
                                <hr className="margintop-sm marginbottom-sm"/>
                                <div className="table-container">
                                    <table className="table is-striped is-fullwidth is-size-7">
                                        <thead>
                                            <tr>
                                                <th>
                                                </th>
                                                <th>
                                                    Access Group
                                                </th>
                                                <th>
                                                    Features Enabled
                                                </th>
                                                <th>
                                                    Hierarchy
                                                </th>
                                                {/* <th></th>
                                                <th></th> */}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {access_groups.map((per, index) => (
                                                <tr key={index}>
                                                    <td width="10%">
                                                    <input
                                                        checked={(selected_groups[`${per.id}`] ? true : false)}
                                                        type="checkbox"
                                                        id="selectAllPeopleId"
                                                        className=""
                                                        onChange={selectAccessGroup.bind(this,per.id)}
                                                    />
                                                    </td>
                                                    <td width="10%">
                                                        {per.name}
                                                    </td>
                                                    <td width="50%">
                                                        {Object.keys(per.permissions[Object.keys(per.permissions)[0]]).map((feat, index) => (
                                                                per.permissions[Object.keys(per.permissions)[0]][feat] ? <span key={index} className="tag is-light marginbottom-xs marginright-sm">{feat.replace(/_/g, " ").replace('PULSE', 'CUSTOM CHECK-IN')}</span> : ''
                                                            ))}
                                                    </td>
                                                    <td width="20%">
                                                        {!per.demographic_toggle && (per.permissions && (per.permissions.direct_level || per.permissions.skip_level))?
                                                            per.permissions && per.permissions.direct_level && per.permissions.skip_level ?
                                                                <span className="">
                                                                    Hierarchy (Direct & Skip)
                                                                </span> : 
                                                                    per.permissions && per.permissions.direct_level ?
                                                                        <span className="">
                                                                            Hierarchy (Only Direct)
                                                                        </span> :
                                                                        <span className="">
                                                                            Hierarchy (Only Skip)
                                                                        </span>
                                                            :  per.permissions && per.permissions.hierarchy_wise ?
                                                                <span className="">
                                                                    Hierarchy
                                                                </span> :
                                                                <span className="">
                                                                    Demographic
                                                                </span>}
                                                    </td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </div>
                                <div className="buttons">
                                    <button onClick={updateStep.bind(this, nextStep)} className="is-primary button is-small paddingleft-md paddingright-md" disabled={selected_access_groups_id.length > 0 ? false: true}>
                                        Next
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
    )
}
