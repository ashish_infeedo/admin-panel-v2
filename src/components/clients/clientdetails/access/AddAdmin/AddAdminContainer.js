import React, { Component } from 'react'
import classnames from 'classnames'
import { connect } from 'react-redux';
import SelectAccessGroup from './SelectAccessGroup'
import AddEmails from './AddEmails'
import ReviewAccess from './ReviewAccess';
import CustomizeDemographicAccess from './CustomizeDemographicAccess';
import { NotificationManager } from 'react-notifications';
import AdvancedCustomizeDemographicAccess from './AdvancedCustomizeDemographicAccess';

class AddAdminContainer extends Component {
    state = {
        step_no: 1,
        search_emails:[],
        selected_access_groups: {},
        selected_access_groups_id: [],
        is_demographic_selected: false,
        is_hierarchy_selected: false
    }

    componentDidMount () {
        this.checkForEditInfo();
    }

    checkForEditInfo = () => {
        const {is_edit, current_admin} = this.props;
        let {selected_access_groups_id, is_demographic_selected, is_hierarchy_selected} = this.state;
        if(is_edit){
            let selected_access_groups = {};
            if(current_admin && current_admin.roles){
                current_admin.roles.forEach((role) => {
                    if(role.id){
                        selected_access_groups[role.id] = true;
                        selected_access_groups_id.push(role.id);
                    }
                    if(role.demographic_toggle){
                        is_demographic_selected = true;
                    }

                    if(!role.demographic_toggle){
                        is_hierarchy_selected = true;
                    }
                })
                this.setState({
                    selected_access_groups: JSON.parse(JSON.stringify(selected_access_groups)),
                    selected_access_groups_id: JSON.parse(JSON.stringify(selected_access_groups_id)),
                    is_demographic_selected,
                    is_hierarchy_selected
                })
            }
        }
    }

    showAccessGroupCreationModal = _ => {
        this.setState({is_creation_modal_active: true});
    }
    
    closeAccessModal = _ => {
        this.setState({is_creation_modal_active: false}, () => {this.props.getAccessGroupsList.bind(this, this.props.client_id)});
    }

    selectAccessGroup = (access_id) => {
        const {selected_access_groups_id} = this.state;
        const {access_groups} = this.props;
        let is_demographic_selected = false; 
        let is_hierarchy_selected = false; 
        let selected_access = {}
        if(selected_access_groups_id.length <= 2 || selected_access_groups_id.indexOf(access_id) !== -1){
            if(selected_access_groups_id.indexOf(access_id) === -1){
                access_groups.forEach((per, index) => {
                    if(parseInt(access_id) === parseInt(per.id)){
                        selected_access = per;
                    }
                    if(selected_access_groups_id.indexOf(parseInt(per.id)) !== -1){
                        if(per.demographic_toggle){
                            is_demographic_selected = true;
                        }
                        else{
                            is_hierarchy_selected = true;
                        }
                    }
                })
    
                if(is_demographic_selected && selected_access.demographic_toggle){
                    NotificationManager.error('You cannot select 2 demographic access groups');
                }
                else if(is_hierarchy_selected && !selected_access.demographic_toggle){
                    NotificationManager.error('You cannot select 2 hierarchy access groups');
                }
                else{
                    this.selectAG(access_id);
                    if(selected_access.demographic_toggle){
                        this.setSelectedDemographic(true);
                    }
                    else{
                        this.setSelectedHierarchy(true);
                    }
                }
            }
            else{
                access_groups.forEach((per, index) => {
                    if(parseInt(access_id) === parseInt(per.id)){
                        selected_access = per;
                    }
                })
                if(selected_access.demographic_toggle){
                    this.setSelectedDemographic(false);
                }
                this.selectAG(access_id);
            }
        }
        else{
            NotificationManager.error('You can only select 2 access groups');
        }
    }


    setSelectedDemographic = (is_demographic_selected) => {
        this.setState({
            is_demographic_selected
        })
    }

    setSelectedHierarchy = (is_hierarchy_selected) => {
        this.setState({
            is_hierarchy_selected
        })
    }

    selectAG = (access_id) => {
        let {selected_access_groups, selected_access_groups_id} = this.state;
        selected_access_groups[access_id] = !selected_access_groups[access_id];
        if(selected_access_groups_id.indexOf(access_id) !== -1){
            const element_index = selected_access_groups_id.indexOf(access_id)
            selected_access_groups_id.splice(element_index, 1);
        }
        else{
            selected_access_groups_id.push(access_id);
        }
        this.setState({
            selected_access_groups: JSON.parse(JSON.stringify(selected_access_groups)),
            selected_access_groups_id: JSON.parse(JSON.stringify(selected_access_groups_id))
        })
    }

    updateStep = (step) => {
        this.setState({
            step_no : step
        })
    }

    addEmails = () => {

    }

    toggleAddAdmin = () => {
        this.props.toggleAddAdmin()
    }

    render() {
        const {selected_access_groups, search_emails, step_no, selected_access_groups_id, is_hierarchy_selected, is_demographic_selected} = this.state
        const {access_groups, client_id, current_admin, is_edit, client_details}  = this.props
        return (
            <div className="new-admin-container">
                <p className="paddingleft-sm">
                    <button onClick={this.toggleAddAdmin.bind()} className="button is-info is-inverted margineft-xs" >
                        <i className={classnames('fas fa-arrow-left marginright-xs ')}></i>
                        Go Back
                    </button>
                </p>
                <div className="paddingleft-lg">
                    <h1 className="is-size-4 has-text-weight-bold">
                        Add an Admin
                    </h1>
                    <p className="is-size-7">
                        Customize admin access to specific locations, department, or dashboard features. Give quick access anytime by pre-defining Access Groups.
                    </p>
                <div className="height-steps margintop-md">
                    <ul className="steps is-thin is-vertical">
                        <li className="steps-segment access-section">
                            <span className="steps-marker"></span>
                            <div className="steps-content no-marginbottom">
                                <span className="has-text-weight-bold">
                                <span className={classnames('serial-check', {'checked': step_no > 1})}>
                                    {step_no === 1 ? 1 :
                                    <i className={classnames("fas fa-check")}></i> }
                                </span>
                                     Select Access Group(s) 
                                </span>
                                <div className={classnames('', {'hidden' : step_no !== 1})}>
                                    <SelectAccessGroup 
                                        access_groups = {access_groups}
                                        selectAccessGroup = {this.selectAccessGroup}
                                        updateStep = {this.updateStep}
                                        nextStep={is_edit ? (is_demographic_selected ? 3 : 4) : 2}
                                        selected_groups = {selected_access_groups}
                                        selected_access_groups_id={selected_access_groups_id}
                                        client_id = {client_id}/>
                                </div>
                            </div>
                        </li>
                        <li className="steps-segment access-section" disabled={!is_edit}>
                            <span className="steps-marker"></span>
                            <div className="steps-content no-marginbottom">
                                <span className="has-text-weight-bold"> 
                                    <span className={classnames('serial-check', {'checked': step_no > 2})}>
                                        {step_no < 3 ? 2 :
                                        <i className={classnames("fas fa-check")}></i> }
                                    </span>
                                    Add Email IDs 
                                </span>
                                <div className={classnames('', {'hidden' : step_no !== 2})}>
                                    {is_edit ? null
                                        : <AddEmails
                                            search_emails={search_emails}
                                            addEmails={this.addEmails}
                                            updateStep={this.updateStep}
                                            client_id = {client_id}
                                            nextStep={is_demographic_selected ? 3 : 4}
                                            is_hierarchy_selected={is_hierarchy_selected}/>}
                                </div>
                            </div>
                        </li>
                        <li className="steps-segment access-section">
                            <span className="steps-marker"></span>
                            <div className="steps-content no-marginbottom">
                                <span className="has-text-weight-bold"> 
                                    <span className={classnames('serial-check', {'checked': step_no > 3})}>
                                    {step_no < 4 ? 3 :
                                    <i className={classnames("fas fa-check")}></i> }
                                    </span>
                                    Customize Demographic Access 
                                </span>
                                <div className={classnames('', {'hidden' : step_no !== 3})}>
                                    
                                {client_details.advanced_demographic_enabled ?
                                            <AdvancedCustomizeDemographicAccess
                                                updateStep={this.updateStep}
                                                previous_step={is_edit ? 1 : 2}
                                                current_admin={current_admin}
                                                is_edit={is_edit}
                                                client_id={client_id}
                                                selected_access_groups={selected_access_groups}/>
                                        : <CustomizeDemographicAccess
                                                updateStep={this.updateStep}
                                                previous_step={is_edit ? 1 : 2}
                                                current_admin={current_admin}
                                                is_edit={is_edit}
                                                client_id={client_id}
                                         selected_access_groups={selected_access_groups}/>}
                                </div>
                            </div>
                        </li>
                        <li className="steps-segment access-section review-access">
                            <span className="steps-marker"></span>
                            <div className="steps-content no-marginbottom">
                                <span className="has-text-weight-bold">
                                <span className={classnames('serial-check')}>
                                    {step_no < 5 ? 4 :
                                    <i className={classnames("fas fa-check")}></i> }
                                </span>
                                Review </span>
                                { step_no === 4 ?
                                    <div className={classnames('', {'hidden' : step_no !== 4})}>
                                        <ReviewAccess
                                            updateStep={this.updateStep}
                                            selected_access_groups={selected_access_groups}
                                            current_admin={current_admin}
                                            is_edit={is_edit}
                                            client_id = {client_id}
                                            previous_step={is_edit ? (is_demographic_selected ? 3 : 1) : (is_demographic_selected ? 3 : 2)}
                                            toggleAddAdmin = {this.props.toggleAddAdmin}
                                            />
                                    </div> : null}
                            </div>
                        </li>
                    </ul>
                </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    client_details: state.client.client_details
  })

export default connect(mapStateToProps, {})(AddAdminContainer)