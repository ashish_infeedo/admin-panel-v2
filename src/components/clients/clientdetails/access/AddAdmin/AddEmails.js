import React, { Component } from 'react'
import classnames from 'classnames'
import {connect} from 'react-redux';
import {accessUploadCleanFile, searchEmailIds, storeAccessEmails, storeAccessFileURL} from '../../../../../actions/AccessActions';
import ReactTags from 'react-tag-autocomplete';
import upload_icon from '../../../../../assets/images/upload-csv-default.svg';
import { NotificationManager } from 'react-notifications';

class AddEmails extends Component {
    state = {
        loading: false,
        files: [],
        file_uploaded: false,
        email_tags: [],
        email_suggestions: []
    }

    componentDidMount() {
    }

    componentDidUpdate() {

    }

    openUploadModal = () => {
        document.getElementById('accessuploadUsers').click();
    }

    uploadFile = (e) => {
        if(e.target.files){
            this.setState({
                files: e.target.files,
                file_uploaded: true}, () => this.cleanUserCSV());
        }
    }

    cleanUserCSV = async () => {
        const {files} = this.state;
        const formData = new FormData();
        this.setState({
            loading: true
        })
        formData.append('bulk_access', files[0]);
        const response = await this.props.accessUploadCleanFile(formData);
        if(response && response.status === 200 && response.data){
            if(response.data.success){
                this.props.storeAccessFileURL(response.data.file_link);
                this.setState({
                    file_uploaded: true
                })
            }
            else{
                let link = document.createElement("a");
                link.setAttribute('download', 'clean');
                link.href = response.data.file_link;
                document.body.appendChild(link);
                link.click();
                link.remove();
            }
        }
        this.setState({
            loading: false
        })
    }

    searchEmails = (tags) => {
    }

    handleDelete = (i) => {
        const {email_tags} = this.state;
        let tags = email_tags.slice(0)
        tags.splice(i, 1)
        this.setState({ 
            email_tags: tags 
        })
    }

    handleAddition = (tag) => {
        const {email_tags} = this.state;
        let flag = true;
        email_tags.forEach((email) => {
            if(email.name === tag.name){
                flag = false;
                NotificationManager.error('You have already selected this admin.');
            }
        })

        if(flag === true){
            const tags = [].concat(this.state.email_tags, tag)
            this.setState({ 
                email_tags: tags 
            })
        }
    }

    onChangeEmails = async (keyword) => {
        const {client_id} = this.props;
        if(keyword.length > 1){
            const response = await this.props.searchEmailIds(keyword, client_id);
            if(response && response.status === 200 && response.data){
                this.setState({
                    email_suggestions: response.data
                })
            }
        }
    }

    addEmailsOrFile = () => {
        const {email_tags} = this.state;
        const {nextStep} = this.props;
        let email_list = [];
        email_tags.forEach((email) => {
            email_list.push(email.name);
        })
        this.props.storeAccessEmails(email_list);
        this.props.updateStep(nextStep);
    }

    render() {
        const {loading, email_suggestions, email_tags, file_uploaded} = this.state;
        const {is_hierarchy_selected} = this.props;
        return (
            <div className="card full-height box-blue-border mg-top-15 mg-bottom-20">
                <div className="card-content">
                    <div className="columns flex-wrap">
                        <div className="column is-12 no-paddingbottom">
                            <span className="save-view-label mg-bottom-5 font-size-10" >
                                Email ID(s)
                                <sup>*</sup>
                            </span>
                        </div>
                        <div className="column is-12 add-email">
                            <span className="columns padding-sm">
                                <div className="column no-padding">
                                    <ReactTags
                                        tags={email_tags}
                                        suggestions={email_suggestions}
                                        handleDelete={this.handleDelete.bind(this)}
                                        handleAddition={this.handleAddition.bind(this)}
                                        handleInputChange={this.onChangeEmails.bind(this)}
                                        placeholder="Search Email"
                                        noSuggestionsText="No Email Found"/>
                                </div>
                                <button id="accessBulkupload" onClick={this.openUploadModal} className={classnames('button is-primary no-padding order-icon is-small blue-border blue-backgroud',{'is-loading': loading})} disabled={is_hierarchy_selected ? false : true}>
                                    <span className="icon is-small" data-tip="Bulk Upload">
                                        {loading ? null : 
                                            <img src={upload_icon} width="16px" alt="upload"/>
                                        }

                                    </span>
                                </button>
                            </span>
                        </div>
                        <div className="column is-12">
                            <div className="">
                                <button id="ag-save-email" className={classnames('button is-small is-outlined padding-left-right-20')} onClick={this.props.updateStep.bind(this,1)}>Previous</button>
                                <button id="ag-next" onClick={this.addEmailsOrFile} className={classnames('button is-small is-primary mg-left-10 padding-left-right-20')} disabled={email_tags.length > 0 || file_uploaded ? false : true}>Next</button>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="file" id='accessuploadUsers' onChange={this.uploadFile} accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" hidden/>
            </div>
        )
    }
}


export default connect(null, {accessUploadCleanFile, searchEmailIds, storeAccessEmails, storeAccessFileURL})(AddEmails)
