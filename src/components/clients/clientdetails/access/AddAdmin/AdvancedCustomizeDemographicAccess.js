import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import Dropdown from '../../../../generic/dropdown/Dropdown';
import SelectDropdown from '../../../../generic/dropdown/SelectDropdown';
import {getCategoriesForDemographics, getUserCategoriesForDemographics, saveDemographicAccessData} from '../../../../../actions/AccessActions';
import ReactTooltip from 'react-tooltip';
class AdvancedCustomizeDemographicAccess extends Component {

    state = {
        block_data : [{   
            id:1,
            intrablock_operator: 'AND',
            intrablock_index: 0,
            intrablock_value: 'And',
            permissions: [{
                id: 1,
                key: 'cohorts',
                data: {},
                selected_demographic_index: 0
            }]
        }],
        block_count: 1,
        rules_type: [{
                value: 'AND',
                label: 'And'
            },
            {
                value: 'OR',
                label: 'Or',
        }],
        demographs_categories: [{
                value: 'cohorts',
                label: 'Cohorts'
            },
            {
                value: 'locations',
                label: 'Locations'
            },
            {
                value: 'sub_department',
                label: 'Sub Departments'
            },
            {
                value: 'departments',
                label: 'Departments'
            },
            {
                value: 'teams',
                label: 'Teams'
            },
            {
                value: 'managers',
                label: 'Managers'
            },
            {
                value: 'functions',
                label: 'Functions'
            },
            {
                value: 'business',
                label: 'Business'
            },
            {
                value: 'l_team',
                label: 'L Teams'
            },
            {
                value: 'hrbp',
                label: 'HRBP'
            },
            {
                value: 'city',
                label: 'City'
            },
            {
                value: 'country',
                label: 'Country'
            },
            {
                value: 'employee_type',
                label: 'Employee Type'
            },
            {
                value: 'sub_function',
                label: 'Sub Function'
            },
            {
                value: 'nationality',
                label: 'Nationality'
            },
            {
                value: 'region',
                label: 'Region'
            },
            {
                value: 'division',
                label: 'Division'
            },
            {
                value: 'vertical',
                label: 'Vertical'
        }],
        access_levels: {
            cohorts: {
                options: [],
                title: 'Cohorts',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: false,
                key:'cohorts'
            },
            locations: {
                options: [],
                title: 'Locations',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: false,
                key:'locations'
            },
            sub_department: {
                options: [],
                title: 'Sub Departments',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key:'sub_departments'
            },
            departments: {
                options: [],
                title: 'Departments',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: false,
                key:'departments'
            },
            teams: {
                options: [],
                title: 'Teams',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key:'teams'
            },
            managers: {
                options: [],
                title: 'Managers',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key:'managers'
            },
            functions: {
                options: [],
                title: 'Functions',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key:'functions'
            },
            business: {
                options: [],
                title: 'Business',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key:'business'
            },
            l_team: {
                options: [],
                title: 'L Teams',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key: 'l_teams'
            },
            hrbp: {
                options: [],
                title: 'HRBPs',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key: 'hrbps'
            },
            city:{
                options: [],
                title: 'City',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key: 'city'
            },
            country:{
                options: [],
                title: 'Country',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key: 'country'
            },
            employee_type:{
                options: [],
                title: 'Employee Type',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key: 'employee_type'
            },
            sub_function:{
                options: [],
                title: 'Sub-Functions',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key: 'sub_function'
            },
            nationality:{
                options: [],
                title: 'Nationality',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key: 'nationality'
            },
            region:{
                options: [],
                title: 'Regions',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key: 'region'
            },
            division:{
                options: [],
                title: 'Division',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key: 'division'
            },
            vertical: {
                options: [],
                title: 'Vertical',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key: 'vertical'
            }
        },
        access_type: 1,
        block_operator: {}
    }

    componentDidMount(){
        this.getCategoriesListForDemographics();
    }

    componentDidUpdate(prevProps, prevState){
        const{is_edit, current_admin} = this.props;
        const {access_levels} = this.state;
        if(JSON.stringify(current_admin) !== JSON.stringify(prevProps.current_admin) && is_edit){
            this.setDemographicAccess();
        }

        if(JSON.stringify(access_levels) !== JSON.stringify(prevState.access_levels) && is_edit){
            this.setDemographicAccess();
        }
    }

    getCategoriesListForDemographics = async () => {
        let {access_levels, block_data} = this.state;
        const {client_id} = this.props;
        await Object.keys(access_levels).forEach(
            async (category) => {
            let response = {};
            if(access_levels[category].is_user){
                response = await this.props.getUserCategoriesForDemographics(access_levels[category].key, client_id);
            }
            else{
                response = await this.props.getCategoriesForDemographics(access_levels[category].key, client_id);
            }
            if(response && response.status === 200 && response.data){
                access_levels[category].options = response.data;
                access_levels[category].search_options = JSON.parse(JSON.stringify(response.data));
                access_levels[category].data_loaded = true;
            }
            this.setState({
                access_levels: JSON.parse(JSON.stringify(access_levels))
            }, () => {
                if(category === 'cohorts'){
                    block_data[0]["permissions"][0]["data"] = access_levels['cohorts'];
                }
            })
        })
    }

    setDemographicAccess = () => {
        const {is_edit, current_admin} = this.props;
        let {access_type, access_levels, block_count, demographs_categories, block_operator} = this.state;
        let block_data=[];
        if(is_edit){
            if(current_admin.roles){
                current_admin.roles.forEach((role) => {
                    if(role.access_groups.length > 0){
                        access_type = 2;
                        block_count=0;
                    }
                    role.access_groups.forEach((access) => {
                        let b_data={};
                        if(access.intrablock_operator){
                            b_data.id = block_count;
                            b_data.intrablock_operator=access.intrablock_operator;
                            if(access.intrablock_operator === 'AND'){
                                b_data.intrablock_index=0;
                                b_data.intrablock_value='And';
                            }
                            else{
                                b_data.intrablock_index=1;
                                b_data.intrablock_value='Or';
                            }
                            b_data.permissions=[];
                            Object.keys(access.permissions).forEach((per, index) => {
                                let per_data = {};
                                per_data.id = index+1;
                                per_data.key = per;
                                demographs_categories.forEach((category, category_index) => {
                                    if(category.value === per){
                                        per_data.selected_demographic_index = category_index;
                                    }
                                })
                                let access_level = JSON.parse(JSON.stringify(access_levels[per]));
                                let selected_demographs = access.permissions[per];
                                if(access_level){
                                    access_level.selected_count = selected_demographs.length;
                                    selected_demographs.forEach((sel) => {
                                        access_level.selected_options[sel] = true;
                                    })
                                    per_data.data = access_level;
                                }
                                b_data.permissions.push(per_data);
                            })
                            block_data.push(b_data);
                            block_count=block_count+1;
                        }
                        if(access.interblock_operator){
                            block_operator[block_count-1] = access.interblock_operator;
                        }
                    })
                    this.setState({
                        block_data: JSON.parse(JSON.stringify(block_data)),
                        block_operator: JSON.parse(JSON.stringify(block_operator)),
                        access_type,
                        block_count
                    })
                })
            }
        }
    }

    addDemographicInBlock = (demographic_type, selected_value) => {
        let {block_data, access_levels, demographs_categories} = this.state;
        let block_id = demographic_type.split('_')[0];
        let demographic_id = demographic_type.split('_')[1];
        block_data.forEach((data) => {
            if(parseInt(data.id) === parseInt(block_id)){
                let block_selected_data = data;
                block_selected_data.permissions.forEach((per) => {
                    if(parseInt(per.id) === parseInt(demographic_id)){
                        per.key= selected_value.value;
                        per.data= access_levels[selected_value.value];
                        demographs_categories.forEach((category, category_index) => {
                            if(category.value === selected_value.value){
                                per.selected_demographic_index = category_index;
                            }
                        })
                        this.setState({
                            block_data: JSON.parse(JSON.stringify(block_data))
                        })
                    }
                })
            }
        })
    }

    loadMore = (key, extra_info) => {
        const { block_data } = this.state;
        let new_block_data = JSON.parse(JSON.stringify(block_data));

        let block_id = extra_info.split('_')[0];
        let demographic_id = extra_info.split('_')[1];

        new_block_data.forEach((data) => {
            if(parseInt(data.id) === parseInt(block_id)){
                let block_selected_data = data;
                block_selected_data.permissions.forEach((per) => {
                    if(parseInt(per.id) === parseInt(demographic_id)){
                        per.data.show_only += 50;
                    }
                })
            }
        })

        this.setState({
            block_data: new_block_data
        })
    }

    onSearch = (e, extra_info) => {
        const {block_data} = this.state;
        let new_block_data = JSON.parse(JSON.stringify(block_data));
        const key = e.target.name;
        let block_id = extra_info.split('_')[0];
        let demographic_id = extra_info.split('_')[1];

        new_block_data.forEach((data) => {
            if(parseInt(data.id) === parseInt(block_id)){
                let block_selected_data = data;
                block_selected_data.permissions.forEach((per) => {
                    if(parseInt(per.id) === parseInt(demographic_id)){
                        per.data.search_text = e.target.value;
                        if(per.data.search_text.length > 0) {
                            const results = per.data.options.filter(option => option.title.toLowerCase().includes(per.data.search_text.toLowerCase()));
                            per.data.search_options = results;
                        } else if(!per.data.search_text.length) {
                            per.data.search_options = JSON.parse(JSON.stringify(per.data.options))
                        }
                        per.data.show_only = 50;
                    }
                })
            }
        })

        this.setState({
            block_data: new_block_data
        })
    }

    selectOption = (key, title, previous_selected = false, extra_info) => {
        let {access_levels, total_selected, block_data} = this.state;
        let block_id = extra_info.split('_')[0];
        let demographic_id = extra_info.split('_')[1];

        block_data.forEach((data) => {
            if(parseInt(data.id) === parseInt(block_id)){
                let block_selected_data = data;
                block_selected_data.permissions.forEach((per) => {
                    if(parseInt(per.id) === parseInt(demographic_id)){
                        let sub_access = per.data;
                        if(sub_access.selected_options[title] !== true){
                            sub_access.selected_options[title] = true;
                            sub_access.selected_count = (++sub_access.selected_count);
                            total_selected = total_selected + 1;
                        }
                        else{
                            sub_access.selected_options[title] = !sub_access.selected_options[title];
                            sub_access.selected_count = (--sub_access.selected_count);
                            total_selected = total_selected - 1;
                        }
                        per.data = sub_access;
                    }
                })
            }
        })
        
        this.setState({
            access_levels: JSON.parse(JSON.stringify(access_levels)), 
            block_data: JSON.parse(JSON.stringify(block_data)),
            total_selected
        });
    }

    addLogicForDemographic = (block_index, selected_value) => {
        let {block_data} = this.state;
        let block = JSON.parse(JSON.stringify(block_data));
        block[block_index].intrablock_operator = selected_value.value;
        if(selected_value.value === "AND"){
            block[block_index].intrablock_index = 0;
            block[block_index].intrablock_value = 'And';
        }
        else{
            block[block_index].intrablock_index = 1;
            block[block_index].intrablock_value = 'Or';
        }
        this.setState({
            block_data: JSON.parse(JSON.stringify(block))
        })
    }

    addRule = (block_count) => {
        let {block_data, access_levels} = this.state;
        let new_rule = {};
        let rule_count = block_data[block_count].permissions.length + 1;
        new_rule.id = rule_count;
        new_rule.data = access_levels['cohorts'];
        new_rule.key = 'cohorts';
        new_rule.selected_demographic_index = 0;
        let new_block_data = JSON.parse(JSON.stringify(block_data));
        new_block_data[block_count].permissions.push(new_rule);
        this.setState({
            block_data: new_block_data
        })
    }

    addBlock = () => {
        const {block_count, access_levels} = this.state;
        let {block_data, block_operator} = this.state;
        let new_block = {};
        new_block.id = block_count + 1;
        new_block.intrablock_operator = 'AND';
        new_block.intrablock_index = 0;
        new_block.intrablock_value = 'And';
        new_block.permissions = [];
        let new_rule = {};
        new_rule.id = 1;
        new_rule.data = access_levels['cohorts'];
        new_rule.key = 'cohorts';
        new_rule.selected_demographic_index= 0;
        new_block.permissions.push(new_rule);
        block_data.push(new_block);
        block_operator[block_count-1] = 'AND';
        this.setState({
            block_data: JSON.parse(JSON.stringify(block_data)),
            block_operator: JSON.parse(JSON.stringify(block_operator)),
            block_count: block_count + 1
        })
    }

    deleteRule = (block_id, content_id) => {
        let {block_data} = this.state;
        let block = JSON.parse(JSON.stringify(block_data[block_id])) ;
        let block_index = 0;
        let flag = false;
        block.permissions.forEach((name, index) => {
            if(name.id === content_id){
                flag = true;
                block_index = index;
            }
        })
        if(flag){
            block.permissions.splice(block_index, 1);
            block_data[block_id] = block;
            this.setState({
                block_data: JSON.parse(JSON.stringify(block_data))
            })
        }
    }

    giveDemographicAccess = () => {
        const {block_data, block_operator, access_type} = this.state;
        let access_data = [];
        if(access_type === 2){
            block_data.forEach(
                (data, index) => {
                    let block = {};
                    block.intrablock_operator = data.intrablock_operator;
                    block.permissions = {};
                    data.permissions.forEach((per) => {
                        let key = per.key;
                        let selected_data = []
                        Object.keys(per.data.selected_options).forEach((s_data) => {
                            if(per.data.selected_options[s_data]){
                                selected_data.push(s_data);
                            }
                        })
                        if(selected_data.length > 0){
                            block.permissions[key] = selected_data;
                        }
                    })
                    access_data.push(block);
                    if(block_operator[index]){
                        let inter_block = {};
                        inter_block['interblock_operator'] = block_operator[index];
                        access_data.push(inter_block);
                    }
            })
        }
        this.props.updateStep(4);
        this.props.saveDemographicAccessData(access_data);
    }

    setAccessType = (access_type) => {
        this.setState({
            access_type
        })
    }

    addOperatorForBlock = (index, selected_value) =>{
        let {block_operator} = this.state;
        block_operator[index] = selected_value.value;
        this.setState({
            block_operator: JSON.parse(JSON.stringify(block_operator))
        })
    }

    render(){
        const {block_data, demographs_categories, access_levels, rules_type, access_type, block_operator} = this.state;
        const {previous_step} = this.props;
        return (
            <div className="card full-height box-blue-border mg-top-15 mg-bottom-20">
                <div className="card-content">
                    <div className="columns flex-wrap padding-sm">
                        <div className="column is-12 no-padding">
                            <span className="text-access has-text-weight-bold is-size-6">Access Type</span><br/>
                            <div className="columns no-padding-left">
                                <div className="column is-5 no-padding-left">
                                    <label className="radio pointer has-text-centered">
                                        <input type="radio" onChange={()=>{}} value="full" name="access" checked={access_type === 1 ? true : false} onClick={this.setAccessType.bind(this, 1)}/> 
                                        <span className="is-size-6 mg-left-10">
                                            Full Demographic Access
                                        </span>
                                    </label>
                                </div>
                                <ReactTooltip id="info-circle-full" className="previous-tooltip" effect="solid" place="top" html={true}/>
                                <div className="column is-5">
                                    <label className="radio pointer has-text-centered">
                                        <input type="radio" onChange={()=>{}} value="partial" name="access" checked={access_type === 2 ? true : false} onClick={this.setAccessType.bind(this, 2)}/> 
                                        <span className="is-size-6 mg-left-10">
                                            Partial Demographic Access
                                        </span>
                                    </label>
                                </div>
                                <ReactTooltip id="info-circle-partial" className="previous-tooltip" effect="solid" place="top" html={true}/>
                            </div>
                        </div>
                        { access_type === 1 ? null 
                           : <div className="column is-12 flex-wrap no-padding advanced-demographic-access-section paddingtop-lg">
                                <span className="text-access font-14 no-paddingtop">DEMOGRAPHS</span><br/>
                                { block_data.map(
                                    (data, index) => (
                                        <React.Fragment>
                                        <div className="column is-12 columns flex-wrap no-padding mg-top-30">
                                            {data.permissions.map((name, per_index) => (
                                                <div className="column is-12 columns flex-wrap no-padding access-row no-margin">
                                                    { name.id === 1 ? null
                                                        :<div className="column is-1 paddingtop-sm margintop-xs">
                                                            <button onClick={this.deleteRule.bind(this, index, name.id)} className="button is-white transparent-bg inverted-button"><i className={classnames('fas fa-times')}></i></button>
                                                        </div>}
                                                    { per_index === 1 ?
                                                        <div className="column is-2 no-padding-left">
                                                            <Dropdown
                                                                options={rules_type}
                                                                defaultValue={rules_type[data.intrablock_index]}
                                                                isMulti={false}
                                                                type={`${index}`}
                                                                handleChange={this.addLogicForDemographic}
                                                                classes="basic-multi-select"/>
                                                        </div>
                                                        : (name.id === 1 ?
                                                            <div className="column is-3 paddingtop-md">
                                                                <span className="margintop-xs">When the</span>
                                                            </div> 
                                                            : <div className="column is-1 paddingtop-md">
                                                                <span className="margintop-xs">{data.intrablock_value}</span>
                                                            </div> )}
                                                    <div className="column is-3">
                                                        <Dropdown
                                                            options={demographs_categories}
                                                            defaultValue={demographs_categories[name.selected_demographic_index]}
                                                            isMulti={false}
                                                            type={`${data.id}_${name.id}`}
                                                            handleChange={this.addDemographicInBlock}
                                                            classes="basic-multi-select"/>
                                                    </div>
                                                    <div className="column is-1 paddingtop-md has-text-centered">
                                                        <span>is</span>
                                                    </div>
                                                    {access_levels[name.key] ? 
                                                        <div key={name} className="column is-3 demographic-options paddingtop-xs">
                                                        <SelectDropdown 
                                                            access_data={name.data} 
                                                            access_key={name.key} 
                                                            onSearch={this.onSearch} 
                                                            loadMore={this.loadMore} 
                                                            selectOption={this.selectOption}
                                                            show_title={false}
                                                            extra_info={`${data.id}_${name.id}`}/>
                                                    </div> : null}
                                                </div>
                                            ))}
                                            <div key={index} className="column is-12 no-padding">
                                                <button onClick={this.addRule.bind(this, index)} className="has-text-info button is-white transparent-bg inverted-button">+ Add Rule</button>
                                            </div>
                                        </div>
                                        <hr className="dark-seperator column is-12 no-padding margintop-sm marginbottom-md"/>
                                        { block_operator[index] ?
                                            <div className="block-divider">
                                                <Dropdown
                                                    options={rules_type}
                                                    defaultValue={ block_operator[index] === 'AND' ? rules_type[0] : rules_type[1]}
                                                    isMulti={false}
                                                    type={`${index}`}
                                                    handleChange={this.addOperatorForBlock}
                                                    classes="width-100px advanced-dropdown-operator"/>
                                            </div> : null}
                                        </React.Fragment>
                                    ))
                                }
                                <div className="column is-12 columns no-padding">
                                    <button onClick={this.addBlock} className="has-text-info button is-white transparent-bg inverted-button">+ Add Block</button>
                                </div>
                            </div>}
                        <div className="column is-12 no-padding">
                            <div className="is-pulled-right">
                                <button id="ag-save-demographs" className={classnames('button is-link is-outlined padding-left-right-20')} onClick={this.props.updateStep.bind(this,previous_step)}>Previous</button>
                                <button id="ag-next-demographs" onClick={this.giveDemographicAccess} className={classnames('button is-info mg-left-10 padding-left-right-20')}>Next</button>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    admin_list: state.access.admin_list
  })

// Porptypes
// ---------
AdvancedCustomizeDemographicAccess.propTypes = {
};

export default connect(mapStateToProps, {getCategoriesForDemographics, getUserCategoriesForDemographics, saveDemographicAccessData})(AdvancedCustomizeDemographicAccess);

