import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import {getCategoriesForDemographics, getUserCategoriesForDemographics, saveDemographicAccessData} from '../../../../../actions/AccessActions';
import SelectDropdown from '../../../clientdetails/access/SelectDropdown';

class CustomizeDemographicAccess extends Component {

    state = {
        access_levels: {
            cohorts: {
                options: [],
                title: 'Cohorts',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: false,
                key:'cohorts'
            },
            locations: {
                options: [],
                title: 'Locations',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: false,
                key:'locations'
            },
            sub_department: {
                options: [],
                title: 'Sub Departments',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key:'sub_departments'
            },
            teams: {
                options: [],
                title: 'Teams',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key:'teams'
            },
            managers: {
                options: [],
                title: 'Managers',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key:'managers'
            },
            functions: {
                options: [],
                title: 'Functions',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key:'functions'
            },
            business: {
                options: [],
                title: 'Business',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key:'business'
            },
            l_team: {
                options: [],
                title: 'L Teams',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key: 'l_teams'
            },
            hrbp: {
                options: [],
                title: 'HRBPs',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key: 'hrbps'
            },
            city:{
                options: [],
                title: 'City',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key: 'city'
            },
            country:{
                options: [],
                title: 'Country',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key: 'country'
            },
            departments: {
                options: [],
                title: 'Departments',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: false,
                key:'departments'
            },
            employee_type:{
                options: [],
                title: 'Employee Type',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key: 'employee_type'
            },
            sub_function:{
                options: [],
                title: 'Sub-Functions',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key: 'sub_function'
            },
            nationality:{
                options: [],
                title: 'Nationality',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key: 'nationality'
            },
            region:{
                options: [],
                title: 'Regions',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key: 'region'
            },
            division:{
                options: [],
                title: 'Division',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key: 'division'
            },
            vertical: {
                options: [],
                title: 'Vertical',
                selected_count: 0,
                data_loaded: false,
                search_text: '',
                search_options: [],
                selected_options: {},
                show_only: 50,
                is_user: true,
                key: 'vertical'
            }
        },
        initial_selected_sub_access: {},
        selected_sub_access: {},
        access_type : 1,
        total_selected : 0
    }

    async componentDidMount(){
        await this.getCategoriesListForDemographics();
    }

    componentDidUpdate(prevProps, prevState){
        const{is_edit, current_admin} = this.props;
        const{access_levels} = this.state;
        if(JSON.stringify(current_admin) !== JSON.stringify(prevProps.current_admin) && is_edit){
            this.setDemographicAccess();
        }

        if(JSON.stringify(access_levels) !== JSON.stringify(prevState.access_levels) && is_edit){
            this.setDemographicAccess();
        }
    }

    setDemographicAccess = () => {
        const {is_edit, current_admin} = this.props;
        let {access_type, access_levels} = this.state;
        let total_selected = 0;
        let selected_sub_access = {};
        if(is_edit){
            if(current_admin.roles){
                current_admin.roles.forEach((role) => {
                    if(role.access_groups.length > 0){
                        access_type = 2;
                        role.access_groups.forEach((access) => {
                            Object.keys(access.permissions).forEach((per) => {
                                let access_level = access_levels[per];
                                let selected_demographs = access.permissions[per];
                                if(access_level){
                                    access_level.selected_count = selected_demographs.length;
                                    selected_demographs.forEach((sel) => {
                                        access_level.selected_options[sel] = true;
                                        if(selected_sub_access[per]){
                                            selected_sub_access[per].push(sel);
                                            total_selected = total_selected + 1;
                                        }
                                        else{
                                            selected_sub_access[per] = [sel];
                                            total_selected = total_selected + 1;
                                        }
                                    })
                                }
                            })
                        })
                    }
                })
            }
            let selected_on_top_access_levels = this.setSelectedOnTop(access_levels);
            this.setState({
                access_levels: selected_on_top_access_levels,
                access_type,
                total_selected,
                selected_sub_access: JSON.parse(JSON.stringify(selected_sub_access))
            })
        }
    }

    //fn that takes access level on input and return a copy of access level with selected options on top.
    setSelectedOnTop = (access_levels, demographic_key) => {
        const {selected_sub_access } = this.state;
        let access_level_copy = JSON.parse(JSON.stringify(access_levels));
        let selected_element = [];
        let non_selected_element = [];
        if (demographic_key !== undefined) {
            if (access_level_copy[demographic_key] && access_level_copy[demographic_key].search_options.length) {
                access_level_copy[demographic_key].search_options.forEach((option, index2) => {
                    if (access_level_copy[demographic_key].selected_options[option.title]) {
                        selected_element.push(option);
                    } else {
                        non_selected_element.push(option);
                    }
                });
                access_level_copy[demographic_key].search_options = selected_element.concat(non_selected_element);
                selected_element = [];
                non_selected_element = [];
            }
        } else if (Object.keys(selected_sub_access).length) {
            Object.entries(selected_sub_access).forEach(([demographic_name, filters_array], index) => {
                if (access_level_copy[demographic_name] && access_level_copy[demographic_name].search_options.length) {
                    access_level_copy[demographic_name].search_options.forEach((option, index2) => {
                        if (access_level_copy[demographic_name].selected_options[option.title]) {
                            selected_element.push(option);
                        } else {
                            non_selected_element.push(option);
                        }
                    });
                    access_level_copy[demographic_name].search_options = selected_element.concat(non_selected_element);
                    selected_element = [];
                    non_selected_element = [];
                }
            });
        }
        return access_level_copy;
    }

    getCategoriesListForDemographics = async () => {
        const {client_id} = this.props;
        const {access_levels} = this.state;
        await Object.keys(access_levels).forEach(
            async (category) => {
            const response = await this.props.getCategoriesForDemographics(access_levels[category].key, client_id);
            if(response && response.status === 200 && response.data){
                access_levels[category].options = response.data;
                access_levels[category].search_options = JSON.parse(JSON.stringify(response.data));
                access_levels[category].data_loaded = true;
            }
            this.setState({
                access_levels: JSON.parse(JSON.stringify(access_levels))
            })
        })
    }

    giveDemographicAccess = () => {
        const {selected_sub_access, access_type} = this.state;
        let access_data = [];
        let selected_data = {}
        if(access_type === 2){
            let access = {};
            access['intrablock_operator'] = 'AND'
            Object.keys(selected_sub_access).forEach((access) => {
                if(selected_sub_access[access].length > 0){
                    selected_data[access] = selected_sub_access[access];
                }
            })

            access['permissions'] = selected_data;
            access_data.push(access);
        }
        this.props.updateStep(4);
        this.props.saveDemographicAccessData(access_data);
    }

    loadMore = (key) => {
        const { access_levels } = this.state;
        if(access_levels[key].show_only < access_levels[key].options.length){
            access_levels[key].show_only += 50;
            this.setState({access_levels});
        }
    }

    onSearch = (e) => {
        const {access_levels} = this.state;
        const key = e.target.name;
        access_levels[key].search_text = e.target.value;
        if(access_levels[key].search_text.length > 0) {
            const results = access_levels[key].options.filter(option => option.title.toLowerCase().includes(access_levels[key].search_text.toLowerCase()));
            access_levels[key].search_options = results;
        } else if(!access_levels[key].search_text.length) {
            let selected_on_top_access_levels = this.setSelectedOnTop(access_levels, key);
            access_levels[key].search_options = JSON.parse(JSON.stringify(selected_on_top_access_levels[key].search_options))
        }
        access_levels[key].show_only = 50;
        this.setState({access_levels});
    }

    selectOption = (key, title, previous_selected = false) => {
        const {access_levels, total_selected, initial_selected_sub_access, selected_sub_access} = this.state;
        let flag = false;
        const state = (access_levels[key].selected_options[title] ? access_levels[key].selected_options[title] : false)
        // ----------------------------
        // to check enable disable next
        if(previous_selected){
        }

        if(selected_sub_access[key]){
            if(selected_sub_access[key].indexOf(title) === -1){
                selected_sub_access[key].push(title);
                flag = true;
            }
            else{
                let list = selected_sub_access[key];
                const access_index = list.indexOf(title)
                if(access_index > -1){
                    list.splice(access_index, 1);
                }
                selected_sub_access[key] = list;
            }
        }
        else{
            flag = true;
            selected_sub_access[key] = [title];
        }
        // to check enable disable next end
        // --------------------------------
        access_levels[key].selected_options[title] = !state;
        if(flag === true){
            access_levels[key].selected_count = (++access_levels[key].selected_count);
            this.setState({total_selected : (total_selected + 1)});
        }
        else{
            access_levels[key].selected_count = (--access_levels[key].selected_count);
            this.setState({total_selected : (total_selected - 1)});
        }

        this.setState({
            access_levels, 
            initial_selected_sub_access, 
            selected_sub_access
        });
    }

    setAccessType = (access_type) => {
        this.setState({
            access_type
        })
    }

    render(){
        const {access_levels, access_type, total_selected} = this.state;
        const {previous_step} = this.props;
        return (
            <div className="card full-height box-blue-border mg-top-15 mg-bottom-20">
                <div className="card-content">
                    <div className="columns flex-wrap">
                        <div className="column is-12">
                            <span className="text-access has-text-weight-bold is-size-6">Access Type</span><br/>
                            <div className="columns no-padding-left">
                                <div className="column is-5 no-padding-left">
                                    <label className="radio pointer has-text-centered">
                                        <input type="radio" value="full" name="access" checked={access_type === 1 ? true : false} onChange={this.setAccessType.bind(this, 1)}/> 
                                        <span className="is-size-6 mg-left-10">
                                            Full Demographic Access
                                        </span>
                                    </label>
                                </div>
                                <div className="column is-5">
                                    <label className="radio pointer has-text-centered">
                                        <input type="radio" value="partial" name="access" checked={access_type === 2 ? true : false} onChange={this.setAccessType.bind(this, 2)}/> 
                                        <span className="is-size-6 mg-left-10">
                                            Partial Demographic Access
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        {access_type === 2 ?
                            <div className="column is-12 columns flex-wrap">
                                <span className="text-access is-size-7 column is-12 no-padding">DEMOGRAPHS</span>
                                    {Object.entries(access_levels).map(
                                        ([key, value]) => (
                                            (value.options.length ? <div key={key} className="column is-4">
                                                <SelectDropdown 
                                                    access_data={value} 
                                                    access_key={key} 
                                                    onSearch={this.onSearch} 
                                                    loadMore={this.loadMore} 
                                                    selectOption={this.selectOption}
                                                    show_title={true}
                                                    extra_info={{}}/>
                                            </div> : null)
                                        )
                                    )}
                            </div> : null}
                        <div className="column is-12 no-padding-top no-padding-bottom">
                            <div className="">
                                <button id="ag-save-demographs" className={classnames('button is-small is-outlined padding-left-right-20')} onClick={this.props.updateStep.bind(this,previous_step)}>Previous</button>
                                <button id="ag-next-demographs" onClick={this.giveDemographicAccess} className={classnames('button is-primary is-small mg-left-10 padding-left-right-20')} disabled={(total_selected === 0 && access_type === 2)}>Next</button>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    admin_list: state.access.admin_list
  })

export default connect(mapStateToProps, {getCategoriesForDemographics, getUserCategoriesForDemographics, saveDemographicAccessData})(CustomizeDemographicAccess);

