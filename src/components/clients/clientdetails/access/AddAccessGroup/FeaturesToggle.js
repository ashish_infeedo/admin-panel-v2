import React from 'react'
import Switch from "react-switch";
import classnames from 'classnames';

export default function FeaturesToggle({
    features, 
    access_type,
    toggleFeature
}) {
    return (
        <div className="features-section ">
            <ul className="columns flex-wrap">
            {
                Object.keys(features).map((feat) => (
                        <li className="column is-6 is-size-7">
                            <Switch className="marginright-md switch" onChange={toggleFeature.bind(this, feat, access_type)} checked={features[feat]} disabled={false}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={15} width={35}/>
                            <span className="">{feat.replace(/_/g, " ")}</span> 
                        </li>
                ))
            }
            </ul>
        </div>
    )
}
