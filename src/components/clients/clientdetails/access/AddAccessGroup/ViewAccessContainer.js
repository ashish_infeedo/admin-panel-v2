import React, { Component } from 'react'
import ReactTooltip from 'react-tooltip';
import classnames from 'classnames'
import { connect } from 'react-redux';

import CreateAccessGroupModal from '../AddAccessGroup/CreateAccessGroupModal'
import {deleteAccessGroups} from '../../../../../actions/AccessActions';
import ConfirmModal from '../../../../generic/ConfirmModal';

class AddAdminContainer extends Component {
    state = {
        is_creation_modal_active : false,
        step_no: 1,
        delete_modal_active: false,
        deleting: false,
        current_access: {}
    }

    componentDidMount () {
    }
    
    closeAccessModal = _ => {
        this.setState({
            is_creation_modal_active: false,
        }, () => {this.props.getAccessGroupsList()});
    }

    showAccessGroupCreationModal = _ => {
        this.setState({
            is_creation_modal_active: true,
            is_edit: false,
            current_access: {}
        });
    }
    
    closeConfirmModal = _ => {
        this.setState({
            delete_modal_active: false,
            current_access: {},
            deleting: false
        });
    }

    openDeleteConfirmModal = (current_access) => {
        if(current_access.admin_count === 0) {
        this.setState({
            delete_modal_active: true,
            current_access
        })
    }
    }

    deleteAccess = async () => {
        const {current_access} = this.state;
        const {client_id} = this.props;
        this.setState({
            deleting: true
        })
        const response = await this.props.deleteAccessGroups(client_id, current_access.id);
        if(response && response.status === 200){
            this.props.getAccessGroupsList();
        }
        this.closeConfirmModal();
    }

    editAccessGroups = (current_access) => {
            this.setState({
                is_creation_modal_active: true,
                is_edit: true,
                current_access
            })
    }

    render() {
        const {is_creation_modal_active, deleting, delete_modal_active, is_edit, current_access} = this.state
        const {access_groups, client_id}  = this.props
        return (
            <div className="new-admin-container">
                <p className="has-text-weight-semibold is-6 level">
                    {access_groups.length} Access Groups 
                    <button className="button is-primary level-right is-small" onClick={this.showAccessGroupCreationModal}>Add Access Group</button>
                </p>
                <div className="table-container">
                    <table className="table is-striped is-fullwidth is-size-7">
                        <thead>
                            <tr>
                                <th>
                                    Access Group
                                </th>
                                <th>No. Of Admins</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                            <tbody>
                                {access_groups.map(group => (
                                    <tr>
                                        <td width="10">
                                            {group.name}
                                        </td>
                                        <td width="10">
                                            {group.admin_count}
                                        </td>
                                        <td width="10">
                                            <span className="has-text-info text-underline cursor-pointer" onClick={this.editAccessGroups.bind(this, group)}>
                                                Edit
                                            </span>
                                        </td>
                                        <td width="10">
                                            <span data-for={`tooltip-${group.id}`} data-tip={`Access Group is currently being used by ${group.admin_count} admins. Please revoke their access before deleting.`} className={classnames('', {'has-text-info text-underline cursor-pointer': group.admin_count === 0, 'is-disabled': group.admin_count > 0})}  onClick={this.openDeleteConfirmModal.bind(this, group)}>
                                                Delete
                                            </span>
                                                {
                                                    group.admin_count ?
                                                    <ReactTooltip id={`tooltip-${group.id}`} place="top" effect="solid"/>
                                                    : null
                                                }
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                { is_creation_modal_active ? 
                    <CreateAccessGroupModal
                        is_active = {is_creation_modal_active } 
                        onClose = {this.closeAccessModal}
                        client_id={client_id}
                        edit_access={is_edit}
                        header_text={is_edit ? "Edit Access Group" : "Create Access Group"}
                        current_access={current_access}/> : null}
                {delete_modal_active ?
                    <ConfirmModal
                        text="Are you sure you want to delete?"
                        title="Delete Access Group"
                        confirm_text="Yes"
                        doing={deleting}
                        is_active={delete_modal_active}
                        onConfirm={this.deleteAccess}
                        onClose={this.closeConfirmModal}/> : null}
            </div>
        )
    }
}

export default connect(null, {deleteAccessGroups})(AddAdminContainer)