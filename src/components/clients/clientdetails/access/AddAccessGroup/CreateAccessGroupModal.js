import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import Switch from "react-switch";
import ReactTooltip from 'react-tooltip';
import {saveAccessGroups} from '../../../../../actions/AccessActions';

class CreateAccessGroupModal extends Component {

    state = {
        role_demographic_toggle : 1,
        group_name : '',
        group_name_error: '',
        access_name: [],        
        disable_name: false,
        save_group_in_progress: false,
        access_groups: {
            'direct_level': false,
            'skip_level': false,
            'demographic_wise': true,
            'hierarchy_wise': false
        },
        features_list : {
            'PEOPLE_TO_MEET' : 'People to Meet (PTM)',
            'EXIT' : 'Exit Module',
            'CRISIS_MANAGEMENT' : 'Crisis Management',
            'DASHBOARD_SETUP' : 'Setup Page',
            'EMPLOYEE_CHATS' : 'Employee Chats',
            'UNIVERSAL_FILTERS' : 'Dashboard Filters',
            'PII' : "PII'S",
            'EXPORTS' : "Data Exports",
            'RESPONSE_RATE' : 'Response Rate',
            'ADMINS_CHAT_ACCESS' : 'Admin Chat Access',
            'ANONYMOUS_BAT' : 'Anonymous Messages',
            'MOMENTS_THAT_MATTER': 'Moments that Matter',
            'CONFIGURE_REPORT': 'Configure Report',
            'PULSE': 'Custom Check-In',
            'TEXT_SENS' : 'TextSens',
            'DRIVER_ELEMENT' : 'Drivers & Elements',
            'ENGAGEMENT_SCORE' : 'Engagement Score',
        },
        features: {
            'PEOPLE_TO_MEET': 1,
            'EXIT': 1,
            'DRIVER_ELEMENT': 1,
            'UNIVERSAL_FILTERS': 1,
            'CRISIS_MANAGEMENT': 1,
            'DASHBOARD_SETUP': 1,
            'EMPLOYEE_CHATS': 1,
            'MOMENTS_THAT_MATTER': 1,
            'PULSE': 1,
            'ANONYMOUS_BAT': 1,
            'ENGAGEMENT_SCORE': 1,
            'PII': 1,
            'CONFIGURE_REPORT': 1,
            'EXPORTS': 1,
            'RESPONSE_RATE': 1,
            'TEXT_SENS': 1,
            'ADMINS_CHAT_ACCESS': 1
        },
        features_status: {
            demographic_wise: {
                'PEOPLE_TO_MEET': 1,
                'EXIT': 1,
                'CRISIS_MANAGEMENT': 1,
                'DASHBOARD_SETUP': 1,
                'EMPLOYEE_CHATS': 1,
                'UNIVERSAL_FILTERS': 1,
                'PII': 1,
                'CONFIGURE_REPORT': 1,
                'EXPORTS': 1,
                'RESPONSE_RATE': 1,
                'ADMINS_CHAT_ACCESS': 1,
                'ANONYMOUS_BAT': 1,
                'MOMENTS_THAT_MATTER': 1,
                'PULSE': 1,
                'ENGAGEMENT_SCORE': 1,
                'DRIVER_ELEMENT': 1,
                'TEXT_SENS': 1
            }
        },
        access_type:{
            'direct_level': 'Dashboard Features (Direct Level)',
            'skip_level': 'Dashboard Features (Skip Level)',
            'demographic_wise': 'Dashboard Features',
            'hierarchy_wise': 'Dashboard Features'
        },
        hierarchy_access_toggle: false,
        direct_level: true,
        skip_level: true,
        current_access: {},
        is_filters_enabled: true,
        client_dependent_features: {
            'EXIT' : 'exit_enable',
            'CRISIS_MANAGEMENT' : 'crisis_enable',
            'ANONYMOUS_BAT' : 'enable_anonymous_bat',
            'PULSE': 'activate_pulse',
            'MOMENTS_THAT_MATTER': 'activate_mtm',
            'CONFIGURE_REPORT': 'instant_config_report'
        },
        disabled_features: [],
        dependent_features: {
            'PII' : ['UNIVERSAL_FILTERS', 'EXPORTS', 'CONFIGURE_REPORT'],
            'EXPORTS' : ['CONFIGURE_REPORT'],
            'UNIVERSAL_FILTERS': ['CONFIGURE_REPORT']
        },
    }

    componentDidMount(){
        const {edit_access} = this.props;
        this.checkForDisableFeatures();
        if(edit_access === true){
            this.setAccessData();
        }
        else {
            this.setFeaturesAccess();
        }
        this.setValidationsForModal();
    }

    componentDidUpdate(prevProps){
        const {client_details} = this.props;
        if(JSON.stringify(prevProps.client_details) !== JSON.stringify(client_details)){
            this.checkForDisableFeatures();
        }
    }

    checkForDisableFeatures = () => {
        const {client_details} = this.props;
        let {disabled_features, client_dependent_features, features} = this.state;
        if(client_details){
            Object.keys(client_dependent_features).forEach((feature_name) => {
                if(!client_details[client_dependent_features[feature_name]]){
                    disabled_features.push(feature_name);
                    features[feature_name] = 0;
                }
            })
            this.setState({
                disabled_features: JSON.parse(JSON.stringify(disabled_features)),
                features: JSON.parse(JSON.stringify(features))
            })
        }
    }

    setFeaturesAccess = () => {
        let {features_status, features} = this.state;
        Object.keys(features_status).forEach((feature) =>{
            features_status[feature] = features
        })
        this.setState({
            features_status: JSON.parse(JSON.stringify(features_status))
        })
    }


    setValidationsForModal = () => {
        const {access_list} = this.props;
        let access_name = [];
        access_list.forEach((access) => {
            access_name.push(access.name);
        })
        this.setState({
            access_name
        })
    }

    setAccessData = () => {
        const {current_access} = this.props;
        let {access_groups}= this.state;
        if(current_access.permissions){
            Object.keys(access_groups).map((access) => {
                if(current_access.permissions[access]){
                    access_groups[access] = true;
                }
                else{
                    access_groups[access] = false;
                }
            })
        }
        this.setState({
            role_demographic_toggle: current_access.demographic_toggle ? 1 : 0,
            group_name: current_access.name ? current_access.name : "",
            access_groups: JSON.parse(JSON.stringify(access_groups)),
            features_status: JSON.parse(JSON.stringify(current_access.permissions))
        })
    }

    setRoleName = (e) => {
        let {group_name_error, disable_name, access_name} = this.state;
        if(e.target.value.length < 2 && e.target.value.length > 0){
            group_name_error = "";
            disable_name = true;
        }
        else if(e.target.value.length === 0){
            group_name_error = "Please enter a Name to proceed";
            disable_name = true;
        }
        else if(access_name.includes(e.target.value.toLocaleLowerCase())){
            group_name_error = "Access group already exists";
            disable_name = true;
        }
        else if(e.target.value.length > 0 && e.target.value.trim() === ""){
            group_name_error = "Enter a valid name";
            disable_name = true;
        }
        else{
            group_name_error = "";
            disable_name = false;
        }

        this.setState({
            group_name: e.target.value,
            group_name_error,
            disable_name
        })
    }

    toggleDemographicView = (access) => {
        const {role_demographic_toggle, access_groups, direct_level, skip_level} = this.state;
        let {hierarchy_access_toggle} = this.state;
        access_groups[access] = !access_groups[access];
        if(access_groups[access]){
            access_groups['direct_level']= false;
            access_groups['skip_level']= false;
            access_groups['hierarchy_wise']= false;
            hierarchy_access_toggle = false;
        }
        else{
            access_groups['direct_level']= direct_level;
            access_groups['skip_level']= skip_level;
            access_groups['hierarchy_wise']= hierarchy_access_toggle;
        }

        this.setState({
            role_demographic_toggle: role_demographic_toggle ? 0 : 1,
            access_groups: JSON.parse(JSON.stringify(access_groups)),
            hierarchy_access_toggle
        }, () => {
            this.setPermissionsForAccessGroup();
        })
    }

    setPermissionsForAccessGroup = () => {
        const {features, access_groups} = this.state;
        let features_status = {}
        Object.keys(access_groups).map((access) => {
            if(access_groups[access]){
                features_status[access] = features;
            }
        })

        this.setState({
            features_status: JSON.parse(JSON.stringify(features_status))
        })
    }

    saveAccessGroup = async () => {
        const {access_groups, features_status, group_name, role_demographic_toggle} = this.state;
        const {edit_access, current_access} = this.props;
        let total_views = 0;
        let permissions = {};
        this.setState({
            save_group_in_progress: true
        })

        
        Object.keys(access_groups).forEach((access) => {
            if(access_groups[access]){
                total_views = total_views + 1;
            }
        })
        let data = {
            total_views : total_views,
            name: group_name,
            permissions: features_status,
            demographic_toggle : role_demographic_toggle
        }
        if(current_access && current_access.id) {
            data.id = current_access.id
        }
    
        const response = await this.props.saveAccessGroups(this.props.client_id, data, edit_access);

        this.setState({
            save_group_in_progress: false
        })

        if(response && response.status === 200){
            this.props.onClose(true);
        }

    }   

    toggleHierarchyAccess = (access) => {
        const {hierarchy_access_toggle, access_groups, direct_level, skip_level} = this.state;
        let {role_demographic_toggle} = this.state;
        access_groups[access] = !access_groups[access];
        if(access_groups[access]){
            access_groups['demographic_wise']= false;
            access_groups['direct_level']= false;
            access_groups['skip_level']= false;
            role_demographic_toggle= 0;
        }
        else{
            access_groups['direct_level']= direct_level;
            access_groups['skip_level']= skip_level;
        }

        this.setState({
            hierarchy_access_toggle: access_groups[access] ? 0 : 1,
            access_groups: JSON.parse(JSON.stringify(access_groups)),
            role_demographic_toggle
        }, () => {
            this.setPermissionsForAccessGroup();
        })
    }

    selectSkipHierarchyAccess = (access) => {
        const {access_groups, features} = this.state;
        let {features_status} = this.state;
        access_groups[access] = !access_groups[access];
        if(access_groups[access] === false && features_status[access]){
            delete features_status[access];
        }
        else{
            features_status[access] = features;
        }
        this.setState({
            skip_level: access_groups[access],
            features_status: JSON.parse(JSON.stringify(features_status)),
            access_groups
        })
    }

    selectDirectHierarchyAccess = (access) => {
        const {access_groups, features} = this.state;
        let {features_status} = this.state;
        access_groups[access] = !access_groups[access];
        if(access_groups[access] === false && features_status[access]){
            delete features_status[access];
        }
        else{
            features_status[access] = features;
        }
        this.setState({
            direct_level: access_groups[access],
            features_status: JSON.parse(JSON.stringify(features_status)),
            access_groups
        })
    }

    giveAccessForFeatures = (feature, access) => {
        let {features_status, dependent_features} = this.state;
        if(features_status[access]){
            features_status[access][feature] = features_status[access][feature] ? 0 : 1;
            if(dependent_features[feature]){
                let d_list = dependent_features[feature];
                d_list.forEach((name) => {
                    features_status[access][name] = features_status[access][feature] ? 1 : 0;
                    if(name === 'CONFIGURE_REPORT'){
                        features_status[access][name] = features_status[access]['EXPORTS'] && features_status[access]['UNIVERSAL_FILTERS'] ? 1 : 0;
                    }
                })
            }
            this.setState({
                features_status: JSON.parse(JSON.stringify(features_status))
            })
        }

    }

    render(){
        const {is_active, header_text, edit_access} = this.props
        const {access_groups, group_name, role_demographic_toggle, features_list, features_status, access_type, group_name_error, disable_name, save_group_in_progress, disabled_features} = this.state
        return (
            <div className={classnames('modal create-access-group-modal', { 'is-active': is_active})}>
            <div className="modal-background" onClick={this.props.onClose}></div>
            <div className="modal-card">
                <header className="modal-card-head">
                    <p className="modal-card-title is-size-5">{header_text}</p>
                    <button onClick={this.props.onClose} className="delete" aria-label="close"></button>
                </header>
                <section className="modal-card-body">
                    <p className="has-text-grey-lighter is-size-7 marginbottom-xs is-uppercase">ACCESS GROUP NAME:*</p>
                    <div className="field">
                        <div className="control">
                            <input 
                                autoFocus 
                                onChange={this.setRoleName} 
                                className="input" 
                                type="text" 
                                placeholder="Access Group Name" 
                                value={group_name}/>
                                <p className="error-save-view is-size-7 has-text-danger">{group_name_error}</p>

                        </div>
                    </div>
                    <hr className="margintop-md marginbottom-md"/>
                    <div className="is-size-7">
                            <strong>Allow demographic customization</strong> while giving admin access
                            <Switch 
                                className="marginleft-sm switch" 
                                onChange={this.toggleDemographicView.bind(this, 'demographic_wise')}
                                checked={role_demographic_toggle ? true : false} 
                                disabled={false}  
                                uncheckedIcon={false} 
                                checkedIcon={false} 
                                onColor={'#23d160'} 
                                disabled={edit_access ? true : false} 
                                height={15} 
                                width={35}/>
                    </div>
                    {role_demographic_toggle ? null :
                        <React.Fragment>
                            <hr className="margintop-md marginbottom-md"/>
                            <div className="is-size-7">
                                <strong>Allow Same hierarchy</strong>
                                    <Switch 
                                        className="marginleft-sm switch" 
                                        onChange={this.toggleHierarchyAccess.bind(this, 'hierarchy_wise')} 
                                        checked={access_groups['hierarchy_wise']}  
                                        disabled={false}  
                                        uncheckedIcon={false} 
                                        checkedIcon={false} 
                                        onColor={'#23d160'} 
                                        height={15} 
                                        width={35}/>
                            </div>
                        </React.Fragment>}
                        {!access_groups['hierarchy_wise'] && !role_demographic_toggle ? 
                            <div className="is-size-7">
                                <strong>Choose hierarchy for feature customization</strong>
                                <label className="checkbox marginleft-sm ">
                                    <input 
                                        type="checkbox" 
                                        checked={access_groups['direct_level']} 
                                        onChange={this.selectDirectHierarchyAccess.bind(this, 'direct_level')}
                                        className="marginright-xs checkbox"/>
                                    Direct
                                </label>
                                <label className="checkbox marginleft-sm ">
                                    <input 
                                        type="checkbox" 
                                        checked={access_groups['skip_level']} 
                                        onChange={this.selectSkipHierarchyAccess.bind(this, 'skip_level')} 
                                        className="marginright-xs checkbox"/>
                                    Skip
                                </label>
                            </div>
                            : ''
                        }
                    <hr className="mg-bottom-15 mg-top-20"/>
                    <div className="level-access">
                        { Object.keys(access_groups).map((access, index) => (
                            access_groups[access] ?
                                <div className={classnames('no-margin columns flex-wrap', {'mg-top-30': index !== 0})}>
                                    <h6 className=" column no-padding has-text-info has-text-weight-semibold is-12 marginbottom-sm">{access_type[access]}</h6>
                                    {Object.keys(features_list).map((feature) => (
                                        disabled_features.indexOf(feature) !== -1 ? null :
                                        <div className="column is-6 line-height-1 no-padding mg-bottom-10">
                                            <span data-tip="Feature cannot be toggled off because it is default." data-for={`${features_list[feature]}`}> 
                                                <Switch 
                                                    onChange={this.giveAccessForFeatures.bind(this, feature, access)} 
                                                    checked={features_status[access] && features_status[access][feature] ? true : false} 
                                                    uncheckedIcon={false} 
                                                    checkedIcon={false} 
                                                    onColor={'#4a8cb2'} 
                                                    className="marginright-md"
                                                    height={15} 
                                                    width={30} 
                                                    disabled={feature === 'DRIVER_ELEMENT' || feature === 'ENGAGEMENT_SCORE' || feature === 'TEXT_SENS' || ((feature === 'UNIVERSAL_FILTERS' || feature === 'EXPORTS') && (features_status[access] && !features_status[access]['PII'])) || ((feature === 'CONFIGURE_REPORT') && (features_status[access] && (!features_status[access]['EXPORTS'] || !features_status[access]['UNIVERSAL_FILTERS'])))}
                                                    />
                                            </span>
                                            {
                                                feature === 'DRIVER_ELEMENT' || feature === 'ENGAGEMENT_SCORE' || feature === 'TEXT_SENS' ?
                                                <ReactTooltip id={`${features_list[feature]}`} place="top" effect="solid"/> : null
                                                
                                            }
                                            <span className="font-14">{features_list[feature]}</span> 
                                        </div>
                                    ))}
                                </div> : null
                        ))}
                    </div>
                </section>
                <footer className="modal-card-foot">
                    <button className={classnames('button is-success', {'is-loading': save_group_in_progress})} disabled={disable_name || Object.keys(features_status).length === 0 ? true : false} onClick={this.saveAccessGroup}>Save changes</button>
                    <button className="button" onClick={this.props.onClose.bind(this, true)}>Cancel</button>
                </footer>
            </div>
        </div>
        )
    }
}

const mapStateToProps = (state) => ({
    access_list: state.access.access_groups,
    client_details: state.client.client_details,
  })

export default connect(mapStateToProps, {saveAccessGroups})(CreateAccessGroupModal);