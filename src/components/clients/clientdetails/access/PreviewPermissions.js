import React, { Component } from 'react'

export default class PreviewPermissions extends Component {
    render() {
        const {permissions, access_type, is_bat_enabled, selected_member, selected_driver_elements, driver_map, element_map} = this.props;
        return (
            <div>
                <h3 className="has-text-weight-bold is-capitalized mg-10">User</h3>
                <hr className="is-marginless"/>
                <div className="columns flex-wrap">
                    <div className="column is-6 no-padding-bottom">
                        <h3 className="has-text-weight-bold is-capitalized margin-subhead is-size-7">Name</h3>
                        <p className="access-p">{selected_member.name}</p>
                    </div>
                    <div className="column is-6 no-padding-bottom">
                        <h3 className="has-text-weight-bold is-capitalized margin-subhead is-size-7">Email</h3>
                        <p className="access-p">{selected_member.email}</p>
                    </div>
                </div>
                <hr className="is-marginless"/>
                <h3 className="has-text-weight-bold is-capitalized mg-10">Access Types</h3>
                <hr className="is-marginless"/>
                {access_type === 1 ? Object.entries(permissions).map(([key, value], index) => (
                    (value.length ? 
                    <div key={key}>
                        <h3 className="has-text-weight-bold is-capitalized margin-subhead is-size-7">{key}</h3>
                            <div className="tags is-marginless mg-10">
                                {value.map((access, index) => (
                                    <span key={index} className="tag is-warning is-rounded mg-tag-access">{access}</span>
                                ))}
                            </div>
                    </div>: null)
                )): (access_type === 0 ? <div className="tags is-marginless mg-10">
                        <span className="tag is-primary is-rounded mg-10">Full Access</span>
                    </div> 
                    : (
                        Object.entries(selected_driver_elements).map(([key, value], index) => ( 
                            <div key={index}>
                                <h3 className="has-text-weight-bold is-capitalized mg-10 manager-heading">Driver: {driver_map[key]}</h3>
                                <div className="tags is-marginless mg-10">
                                <h3 className="has-text-weight-bold is-capitalized margin-subhead is-size-7">Elements:</h3>
                                {Object.entries(value.selected_elements).map(([key, value], index) => ( 
                                    <span key={index} className="tag is-warning is-rounded mg-tag-access">{element_map[key]}</span>
                                ))}
                                 </div>
                            </div> 
                        ))
                    ))
                }
                <br/>
                {access_type !== 2 ? <div>
                    <hr className="is-marginless"/>
                    <h3 className="has-text-weight-bold is-capitalized mg-10">Features</h3>
                    <hr className="is-marginless"/>
                    <div className="columns flex-wrap">
                        <div className="column is-6 no-padding-bottom">
                            <h3 className="has-text-weight-bold is-capitalized margin-subhead is-size-7">Anonymous Bat</h3>
                            <p className="access-p">{is_bat_enabled === true ? <span className="has-text-success">Enabled</span> : <span className="has-text-danger">Disbaled</span>}</p>
                        </div>
                    </div>
                </div> :  null}
            </div>
        )
    }
}
