import React, { Component } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// components
import GiveAccessSteps from './GiveAccessSteps';
import GiveAccessStep1 from './GiveAccessStep1';
import GiveAccessStep2 from './GiveAccessStep2';
// actions
import {getMemberUsers} from '../../../../actions/AccessActions';

class GiveAccessModal extends Component {

    state = {
        giving_access: false,
        loading_members: false,
        members: [],
        selected_member_email: '',
        selected_member: {},
        disable_next: true,
        disable_previous: true,
        step_number: 0
    }

    componentDidMount(){
        this.getMemberUsers();
    }

    componentDidUpdate(prevProps) {
        const { is_active, is_edit } = this.props;
        if(prevProps.is_active !== is_active && is_active === false){
            this.setState({
                step_number: 0,
                disable_next: true,
                disable_previous: true,
                selected_member_email: '',
            })
        }
        if(prevProps.is_active !== is_active && is_active === true){
            if(is_edit){
                this.setState({step_number: 1});
            }
        }
    }

    getMemberUsers = async () => {
        const { client_id } = this.props;
        this.setState({loading_members: true});
        const response = await this.props.getMemberUsers(client_id);
        this.setState({loading_members: true, members: response});
    }

    selectMember = (member) => {
        this.setState({selected_member_email: member.email, selected_member: member});
    }

    disableNext = (value) => {
        this.setState({disable_next: value});
    }

    onNext = () => {
        let {step_number} = this.state;
        step_number +=1;
        if(step_number > 0) {
            this.setState({disable_previous: false});
        } else {
            this.setState({disable_previous: true});
        }
        if(step_number === 2){
            this.disableNext(true);
        }
        this.setState({step_number});
    }

    loadingNext = (value) => {
        this.setState({giving_access: value});
    }

    onPrevious = () => {
        let {step_number} = this.state;
        step_number -=1;
        if(step_number > 0) {
            this.setState({disable_previous: false});
        } else {
            this.setState({disable_previous: true});
        }
        this.setState({step_number});
    }

    render() {
        const { giving_access, members, disable_next, disable_previous, step_number, selected_member_email, selected_member} = this.state;
        const { onClose, is_active, client_id, admin, admin_type, is_edit } = this.props;
        return (
            <div className={classnames('modal',{'is-active': is_active})}>
                <div className="modal-background"></div>
                <div className="modal-card">
                    <header className="modal-card-head pd-10">
                    <GiveAccessSteps is_edit={is_edit} step_number={step_number} selected_member_email={selected_member_email}/>
                    </header>
                    <section className="modal-card-body">
                        {step_number === 0 && !is_edit ? <GiveAccessStep1 members={members} selectMember={this.selectMember} disableNext={this.disableNext}/> : null}
                        {step_number > 0 || is_edit ? <GiveAccessStep2 client_id={client_id} selected_member={is_edit ? admin : selected_member} is_edit={is_edit} admin_type={admin_type} step_number={step_number} disableNext={this.disableNext} loadingNext={this.loadingNext} closeModal={this.props.onClose} getAdminsOrSubAdminsOrManagers={this.props.getAdminsOrSubAdminsOrManagers}/> : null}
                    </section>
                    <footer className="modal-card-foot pd-10">
                        <button onClick={onClose} className="button">Cancel</button>
                        <button onClick={this.onPrevious} className="button" disabled={disable_previous}>Previous</button>
                        <button onClick={this.onNext} className={classnames('button is-success',{'is-loading': giving_access})} disabled={disable_next}>{step_number !== 2 ? 'Next' : 'Give Access'}</button>
                    </footer>
                </div>
            </div>
        )
    }
}


// PropTypes
// ---------
GiveAccessModal.propTypes = {
    onClose: PropTypes.func.isRequired,
    is_active: PropTypes.bool.isRequired,
    client_id: PropTypes.number.isRequired,
    admin: PropTypes.object.isRequired,
    admin_type: PropTypes.number.isRequired,
    getAdminsOrSubAdminsOrManagers: PropTypes.func.isRequired,
    is_edit: PropTypes.bool.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    
})
  
export default connect(mapStateToProps, {getMemberUsers})(GiveAccessModal);