import React, { Component } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';
import { withRouter } from 'react-router-dom';
// components
import TextInputgroup from '../../generic/TextInputGroup';
import ConfirmModal from '../../generic/ConfirmModal';
import {looseFocus} from '../../../helper/LooseFocus';
// actions
import { getCohorts, deleteCohort, addCohorts, editCohort } from '../../../actions/ClientsActions';

class Cohorts extends Component {
  
  state = {
    client_id: null,
    search: '',
    loading: true,
    deleting: false,
    adding: false,
    editing: false,
    cohorts: [],
    original_cohorts: [],
    new_cohorts: '',
    edit_cohort_text: '',
    edit_cohort_id: null,
    total_new_cohorts: 0,
    delete_modal: {
      title: 'Delete Cohort',
      text: 'Are you sure you want to delete?',
      confirm_text: 'Yes',
      is_active: false
    },
    selected_cohort: {},
    disable_add: true
  }

  componentDidMount(){
    const { client_id } = this.props.match.params;
    this.setState({client_id: client_id});
    this.getCohorts(client_id);
  }

  getCohorts = async (client_id) => {
    const response = await this.props.getCohorts(client_id);
    this.setState({loading: false, cohorts: response || [], original_cohorts: response || []});
  }

  selectEditCohort = (cohort) => {
    this.setState({edit_cohort_text: cohort.title, edit_cohort_id: cohort.id});
  }

  editCohort = async (cohort) => {
    const id = cohort.id;
    const { edit_cohort_text, client_id } = this.state;
    looseFocus();
    this.setState({editing: true});
    await this.props.editCohort(client_id, id, edit_cohort_text);
    this.getCohorts(client_id);
    this.setState({edit_cohort_text: '', edit_cohort_id: null});
    this.setState({editing: false});
    looseFocus();
  }

  cancelEdit = () => {
    this.setState({edit_cohort_text: '', edit_cohort_id: null});
    looseFocus();
  }

  onSearch = (e) => {
    this.setState({ [e.target.name]: e.target.value });
    const search = e.target.value;
    const { original_cohorts } = this.state;
    const cohorts = original_cohorts;
    if(search.length > 0) {
        const results = cohorts.filter(cohort => cohort.title.toLowerCase().includes(search.toLowerCase()));
        this.setState({cohorts: results});
    } else if(!search.length) {
        this.setState({cohorts: original_cohorts});
    }
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  onChangeCohorts = (e) => {
    this.onChange(e);
    const cohorts = e.target.value;
    if(cohorts.length > 0) {
      let cohorts_array = cohorts.split(',');
      if (cohorts_array[cohorts_array.length - 1] === "") {
        cohorts_array.pop();
      }
      const count = cohorts_array.length;
      this.setState({total_new_cohorts: count, disable_add: false});
    } else {
      this.setState({total_new_cohorts: 0, disable_add: true});
    }
  }

  confirmDelete = async () => {
    const { id } = this.state.selected_cohort;
    const { client_id } = this.state;
    this.setState({deleting: true});
    await this.props.deleteCohort(id);
    this.closeDeleteModal();
    this.getCohorts(client_id);
    this.setState({deleting: false});
  }

  openDeleteModal = (cohort) => {
    const { delete_modal } = this.state;
    delete_modal.title += ` ${cohort.title}`;
    delete_modal.is_active = true;
    this.setState({delete_modal: delete_modal, selected_cohort: cohort});
  }

  closeDeleteModal = () => {
    const { delete_modal } = this.state;
    delete_modal.is_active = false;
    delete_modal.title = 'Delete Cohort';
    this.setState({delete_modal: delete_modal});
  }

  onSubmit = async (e) => {
    e.preventDefault();
    const { client_id, new_cohorts } = this.state;
    this.setState({adding: true});
    await this.props.addCohorts(client_id, new_cohorts);
    this.getCohorts(client_id);
    this.setState({adding: false, new_cohorts: '', disable_add: true, total_new_cohorts: 0});
  }

  render() {
    const { search, cohorts, new_cohorts, loading, deleting, editing, adding, delete_modal, edit_cohort_text, edit_cohort_id, total_new_cohorts, disable_add } = this.state;
    return (
      <div>
      <div className="columns">
        <div className="column is-3">
        <TextInputgroup
            name="search"
            placeholder="Search Cohorts"
            value={search}
            type="text"
            onChange={this.onSearch}
            icon="fas fa-search"
        />
        </div>
        <div className="column is-8">
          <form onSubmit={this.onSubmit} className="columns">
            <div className="column is-10">
            <TextInputgroup
                name="new_cohorts"
                placeholder="Add Comma Separated Cohorts"
                value={new_cohorts}
                type="text"
                onChange={this.onChangeCohorts}
                icon="fas fa-users"
            />
            </div>
            <div className="column is-3 pd-up-20">
              <button type="submit" className={classnames('button is-success is-fullwidth',{'is-loading': adding})} disabled={disable_add}>
                <span className="icon">
                  <i className="fas fa-plus"></i>
                </span>
                <span>Add ({total_new_cohorts})</span>
              </button>
            </div>
          </form>
        </div>
      </div>
      <div className="card">
      <div className="card-content is-paddingless">
        <div className="content">
          <div className="custom-table">
            <table className="table is-striped">
              <thead>
                  <tr>
                  <th>S.No.</th>
                  <th>Cohort</th>
                  <th>Created On</th>
                  <th>Actions</th>
                  </tr>
              </thead>
              <tbody>
              {!loading ? cohorts.map((cohort, index) => (
                <tr key={index}>
                <td>{index + 1}</td>
                <td className={classnames({'is-paddingless': cohort.id === edit_cohort_id})}>
                { cohort.id === edit_cohort_id ?
                <TextInputgroup
                    name="edit_cohort_text"
                    placeholder=""
                    value={edit_cohort_text}
                    type="text"
                    onChange={this.onChange}
                    icon="fas fa-pen"
                    is_small={true}
                    auto_focus={true}
                />
                : cohort.title
                }
                </td>
                <td>{moment.unix(cohort.created).format('DD/MM/YYYY')}</td>
                <td>
                { cohort.id === edit_cohort_id ?
                <div className="buttons">
                    <button onClick={this.editCohort.bind(this, cohort)} className={classnames('button is-success is-small is-rounded is-outlined',{'is-loading': editing})}>
                        <span>Save</span>
                    </button>
                    <button onClick={this.cancelEdit} className="button is-danger is-small is-rounded is-outlined">
                        <span>Cancel</span>
                    </button>
                </div>
                : 
                <div className="buttons">
                    <button onClick={this.selectEditCohort.bind(this, cohort)} className="button is-info is-small is-rounded is-outlined">
                        <span>Edit</span>
                    </button>
                    <button onClick={this.openDeleteModal.bind(this, cohort)} className="button is-danger is-small is-rounded is-outlined">
                        <span>Delete</span>
                    </button>
                </div>
                }
                </td>
                </tr>
              )) : null}
              </tbody>
            </table>
            {(cohorts.length === 0 && !loading) ? <p className="no-data has-text-centered">No Data</p> : null}
            {loading ? <p className="no-data has-text-centered">Loading Data...</p> : null}
          </div>
        </div>
      </div>
      </div>
      <ConfirmModal 
      title={delete_modal.title}
      text={delete_modal.text}
      confirm_text={delete_modal.confirm_text}
      onConfirm={this.confirmDelete}
      onClose={this.closeDeleteModal}
      is_active={delete_modal.is_active}
      doing={deleting}/>
      </div>
    )
  }
}

// PropTypes
// ---------
Cohorts.propTypes = {
  getCohorts: PropTypes.func.isRequired,
  deleteCohort: PropTypes.func.isRequired,
  addCohorts: PropTypes.func.isRequired,
  editCohort: PropTypes.func.isRequired
}

export default connect(null, {getCohorts, deleteCohort, addCohorts, editCohort})(withRouter(Cohorts));