import React, { Component } from 'react';
import Rating from 'react-rating';

import very_bad from '../../../../../assets/images/moods/veryBad.png'
import bad from '../../../../../assets/images/moods/bad.png'
import neutral from '../../../../../assets/images/moods/neutral.png'
import good from '../../../../../assets/images/moods/good.png'
import very_good from '../../../../../assets/images/moods/veryGood.png'

class LifecycleMCQOptions extends Component {

    state = {
        options: this.props.options,
        image_map: {
            0: very_bad,
            1: bad,
            2: neutral,
            3: good,
            4: very_good
        }
    }

    editOption = (i) => {
        const { options } = this.state;
        options[i].edit = true;
        options[i].temp = options[i].content;
        this.setState({options}); 
    }

    changeValue = (i, e) => {
        const { options } = this.state;
        options[i].temp = e.target.value;
        this.setState({options})
    }

    changeSentimentScore = (i, score) => {
        const { options } = this.state;
        options[i].sentiment_score = score-1;
        this.props.onEditOption(options);
    }
        
    saveEditOption = i => {
        const { options } = this.state;
        options[i].content = options[i].temp;
        delete options[i].temp;
        delete options[i].edit;
        this.setState({options});
        this.props.onEditOption(options);
    }

    cancelEditOption = i => {
        const { options } = this.state;
        delete options[i].temp;
        delete options[i].edit;
        this.setState({options});
    }


    render() {
        const { options, image_map } = this.state;
        const {question_index} = this.props;

        // const symbols = {
        //     empty: ['far fa-sad-tear grey','far fa-frown grey', 'far fa-meh grey', 'far fa-smile grey', 'far fa-laugh-beam grey'],
        //     full: ['far fa-sad-tear red','far fa-frown red', 'far fa-meh red', 'far fa-smile red', 'far fa-laugh-beam red']
        // }
        return (
            <>
                <ol className="radio-options">
                    {options.map((opt, i) => (
                        <li key={opt.content}>
                        {
                            question_index !== 0 ?
                                <Rating className="rating" initialRating={opt.sentiment_score+1} onChange={this.changeSentimentScore.bind(this,i)}/>
                            :
                                <img width="30px" src={image_map[i]} alt='mood' />
                        }
                        {opt.edit ?  
                        <span>
                            <input className="input is-small" value={opt.temp} type="text" placeholder="Normal input" onChange={this.changeValue.bind(this, i)}/> 
                            <span className="icon has-text-info" >
                                <i className="fas fa-check" aria-hidden="true" onClick={this.saveEditOption.bind(this, i)}></i>
                                <i className="fas fa-times-circle" aria-hidden="true" onClick={this.cancelEditOption.bind(this, i)}></i>
                            </span>
                        </span>

                        : <span>
                            {opt.content}
                            {
                                question_index === 0 ? null :
                                <span className="icon has-text-info" onClick={this.editOption.bind(this, i)}>
                                        <i className="fas fa-pen" aria-hidden="true"></i>
                                </span>
                            }
                        </span> 
                        }

                    </li>
                    ))}
                </ol>
            </>
        );
    }
}

export default LifecycleMCQOptions;
