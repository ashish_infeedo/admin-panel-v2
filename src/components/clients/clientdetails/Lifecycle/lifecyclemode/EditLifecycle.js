import React, { Component } from 'react';
import { CSVLink } from 'react-csv';
import { NotificationManager } from 'react-notifications';
import { connect } from 'react-redux';

//actions
import { getTouchpointsList, fetchStage , fetchLanguages, updateStages, archiveStage, downloadQB} from '../../../../../actions/LifecycleActions';
import ConfirmModal from '../../../../generic/ConfirmModal';
import Dropdown from '../../../../generic/Dropdown';
import LifecycleCSVLoader from './LifecycleCSVLoader';

class EditLifecycle extends Component {
    state = {
        touchpoints_list: [],
        selected_touchpoint: {},
        original_touchpoint_list: '',
        selected_cohort: {},
        language_map: [],
        stage_data: {},
        original_stage_data: '',
        selected_language: {},
        loading: false,
        export_qb_data: [],
        export_csv_headers: [
            { label: "Stage", key: "stage" },
            { label: "Question", key: "questions" },
            { label: "Driver", key: "employee_driver" },
            { label: "Element", key: "engagement_elements" },
            { label: "5", key: "4" },
            { label: "4", key: "3" },
            { label: "3", key: "2" },
            { label: "2", key: "1" },
            { label: "1", key: "0" }
        ],
        update_confirmation_msg: '',
        is_confirmation_active: false,
        is_archiving:false,
        form_error: false
    }
    csvLink = React.createRef()
    componentDidMount () {
        this.getTouchpointsList()
        this.setState({selected_cohort: this.props.cohorts[0]})
    }
    getTouchpointsList = async() => {
        let {client_details} = this.props;
        let response = await getTouchpointsList(client_details.id)
        if(response && response.status === 200 && response.data) {
            response.data.forEach(touchpoint => {
                touchpoint.stage = touchpoint.duration+ ' ' +touchpoint.unit + ` ${touchpoint.status ? '(Archived)' : ''}`
            });
            response.data.reverse()
            this.setState({touchpoints_list: response.data, original_touchpoint_list: JSON.stringify(response.data)})
        } else {

        }
    }
    selectCohort = (cohort) => {
        let {original_touchpoint_list} = this.state;
        let temp_touchpoint_list = [];
        JSON.parse(original_touchpoint_list).forEach(touchpoint => {
            if(touchpoint.cohort_id === cohort.id) {
                temp_touchpoint_list.push(touchpoint)
            }
        })
        this.setState({selected_cohort: cohort, touchpoints_list: JSON.parse(JSON.stringify(temp_touchpoint_list))})
    }

    selectTouchpoint = (touchpoint) => {
        this.setState({selected_touchpoint: touchpoint}, () => {
            this.fetchLanguages()
        })
    }

    fetchLanguages = async() => {
        const {client_details} = this.props;
        const {selected_touchpoint} = this.state;
        let response = await fetchLanguages(client_details.id, selected_touchpoint.survey_id)
        if(response && response.status === 200 && response.data) {
            let language_array = [];
            if(response.data.length > 1) {
                response.data.forEach(function(key, index) {
                    var item = {
                        value: Object.keys(key)[0],
                        key: key[Object.keys(key)[0]]
                    }
                    language_array.push(item)
                })
                this.setState({language_map: language_array})
            } else {
                let a = {
                    value: Object.keys(response.data[0])[0],
                    key: response.data[0][Object.keys(response.data[0])[0]]
                }
                this.setState({selected_language: a})
                this.fetchStage()
            }
            
            this.setState({language_map : JSON.parse(JSON.stringify(language_array))})
        }
    }   
    
    fetchStage = async() => {
        const {client_details} = this.props;
        this.setState({loading: true})
        const {selected_touchpoint, selected_language} = this.state;
        let response = await fetchStage(client_details.id, selected_touchpoint.survey_id, selected_language.key)
        if(response && response.status === 200 && response.data) {
            (response.data['questions']).forEach(question => {
                if(question.type === 'radio') {
                    question['options'].forEach(option => {
                        option.sentiment_score = option.sentiment_score - 1
                    })
                }
            })
            let stage = {};
            stage[response.data.stageText] = response.data
            this.setState({stage_data: stage, original_stage_data: JSON.stringify(stage), loading: false})
        }
    }

    selectLanguage = (language) => {
        this.setState({selected_language: language}, () => {
            this.fetchStage()
        })
    }

    onEditOption = ({ stage, question_index }, options) => {
        let { stage_data } = this.state;
        stage_data[stage].questions[question_index].options = options;
        this.setState({ stage_data });
    }


    editQuestion = (stage_index, q_index) => {
        let { stage_data } = this.state;
        let stage = Object.keys(stage_data)[stage_index]
        stage_data[stage]['questions'][q_index].edit_question = true;
        stage_data[stage]['questions'][q_index].temp_question = stage_data[stage]['questions'][q_index].content;
        this.setState({ stage_data });
    }

    changeValue = (stage_index, q_index, e) => {
        let { stage_data } = this.state;
        let stage = Object.keys(stage_data)[stage_index]
        stage_data[stage]['questions'][q_index].temp_question = e.target.value
        this.setState({ stage_data });
    }   
    
    saveEditedQuestion = (stage_index, q_index) => {
        let { stage_data } = this.state;
        let stage = Object.keys(stage_data)[stage_index]
        stage_data[stage]['questions'][q_index].edit_question = false;
        stage_data[stage]['questions'][q_index].content = stage_data[stage]['questions'][q_index].temp_question
        this.setState({ stage_data });
    }

    cancelEditQuestion = (stage_index, q_index) => {
        let { stage_data } = this.state;
        let stage = Object.keys(stage_data)[stage_index]
        stage_data[stage]['questions'][q_index].temp_question = stage_data[stage]['questions'][q_index].content
        this.setState({ stage_data });
    }

    openConfirmationModal = (is_archiving) => {
        if(is_archiving) {
            this.setState({update_confirmation_msg: '1. Archive  the existing edited question <br><br> 2. New Question will be added <br><br> 3. Edit or Add Driver and Element <br><br> 4. Delete the particular Question', is_confirmation_active: true, is_archiving})
        } else {
            this.setState({update_confirmation_msg: '<center>Updating will change only existing edited <br> Questions &  Options</center>', is_confirmation_active: true, is_archiving})
        }
    }

    updateStages = async() => {
        const {selected_touchpoint, selected_language, stage_data, is_archiving} = this.state;
        const {client_details} = this.props;
        let form_error = false;
        let payload = stage_data[Object.keys(stage_data)];
        (stage_data[Object.keys(stage_data)]['questions']).forEach(question => {
            if(question.type === 'radio') {
                question['options'].forEach(option => {
                    option.sentiment_score = option.sentiment_score + 1
                })
            }

            if(!question.employee_driver && question.engagement_element) {
                form_error = true
            }
        })
        payload.archive = is_archiving;
        if(form_error) {
            NotificationManager.error('There is an error in your Stage, Please check and correct it.')    
        } else {
            let response = await updateStages(client_details.id, selected_touchpoint.survey_id, selected_language.key, payload);
            if(response && response.status === 200) {
                this.setState({update_confirmation_msg: '', is_confirmation_active: false}, () => {
                    this.resetEditStage();
                    NotificationManager.info('Stage Updated');
                })
            }
        }
    }

    resetEditStage = () => {
        this.setState({
            selected_touchpoint: {},
            selected_cohort: {},
            language_map: [],
            stage_data: {},
            original_stage_data: '',
            selected_language: {}
        })
    }

    archiveStage = async() => {
        const response = await archiveStage(this.props.client_details.id, this.state.selected_touchpoint.survey_id)
        if(response && response.status === 200) {
            this.getTouchpointsList()
            this.resetEditStage()
            NotificationManager.info('Stage Archived')
        }
    }
    deleteQuestion = (question_index) => {
        let confirm = window.confirm('Are you sure want to delete this question?')

        if(confirm) {
            let {stage_data} = this.state;
            stage_data[Object.keys(stage_data)]['questions'].splice(question_index, 1)
            this.setState({stage_data})
        }
    }

    updateDriverElement = (stage_index, q_index, is_driver, data) => {
        let { stage_data} = this.state;

        let stage = Object.keys(stage_data)[stage_index]
        if(is_driver) {
            stage_data[stage]['questions'][q_index].employee_driver = data
        } else {
            stage_data[stage]['questions'][q_index].engagement_element = data
        }

        this.setState({stage_data})
    }

    downloadQB = async() => {
        let {client_details} = this.props;
        let {selected_cohort} = this.state;
        let response = await downloadQB(client_details.id, selected_cohort.id)
        if(response && response.status === 200 && response.data.length &&  response.data !== 'No Questions') {
            this.setState({export_qb_data: response.data}, () =>{
                this.csvLink.current.link.click()
            })
        } else {
            NotificationManager.error(response.data)
        }
        
    }

    closeConfirmModal = () => {
        this.setState({is_confirmation_active: false, update_confirmation_msg:""})
    }

    render() {
        const {touchpoints_list, selected_touchpoint, selected_cohort, stage_data, selected_language, loading, language_map, export_qb_data, export_csv_headers, update_confirmation_msg, is_confirmation_active, form_error} = this.state;
        const {cohorts, drivers, elements, client_details} = this.props;
        return (
            <div className="edit-lifeCycle">
                <div className="columns">
                    <div className="column is-2">
                    <strong className="is-size-7">Select Cohort</strong><br/>
                    <Dropdown dropdown_items={cohorts} selected_item={selected_cohort} onClick={this.selectCohort} identity='title' classes='' />
                    </div>
                    <div className="column is-2">
                        <strong className="is-size-7">Select Touchpoint</strong><br/>
                        <Dropdown dropdown_items={touchpoints_list} selected_item={selected_touchpoint} onClick={this.selectTouchpoint} identity='stage' classes='' />
                    </div>
                    {
                        language_map.length ?
                        <div className="column is-2">
                            <strong className="is-size-7">Select Language</strong><br/>
                            <Dropdown dropdown_items={language_map} selected_item={selected_language} onClick={this.selectLanguage} identity='value' classes='' />
                        </div> : null
                    }
                    {
                        Object.keys(selected_touchpoint).length ?
                            <div className="column text-right">
                                <strong className="is-size-7">Reset Touchpoint</strong><br/>
                                <button onClick={this.resetEditStage} className="button is-outlined">Reset</button>
                            </div>
                        : null
                    }
                    {
                        touchpoints_list.length?
                            <div className="column text-right">
                                <strong className="is-size-7">Download QB</strong><br/>
                                <button onClick={this.downloadQB} className="button is-outlined is-info">
                                    Download
                                </button>   
                                <CSVLink filename={`${client_details.title}-QB.csv`} headers={export_csv_headers} data={export_qb_data} ref={this.csvLink}></CSVLink>
                            </div>
                        : null
                    }
                </div>
                {
                    !loading ?
                    <div className="stage-content">
                        {stage_data && Object.keys(stage_data).length? 
                            <LifecycleCSVLoader 
                                cancelEditQuestion={this.cancelEditQuestion} 
                                saveEditedQuestion={this.saveEditedQuestion} 
                                changeValue={this.changeValue} 
                                onEditOption={this.onEditOption} 
                                editQuestion={this.editQuestion} 
                                question_bank={stage_data} 
                                drivers={drivers} 
                                elements={elements}
                                selected_language={selected_language} 
                                action={this.openConfirmationModal}
                                parent={'edit'}
                                archiveStage={this.archiveStage}
                                deleteQuestion={this.deleteQuestion}
                                updateDriverElement={this.updateDriverElement}
                                client_details={client_details}
                                form_error={form_error}
                                is_archived={selected_touchpoint.status}
                            /> 
                        : null}
                    </div> : <p className="text-center">Loading Stage...</p>
                }

                <ConfirmModal
                    text={update_confirmation_msg}
                    title="Are you sure?"
                    confirm_text="OK"
                    is_active={is_confirmation_active}
                    onConfirm={this.updateStages}
                    onClose={this.closeConfirmModal}
                 />
            </div>
        );
    }
}
const mapStateToProps = (state) => ({
    client_details: state.client.client_details
})

export default connect(mapStateToProps, {getTouchpointsList, fetchStage, fetchLanguages, updateStages, archiveStage, downloadQB})(EditLifecycle);
