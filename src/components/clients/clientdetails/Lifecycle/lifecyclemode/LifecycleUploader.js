import React, { Component } from 'react';
import CSVReader from 'react-csv-reader';

//Components
import Dropdown from '../../../../generic/Dropdown';

//Assets
import sample_file from '../../../../../assets/resources/Sample_Stage_Creation.csv'


class LifecycleUploader extends Component {

    downloadCsv = () => {
        window.open(sample_file)
    }

    render() {
        const { cohorts, onClick, onFileLoaded, languages, selected_language, selected_cohort, question_bank, client_details } = this.props;
        return (
            <div className="columns">
                {
                    client_details.multilingual_enabled ? 
                        <div className="column">
                            <strong>Select Language:</strong><br />
                            <Dropdown classes={'justify-space-between is-fullwidth'} selected_item={selected_language} dropdown_items={languages} onClick={this.props.updateLanguage} />
                        </div>
                    : null

                }
                <div className="column">
                    <strong>Select Cohort:</strong><br />
                    <Dropdown dropdown_items={cohorts} selected_item={selected_cohort} onClick={onClick} identity='title' classes='' />
                </div>
                {
                    !Object.keys(question_bank).length ?
                        <>
                            <div className="column">
                                <strong>Select File:</strong><br />
                                <CSVReader
                                    cssClass="csv-reader-input"
                                    label=""
                                    onFileLoaded={onFileLoaded}
                                    onError={this.handleError}
                                    inputId="ObiWan"
                                    inputStyle={{ color: 'red' }}
                                />
                            </div>
                            <div className="column">
                                <button className="button is-outlined" onClick={this.downloadCsv}>
                                    <span className="icon">
                                        <i className="fas fa-download"></i>
                                    </span>
                                    <span>Download Sample CSV</span>
                                </button>
                            </div>
                        </>
                        : <>

                            <div className="column">
                                <strong>Reset QB:</strong><br />
                                <button className="button is-outlined" onClick={this.props.resetQB}>
                                    <span>Reset</span>
                                </button>
                            </div>
                        </>

                }
            </div>
        );
    }
}

export default LifecycleUploader;
