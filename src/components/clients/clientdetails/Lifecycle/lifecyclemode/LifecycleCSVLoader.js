import React from 'react'
import LifecycleMCQOptions from './LifecycleMCQOptions';
import LifecycleDriversElements from './LifecycleDriversElements';

function LifecycleCSVLoader({question_bank, 
    drivers, 
    elements, 
    selected_language, 
    saveEditedQuestion, 
    editQuestion, 
    onEditOption, 
    cancelEditQuestion, 
    changeValue, 
    action,
    parent,
    archiveStage,
    deleteQuestion,
    updateDriverElement,
    client_details,
    is_archived
    }) {
    return (
        <>
        <div className="stages">   
            {question_bank ? Object.keys(question_bank).map((stage, i) => (
                <div className="card marginbottom-md">
                    <div className="card-content">
                        <div className="columns flex-wrap tenure" key={stage}>
                            <div className="hero is-dark column is-12 border-left-right-5" >
                                <div className="hero-body is-paddingless">
                                    <p className='is-size-5 has-text-weight-semibold'>{stage} {client_details.multilingual_enabled ? `(${selected_language.value})` : null}
                                        {
                                            parent === 'edit' && !is_archived ? 
                                            <button onClick={archiveStage} className="button is-outlined is-danger is-small pull-right">
                                                Archive
                                            </button> : null
                                        }
                                    </p>
                                </div>
                            </div>
                            {question_bank[stage].questions.length ?
                            question_bank[stage].questions.map((question, question_index) => (
                                <div className="column is-12 mg-bottom-10 border-all-grey border-5 no-padding" key={question_index}>
                                    <header className="card-header">
                                        <p className="card-header-title">
                                            <span className="tag is-dark marginright-sm">{question.type === 'radio' ? 'MCQ': 'Textual'}</span>Q{question_index+1}. 
                                            {
                                                question.edit_question ?
                                                question_index === 0 ? null :
                                                <>
                                                    <input className="input is-small" value={question.temp_question} type="text" placeholder="Normal input" onChange={changeValue.bind(this, i, question_index)}/> 
                                                    <span className="icon has-text-info" >
                                                        <i className="fas fa-check" aria-hidden="true" onClick={saveEditedQuestion.bind(this, i, question_index)}></i>
                                                        <i className="fas fa-times-circle" aria-hidden="true" onClick={cancelEditQuestion.bind(this, i, question_index)}></i>
                                                    </span>
                                                </>
                                                : 
                                                question.content
                                            }
                                        </p>
                                        {
                                            question.edit_question ?null :
                                                question_index === 0 ? null :
                                                    <a className="card-header-icon" aria-label="more options">
                                                        <span className="icon has-text-info" onClick={editQuestion.bind(this, i, question_index)}>
                                                            <i className="fas fa-pen" aria-hidden="true"></i>
                                                        </span>
                                                        <span className="icon has-text-danger" onClick={deleteQuestion.bind(this, i, question_index)}>
                                                            <i className="fas fa-trash-alt" aria-hidden="true"></i>
                                                        </span>
                                                    </a>
                                        }
                                    </header>
                                    <div className="card-content is-paddingless">
                                        <div className="content marginbottom-sm">
                                            {question.type === 'string' ? null : 
                                                <LifecycleMCQOptions question_index={question_index} options={question.options} onEditOption={onEditOption.bind(this, {stage, question_index})}/> 
                                            }
                                        </div>
                                    </div>
                                    {question_index > 0 ? <footer className="card-footer">
                                        <div className="card-footer-item flex-left">
                                            <span className="mg-rt-15">Driver</span>
                                            <LifecycleDriversElements is_driver={true} updateDriverElement={updateDriverElement} data={drivers} question_index={question_index} stage_index={i} value={question.employee_driver}/><br/>
                                            {
                                                !question.employee_driver && question.engagement_element ? 
                                                <span className="has-text-danger marginleft-md">
                                                    It is mandatory to select/enter a Driver<br/> if an Element is associated with this question.
                                                </span>
                                                :
                                                null

                                            }
                                        </div>
                                        <div className="card-footer-item flex-left">
                                            <span className="mg-rt-15">Element</span>
                                            <LifecycleDriversElements is_driver={false} updateDriverElement={updateDriverElement} data={elements} question_index={question_index} stage_index={i} value={question.engagement_element}/>
                                        </div>
                                    </footer> : null}
                                </div>
                            )) : 
                            <p className="has-text-centered">
                                There are no Questions in this stage.
                            </p>}
                        </div>
                    </div>
                </div>
            )) : null}
            </div>
            {parent === 'edit'?
                <button className="button is-danger marginright-md is-outlined" onClick={action.bind(this, true)}>Submit</button>
            : null
            }
        <button className="button is-success is-outlined" onClick={action.bind(this, false)}>{parent === 'edit' ? 'Update' : 'Submit'}</button>
        </>
    );
}

export default LifecycleCSVLoader;
