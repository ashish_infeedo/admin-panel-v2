import React, { Component } from 'react';
import Autosuggest from 'react-autosuggest';


class LifecycleDriversElements extends Component {
    state = {
        data:[],
        value:''
    }

    componentDidMount(){
        this.setState({data: this.props.data,
          value: this.props.value})
    }
    
    
    onSuggestionsFetchRequested = ({value}) => {
        this.setState({
            data: this.getSuggestions(value)
        });
      };

      getSuggestions = (value) => {
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;
      
        return inputLength === 0 ? [] : this.props.data.filter(lang =>
          lang.name.toLowerCase().slice(0, inputLength) === inputValue
        );
      };

       getSuggestionValue = suggestion => suggestion.name;

       renderSuggestion = suggestion => (
        <div>
          {suggestion.name}
        </div>
      );

      onChange = (event, { newValue }) => {
        const {stage_index, question_index, is_driver} = this.props;
        this.setState({
          value: newValue
        }, () => {
          this.props.updateDriverElement(stage_index, question_index, is_driver, newValue)
        });
    };
    onSuggestionsClearRequested = (type) => {
        this.setState({
          [type]: []
        });
      };

    render() {
        const {data, value} = this.state;
        const inputProp = {
            placeholder: 'type here',
            value,
            onChange: this.onChange
          };
        return (
            <Autosuggest
                suggestions={data}
                onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                getSuggestionValue={this.getSuggestionValue}
                highlightFirstSuggestion={true}
                renderSuggestion={this.renderSuggestion}
                inputProps={inputProp}
                />
        );
    }
}

export default LifecycleDriversElements;
