import React, { Component } from 'react';
import { connect } from 'react-redux';

import LifecycleUploader from './LifecycleUploader';
import LifecycleCSVLoader from './LifecycleCSVLoader';
import {languages} from '../../../../../constants'

import { uploadStages } from '../../../../../actions/LifecycleActions';
import NotificationManager from 'react-notifications/lib/NotificationManager';

class CreateLifecycle extends Component {
    state = {
        selected_cohort: {},
        csv: [],
        question_bank: {},
        temp_data: '',
        languages: languages,
        selected_language: {key: 1, value: "English"}
    }

    async componentDidMount() {
        
    }

    getGuidelines = () => (
        <article className="message is-small">
            <div className="message-body ">
                <b>Use this section to add lifecycle touchpoints to a client group. Before you use this feature, read the guidelines below:</b>
                <ul>
                    <li>First Question of every touchpoint should be Anchor Question.</li>
                    <li>Duplicate touchpoints will not be added.</li>
                    <li>MCQ with less than 2 options will not be added.</li>
                    <li>For options with the same sentiment, add them to the same cell and separate with an "_".</li>
                    <li>In the first column, touchpoint should be in {"<duration> <days/months/years>"} format</li>
                    <li>Duration should be atleast 1 and accepted units are days, months and years only.</li>
                    <li>The sample file downloaded is in French please mention the Language in the Language column and edit your questions.
Supported language keywords are as follows : French, English, Malay, Indonesian</li>
                </ul>
            </div>
        </article>
    )

    selectCohort = (selected) => {
        this.setState({
            selected_cohort: selected
        });
    }

    onFileLoaded = csv => {
        this.setState({ csv }, () => {
            if(this.props.client_details.multilingual_enabled) {
                this.checkLanguage()
            } else {
                this.transformQB()
            }
        });
    }
    
    checkLanguage = () => {
        const {selected_language, csv} = this.state;
        let is_different = false;
        csv.forEach((question, index) => {
            if(index !== 0 && question[0] !== selected_language.value) {
                is_different = true
            }
        })

        if(is_different) {
            this.setState({csv: []})
            NotificationManager.error(`Language mentioned in QB doesn't match with selected language`)
        } else {
            this.transformQB()
        }
    }

    transformQB = () => {
        let { csv, question_bank, selected_cohort } = this.state

        if (!question_bank.length) {
            csv.shift();
            let temp_data = {};
            csv.forEach((arr, index) => {
                if (arr.length) {
                    const obj = {
                        language: arr[0],
                        content: arr[2],
                        engagement_element: arr[3],
                        employee_driver: arr[4],
                        options: this.getOptions(arr.slice(5, 10)),
                        type: arr.slice(5, 10).every(el => el === "") ? "string" : "radio",
                        question_index: index+1
                    }

                    if (!temp_data[arr[1]]) {
                        temp_data[arr[1]] = {
                            cohort_id: selected_cohort.id,
                            duration: arr[1].split(' ')[0],
                            unit: arr[1].split(' ')[1].charAt(arr[1].split(' ')[1].length) === 's' ? arr[1].split(' ')[1].substr(0, arr[1].split(' ')[1].length - 1) : arr[1].split(' ')[1],
                            questions: [obj],
                        }
                    } else {
                        temp_data[arr[1]].questions.push(obj);
                    }
                }
            })
            question_bank = temp_data;
            this.setState({ question_bank: question_bank, temp_data: JSON.stringify(temp_data) }, () => {});
        } 
    }

    getOptions = arr => {
        const result = [];
        let index = 1;
        // arr.filter((a,i) => ( a ? result.push({name:a, polarity: i}): null));
        arr.filter((opt, i) => {
            if (opt) {
                result.push({
                    option_index: index,
                    type: 'string',
                    sentiment_score: i+1,
                    content: opt
                });
                index++;
            }
        })
        return result.length ? result : null;
    }

    onEditOption = ({ stage, question_index }, options) => {
        let { question_bank } = this.state;
        question_bank[stage].questions[question_index].options = options;
        this.setState({ question_bank });
    }


    editQuestion = (stage_index, q_index) => {
        let { question_bank } = this.state;
        let stage = Object.keys(question_bank)[stage_index]
        question_bank[stage]['questions'][q_index].edit_question = true;
        question_bank[stage]['questions'][q_index].temp_question = question_bank[stage]['questions'][q_index].content;
        this.setState({ question_bank });
    }

    changeValue = (stage_index, q_index, e) => {
        let { question_bank } = this.state;
        let stage = Object.keys(question_bank)[stage_index]
        question_bank[stage]['questions'][q_index].temp_question = e.target.value
        this.setState({ question_bank });
    }   
    
    saveEditedQuestion = (stage_index, q_index) => {
        let { question_bank } = this.state;
        let stage = Object.keys(question_bank)[stage_index]
        question_bank[stage]['questions'][q_index].edit_question = false;
        question_bank[stage]['questions'][q_index].content = question_bank[stage]['questions'][q_index].temp_question
        this.setState({ question_bank });
    }

    cancelEditQuestion = (stage_index, q_index) => {
        let { question_bank } = this.state;
        let stage = Object.keys(question_bank)[stage_index]
        question_bank[stage]['questions'][q_index].temp_question = question_bank[stage]['questions'][q_index].content
        this.setState({ question_bank });
    }

    updateLanguage = (language) => {
        this.resetQB()
        this.setState({selected_language: JSON.parse(JSON.stringify(language))})
    }

    resetQB = () => {
        this.setState({csv: [],loading: false, question_bank: {},temp_data: '',})
    }

    uploadStages = async() => {
        const {selected_language} = this.state;
        let payload = this.getTransformedPayload()
        let form_error = false;
        payload.forEach(stage => {
            stage.questions.forEach(question => {
                if(!question.employee_driver && question.engagement_element) {
                    form_error = true
                }
            })
        })
        if(!form_error) {
            let response = await uploadStages(this.props.client_id, selected_language.key, payload);
            if(response && response.status === 200) {
                this.resetQB()
                NotificationManager.info('Question Bank Uploaded Successfully')
            }
        } else {
            NotificationManager.error('There is an error in your Stage, Please check and correct it.')    
        }
    }
    
    getTransformedPayload = () => {
        const {question_bank} = this.state;
        let array = []

        Object.keys(question_bank).forEach(stage => {
            array.push(question_bank[stage])
        })

        return array;
    }

    updateDriverElement = (stage_index, q_index, is_driver, data) => {
        let { question_bank } = this.state;
        let stage = Object.keys(question_bank)[stage_index]
        if(is_driver) {
            question_bank[stage]['questions'][q_index].employee_driver = data
        } else {
            question_bank[stage]['questions'][q_index].engagement_element = data
        }

        this.setState({question_bank})
    }

    deleteQuestion = (stage_index, question_index) => {
        let confirm = window.confirm('Are you sure want to delete this question?')

        if(confirm) {
            let { question_bank } = this.state;
            question_bank[Object.keys(question_bank)[stage_index]]['questions'].splice(question_index, 1)
            this.setState({question_bank})
        }
    }


    render() {
        const { question_bank, selected_language, languages, selected_cohort } = this.state;
        const {cohorts, drivers, elements, loading, client_details} = this.props;
        return (
            <div className="columns flex-wrap">
                <div className="column is-12">
                    {this.getGuidelines()}
                </div>
                <div className="column is-12">
                    {loading ? 'loading...' :
                        <LifecycleUploader
                            cohorts={cohorts}
                            onClick={this.selectCohort}
                            onFileLoaded={this.onFileLoaded} 
                            languages={languages}
                            updateLanguage={this.updateLanguage}
                            selected_language={selected_language}
                            selected_cohort={selected_cohort}
                            resetQB={this.resetQB}
                            question_bank={question_bank}
                            client_details = {client_details}
                        />
                    }
                </div>
                <div className="column is-12">
                    {question_bank && Object.keys(question_bank).length ? 
                        <LifecycleCSVLoader 
                            cancelEditQuestion={this.cancelEditQuestion} 
                            saveEditedQuestion={this.saveEditedQuestion} 
                            changeValue={this.changeValue} 
                            onEditOption={this.onEditOption} 
                            editQuestion={this.editQuestion} 
                            question_bank={question_bank} 
                            drivers={drivers} 
                            elements={elements}
                            selected_language={selected_language} 
                            action ={this.uploadStages}
                            parent={'create'}
                            deleteQuestion={this.deleteQuestion}
                            client_details = {client_details}
                            updateDriverElement={this.updateDriverElement}
                        /> 
                    : null
                    }
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => ({
    client_details: state.client.client_details
})

export default connect(mapStateToProps, {uploadStages})(CreateLifecycle);
