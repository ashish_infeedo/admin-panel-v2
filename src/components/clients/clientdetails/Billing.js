import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import DatePicker from 'react-date-picker';
import classnames from 'classnames';
import moment from 'moment';
// components
import TextInputgroup from '../../../components/generic/TextInputGroup';
import Dropdown from '../../../components/generic/Dropdown';
// Actions
import { getBillingContacts, getAdmins, updateClientDetails, updateContactPersonDetails } from '../../../actions/ClientsActions';

//contants
import { countries, billing_cycles, plans } from '../../../constants'

class Billing extends Component {
  state = {
    loading: true,
    // API variables
    id: '',
    gstin: '',
    status: null, //plans
    billing_cycle: null,
    invoice_cycle: null,
    subscription_start_date: null,
    subscription_end_date: null,
    payment_status: null,
    billing_cycles: [],
    plans: [],
    // Contact Person
    bill_id: null,
    name: '',
    user_id: null,
    address1: '',
    address2: '',
    city: '',
    state: '',
    country: '',
    pincode: '',
    selected_person: { key: 0, value: 'loading...' },
    admins: [],
    // Payment status
    all_payment_status: [
      { key: 0, value: 'Yet to Pay' },
      { key: 1, value: 'Paid' }
    ],
    selected_cycle: {},
    selected_plan: {},
    selected_payment_status: {},
    selected_country: { key: 0, value: 'loading...' },
    reminder_color: false,
    reminder_text: '',
    prop_set: false,
    updating_billing: false,
    updating_contact: false,
    is_pilot: 0,
    email_sender: '',
    // countries
    countries: []
  }

  // componentDidMount
  // -----------------
  componentDidMount() {
    const { client_id } = this.props.match.params;
    this.setState({ client_id: client_id, countries: countries, billing_cycles: billing_cycles, plans: plans }, () => this.getBillingContacts());
  }

  componentDidUpdate(prevProps) {
    const { id } = this.props.client_details;
    if (prevProps.client_details.id !== id) {
      this.setBillingDetails();
    }
  }

  // set billing details
  // -------------------
  setBillingDetails = () => {
    const { gstin, invoice_cycle, billing_cycle, id, status, payment_status, subscription_start_date, subscription_end_date, is_pilot, email_sender } = this.props.client_details;
    const { billing_cycles, plans, all_payment_status } = this.state;
    // setting current values
    const current_cycle = billing_cycles.filter(cycle => cycle.key === billing_cycle)[0];
    const current_plan = plans.filter(cycle => cycle.key === status)[0];
    const current_payment_status = all_payment_status.filter(cycle => cycle.key === payment_status)[0];

    this.setState({
      billing_cycle: (current_cycle ? current_cycle.key : null),
      status: (current_plan ? current_plan.key : null),
      payment_status: (current_payment_status ? current_payment_status.key : null),
      selected_cycle: (current_cycle ? current_cycle : {}),
      selected_plan: (current_plan ? current_plan : {}),
      selected_payment_status: (current_payment_status ? current_payment_status : {}),
      prop_set: true,
      subscription_start_date: (subscription_start_date ? new Date(subscription_start_date * 1000) : null),
      subscription_end_date: (subscription_end_date ? new Date(subscription_end_date * 1000) : null),
      gstin: gstin,
      invoice_cycle: invoice_cycle,
      id: id,
      is_pilot: is_pilot,
      email_sender : email_sender
    })
  }

  // Get Billing Contact
  // -------------------
  getBillingContacts = async () => {
    const { client_id } = this.state;
    await this.props.getBillingContacts(client_id);
    const { name, user_id, address1, address2, city, state, country, pincode, id } = this.props.billing_contacts;
    this.setState({ bill_id: id, name: name, user_id: user_id, address1: address1, address2: address2, city: city, state: state, country: country, pincode: pincode });
    if (country) {
      this.setState({ selected_country: { key: 0, value: country } });
    } else {
      this.setState({ selected_country: { key: 0, value: 'Not Selected' } });
    }
    this.setState({ loading: false });
    this.getAdmins();
    this.makePaymentStatusReminder();
    this.setBillingDetails();
  }

  // Get Admins
  // ----------
  getAdmins = async () => {
    const { client_id } = this.state;
    const { name } = this.props.billing_contacts;
    await this.props.getAdmins(client_id);
    const admins = [];
    let selected_person = {}
    this.props.admins.forEach((admin, index) => {
      admin.key = index;
      admin.value = admin.name;
      admins.push(admin);
      if (admin.user_name === name) {
        selected_person = {key:admin.key, value: admin.user_name, user_name: admin.user_name};
      }
    });
    if (!selected_person.value) {
      selected_person = { key: 0, value: 'Not Selected' };
    }
    if (admins.length === 0) {
      selected_person = { key: 0, value: 'Not Available' };
    }
    this.setState({ admins: admins, selected_person: selected_person, name: selected_person.name });
  }

  // Make Reminder text
  // ------------------
  makePaymentStatusReminder = () => {
    const { status, payment_status, subscription_end_date, invoice_cycle, payment_reminder } = this.props.client_details;
    const { user_id } = this.props.billing_contacts;
    const date = new Date();
    const time = date.getTime();
    const days_left_for_reminder = Math.round(((subscription_end_date - invoice_cycle * 86400) - time / 1000) / 60 / 60 / 24);
    let text = '';
    if ((status === 2 || status === 3) && user_id) {
      text += 'Enabled';
      if (payment_status === 1) {
        text += ` for next billing cycle. Reminders trigger after ${days_left_for_reminder} Days`;
      } else if (payment_status === 0) {
        text += ` for current billing cycle ${payment_reminder ? payment_reminder : 0}/4 reminders sent`;
      }
      this.setState({ reminder_color: true, reminder_text: text });
    } else {
      text = 'Disabled';
      this.setState({ reminder_color: false, reminder_text: text });
    }

  }

  // Update billing Details
  // ----------------------
  updateBillingDetails = async () => {
    let confirmation_1 = window.confirm('Are you sure?');
    if (confirmation_1) {
      let confirmation_2 = window.confirm('Are you double sure?');
      if(confirmation_2) {
        let confirmation_3 = window.confirm(`These changes will not be reverted later and tech team won't be able to help?`);
        
        if(confirmation_3) {
          this.setState({ updating_billing: true });
          const { gstin, invoice_cycle, billing_cycle, status, payment_status, subscription_start_date, subscription_end_date, is_pilot, email_sender, client_id } = this.state;
          const payload = {
            gstin: gstin,
            invoice_cycle: parseInt(invoice_cycle),
            billing_cycle: billing_cycle,
            status: status,
            payment_status: payment_status,
            subscription_start_date: moment(subscription_start_date).unix(),
            subscription_end_date: moment(subscription_end_date).unix(),
            is_pilot: is_pilot, 
            email_sender : email_sender
          }
          await this.props.updateClientDetails(client_id, payload);
          this.setState({ updating_billing: false });
        }
      }
    }
    
  }

  // update ContactPerson Details
  // ----------------------------
  updateContactPersonDetails = async () => {
    this.setState({ updating_contact: true });
    const { user_id, address1, address2, city, state, country, pincode, client_id, bill_id } = this.state;
    const payload = {
      user_id: user_id,
      address1: address1,
      address2: address2,
      city: city,
      state: state,
      country: country,
      pincode: pincode
    }
    await this.props.updateContactPersonDetails(client_id, bill_id, payload);
    this.setState({ updating_contact: false });
  }

  // Select Billing Cycle
  // --------------------
  selectCycle = (cycle) => {
    const { subscription_start_date } = this.state;
    this.setState({ selected_cycle: cycle, billing_cycle: cycle.key });
    this.makeEndSubscriptionDate(subscription_start_date, cycle.key);
  }

  // Select Plan
  // ------------
  selectPlan = (plan) => {
    this.setState({ selected_plan: plan, status: plan.key });
  }

  // Select Payment Status
  // ---------------------
  selectPaymentStatus = (status) => {
    this.setState({ selected_payment_status: status, payment_status: status.key })
  }

  // Select Contact Person
  // ---------------------
  selectContactPerson = (person) => {
    this.setState({ selected_person: person, person: person.user_name, user_id: person.id });
  }

  // select Country
  // --------------
  selectCountry = (country) => {
    this.setState({ selected_country: country, country: country.value });
  }

  // OnDate change
  // -------------
  onDateChange = (date) => {
    const { billing_cycle } = this.state;
    this.setState({ subscription_start_date: date });
    this.makeEndSubscriptionDate(date, billing_cycle);
  }

  // make end subscription date
  // --------------------------
  makeEndSubscriptionDate = (subscription_start_date, billing_cycle) => {
    let subscription_end_date = new Date(subscription_start_date.getTime());;
    let numberOfMonths = 0;

    switch (billing_cycle) {
      case 0:
        numberOfMonths = 3;
        break;
      case 1:
        numberOfMonths = 6;
        break;
      case 2:
        numberOfMonths = 12;
        break;
      case 3:
        numberOfMonths = 1;
        break;
      case 4:
        numberOfMonths = 9;
        break;
      default:
        numberOfMonths = 0;
        break;
    }

    subscription_end_date.setMonth(subscription_end_date.getMonth() + numberOfMonths);
    this.setState({ subscription_end_date: subscription_end_date });
  }

  // On change
  // ---------
  onChange = e => this.setState({ [e.target.name]: e.target.value });

  changePilotStatus = (status) => {
    this.setState({is_pilot: status})
  }

  // Render
  // ------
  render() {
    const { gstin, invoice_cycle, billing_cycles, selected_cycle, plans, selected_plan, all_payment_status, selected_payment_status, reminder_color, reminder_text, subscription_end_date, loading, is_pilot, email_sender } = this.state;
    const { address1, address2, city, state, pincode, admins, selected_person, updating_billing, updating_contact, countries, selected_country } = this.state;
    return (
      <div className="columns">
        <div className="column is-4 is-offset-1">
          <h2 className="has-text-weight-semibold is-size-5 has-text-centered">Client Billing Details</h2><br />
          <div className="form-field mg-bottom-20">
            <label className="label">GSTIN</label>
            <TextInputgroup
              name="gstin"
              placeholder="GSTIN"
              value={gstin || ''}
              type="text"
              onChange={this.onChange}
              icon="fas fa-money-check"
            />
          </div>
          <div className="form-field mg-bottom-20">
            <label className="label">Invoice Cycle</label>
            <TextInputgroup
              name="invoice_cycle"
              placeholder="Invoice Cycle"
              value={invoice_cycle || ''}
              type="number"
              onChange={this.onChange}
              icon="fas fa-calendar-week"
            />
          </div>

          <div className="columns">
            <div className="column is-6">
              <label className="label">Billing Cycle</label>
              <Dropdown selected_item={selected_cycle} dropdown_items={billing_cycles} onClick={this.selectCycle} />
            </div>
            <div className="column is-6">
              <label className="label">Plan</label>
              <Dropdown selected_item={selected_plan} dropdown_items={plans} onClick={this.selectPlan} />
            </div>
          </div>
          <div className="form-field mg-bottom-20">
            <label className="label">Payment Status</label>
            <Dropdown selected_item={selected_payment_status} dropdown_items={all_payment_status} onClick={this.selectPaymentStatus} />
          </div>
          <div className="form-field mg-bottom-20">
            <label className="label">Payment Reminder Status</label>
            <p className={classnames({ 'has-text-success': reminder_color, 'has-text-danger': !reminder_color })}>{loading ? 'Loading...' : reminder_text}</p>
          </div>
          <div className="form-field mg-bottom-20">
            <label className="label">Subscription Start Date</label>
            <DatePicker onChange={this.onDateChange} value={this.state.subscription_start_date} />
          </div>
          <div className="form-field mg-bottom-20">
            <label className="label">Subscription End Date</label>
            <p><b>{subscription_end_date ? moment(subscription_end_date).format('DD/MM/YYYY') : '--/--/----'}</b> </p>
          </div>
          <hr />
          <div className="form-field mg-bottom-20">
            <label className="label">Is Pilot</label>
            <label className="radio">
              <input type="radio" name="is_pilot" value="1" checked={is_pilot === 1} readOnly onClick={this.changePilotStatus.bind(this, 1)} />
              &nbsp;&nbsp;Enable
            </label>&nbsp;&nbsp;
            <label className="radio">
              <input type="radio" value="0" name="is_pilot" checked={is_pilot === 0} readOnly onClick={this.changePilotStatus.bind(this, 0)} />
              &nbsp;&nbsp;Disable
            </label>
          </div>
          <div className="form-field mg-bottom-20">
            <label className="label">Email Sender</label>
            <input name="email_sender" type="text" className="input" value={email_sender} placeholder="Email Sender" onChange={this.onChange} />
          </div>

          <button onClick={this.updateBillingDetails} className={classnames('button is-primary', { 'is-loading': updating_billing })}>
            <span className="icon">
              <i className="far fa-save"></i>
            </span>
            <span>Save</span>
          </button>
        </div>

        <div className="column is-4 is-offset-2">
          <h2 className="has-text-weight-semibold is-size-5 has-text-centered">Contact Person Details</h2><br />
          <label className="label">Contact Person</label>
          <Dropdown selected_item={selected_person} dropdown_items={admins} identity={'user_name'} onClick={this.selectContactPerson} />
          <br /><br />
          <label className="label">Address Line 1</label>
          <TextInputgroup
            name="address1"
            placeholder="Address 1"
            value={address1 || ''}
            type="text"
            onChange={this.onChange}
            icon="fas fa-map-marked-alt"
          />
          <label className="label">Address Line 2</label>
          <TextInputgroup
            name="address2"
            placeholder="Address 2"
            value={address2 || ''}
            type="text"
            onChange={this.onChange}
            icon="fas fa-map-marked-alt"
          />
          <label className="label">City</label>
          <TextInputgroup
            name="city"
            placeholder="City"
            value={city || ''}
            type="text"
            onChange={this.onChange}
            icon="fas fa-map-marker-alt"
          />
          <label className="label">State</label>
          <TextInputgroup
            name="state"
            placeholder="State"
            value={state || ''}
            type="text"
            onChange={this.onChange}
            icon="fas fa-map-marker-alt"
          />
          <label className="label">Country</label>
          <Dropdown selected_item={selected_country} dropdown_items={countries} onClick={this.selectCountry} />
          <br /><br />
          <label className="label">Pincode</label>
          <TextInputgroup
            name="pincode"
            placeholder="Pincode"
            value={pincode || ''}
            type="text"
            onChange={this.onChange}
            icon="fas fa-map-marker-alt"
          />
          <br /><br />
          <button onClick={this.updateContactPersonDetails} disabled={selected_person.value === 'Not Selected' || selected_person.value === 'loading...' || selected_person.value === 'Not Available'} className={classnames('button is-primary', { 'is-loading': updating_contact })}>
            <span className="icon">
              <i className="far fa-save"></i>
            </span>
            <span>Save</span>
          </button>
        </div>
      </div>
    )
  }
}

// PropTypes
// ---------
Billing.propTypes = {
  client_details: PropTypes.object.isRequired,
  billing_contacts: PropTypes.object.isRequired,
  admins: PropTypes.array.isRequired,
  getAdmins: PropTypes.func.isRequired,
  getBillingContacts: PropTypes.func.isRequired,
  updateClientDetails: PropTypes.func.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
  client_details: state.client.client_details,
  billing_contacts: state.client.billing_contacts,
  admins: state.client.admins
})

export default connect(mapStateToProps, { getBillingContacts, getAdmins, updateClientDetails, updateContactPersonDetails })(Billing);