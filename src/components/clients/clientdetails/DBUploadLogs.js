import React, { Component } from 'react'
import { connect } from 'react-redux';
import moment from 'moment';

import { getDBUploadLogs } from '../../../actions/ClientsActions';

class DBUploadLogs extends Component {
    state ={
        logs: []
    }

    componentDidMount () {
        this.getDBUploadLogs()
    }
    componentDidUpdate () {
        if(this.props.client_details) {
            this.getDBUploadLogs()
        }
    }
    getDBUploadLogs = async _ =>{
        let response = await this.props.getDBUploadLogs(this.props.client_details.id)
        if(response && response.length) {
            this.setState({logs: response})
        }
    }
    render() {
        const {logs} = this.state
        return (
            <div className="upload-logs">
                <table className="table is-striped is-fullwidth">
                    <thead>
                        <tr>
                            <th>Cohort Name</th>
                            <th>Enabled Users</th>
                            <th>Created Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            logs.map(log => (
                                <tr>
                                    <td>{log.cohort_name}</td>
                                    <td>{log.number_of_enabled_users}</td>
                                    <td>{moment.unix(log.created).format('DD/MM/YYYY')}</td>
                                </tr>
                            ))
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    client_details: state.client.client_details,
})

export default connect(mapStateToProps, {getDBUploadLogs})(DBUploadLogs)