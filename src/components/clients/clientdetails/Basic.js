import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classnames from 'classnames';
// components
import TextInputgroup from '../../generic/TextInputGroup';
// actions
import { updateClientDetails } from '../../../actions/ClientsActions';

class Basic extends Component {
  
  state = {
    id: '',
    title: '',
    title_original: '',
    email_domain: '',
    email_domain_original: '',
    max_data_upload_limit: 0,
    max_data_upload_limit_original: 0,
    poc_emails: '',
    poc_emails_original: '',
    disable_save: true,
    saving: false
  }

  componentDidMount(){
    const {title, email_domain, id, max_data_upload_limit, poc_emails} = this.props.client_details;
    if(this.state.title === '' || this.state.email_domain === '' || this.state.max_data_upload_limit === 0, this.state.poc_emails === ''){
      this.setState({title, email_domain, id, title_original: title, email_domain_original: email_domain, max_data_upload_limit_original: max_data_upload_limit, poc_emails_original: poc_emails})
    }
  }

  componentDidUpdate(prevProps){
    const {title, email_domain, id, max_data_upload_limit, poc_emails} = this.props.client_details;
    if(title !== prevProps.client_details.title || email_domain !== prevProps.client_details.email_domain || id !== prevProps.client_details.id || max_data_upload_limit !== prevProps.client_details.max_data_upload_limit || poc_emails !== prevProps.client_details.poc_emails ){
      this.setState({title, email_domain, id, max_data_upload_limit, poc_emails, title_original: title, email_domain_original: email_domain, max_data_upload_limit_original: max_data_upload_limit, poc_emails_original : poc_emails})
    }
  }

  updateClientDetails = async (e) => {
    e.preventDefault();
    const { title, email_domain, id, max_data_upload_limit, poc_emails } = this.state; 
    const payload = {title: title, email_domain: email_domain, max_data_upload_limit: max_data_upload_limit, poc_emails};
    this.setState({saving: true});
    await this.props.updateClientDetails(id, payload);
    this.setState({title_original: title, email_domain_original: email_domain, max_data_upload_limit_original: max_data_upload_limit, poc_emails_original: poc_emails,  disable_save: true, saving: false});
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value }, () => this.checkIfValuesChanged());
  }

  checkIfValuesChanged = () => {
    const { title, email_domain, title_original, email_domain_original, max_data_upload_limit_original, max_data_upload_limit, poc_emails_original, poc_emails } = this.state;
    
    if(title !== title_original) {
      this.setState({disable_save: false});
    } else if(email_domain !== email_domain_original) {
      this.setState({disable_save: false});
    } else if(max_data_upload_limit !== max_data_upload_limit_original) {
      this.setState({disable_save: false});
    } else if(poc_emails !== poc_emails_original) {
      this.setState({disable_save: false});
    } else {
      this.setState({disable_save: true});
    }
  }

  render() {
    const { title, email_domain, disable_save, saving, max_data_upload_limit, poc_emails } = this.state;
    return (
        <form onSubmit={this.updateClientDetails}>
          <label className="label">Client Name</label>
          <TextInputgroup
              name="title"
              placeholder="Client Name"
              value={title || ''}
              type="text"
              onChange={this.onChange}
              icon="fas fa-user-circle"
          />
          <label className="label">Primary Email Domain</label>
          <TextInputgroup
              name="email_domain"
              placeholder="Email Domain"
              value={email_domain || ''}
              type="text"
              onChange={this.onChange}
              icon="fas fa-envelope"
          />

          <label className="label">Data Upload Limit</label>
          <TextInputgroup
              name="max_data_upload_limit"
              placeholder="Data Upload Limit"
              value={max_data_upload_limit || ''}
              type="number"
              onChange={this.onChange}
              icon="fas fa-upload"
              required={false}
          />

          <label className="label">POC Emails</label>
          <TextInputgroup
              name="poc_emails"
              placeholder="POC Emails (Comma separated)"
              value={poc_emails || ''}
              type="text"
              onChange={this.onChange}
              icon="fas fa-envelope"
              required={max_data_upload_limit ? true : false}
          />

          <button disabled={disable_save} type="submit" className={classnames('button is-primary', {'is-loading': saving})}>
            <span className="icon">
              <i className="far fa-save"></i>
            </span>
            <span>Save</span>
          </button>
        </form>
    )
  }
}

// PropTypes
// ---------
Basic.propTypes = {
  client_details: PropTypes.object.isRequired,
  updateClientDetails: PropTypes.func.isRequired,
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
  client_details: state.client.client_details,
})

export default connect(mapStateToProps, { updateClientDetails })(Basic);
