import React, { Component } from 'react';
import moment from 'moment';
import classnames from 'classnames';

export default class UserTimelineDetails extends Component {

    state = {
        details_open: false
    }

    toggleOpen = () => {
        const { details_open } = this.state;
        this.setState({details_open: !details_open});
    }


    render() {
        const { details_open } = this.state;
        const { projection } = this.props;
        return (
            <div className="timeline-content timeline-content-custom">
                <p className="heading has-text-weight-bold has-text-link no-padding-margin-bottom"> 
                    <span className={classnames('tag is-light is-capitalised', {'is-link': projection.survey_type === 'mtm', 'is-primary': projection.survey_type === 'tenure'})}>{projection.survey_type}</span>
                    &nbsp;<i className="fas fa-arrow-right has-text-grey"></i>&nbsp;
                    {projection.reachout && projection.reachout.scheduled_for ? moment(projection.reachout.scheduled_for).format('DD MMMM YYYY') : 'NA'} 
                    <span className="has-text-dark"> [{projection.survey_title}]</span>
                    <span className={classnames('', {
                        'has-text-grey' : projection.status === 'projected',
                        'has-text-danger': projection.status === 'cancelled',
                        'has-text-info' : projection.status === 'pending', 
                        'has-text-success' : projection.status === 'success'
                    })}> [{projection.status}]</span>
                    <span className="padding-left-5 has-text-grey is-hoverable is-size-6 v-middle-align">
                        {details_open 
                        ? <i onClick={this.toggleOpen} className="far fa-minus-square"></i> 
                        : <i onClick={this.toggleOpen} className="far fa-plus-square"></i>}
                    </span>
                </p>
                {details_open ? <div className="is-size-7">
                    <ul>
                        <li>
                            <b>REACHOUT SCHEDULED FOR:</b> {projection.reachout && projection.reachout.scheduled_for ? moment(projection.reachout.scheduled_for).format('DD MMMM YYYY') : 'Not Scheduled Yet'}
                        </li>
                        <li>
                            <b>REACHOUT SENT ON:</b> {projection.reachout && projection.reachout.sent_at ? moment(projection.reachout.sent_at).format('DD MMMM YYYY') : 'Not Sent Yet'}
                        </li>
                        {projection.survey_type === 'mtm' ? 
                        <>
                            <li>
                                <b>NOTIFICATION SCHEDULED FOR:</b> {projection.pre_reachout && projection.pre_reachout.scheduled_for ? moment(projection.pre_reachout.scheduled_for).format('DD MMMM YYYY') : 'Not Scheduled Yet'}
                            </li>
                            <li>
                                <b>NOTIFICATION SENT ON:</b> {projection.pre_reachout && projection.pre_reachout.sent_at ? moment(projection.pre_reachout.sent_at).format('DD MMMM YYYY') : 'Not Sent Yet'}
                            </li>
                        </>
                        : null}
                    </ul>
                </div> : null}
            </div>
        )
    }
}

