import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { disableBodyScroll, enableBodyScroll } from '../../../../helper/PreventScroll';

// components
import TextInputgroup from '../../../generic/TextInputGroup';
import Pagination from '../../../generic/Pagination';
import ConfirmModal from '../../../generic/ConfirmModal';
import UserTimeline from './UserTimeline';

// Actions
import { getUsers, changeUserStatus, makePrimaryAdmin } from '../../../../actions/ClientsActions';
import { uploadUserCsv, cleanUserCsv, deleteUserCsv} from '../../../../actions/UsersUpload';
import { getIAMPermssions } from '../../../../actions/AccessActions';
import {ScrollToTop} from '../../../../helper/Scroll';
import DynamicTableColumns from './DynamicTableColumns';
import sample_download from '../../../../assets/resources/Sample_DB_Upload.csv'
class Users extends Component {
  state = {
    client_id: null,
    search: '',
    status: 'enable',
    users: [],
    change_status_users: '',
    total_emails: 0,
    total_pages: null,
    current_page: 1,
    total_users: null,
    loading: false,
    adding: false,
    updating: false,
    selected_user: {},
    admin_modal: {
      title: 'Make Admin - ',
      text: 'Are you sure you want to change primary admin?',
      confirm_text: 'Yes',
      is_active: false
    },
    confirm_upload_modal: {
      title: 'Upload Users CSV',
      text: 'Are you sure you want to upload CSV',
      confirm_text: 'Yes',
      is_active: false
    },
    confirm_clean_modal: {
      title: 'Clean CSV',
      text: 'Are you sure you want to clean CSV',
      confirm_text: 'Yes',
      is_active: false
    },
    confirm_delete_modal: {
      title: 'Delete Users',
      text: 'Are you sure you want to delete users from CSV',
      confirm_text: 'Yes',
      is_active: false
    },
    role_mapping: {
      0: "inFeedo's Super Admin",
      1: "Admin",
      2: "IAM Admin",
      3: "Member",
      4: "Manager"
    },
    disable_change: true,
    uploading: false,
    cleaning: false,
    deleting: false,
    upload_files: [],
    delete_files: [],
    clean_files: [],
    extra_columns_list:[
      {
        key : 'department',
        default : true,
        column_name : 'Department'
      },
      {
        key : 'location',
        default : true,
        column_name : 'Location'
      },
      {
        key : 'city',
        enabled : false,
        column_name : 'City'
      },
      {
        key : 'country',
        enabled : false,
        column_name : 'Country'
      },
      {
        key : 'designation',
        enabled : false,
        column_name : 'Designation'
      },
      {
        key : 'vertical',
        enabled : false,
        column_name : 'Vertical'
      },
      {
        key : 'division',
        enabled : false,
        column_name : 'Division'
      },
      {
        key : 'employee_type',
        enabled : false,
        column_name : 'Employee Type'
      },
      {
        key : 'employee_code',
        enabled : false,
        column_name : 'Employee Code'
      },
      {
        key : 'nationality',
        enabled : false,
        column_name : 'Nationality'
      },
      {
        key : 'gender',
        enabled : false,
        column_name : 'Gender'
      },
      {
        key : 'function',
        enabled : false,
        column_name : 'Function'
      },
      {
        key : 'hrbp',
        enabled : false,
        column_name : 'HRBP'
      },
      {
        key : 'manager',
        enabled : false,
        column_name : 'Manager'
      },
      {
        key : 'region',
        enabled : false,
        column_name : 'Region'
      },
      {
        key : 'sub_department',
        enabled : false,
        column_name : 'Sub Department'
      },
      {
        key : 'sub_function',
        enabled : false,
        column_name : 'Sub Function'
      },
      {
        key : 'team',
        enabled : false,
        column_name : 'Team'
      },
      {
        key : 'role',
        enabled : false,
        column_name : 'Role'
      },
      {
        key : 'region',
        enabled : false,
        column_name : 'Region'
      }
    ],
    selected_column_count:0,
    show_columns_modal : false,
    all_columns_selected : false,
    extra_columns_list_copy : {},
    page_size: 40,
    profile: {},
    selected_projection_user: {},
    open_projections: false
  }

  componentDidMount() {
    const { client_id } = this.props.match.params;
    this.setState({
      loading: true,
      extra_columns_list_copy: JSON.parse(JSON.stringify(this.state.extra_columns_list))
    });
    if(sessionStorage.profile) {
      this.setState({profile: JSON.parse(sessionStorage.profile)});
    }
    this.setState({client_id: client_id}, () => {this.getUsers()});
  }

  getUsers = async () => {
    const { current_page, search, client_id } = this.state;
    const response  = await this.props.getUsers(client_id, current_page, search);
    this.setState({users: response.data, total_pages: response.total_pages, total_users: response.total_users, loading: false, page_size: response.page_size})
  }

  // Pagination Methods
  // ------------------
  onNext = () => {
      let { current_page } = this.state;
      current_page += 1; 
      this.setState({current_page: current_page}, () => {this.getUsers()});
      ScrollToTop();
  }

  onPrevious = () => {
      let { current_page } = this.state;
      current_page -= 1;
      this.setState({current_page: current_page}, () => {this.getUsers()});
      ScrollToTop();
  }

  jumpToLast = () => {
      const { total_pages } = this.state;
      this.setState({current_page: total_pages}, () => {this.getUsers()});
      ScrollToTop();
  }

  // Search
  // ------
  onSearch = e => {
    this.setState({ [e.target.name]: e.target.value });
    const search = e.target.value;
    if(search.length > 1) {
        this.setState({current_page: 1}, () => {this.getUsers()});
    } else if(!search.length) {
        this.setState({current_page: 1}, () => {this.getUsers()});
    }
  };
  
  // On submit 
  // ---------
  onSubmit = async (e) => {
    e.preventDefault();
    const { client_id, change_status_users, status } = this.state;
    this.setState({adding: true});
    const users = change_status_users.split(',').map(user => user.trim());
    if (users[users.length - 1] === "") {
      users.pop();
    }
    const enabled = (status === 'enable' ? 1 : 0);
    await this.props.changeUserStatus(client_id, users, enabled);
    this.getUsers();
    this.setState({adding: false, change_status_users: '', total_emails: 0, disable_change: true});
  }

  onChangeEmails = (e) => {
    this.setState({ [e.target.name]: e.target.value });
    const emails = e.target.value;
    if(emails.length > 0) {
      let emails_array = emails.split(',');
      if (emails_array[emails_array.length - 1] === "") {
        emails_array.pop();
      }
      const count = emails_array.length;
      this.setState({total_emails: count, disable_change: false});
    } else {
      this.setState({total_emails: 0, disable_change: true});
    }
  }

  // Radio Button
  // ------------
  setStatus = (e) => {
    this.setState({status: e.target.value});
  }

  // Modal 
  // -----
  makePrimaryAdmin = async () => {
    const { client_id, selected_user } = this.state;
    this.setState({updating: true});
    await this.props.makePrimaryAdmin(client_id, selected_user.id);
    this.closeAdminModal();
    this.getUsers();
    this.setState({updating: false});
  }

  openAdminModal = (user) => {
    const { admin_modal } = this.state;
    admin_modal.title += ` ${user.name}`;
    admin_modal.is_active = true;
    this.setState({admin_modal: admin_modal, selected_user: user});
  }

  closeAdminModal = () => {
    const { admin_modal } = this.state;
    admin_modal.is_active = false;
    admin_modal.title = 'Make Admin - ';
    this.setState({admin_modal: admin_modal});
  }

  uploadCSV = (e) => {
    const files = Array.from(e.target.files)
    this.setState({upload_files: files}, () => this.openConfirmUploadModal());
  }

  confirmUploadCSV = async () => {
    const { upload_files, client_id } = this.state;
    this.setState({uploading: true});
    const formData = new FormData();
    formData.append('csv', upload_files[0]);
    await this.props.uploadUserCsv(client_id, formData);
    this.setState({uploading: false, upload_files: []});
    this.closeConfirmUploadModal();
  }

  openConfirmUploadModal = () => {
    const { confirm_upload_modal, upload_files } = this.state;
    confirm_upload_modal.text += ` - "${upload_files[0].name}" ?`;
    confirm_upload_modal.is_active = true;
    this.setState({confirm_upload_modal: confirm_upload_modal});
  }

  closeConfirmUploadModal = () => {
    const { confirm_upload_modal } = this.state;
    confirm_upload_modal.is_active = false;
    confirm_upload_modal.text = `Are you sure you want to upload CSV`;
    this.setState({confirm_upload_modal: confirm_upload_modal, upload_files: []});
  }

  cleanCSV = (e) => {
    const files = Array.from(e.target.files)
    this.setState({clean_files: files}, () => this.openConfirmCleanModal());
  }

  confirmCleanCSV = async () => {
    const { clean_files ,client_id } = this.state;
    this.setState({cleaning: true});
    const formData = new FormData();
    formData.append('csv', clean_files[0]);
    const response = await this.props.cleanUserCsv(client_id, formData);
    this.setState({cleaning: false});
    this.downloadCSV('emp_data.csv', response);
    this.closeConfirmCleanModal();
  }

  downloadCSV = (name, data) => {
    var encodedUri = encodeURI(data);
    var link = document.createElement("a");
    link.setAttribute("href", `data:attachment/csv;charset=utf-8,${encodedUri}`);
    link.setAttribute("target", "_blank");
    link.setAttribute("download", name);
    document.body.appendChild(link); 
    link.click();
  }

  openConfirmCleanModal = () => {
    const { confirm_clean_modal, clean_files } = this.state;
    confirm_clean_modal.text += ` - "${clean_files[0].name}" ?`;
    confirm_clean_modal.is_active = true;
    this.setState({confirm_clean_modal: confirm_clean_modal});
  }

  closeConfirmCleanModal = () => {
    const { confirm_clean_modal } = this.state;
    confirm_clean_modal.is_active = false;
    this.setState({confirm_clean_modal: confirm_clean_modal, clean_files: []});
  }

  deleteCSV = (e) => {
    const files = Array.from(e.target.files)
    this.setState({delete_files: files}, () => this.openConfirmDeleteModal());
  }

  confirmDeleteCSV = async () => {
    const { delete_files ,client_id } = this.state;
    this.setState({deleting: true});
    const formData = new FormData();
    formData.append('csv', delete_files[0]);
    const response = await this.props.deleteUserCsv(client_id, formData);
    this.setState({deleting: false});
    this.downloadCSV('delete_users.csv', response.fail_list);
    this.closeConfirmDeleteModal();
  }

  openConfirmDeleteModal = () => {
    const { confirm_delete_modal, delete_files } = this.state;
    confirm_delete_modal.text += ` - "${delete_files[0].name}" ?`;
    confirm_delete_modal.is_active = true;
    this.setState({confirm_delete_modal: confirm_delete_modal});
  }

  closeConfirmDeleteModal = () => {
    const { confirm_delete_modal } = this.state;
    confirm_delete_modal.is_active = false;
    this.setState({confirm_delete_modal: confirm_delete_modal, delete_files: []});
  }

  customizeColumns = () => {
    const { show_columns_modal } = this.state;
    this.setState({
      show_columns_modal: !show_columns_modal
    })

  }

   // apply selected columns
  // ----------------------
  applySelectedColumns = () => {
    const {extra_columns_list_copy} = this.state;

    this.setState({
      extra_columns_list: JSON.parse(JSON.stringify(extra_columns_list_copy))
    },() => {this.closeColumnsModal()});
  }

  closeColumnsModal = () =>{
    this.setState({
      show_columns_modal: false
    })
  }

  selectAllColumns = () => {
    const {extra_columns_list_copy , all_columns_selected} = this.state;
    let {selected_column_count} = this.state;

    extra_columns_list_copy.forEach((column) => {
      if(all_columns_selected === true){
        column.enabled = false
      } else {
        column.enabled = true
      }
    })

    if(all_columns_selected === true){
      selected_column_count = this.getDefaultSelectedColumnsCount(extra_columns_list_copy);
    }
    else{
      selected_column_count = extra_columns_list_copy.length;
    }

    this.setState({
        all_columns_selected : !all_columns_selected,
        selected_column_count : selected_column_count,
        extra_columns_list_copy
      });
  }
  // selected columns by user
  // ------------------------
  selectColumn = (index) => {
    const {extra_columns_list_copy} = this.state;
    let {all_columns_selected , selected_column_count} = this.state ;

    if(extra_columns_list_copy[index].enabled){
      extra_columns_list_copy[index].enabled = !extra_columns_list_copy[index].enabled
      selected_column_count  = selected_column_count - 1;
    } else {
      extra_columns_list_copy[index].enabled = true;
      selected_column_count  = selected_column_count + 1;
    }

    if(selected_column_count === extra_columns_list_copy.length){
      all_columns_selected = true;;
    }
    else{
      all_columns_selected = false;
    }
    
    this.setState({
      all_columns_selected : all_columns_selected,
      extra_columns_list_copy : extra_columns_list_copy,
      selected_column_count : selected_column_count
    });
  }

  returnHeaderElements = () => {
    let html = [];
    const { extra_columns_list } = this.state;

    extra_columns_list.forEach((column, index) => {
      if(column.enabled === true || column.default === true ){
        html.push( <th key={index} className="is-size-7"> {column.column_name} </th>)
      }
    })
    return html;
  }

  returnTableContent = (user) => {
    let html = [];
    const { extra_columns_list } = this.state;

    extra_columns_list.forEach((column, index) => {
      if(column.enabled === true || column.default === true ){
        html.push( <td key={index} className="is-size-7"> {user[column.key]} </td>)
      }
    })
    return html;
  }

  getDefaultSelectedColumnsCount = (columns) =>{
    let selected_column_count  = 0;
    columns.forEach((column) => {
      if(column.default === true){
        selected_column_count = selected_column_count + 1
      }
    })
    return selected_column_count;
  }

  openProjectionModal = (selected_projection_user) => {
    disableBodyScroll();
    this.setState({open_projections: true, selected_projection_user});
  }

  closeProjectionModal = () => {
    enableBodyScroll();
    this.setState({open_projections: false, selected_projection_user: {}});
  }

  render() {
    const { client_id, users, role_mapping, total_pages, status, change_status_users, total_emails, current_page, search, loading, adding, updating, admin_modal, disable_change, uploading, cleaning, deleting, confirm_delete_modal, confirm_clean_modal, confirm_upload_modal , all_columns_selected , page_size , show_columns_modal , extra_columns_list_copy, profile, open_projections, selected_projection_user} = this.state;
    return (
      <div>
        <div className="columns">
          <div className="column is-5">
          <TextInputgroup
              name="search"
              placeholder="Search user"
              value={search}
              type="text"
              onChange={this.onSearch}
              icon="fas fa-search"
          />
          </div>
          <div className="column is-1 pd-up-20">
            <button type="submit" className="button is-small" onClick={this.customizeColumns}>  
              <span className="icon is-small">
                <i className="fas fa-table"></i>
              </span>
              <span>Customize Columns</span>
            </button>
          </div>
          <div className="column pd-up-20">
            <div className="buttons is-pulled-right are-small">
              <a href={sample_download} target="_blank" rel="noopener noreferrer" className="button"><span className="icon"><i className="fas fa-file-download"></i></span><span>Sample CSV</span></a>
              <button onClick={() => {document.getElementById('cleanUsers').click();}} className={classnames('button', {'is-loading': cleaning})}><span className="icon"><i className="fas fa-broom"></i></span><span>Clean User CSV</span></button>
              <button onClick={() => {document.getElementById('uploadUsers').click();}} className={classnames('button', {'is-loading': uploading})}><span className="icon"><i className="fas fa-file-upload"></i></span><span>Upload Users</span></button>
              {profile.id === 2 ? <button onClick={() => {document.getElementById('deleteUsers').click();}} className={classnames('button', {'is-loading': deleting})}><span className="icon"><i className="fas fa-trash"></i></span><span>Delete Users</span></button>: null }
              <input accept=".csv" type="file" id='uploadUsers' onChange={this.uploadCSV} hidden/>
              <input accept=".csv" type="file" id='cleanUsers' onChange={this.cleanCSV} hidden/>
              {profile.id === 2 ? <input accept=".csv" type="file" id='deleteUsers' onChange={this.deleteCSV} hidden/> : null}
            </div>
          </div>
        </div>
        <div className="columns">
          <div className="column is-12">
            <form onSubmit={this.onSubmit} className="columns">
              <div className="column is-8">
              <TextInputgroup
                  name="change_status_users"
                  placeholder="Add comma seperated emails to enable / disable"
                  value={change_status_users}
                  type="text"
                  onChange={this.onChangeEmails}
                  icon="fas fa-users"
              />
              </div>
              <div className="column is-2 pd-up-20">
                <div className="control pd-up-5" onChange={this.setStatus.bind(this)}>
                  <label className="radio">
                    <input type="radio" name="enable" value="enable" checked={ status === 'enable' } readOnly/>
                     &nbsp;Enable
                  </label>&nbsp;&nbsp;
                  <label className="radio">
                    <input type="radio" value="disable" name="disable" checked={ status === 'disable' } readOnly/>
                    &nbsp;Disable
                  </label>
                </div>
              </div>
              <div className="column is-2 pd-up-20">
                <button type="submit" className={classnames('button is-success is-fullwidth',{'is-loading': adding})} disabled={disable_change}>
                  <span className="icon">
                    <i className="fas fa-retweet"></i>
                  </span>
                  <span>Change ({total_emails})</span>
                </button>
              </div>
            </form>
          </div>
        </div>
        <div className="card">
          <div className="card-content is-paddingless">
            <div className="content">
              <div className="custom-table">
                <table className="table is-striped">
                  <thead>
                      <tr>
                      <th className="is-size-7">S.No.</th>
                      <th className="is-size-7">Employee</th>
                      {this.returnHeaderElements()}
                      <th className="is-size-7">User Status</th>
                      <th className="is-size-7">User Type</th>
                      <th className="is-size-7 has-text-right">Actions</th>
                      </tr>
                  </thead>
                  <tbody>
                  {!loading ? users.map((user, index) => (
                    <tr key={index} className={classnames('',{'disable-bg': user.enabled === 0})}>
                    <td className="is-size-7">{index + 1 + (current_page - 1) * page_size}</td>
                    <td className="is-size-7">
                      <Link to={`/admin-panel/clients/${this.state.client_id}/users/${user.id}`}>
                          <b>{user.name}</b>
                      </Link>
                      {/* <b>{user.name}</b> */}
                      <br/> <span className="is-size-7">{user.email}</span>
                    </td>
                    {this.returnTableContent(user)}
                    <td className="is-size-7">{user.enabled === 0 ? <span className="has-text-danger">Disabled</span> : <span className="has-text-success">Enabled</span>}</td>
                    <td className="is-size-7">{user.primary_admin === user.id ? 'Primary Admin' : role_mapping[user.role]}</td>
                    <td>
                      <div className="buttons is-right">
                          <button onClick={this.openAdminModal.bind(this, user)} className={classnames('button is-link is-small is-rounded is-outlined')} disabled={user.primary_admin === user.id}>
                              <span>Make Primary Admin</span>
                          </button>
                          <button onClick={this.openProjectionModal.bind(this, user)} className={classnames('button is-primary is-small is-rounded is-outlined')}>
                              <span>View Reach-out Timeline</span>
                          </button>
                          {/* <br/>
                          {user.role === 2 ? <button onClick={this.getIAMPermssions.bind(this, user)} className={classnames('button is-dark is-small is-rounded is-outlined')} disabled={user.role !== 2}>
                              <span>&nbsp;&nbsp;&nbsp;View Permissions&nbsp;&nbsp;&nbsp;</span>
                          </button> : null} */}
                      </div>
                    </td>
                    </tr>
                  )): null}
                  </tbody>
                </table>
                {(users.length === 0 && !loading) ? <p className="no-data has-text-centered">No Data</p> : null}
                {loading ? <p className="no-data has-text-centered">Loading Data...</p> : null}
              </div>
            </div>
          </div>
        </div>
        <br/>
          {loading
              ? null 
              : <Pagination onNext={this.onNext} onPrevious={this.onPrevious} jumpToLast={this.jumpToLast} current_page={current_page} total_pages={total_pages}/>
          }
           <ConfirmModal 
            title={admin_modal.title}
            text={admin_modal.text}
            confirm_text={admin_modal.confirm_text}
            onConfirm={this.makePrimaryAdmin}
            onClose={this.closeAdminModal}
            is_active={admin_modal.is_active}
            doing={updating}/>

            <ConfirmModal 
            title={confirm_upload_modal.title}
            text={confirm_upload_modal.text}
            confirm_text={confirm_upload_modal.confirm_text}
            onConfirm={this.confirmUploadCSV}
            onClose={this.closeConfirmUploadModal}
            is_active={confirm_upload_modal.is_active}
            doing={uploading}/>

            <ConfirmModal 
            title={confirm_clean_modal.title}
            text={confirm_clean_modal.text}
            confirm_text={confirm_clean_modal.confirm_text}
            onConfirm={this.confirmCleanCSV}
            onClose={this.closeConfirmCleanModal}
            is_active={confirm_clean_modal.is_active}
            doing={cleaning}/>

            <ConfirmModal 
            title={confirm_delete_modal.title}
            text={confirm_delete_modal.text}
            confirm_text={confirm_delete_modal.confirm_text}
            onConfirm={this.confirmDeleteCSV}
            onClose={this.closeConfirmDeleteModal}
            is_active={confirm_delete_modal.is_active}
            doing={deleting}/>

            <DynamicTableColumns
            is_active={show_columns_modal}
            applyColumns={this.applySelectedColumns}
            changeColumns={this.selectColumn}
            onClose={this.closeColumnsModal}
            columns={extra_columns_list_copy}
            onSelectAll = {this.selectAllColumns}
            allColumnsselected = {all_columns_selected}
            />
            {client_id ? 
              <UserTimeline
                is_active={open_projections}
                client_id={parseInt(client_id)}
                user={selected_projection_user}
                onClose={this.closeProjectionModal}
              />
            : null}
      </div>
    )
  }
}

// PropTypes
// ---------
Users.propTypes = {
  getUsers: PropTypes.func.isRequired,
  changeUserStatus: PropTypes.func.isRequired,
  uploadUserCsv: PropTypes.func.isRequired,
  cleanUserCsv: PropTypes.func.isRequired, 
  deleteUserCsv: PropTypes.func.isRequired,
  getIAMPermssions: PropTypes.func.isRequired,
}

export default connect(null, {getUsers, changeUserStatus, makePrimaryAdmin, uploadUserCsv, cleanUserCsv, deleteUserCsv, getIAMPermssions})(withRouter(Users));