import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const DynamicTableColumns = ({
    is_active,
    onClose,
    changeColumns,
    applyColumns,
    columns,
    onSelectAll,
    allColumnsselected
  }) => {
return (
    <div className={classnames('modal', {'is-active': is_active})}>
        <div className="modal-background" onClick={onClose}></div>
        <div className="modal-card border-5 has-text-left">
            <section className="modal-card-body round-up-modal">
            <div className="columns">
                <div className="column">
                    <span className="select-all-title">Select Columns</span>
                    <button onClick = {onSelectAll} className="button is-small is-white inverted-button is-pulled-right no-padding-top"><span> { allColumnsselected ? 'Deselect All' : 'Select All' }</span></button>
                </div>
            </div>
            <div className="columns has-text-left flex-wrap pd-20">
            {columns.length ? columns.map((column, index) => (
            <div key={index} className="column is-4 is-paddingless">
            <label className="checkbox" disabled={false}>
                <input 
                checked={column.default || (column.enabled ? column.enabled : false)} 
                disabled={column.default} 
                onChange={changeColumns.bind(this, index)}
                type="checkbox"/>
                <span className={classnames('checkbox-title', {'has-text-weight-bold': (column.default || column.enabled)})}>{column.column_name}</span>
            </label>
            </div>
            )): null}
            </div>
            </section>
            <footer className="modal-card-foot filters-footer">
                <button onClick={applyColumns} className="button is-link apply">Apply</button>
                <button onClick={onClose} className="button button is-white transparent-bg inverted-button">Cancel</button>
            </footer>
        </div>
    </div>
);
};

// Porptypes
// ---------
DynamicTableColumns.propTypes = {
    is_active: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    changeColumns: PropTypes.func.isRequired,
    applyColumns: PropTypes.func.isRequired
};

// default props
// -------------
DynamicTableColumns.defaultProps = {
    is_active: false
};

export default DynamicTableColumns;
