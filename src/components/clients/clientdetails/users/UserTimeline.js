import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import moment from 'moment';
import Switch from "react-switch";
import { getUserProjections } from '../../../../actions/UserActions';
import UserTimelineDetails from './UserTimelineDetails';

class UserTimeline extends Component {

    state = {
        projections: [],
        loading_projections: false,
        show_cancelled: false
    }

    componentDidMount(){

    }

    componentDidUpdate(prevProps){
        const { is_active } = this.props;
        if(is_active !== prevProps.is_active && is_active){
            this.getUserProjections();
        }
    }

    getUserProjections =  async () => {
        const { client_id, user } = this.props;
        this.setState({loading_projections: true});
        const response = await this.props.getUserProjections(client_id, user.id);
        if(response && response.status === 200){
            let projections = [];
            if(response.data && response.data.length){
                projections = response.data;
            } 
            this.setState({projections});
        }
        this.setState({loading_projections: false});
    }

    toggleShowCancelled = () => {
        const { show_cancelled } = this.state;
        this.setState({show_cancelled: !show_cancelled});
    }

    render() {
        const { is_active, onClose, user } = this.props;
        const { loading_projections, projections, show_cancelled } = this.state;
        return (
            <div className={classnames('modal', {'is-active': is_active})}>
            <div className="modal-background"></div>
                <div className="modal-card width-740 radius-access">
                    <header className="modal-card-head">
                        <p className="modal-card-title is-marginless is-size-5">{user.name}<span className="is-size-7">({user.email})</span></p>
                        <button onClick={onClose} className="delete" aria-label="close"></button>
                    </header>
                    <section className="modal-card-body">
                        <div className="has-text-right">
                            <span className="is-size-7 toggle-text">{show_cancelled ? 'Hide' :'Show'} Cancelled Projections &nbsp;</span>
                            <Switch onChange={this.toggleShowCancelled} checked={(show_cancelled ? true : false)}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={18} width={35}/>
                        </div>
                        {
                            loading_projections ? <p className="has-text-centered padding-40">loading...</p> 
                            :
                            (
                                projections.length ? 
                                    <div className="timeline">
                                        <header className="timeline-header">
                                            <span className="tag is-medium is-primary">Start</span>
                                        </header>
                                        {
                                            projections.map((projection, index) => (
                                                (projection.status !== (show_cancelled ? '' : 'cancelled') ?
                                                    <div key={index} className="timeline-item">
                                                        <div className={classnames('timeline-marker', {
                                                            'is-grey' : projection.status === 'projected',
                                                            'is-danger': projection.status === 'cancelled',
                                                            'is-info' : projection.status === 'pending', 
                                                            'is-success' : projection.status === 'success'
                                                        })}></div>
                                                        <UserTimelineDetails 
                                                            projection={projection}
                                                        />
                                                    </div>
                                                : null)
                                            ))
                                        }
                                        <div className="timeline-header">
                                            <span className="tag is-medium is-primary">End</span>
                                        </div>
                                    </div>
                                : <p className="has-text-centered padding-40">No Data Available</p> 
                            )
                        }
                    </section>
                </div>
            </div>
        )
    }
}

// PropTypes
// ---------
UserTimeline.propTypes = {
    is_active: PropTypes.bool.isRequired,
    client_id: PropTypes.number.isRequired,
    user: PropTypes.object.isRequired,
    getUserProjections: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired
}
  
export default connect(null, {getUserProjections})(UserTimeline);