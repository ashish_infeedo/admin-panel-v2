import React, { Component } from 'react';
import { getCohorts, getTemplates, uploadSignature } from '../../../../actions/EmailActions';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import EmailBox from './EmailBox';
import { flatMap } from 'lodash';
import { isMobileDevice } from '../../../../helper/IsMobileDevice';

class Emails extends Component {
    state = {
        tenure_templates: {
            reachout_templates: {
                name: 'Reachouts',
                templates: [],
            },
            reminder_templates: {
                name: 'Reminders',
                templates: [],
            },
            acknowledgement_templates: {
                name: 'Acknowledgement Emails',
                templates: [],
            }
        },
        slack_templates: {
            reachout_templates: {
                name: 'Reachouts',
                templates: [
                    {
                        content:"",
                        language_id:1,
                        subject:"",
                        title:"Reachout",
                        type:1
                    },
                ],
            },
            reminder_templates: {
                name: 'Reminders',
                templates: [
                    {
                        paragraph1: '',
                        paragraph2: '',
                        language_id:1,
                        subject:"",
                        title:"Client Reminder",
                        type:2
                    },
                    {   
                        paragraph1: '',
                        paragraph2: '',
                        language_id:1,
                        subject:"",
                        title:"Dropped Reminder",
                        type:3
                    },
                    
                ],
            }
        },
        welcome_email_templates: {
            all_templates: {
                name: 'Welcome Templates',
                templates: [],
            },
        },
        cohorts: [],
        selected_cohort: {},
        client_id: null,
        selected_tab: 'tenure',
        loading: true,
        uploading: false,
        url: '',
        language_map: {
            1: 'English',
            2: 'French',
            3: 'Indonesian',
            4: 'Malay',
        },
        selected_language: 1,
    };

    //Did Mount
    componentDidMount() {
        this.setClientId();
    }

    //Did Update
    componentDidUpdate(prevProps, prevState) {
        const { client_details } = this.props;
        const { client_id, selected_cohort, selected_language, selected_tab } = this.state;
        if (JSON.stringify(prevProps.client_details) !== JSON.stringify(client_details)) {
            this.setClientId();
        }
        if (prevState.client_id !== client_id) {
            this.setCohorts();
        }
        if (
            JSON.stringify(selected_cohort) !== JSON.stringify(prevState.selected_cohort) ||
            selected_language !== prevState.selected_language ||
            selected_tab !== prevState.selected_tab
        ) {
            this.setTemplates();
        }
    }

    // Setting Client ID
    setClientId = () => {
        const { client_details } = this.props;
        if (client_details && client_details.id !== undefined) {
            this.setState({ client_id: client_details.id });
        }
    };

    //Setting Cohorts
    setCohorts = async () => {
        const { client_id } = this.state;
        const { getCohorts } = this.props;
        let default_cohort = {};
        let response = await getCohorts(client_id);
        if (response && response.data && response.status === 200) {
            response.data.forEach(cohort => {
                if (cohort.title === 'Default') {
                    default_cohort = cohort;
                }
            });
            if(!Object.keys(default_cohort).length)
            {
                default_cohort = response.data[0];
            }
            this.setState({ cohorts: response.data, selected_cohort: default_cohort }, () => {
                this.setTemplates();
            });
        }
    };

    //Setting Templates.........................
    setTemplates = async () => {
        const { selected_cohort, cohorts, selected_language, client_id, tenure_templates, selected_tab, welcome_email_templates } = this.state;
        const { getTemplates } = this.props;
        this.setState({ loading: true });
        let params = {};
        if (cohorts.length > 1 && selected_cohort.id !== undefined) {
            params.cohort_id = selected_cohort.id;
        }
        if (selected_tab === 'welcome_email') {
            params.welcome_email = true;
        }
        params.language_id = selected_language;
        let response = await getTemplates(params, client_id);
        if (response && response.status === 200 && response.data && response.data) {
            if (selected_tab === 'tenure') {
                let temp_reachout_templates = [];
                let temp_reminder_templates = [];
                let temp_ackw_templates = [];
                temp_reachout_templates.push(
                    ...response.data.splice(0, 5).map((cohort, index) => {
                        cohort.title = `React Out #${index} in ${selected_cohort.title} cohort`;
                        return cohort;
                    })
                );
                temp_reachout_templates.push(
                    ...response.data.splice(0, 1).map((cohort, index) => {
                        cohort.title = `Reduce Sales Cycle React Out in ${selected_cohort.title} cohort`;
                        return cohort;
                    })
                );
                temp_reminder_templates.push(
                    ...response.data.splice(0, 4).map((cohort, index) => {
                        cohort.title = `Reminder #${index} from Email Sender in ${selected_cohort.title} cohort`;
                        return cohort;
                    })
                );
                temp_reminder_templates.push(
                    ...response.data.splice(0, 4).map((cohort, index) => {
                        cohort.title = `Reminder #${index} from Amber in ${selected_cohort.title} cohort`;
                        return cohort;
                    })
                );
                temp_ackw_templates.push(
                    ...response.data.splice(0, 1).map((cohort, index) => {
                        cohort.title = `Positive Acknowledgment Mail in ${selected_cohort.title} cohort`;
                        return cohort;
                    })
                );
                temp_ackw_templates.push(
                    ...response.data.splice(0, 1).map((cohort, index) => {
                        cohort.title = `Neutral Acknowledgment Mail in ${selected_cohort.title} cohort`;
                        return cohort;
                    })
                );
                tenure_templates.reachout_templates.templates = temp_reachout_templates;
                tenure_templates.reminder_templates.templates = temp_reminder_templates;
                tenure_templates.acknowledgement_templates.templates = temp_ackw_templates;
                this.setState({ tenure_templates });
            } 
            
            else if (selected_tab === 'welcome_email') {
                let temp_wlcm_templates = [];
                temp_wlcm_templates.push(
                    ...response.data.map((cohort, index) => {
                        cohort.title = `Welcome Email #${index} in ${selected_cohort.title} cohort`;
                        return cohort;
                    })
                );
                welcome_email_templates.all_templates.templates = temp_wlcm_templates;
                this.setState({ welcome_email_templates });
            }

        }
        this.setState({ loading: false });
    };

    //change Tab
    changeTab = selected_tab => {
        this.setState({ selected_tab, selected_language: 1 });
    };

    // file upload
    handleFiles = async e => {
        const files = Array.from(e.target.files);
        this.setState({ uploading: true });
        const formData = new FormData();
        formData.append('image', files[0]);
        const url = await this.props.uploadSignature(formData);
        this.setState({ uploading: false, url: url.url });
    };
    // Copy Text
    // ---------
    copyText = id => {
        const copyText = document.querySelector(`#${id}`);
        copyText.select();
        document.execCommand('copy');
    };

    //update language
    updateLanguage = key => {
        this.setState({ selected_language: key });
    };

    //update Cohort
    updateCohort = cohort => {
        this.setState({ selected_cohort: cohort });
    };

    setSlackTemplate = (template_type, index, template) => {
        let { slack_templates } = this.state;
        let temp_data_title = (slack_templates[template_type]).templates[index].title
        template.title = temp_data_title;
        (slack_templates[template_type]).templates[index] = template

        this.setState({slack_templates})
    }

    render() {
        const {
            selected_tab,
            tenure_templates,
            welcome_email_templates,
            loading,
            uploading,
            url,
            language_map,
            selected_language,
            cohorts,
            selected_cohort,
            slack_templates
        } = this.state;
        const {client_details} = this.props;
        return (
            <div>
                <div className="columns">
                    { client_details && client_details.multilingual_enabled ?
                    <div className="column is-4">
                        <div>
                            <h1 className="emails-heading-h1"> Select Language</h1>
                        </div>
                        <div className={`dropdown is-hoverable `}>
                            <div className="dropdown-trigger">
                                <button
                                    type="button"
                                    className="button min-width-100"
                                    aria-haspopup="true"
                                    aria-controls="dropdown-menu4">
                                    <span>{language_map[selected_language]}</span>
                                    <span className="icon is-small">
                                        <i className="fas fa-angle-down" aria-hidden="true"></i>
                                    </span>
                                </button>
                            </div>
                            <div className="dropdown-menu min-width-100" id="dropdown-menu4" role="menu">
                                <div className="dropdown-content dropdown-fix-height">
                                    {Object.keys(language_map).length ? (
                                        Object.entries(language_map).map(([key, value], index) => (
                                            <a key={index} onClick={this.updateLanguage.bind(this, key)} className={`dropdown-item`}>
                                                {value}
                                            </a>
                                        ))
                                    ) : (
                                        <a className="dropdown-item">No Data</a>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                     : null }
                    <div className="column is-4 ">
                        {cohorts.length > 1 ? (
                            <div>
                                <div>
                                    <h1 className="emails-heading-h1"> Select Cohort</h1>
                                </div>
                                <div className={`dropdown is-hoverable `}>
                                    <div className="dropdown-trigger">
                                        <button
                                            type="button"
                                            className="button min-width-100"
                                            aria-haspopup="true"
                                            aria-controls="dropdown-menu4">
                                            <span>{selected_cohort.title ? selected_cohort.title : ''}</span>
                                            <span className="icon is-small">
                                                <i className="fas fa-angle-down" aria-hidden="true"></i>
                                            </span>
                                        </button>
                                    </div>
                                    <div className="dropdown-menu min-width-100" id="dropdown-menu4" role="menu">
                                        <div className="dropdown-content">
                                            {cohorts.length ? (
                                                cohorts.map((cohort, index) => (
                                                    <a
                                                        key={index}
                                                        onClick={this.updateCohort.bind(this, cohort)}
                                                        className={`dropdown-item`}>
                                                        {cohort.title ? cohort.title : ''}
                                                    </a>
                                                ))
                                            ) : (
                                                <a className="dropdown-item">No Data</a>
                                            )}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ) : null}
                    </div>
                    <div className="column  width-100 ">
                        <div className={classnames('', { 'email-signature-box': !isMobileDevice() })}>
                            <div>
                                <label className="file-label ">
                                    <input
                                        className="file-input"
                                        type="file"
                                        accept="image/*"
                                        name="signature"
                                        onChange={this.handleFiles}
                                    />
                                    <span className="file-cta">
                                        <span className="file-icon">
                                            <i className="fas fa-upload"></i>
                                        </span>
                                        {uploading ? (
                                            <span className="file-label">Uploading...</span>
                                        ) : (
                                            <span className="file-label">Upload Signature</span>
                                        )}
                                    </span>
                                </label>
                                <p className="help mg-10 has-text-right">
                                    {url ? 'Copy the image URL from below' : 'Note: Keep the dimension below 700 X 500'}
                                </p>
                            </div>
                        </div>
                        {url ? (
                            <div className="columns column">
                                <div className="card column is-offset-10 is-2 is-paddingless is-pulled-right">
                                    <div className="card-image">
                                        <figure className="image">
                                            <img src={url} alt="Placeholder image" className="sign-img" />
                                        </figure>
                                    </div>
                                    <div className="card-content">
                                        <div className="field has-addons is-small">
                                            <div className="control">
                                                <input className="input is-small" id="signatureurl" type="text" value={url} readOnly />
                                            </div>
                                            <div className="control" onClick={this.copyText.bind(this, 'signatureurl')}>
                                                <a className="button is-small is-info">
                                                    <i className="fas fa-copy"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ) : null}
                    </div>
                </div>
                <div className="tabs is-centered is-small is-toggle is-toggle-rounded">
                    <ul>
                        <li
                            className={classnames({ 'is-active': selected_tab === 'tenure' })}
                            onClick={this.changeTab.bind(this, 'tenure')}>
                            <a>
                                <span className="icon is-small">
                                    <i className="fas fa-briefcase"></i>
                                </span>
                                <span>Tenure</span>
                            </a>
                        </li>
                        <li
                            className={classnames({ 'is-active': selected_tab === 'welcome_email' })}
                            onClick={this.changeTab.bind(this, 'welcome_email')}>
                            <a>
                                <span className="icon is-small">
                                    <i className="fas fa-envelope-open-text"></i>
                                </span>
                                <span>Welcome Email</span>
                            </a>
                        </li>
                    </ul>
                </div>
                {loading ? (
                    <p className="no-data has-text-centered">Loading Data...</p>
                ) : (
                    <EmailBox selected_language={selected_language} setSlackTemplate={this.setSlackTemplate} slack_templates={slack_templates} template_array={selected_tab === 'tenure' ? tenure_templates : welcome_email_templates} />
                )}
            </div>
        );
    }
}

Emails.propTypes = {
    client_details: PropTypes.object.isRequired,
};

// Map Redux state to props
// ------------------------
const mapStateToProps = state => ({
    client_details: state.client.client_details,
});

export default connect(mapStateToProps, { getCohorts, getTemplates, uploadSignature })(Emails);
