import React, { Component } from 'react';
import PropTypes from 'prop-types';
import parse from 'html-react-parser';

export default class ErrorStack extends Component {
    render() {
        const { error_array } = this.props;
        return (
            <div className="error-stack-div display-flex mg-bottom-20 pd-20">
                <span className="eng-oval error-info-icon  no-margin-left">!</span>
                <ol type="1">{error_array.length ? error_array.map((error, index) => <li key={index} className="display-list">{parse(error)}</li>) : null}</ol>
            </div>
        );
    }
}

ErrorStack.defaultProps = {
    error_array: [],
};

ErrorStack.propTypes = {
    error_array: PropTypes.array,
};
