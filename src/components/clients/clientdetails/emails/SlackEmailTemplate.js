import React, { Component } from 'react';
import classnames from 'classnames';
import { stringChecker } from './VariableStringChecker';
import ErrorStack from './ErrorStack';

//assets
import slack_reachout from '../../../../assets/images/slack-reachout.svg'
import slack_reminder_ceo from '../../../../assets/images/slack-reminder-ceo.svg'
import slack_reminder_dropped from '../../../../assets/images/slack-reminder-dropped.svg'



export class SlackEmailTemplate extends Component {
    state = {
        original_data_copy: {},
        email_data: {},
        btn_loading: false,
        editing_dropdown_active : false,
        error_messages: [],
        error_blocks: [],
        templates_variables: {
            subject : {
                mandatory: [],
                can_have: ['{{{company_name}}}', '{{{period}}}', '{{{email_sender}}}', '{{{pendingQuestions}}}', '{{{email_sender_designation}}}', '{{{company_name}}}', '{{{first_name}}}'],
                not_allowed: [],
            },
            paragraph1 : {
                mandatory: [`{{{first_name}}}`],
                can_have: ['{{{company_name}}}', '{{{period}}}', '{{{email_sender}}}', '{{{pendingQuestions}}}', '{{{email_sender_designation}}}', '{{{company_name}}}'],
                not_allowed: [],
            },
            paragraph2 : {
                mandatory: [],
                can_have: ['{{{company_name}}}', '{{{period}}}', '{{{email_sender}}}', '{{{pendingQuestions}}}', '{{{email_sender_designation}}}', '{{{company_name}}}', '{{{first_name}}}'],
                not_allowed: [],
            },
            signature :{
                mandatory: [],
                can_have: ['{{{company_name}}}', '{{{period}}}', '{{{email_sender}}}', '{{{pendingQuestions}}}', '{{{email_sender_designation}}}', '{{{company_name}}}', '{{{first_name}}}'],
                not_allowed: []
            },
        }

    };

    componentDidMount() {
        const {email_data} = this.props
        this.setState({email_data, original_data_copy:JSON.stringify(email_data)})
    }
    
    componentDidUpdate(prevProps) {
        const {email_data} = this.props
        if(JSON.stringify(prevProps.email_data) !== JSON.stringify(email_data)) {
            this.setState({email_data, original_data_copy:JSON.stringify(email_data)})
        }
    }

    

    // editor state change
    //---------------------------------------------------
    

    updateEmailTemplate = async () => { 
        this.setState({ btn_loading: true });
        await this.validateConditions();
        
        if(this.state.error_blocks.length === 0) {
            await this.props.updateTemplate(this.state.email_data);
        }
        this.setState({ btn_loading: false });
    };

    // validate conditions
    validateConditions = () => {
        const { email_data, templates_variables } = this.state;
        let error_messages = [];
        let error_blocks = [];
        
        let first_name_found = false;

        Object.keys(email_data).forEach((key, index) => {
            if(Object.keys(templates_variables).includes(key)) {
                let temp_array = stringChecker(templates_variables[key], email_data[key], undefined, '', ` in ${key}`);
                if (temp_array && temp_array.length) {
                    if (!error_blocks.includes(index)) {
                        error_blocks.push(index);
                    }
                    error_messages = error_messages.concat(temp_array);
                }
                if (key === 'paragraph1' && email_data[key].includes(`{{{first_name}}}`) && !first_name_found) {
                    first_name_found = true;
                }
                if (!email_data[key].length) {
                    error_messages.push(`${key} cannot be blank`);
                    if (!error_blocks.length) {
                        error_blocks.push(0);
                    }
                }
            }
        })
        
        if (!first_name_found) {
            error_messages.push(`<b>{{{first_name}}}</b> is missing`);
            if (!error_blocks.length) {
                error_blocks.push(0);
            }
        }
        this.setState({ error_messages, error_blocks });
    };

    openAndCloseEditingDropdown = () => {
        const {editing_dropdown_active} = this.state;
        this.setState({editing_dropdown_active : !editing_dropdown_active})
    }

    onChange = (e) => {
        const{email_data} = this.state;
        email_data[e.target.name] = e.target.value;
        this.setState({email_data});
    }

    render() {
        const { email_data, original_data_copy, btn_loading, editing_dropdown_active, error_messages, error_blocks} = this.state;
        return (
            <div >
                {Object.keys(email_data).length ? (
                    <div className="pd-10 slack-template">
                        <div className = "display-flex">
                            <span className="email-box-heading">{email_data.title}</span>
                            <div className="editing-help-box">
                                <div className={classnames('dropdown', {'is-active' : editing_dropdown_active})} onClick = {this.openAndCloseEditingDropdown}>
                                    <div className="dropdown-trigger">
                                        <button
                                            className="button is-size-7 width-180 justify-space-between"
                                            aria-haspopup="true"
                                            aria-controls="dropdown-menu2">
                                            Editing Help
                                            <span className="icon is-small marginleft-xs">
                                                <i className="fas fa-question-circle "></i>
                                            </span>
                                        </button>
                                    </div>
                                    <div className="dropdown-menu no-margin editing-help-box-menu" id="dropdown-menu2" role="menu">
                                        <div className="dropdown-content pd-20">
                                            <div>
                                                <div>
                                                    <h1>
                                                        <b>Here are some editing shortcuts for your help:</b>
                                                    </h1>
                                                    <ul className="paddingleft-md">
                                                        <li>{`First Name: {{{first_name}}}`}</li>
                                                        <li>{`Company Name: {{{company_name}}}`}</li>
                                                        <li>{`Name of Email Sender : {{{email_sender}}}`}</li>
                                                        <li>{`Designation of Email Sender: {{{email_sender_designation}}}`}</li>
                                                        <li>{`Period: {{{period}}}`}</li>
                                                        <li>{`Pending Questions: {{{pendingQuestions}}}`}</li>
                                                    </ul>
                                                </div>
                                                <br />
                                                <div>
                                                    <h1>
                                                        <b>Slack Shortcuts:</b>
                                                    </h1>
                                                    <ul className="paddingleft-md ">
                                                        <li><strong>Bold</strong> *word you want to bold*</li>
                                                        <li><i>Italic</i> _word you want to italicize_</li>
                                                        <li>😀 {`:smile:`}</li>
                                                        <li>👋 {`:wave:`}</li>
                                                        <li>❤️ {`:heart:`}</li>
                                                        <li>👇 {`:point_down:`}</li>
                                                        <li>🙂 {`:slightly_smiling_face:`}<br/>{`or you can use any other emoji to from the slack directory`}</li>
                                                    </ul>
                                                </div>
                                                <br />
                                                <div>
                                                    <h1>
                                                        <b>Best Email Practices:</b>
                                                    </h1>
                                                    <ul className="paddingleft-md ">
                                                        <li>{`Keep it short and simple`}</li>
                                                        <li>{`Make it Personal. People love their name`}</li>
                                                        <li>{`Keep the subject line catchy`}</li>
                                                        <li>{`Make sure you include the link to Amber's chat otherwise people will not be able to interact with her.`}</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="column no-padding-bottom mg-bottom-10">
                            <span><strong>Reference:</strong></span>
                        </div>
                        <div className="sample-img-container column">
                            <img className="border-all-grey padding-10" src={email_data.type === 1? slack_reachout : email_data.type === 2 ? slack_reminder_ceo : slack_reminder_dropped} alt="sample-image"/>
                        </div>
                        <div  className="email-header align-center">
                            <p className="padding-left-10 is-size-7">
                                <span className="has-text-danger">Note:</span> While editing, type <strong>\n</strong> to start a <strong>new line within a paragraph</strong>
                            </p>
                            <div className="column no-padding-bottom mg-bottom-10">
                                <span><strong>Subject:</strong></span>
                            </div>
                            <div className="column no-padding-top">
                                <input name="subject" type="text" rows= "5" className="input font-14" value = {email_data.subject ? email_data.subject : ''} onChange = {this.onChange}/>
                            </div>
                        </div>
                        <div id="email-content-p1" className="mg-top-bottom-10 padding-10 email-content">
                            <div className="mg-bottom-10">
                                <span><strong>Paragraph 1:</strong></span>
                            </div>
                            <textarea
                                rows={7}
                                name={`paragraph1`}
                                value={email_data.paragraph1 ? email_data.paragraph1 : ''}
                                onChange={this.onChange}
                                autoFocus={false}
                                placeholder={''}
                                required={true}
                                className={`input`}
                            />
                        </div>
                        <div id="email-content-p2" className="mg-top-bottom-10 padding-10 email-content">
                            <div className="mg-bottom-10">
                                <span><strong>Paragraph 2:</strong></span>
                            </div>
                            <textarea
                                rows={3}
                                name={`paragraph2`}
                                value={email_data.paragraph2 ? email_data.paragraph2 : ''}
                                onChange={this.onChange}
                                autoFocus={false}
                                placeholder={''}
                                required={true}
                                className={`input`}
                            />
                        </div>
                        <div className="mg-top-10 padding-10 email-content">
                            <button disabled className="button is-outlined">
                                Chat with Amber!
                            </button>
                        </div>
                        <div  className="email-header align-center no-margin">
                            <div className="column no-padding-bottom mg-bottom-10">
                                <span><strong>Signature:</strong></span>
                            </div>
                            <div className="column no-padding-top">
                                <input type="text" name="signature" rows= "5" className="input font-14" value = {email_data.signature ? email_data.signature : ''} onChange = {this.onChange}/>
                            </div>
                        </div>
                        {error_messages.length ? <ErrorStack error_array={error_messages} /> : null}
                        <button
                            onClick={this.updateEmailTemplate}
                            disabled={original_data_copy === JSON.stringify(email_data)}
                            className={classnames('button is-success  email-save-btn ', {
                                'is-loading': btn_loading,
                            })}>
                            {'Save'}
                        </button>
                    </div>
                ) : null}
            </div>
        );
    }
}

export default (SlackEmailTemplate)
 