import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

class EmailsBox extends Component {
    state = {
        original_data_copy: {},
        email_data: {},
        editorState: EditorState.createEmpty(),
        toolbar: { options: ['inline', 'link'] },
        btn_loading: false,
        editing_dropdown_active : false
    };

    componentDidMount() {
        const { email_data } = this.props;
        if (Object.keys(email_data).length) {
            this.createEditorState();
        }
    }

    componentDidUpdate(prevProps) {
        const { email_data } = this.props;
        if (JSON.stringify(email_data) !== JSON.stringify(prevProps.email_data) && Object.keys(email_data).length) {
            this.createEditorState();
        }
    }

    // editor state
    //----------------------------------------------------
    createEditorState = () => {
        const { email_data } = this.props;
        const local_data_copy = JSON.parse(JSON.stringify(email_data));
        const { original_data_copy } = this.state;
        const contentBlock = htmlToDraft('<p></p>' + local_data_copy.content);
        if (contentBlock) {
            const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
            const editorState = EditorState.createWithContent(contentState);
            const parsed_content = draftToHtml(convertToRaw(editorState.getCurrentContent()));
            local_data_copy.content = parsed_content;
            this.setState({ original_data_copy: JSON.stringify(local_data_copy), editorState, email_data: local_data_copy });
        }
    };

    // editor state change
    //----------------------------------------------------
    onEditorStateChange = editorState => {
        let { email_data } = this.state;
        this.setState({
            editorState,
        });
        email_data.content = draftToHtml(convertToRaw(editorState.getCurrentContent()));
        this.setState(email_data);
    };

    updateEmailTemplate = async () => {
        const { email_data } = this.state;
        this.setState({ btn_loading: true });
        await this.props.updateTemplate(email_data);
        this.setState({ btn_loading: false });
    };

    openAndCloseEditingDropdown = () => {
        const {editing_dropdown_active} = this.state;
        this.setState({editing_dropdown_active : !editing_dropdown_active})
    }

    updateSubject = (e) => {
        const{email_data} = this.state;
        email_data.subject = e.target.value;
        this.setState({email_data});
    }

    render() {
        const { email_data, toolbar, editorState, original_data_copy, btn_loading, editing_dropdown_active} = this.state;
        return (
            <div>
                {Object.keys(email_data).length ? (
                    <div className="pd-10">
                        <div className = "display-flex">
                            <span className = "email-box-heading">{email_data.title}</span>
                        
                        <div className="editing-help-box">
                            <div className={classnames('dropdown', {'is-active' : editing_dropdown_active})} onClick = {this.openAndCloseEditingDropdown}>
                                <div className="dropdown-trigger">
                                    <button
                                        className="button is-size-7 width-180 justify-space-between"
                                        aria-haspopup="true"
                                        aria-controls="dropdown-menu2">
                                        Editing Help
                                        <span className="icon is-small marginleft-xs">
                                            <i class="fas fa-question-circle "></i>
                                        </span>
                                    </button>
                                </div>
                                <div className="dropdown-menu no-margin editing-help-box-menu" id="dropdown-menu2" role="menu">
                                    <div className="dropdown-content pd-20">
                                        <p>
                                            <div>
                                                <h1>
                                                    <b>Here are some editing shortcuts for your help:</b>
                                                </h1>
                                                <ul className="paddingleft-md">
                                                    <li>{`First Name: {{{first_name}}}`}</li>
                                                    <li>{`Company Name: {{{company_name}}}`}</li>
                                                    <li>{`TouchPoint:`}</li>
                                                    <li>{`Link tchat: {{{link}}}`}</li>
                                                    <li>{`Name of Email : {{{primary_admin_full_name}}}`}</li>
                                                    <li>{`Designation of Email Sender: {{{designatiton}}}`}</li>
                                                </ul>
                                            </div>
                                            <br />
                                            <div>
                                                <h1>
                                                    <b>Best Email Practices:</b>
                                                </h1>
                                                <ul className="paddingleft-md ">
                                                    <li>{`Keep it short and simple`}</li>
                                                    <li>{`Make it Personal. People love their name`}</li>
                                                    <li>{`Keep the subject line catchy`}</li>
                                                    <li>{`Make sure you include the link to Amber's chat otherwise people will not be able to interact with her.`}</li>
                                                </ul>
                                            </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div  className="columns email-header align-center">
                            <div className="column is-3 ">
                                <span><b>Email Subject:</b></span>
                            </div>
                            <div className="column is-9">
                            <input type="text" rows= "5" class="form__field" value = {email_data.subject ? email_data.subject : ''} onChange = {this.updateSubject}/>
                            </div>
                        </div>
                        <div id="email-content">
                            <Editor
                                toolbar={toolbar}
                                editorState={editorState}
                                wrapperClassName="demo-wrapper"
                                editorClassName="demo-editor"
                                onEditorStateChange={this.onEditorStateChange}
                                stripPastedStyles={true}
                            />
                        </div>
                        <button
                            onClick={this.updateEmailTemplate}
                            disabled={original_data_copy === JSON.stringify(email_data)}
                            className={classnames('button is-success  email-save-btn ', {
                                'is-loading': btn_loading,
                            })}>
                            {'Save'}
                        </button>
                    </div>
                ) : null}
            </div>
        );
    }
}

EmailsBox.propTypes = {
    client_details: PropTypes.object.isRequired,
};

// Map Redux state to props
// ------------------------
const mapStateToProps = state => ({
    client_details: state.client.client_details,
});

export default connect(mapStateToProps)(EmailsBox);
