import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import EmailTemplate from './EmailTemplate';
import { updateTemplate , getSlackTemplate} from '../../../../actions/EmailActions';
import SlackEmailTemplate from './SlackEmailTemplate';

class EmailsBox extends Component {
    state = {
        selected_template_category : '', // slack / email
        template_array: {},
        selected_template_data: {},
        selected_type: '',
        selected_index: null,
    };

    //did Mount
    componentDidMount() {
        this.setTemplateArray();
    }

    //did Update
    componentDidUpdate(prevProps) {
        const { template_array } = this.props;
        if (JSON.stringify(template_array) !== JSON.stringify(prevProps.template_array)) {
            this.setTemplateArray();
        }
    }

    //setting local template array
    setTemplateArray = () => {
        const { template_array } = this.props;
        this.setState({ template_array: template_array });
    };

    //update the template
    updateTemplate = async email_data => {
        const { updateTemplate, client_details, selected_language } = this.props;
        const { selected_index, selected_type, template_array, selected_template_category } = this.state;
        let params = {};
        if (selected_template_category === 'email') {
            params.content = email_data.content;
        } else {
            params.paragraph1 = email_data.paragraph1;
            params.paragraph2 = email_data.paragraph2;
            params.signature = email_data.signature;
        }
        params.email_id = email_data.id;
        params.subject = email_data.subject;
        if (client_details && client_details.id !== undefined && email_data.id !== undefined) {
            await updateTemplate(selected_template_category, client_details.id, email_data.id, email_data.cohort_id, selected_language, params, email_data.type);
            if (selected_index != null && selected_type.length) {
                template_array[selected_type].templates[selected_index] = email_data;
                this.setState({ template_array, selected_template_data: email_data });
            }
        }
    };

    //open the temoplates holding card
    openAndCloseCard = (type, index) => {
        const element = document.getElementById(`${type}-card-${index}`);
        const icon = document.getElementById('icon');
        if (element) {
            if (element.classList.contains('hide-template-box')) {
                element.classList.remove('hide-template-box');
            } else {
                element.classList.add('hide-template-box');
            }
        }
    };

    //set the selected template
    setSelectedTemplate = (template_type, template, selected_type, selected_index) => {
        if(template_type === 'email') {
            this.setState({ selected_template_category: template_type,  selected_template_data: template, selected_type, selected_index });
        } else {
            this.setState({selected_template_category: template_type}, () => {
                this.getSlackTemplate(selected_type, selected_index)
            })
        }
    };

    getSlackTemplate = async (template_type, template_index) => {
        const { client_details, slack_templates, selected_language }  = this.props
        const type  = (slack_templates[template_type].templates)[template_index].type

        let response = await this.props.getSlackTemplate(client_details.id, type, selected_language)
        if(response && response.status === 200) {
            this.setState({selected_template_category: 'slack',  selected_template_data: response.data, selected_type: template_type, selected_index: template_index}, () => {this.props.setSlackTemplate(template_type, template_index, response.data)})
        }
        

    }

    //render
    render() {
        const { template_array, selected_template_data, selected_template_category } = this.state;
        const { slack_templates, selected_language } = this.props;
        return (
            <div className="columns email_box">
                <div className="column is-6 email-box-right-border">
                    {Object.values(template_array) &&
                    Object.values(template_array)[0] &&
                    Object.values(template_array)[0].templates.length ? (
                        <div className="mg-bottom-10">
                            <h2 className="is-size-5 has-text-weight-bold padding-left-10">Emails</h2>
                            {Object.entries(template_array).map(([selected_type, template_type], index) => (
                                <div className="card mg-10" key={index}>
                                    <div className="email-type-box" onClick={this.openAndCloseCard.bind(this, 'email', index)}>
                                        <span>
                                            <b>{template_type.name}</b>
                                        </span>
                                        <i className="fas fa-chevron-down pull-right"></i>
                                    </div>
                                    <div id={`email-card-${index}`} className="hide-template-box">
                                        {template_type.templates.length
                                            ? template_type.templates.map((template, index2) => (
                                                  <div
                                                      className="email-menu-item"
                                                      key={index2}
                                                      onClick={this.setSelectedTemplate.bind(this, 'email', template, selected_type, index2)}>
                                                      <span>{template.title}</span>
                                                  </div>
                                              ))
                                            : null}
                                    </div>
                                </div>
                            ))}
                        </div>
                    ) : null}
                    {
                        Object.values(slack_templates) 
                        && Object.values(slack_templates)[0] 
                        // && Object.values(slack_templates)[0].templates.length 
                        ?
                        <div className="mg-top-bottom-25">
                            <h2 className="is-size-5 has-text-weight-bold padding-left-10 mg-top-25">Slack</h2>
                            {Object.entries(slack_templates).map(([selected_type, template_type], index) => (
                                <div className="card mg-10" key={index}>
                                    <div className="email-type-box" onClick={this.openAndCloseCard.bind(this, 'slack', index)}>
                                        <span>
                                            <b>{template_type.name}</b>
                                        </span>
                                        <i className="fas fa-chevron-down pull-right"></i>
                                    </div>
                                    <div id={`slack-card-${index}`} className="hide-template-box">
                                        {template_type.templates.length
                                            ? template_type.templates.map((template, index2) => (
                                                  <div
                                                      className="email-menu-item"
                                                      key={index2}
                                                      onClick={this.setSelectedTemplate.bind(this, 'slack', template, selected_type, index2)}>
                                                      <span>{template.title}</span>
                                                  </div>
                                              ))
                                            : null}
                                    </div>
                                </div>
                            ))}
                        </div>
                        : null
                    }
                </div>
                <div className="column is-6">
                    {
                        selected_template_category === 'email' ? 
                        Object.keys(selected_template_data).length ? (
                            <div>
                                <EmailTemplate email_data={selected_template_data} updateTemplate={this.updateTemplate} />
                            </div>
                        ) : (
                            <div className="email-placeholder">Please Select An Email Template</div>
                        )
                        : 
                            <SlackEmailTemplate selected_language={selected_language} email_data={selected_template_data} updateTemplate={this.updateTemplate}/>
                    }
                </div>
            </div>
        );
    }
}

EmailsBox.propTypes = {
    client_details: PropTypes.object.isRequired,
};

// Map Redux state to props
// ------------------------
const mapStateToProps = state => ({
    client_details: state.client.client_details,
});

export default connect(mapStateToProps, { updateTemplate, getSlackTemplate })(EmailsBox);
