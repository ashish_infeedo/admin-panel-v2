export const stringChecker = (conditions, string, index, appendFront,  appendBack) => {
    let error_messages = [];
    const tripleBraceRegex = /{{{(\d+|[a-z$_][a-z\d$_]*?(?:\.[a-z\d$_]*?)*?)}}}/gi;
    //throws error is not a string
    if (typeof string !== 'string') {
        throw new TypeError(`Expected a \`string\` in the first argument, got \`${typeof string}\``);
    }
    if (typeof conditions !== 'object') {
        throw new TypeError(`Expected an \`object\` or \`Array\` in the second argument, got \`${typeof conditions}\``);
    }

    if (conditions.mandatory && conditions.mandatory.length !== 0) {
        conditions.mandatory.forEach(value => {
            if (!string.includes(value)) {
                let message = `Missing Variable <b>${value}</b>`;
                message += (index !== undefined ? ` in message ${index + 1}` : appendBack && appendBack.length ? '' : '.');
                error_messages.push(message);
            }
        });
    }
    if (conditions.not_allowed && conditions.not_allowed.length !== 0) {
        conditions.not_allowed.forEach(value => {
            if (string.includes(value)) {
                let message = `Variable <b>${value}</b> cannot be used`;
                message += (index !== undefined ? ` in message ${index + 1}` : appendBack && appendBack.length ? '' : '.');
                error_messages.push(message);
            }
        });
    }
    let matches = string.match(tripleBraceRegex);
    if (matches) {
        matches.forEach(value => {
            if (!((conditions.can_have && conditions.can_have.includes(value)) || ( conditions.mandatory && conditions.mandatory.includes(value)))) {
                let message = `Variable <b>${value}</b> cannot be used`;
                message += (index !== undefined ? ` in message ${index + 1}` : appendBack && appendBack.length ? '' : '.');
                error_messages.push(message);
            }
        });
    }

    return error_messages;
};
