import classNames from 'classnames';
import React, { Component } from 'react';

import { getLifecycleData } from '../../../actions/LifecycleActions';
import CreateLifecycle from './Lifecycle/lifecyclemode/CreateLifecycle';
import EditLifecycle from './Lifecycle/lifecyclemode/EditLifecycle';

 class Lifecycle extends Component {

  state = {
    selected_tab : 0,   //0 - create, 1 - edit
    cohorts: {},
    drivers: {},
    elements: {},
    loading: true
  }

  componentDidMount () {
    this.getLifecycleData()
  }

  getLifecycleData = async () => {
    const {client_id} = this.props.match.params;
    const responses = await getLifecycleData(client_id);
    this.setState({
        cohorts:  responses[0].data,
        drivers: responses[1].data,
        elements: responses[2].data,
        loading: false
    });
  }

  changeTab = (tab) => {
    if(this.state.selected_tab !== tab) {
      this.setState({selected_tab: tab})
    }
  }

  render() {
   const {client_id} = this.props.match.params;
   const {selected_tab, cohorts, drivers, elements, loading} = this.state;
    return (
      <div className="lifecycle-box">
        <div className="lifecycle-tabs tabs is-toggle is-toggle-rounded is-centered">
          <ul>
            <li onClick={this.changeTab.bind(this, 0)} className={classNames('width-100px', {'is-active': !selected_tab})}><a>Create</a></li>
            <li onClick={this.changeTab.bind(this, 1)} className={classNames('width-100px', {'is-active': selected_tab})}><a>Edit</a></li>
          </ul>
        </div>

        {
          !selected_tab ? 
            <CreateLifecycle 
              client_id = {client_id}
              cohorts = {cohorts}
              drivers = {drivers}
              elements = {elements}
              loading = {loading}
            />
          : 
            <EditLifecycle 
              client_id = {client_id}
              cohorts = {cohorts}
              drivers = {drivers}
              elements = {elements}
            />
        }
      </div>
    )
  }
}

export default Lifecycle;