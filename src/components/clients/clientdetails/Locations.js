import React, { Component } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';
// components
import TextInputgroup from '../../generic/TextInputGroup';
import ConfirmModal from '../../generic/ConfirmModal';
import {looseFocus} from '../../../helper/LooseFocus';
// actions
import { getLocations, addLocations, deleteLocation, editLocation } from '../../../actions/ClientsActions';

class Locations extends Component {

  state = {
    client_id: null,
    search: '',
    locations: [],
    original_locations: [],
    new_locations: '',
    edit_location_id: null,
    edit_location_text: '',
    total_new_locations: 0,
    selected_location: {},
    delete_modal: {
      title: 'Delete Location',
      text: 'Are you sure you want to delete?',
      confirm_text: 'Yes',
      is_active: false
    },
    adding: false,
    loading: false,
    deleting: false,
    editing: false,
    disable_add: true
  }
  
  componentDidMount(){
    const { client_id } = this.props.match.params;
    this.setState({client_id: client_id});
    this.setState({loading: true});
    this.getLocations();
  }

  getLocations = async () => {
    const { client_id } = this.props.match.params;
    const response = await this.props.getLocations(client_id);
    this.setState({locations: response, original_locations: response, loading: false});
  }

  editLocation = async (location) => {
    const id = location.id;
    const { edit_location_text } = this.state;
    looseFocus();
    this.setState({editing: true});
    await this.props.editLocation(id, edit_location_text);
    this.getLocations();
    this.setState({edit_location_text: '', edit_location_id: null});
    this.setState({editing: false});
    looseFocus();
  }

  cancelEdit = () => {
    this.setState({edit_location_text: '', edit_location_id: null});
    looseFocus();
  }

  selectEditLocation = (location) => {
    this.setState({edit_location_text: location.title, edit_location_id: location.id});
  }

  openDeleteModal = (location) => {
    const { delete_modal } = this.state;
    delete_modal.title += ` ${location.title}`;
    delete_modal.is_active = true;
    this.setState({delete_modal: delete_modal, selected_location: location});
  }

  closeDeleteModal = () => {
    const { delete_modal } = this.state;
    delete_modal.is_active = false;
    delete_modal.title = 'Delete Location';
    this.setState({delete_modal: delete_modal});
  }

  onSubmit = async (e) => {
    e.preventDefault();
    const { client_id, new_locations } = this.state;
    this.setState({adding: true});
    await this.props.addLocations(client_id, new_locations);
    this.getLocations();
    this.setState({adding: false, new_locations: '', disable_add: true, total_new_locations: 0});
  }

  onSearch = (e) => {
    this.setState({ [e.target.name]: e.target.value });
    const search = e.target.value;
    const { original_locations } = this.state;
    const locations = original_locations;
    if(search.length > 0) {
        const results = locations.filter(location => location.title.toLowerCase().includes(search.toLowerCase()));
        this.setState({locations: results});
    } else if(!search.length) {
        this.setState({locations: original_locations});
    }
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  onChangeLocations = (e) => {
    this.onChange(e);
    const locations = e.target.value;
    if(locations.length > 0) {
      let locations_array = locations.split(',');
      if (locations_array[locations_array.length - 1] === "") {
        locations_array.pop();
      }
      const count = locations_array.length;
      this.setState({total_new_locations: count, disable_add: false});
    } else {
      this.setState({total_new_locations: 0, disable_add: true});
    }
  }

  looseFocus = () => {
    document.activeElement.blur();
  }

  confirmDelete = async () => {
    const { id } = this.state.selected_location;
    this.setState({deleting: true});
    await this.props.deleteLocation(id);
    this.closeDeleteModal();
    this.getLocations();
    this.setState({deleting: false});
  }

  openDeleteModal = (location) => {
    const { delete_modal } = this.state;
    delete_modal.title += ` ${location.title}`;
    delete_modal.is_active = true;
    this.setState({delete_modal: delete_modal, selected_location: location});
  }

  closeDeleteModal = () => {
    const { delete_modal } = this.state;
    delete_modal.is_active = false;
    delete_modal.title = 'Delete Cohort';
    this.setState({delete_modal: delete_modal});
  }

  render() {
    const { search, adding, loading, editing, deleting, edit_location_id, edit_location_text, new_locations, total_new_locations, locations, delete_modal, disable_add } = this.state;
    return (
      <div>
        <div className="columns">
          <div className="column is-3">
          <TextInputgroup
              name="search"
              placeholder="Search Location"
              value={search}
              type="text"
              onChange={this.onSearch}
              icon="fas fa-search"
          />
          </div>
          <div className="column is-8">
            <form onSubmit={this.onSubmit} className="columns">
              <div className="column is-10">
              <TextInputgroup
                  name="new_locations"
                  placeholder="Add Comma Separated Locations"
                  value={new_locations}
                  type="text"
                  onChange={this.onChangeLocations}
                  icon="far fa-compass"
              />
              </div>
              <div className="column is-3 pd-up-20">
                <button type="submit" className={classnames('button is-success is-fullwidth',{'is-loading': adding})} disabled={disable_add}>
                  <span className="icon">
                    <i className="fas fa-plus"></i>
                  </span>
                  <span>Add ({total_new_locations})</span>
                </button>
              </div>
            </form>
          </div>
        </div>
        <div className="card">
          <div className="card-content is-paddingless">
            <div className="content">
              <div className="custom-table">
                <table className="table is-striped">
                  <thead>
                      <tr>
                      <th>S.No.</th>
                      <th>Location</th>
                      <th>Created On</th>
                      <th>Actions</th>
                      </tr>
                  </thead>
                  <tbody>
                  {!loading ? locations.map((location, index) => (
                    <tr key={index}>
                    <td>{index + 1}</td>
                    <td className={classnames({'is-paddingless': location.id === edit_location_id})}>
                    { location.id === edit_location_id ?
                    <TextInputgroup
                        name="edit_location_text"
                        placeholder=""
                        value={edit_location_text}
                        type="text"
                        onChange={this.onChange}
                        icon="fas fa-pen"
                        is_small={true}
                        auto_focus={true}
                    />
                    : location.title
                    }
                    </td>
                    <td>{moment.unix(location.created).format('DD/MM/YYYY')}</td>
                    <td>
                    { location.id === edit_location_id ?
                    <div className="buttons">
                        <button onClick={this.editLocation.bind(this, location)} className={classnames('button is-success is-small is-rounded is-outlined',{'is-loading': editing})}>
                            <span>Save</span>
                        </button>
                        <button onClick={this.cancelEdit} className="button is-danger is-small is-rounded is-outlined">
                            <span>Cancel</span>
                        </button>
                    </div>
                    : 
                    <div className="buttons">
                        <button onClick={this.selectEditLocation.bind(this, location)} className="button is-info is-small is-rounded is-outlined">
                            <span>Edit</span>
                        </button>
                        <button onClick={this.openDeleteModal.bind(this, location)} className="button is-danger is-small is-rounded is-outlined">
                            <span>Delete</span>
                        </button>
                    </div>
                    }
                    </td>
                    </tr>
                  )) : null}
                  </tbody>
                </table>
                {(locations.length === 0 && !loading) ? <p className="no-data has-text-centered">No Data</p> : null}
                {loading ? <p className="no-data has-text-centered">Loading Data...</p> : null}
              </div>
            </div>
          </div>
        </div>
        <ConfirmModal 
        title={delete_modal.title}
        text={delete_modal.text}
        confirm_text={delete_modal.confirm_text}
        onConfirm={this.confirmDelete}
        onClose={this.closeDeleteModal}
        is_active={delete_modal.is_active}
        doing={deleting}/>
      </div>
    )
  }
}

// PropTypes
// ---------
Locations.propTypes = {
  getLocations: PropTypes.func.isRequired,
  addLocations: PropTypes.func.isRequired,
  deleteLocation: PropTypes.func.isRequired,
  editLocation: PropTypes.func.isRequired
}

export default connect(null, {getLocations, addLocations, deleteLocation, editLocation})(Locations);