import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classnames from 'classnames';
// actions
import { updateClientDetails, getClientDetails } from '../../../actions/ClientsActions';

class Features extends Component {

  state = {
    client_id: null,
    new_client_details: {},
    is_save_disabled: true,
    changed_client_details: {},
    saving: false
  }

  componentDidMount() {
    const { client_id } = this.props.match.params;
    let client_details = {};
    client_details = { ...this.props.client_details }
    this.setState({
      client_id: client_id,
      new_client_details: client_details
    })
  }

  componentDidUpdate(prevProps) {
    const { client_id } = this.props.match.params;
    let client_details = {};
    client_details = { ...this.props.client_details }
    // if(Object.keys(this.props.client_details).length > 0)
    // {
    if (prevProps.client_details !== this.props.client_details) {
      this.setState({
        client_id: client_id,
        new_client_details: client_details
      })
    }
    // }
  }

  getAdmins = async (client_id) => {
    //const response = await this.props.getClientDetails(client_id);
  }

  updateClientDetails = async (e) => {
    this.setState({ saving: true });
    await this.props.updateClientDetails(this.state.client_id, this.state.changed_client_details);
    this.setState({ saving: false });
    await this.props.getClientDetails(this.state.client_id);
    this.setState({
      changed_client_details: {},
      is_save_disabled: true
    })
  }

  emailSpoofing = (e) => {
    let { new_client_details, is_save_disabled, changed_client_details } = this.state;
    let key = e.target.name;

    if (this.props.client_details[key] !== e.target.value) {
      if (key !== 'chat_feedback_url') {
        new_client_details[key] = parseInt(e.target.value);
        changed_client_details[key] = parseInt(e.target.value);
      } else {
        new_client_details[key] = e.target.value;
        changed_client_details[key] = e.target.value;
      }

      this.setState({
        changed_client_details: changed_client_details,
        new_client_details: new_client_details
      }, () => {
        if (Object.keys(changed_client_details).length > 0 && is_save_disabled === true) {
          this.setState({
            is_save_disabled: false
          })
        }
      })
    }
    else {
      if (this.state.new_client_details[key] !== this.props.client_details[key]) {
        if (changed_client_details[key] !== undefined) {
          delete changed_client_details[key];
          new_client_details[key] = this.props.client_details[key]
          this.setState({
            new_client_details: new_client_details,
            changed_client_details: changed_client_details
          }, () => {
            if (Object.keys(this.state.changed_client_details).length === 0 && this.state.is_save_disabled === false) {
              this.setState({
                is_save_disabled: true
              })
            }
          })
        }
      }
    }
  }

  render() {
    const client_details = this.state.new_client_details;
    const { is_save_disabled, saving, changed_client_details } = this.state;
    return (
      <div>
        <div className="features-outer-div" onChange={this.emailSpoofing.bind(this)}>
          <div className="columns is-12 pd-up-20">
            <span className="column is-3 has-text-weight-bold"> Email Spoofing </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="email_spoofing" value="1" checked={client_details.email_spoofing === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="0" name="email_spoofing" checked={client_details.email_spoofing === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
          </div>
          <div className="columns is-12 pd-up-20">
            <span className="column is-3 has-text-weight-bold"> Language Support </span>
            <div className="control column is-2">
              <label className="radio">
                <input disabled={client_details.exit_enable || client_details.welcome_emails_enabled || client_details.activate_projection_engine} type="radio" name="multilingual_enabled" value="1" checked={client_details.multilingual_enabled === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input disabled={client_details.exit_enable || client_details.welcome_emails_enabled || client_details.activate_projection_engine} type="radio" value="0" name="multilingual_enabled" checked={client_details.multilingual_enabled === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
            <div className="notification-msg column is-7">
              {
                client_details.exit_enable || client_details.welcome_emails_enabled || client_details.activate_projection_engine ?
                  <span className="has-text-danger">
                    To Enable this you have to disable
                    {client_details.exit_enable ? ' Exit ' : ''}
                    {
                      client_details.activate_projection_engine ? client_details.exit_enable ? ', Breadboard ' : ' Breadboard  ' : ''
                    }
                    {
                      client_details.welcome_emails_enabled ? client_details.exit_enable || client_details.activate_projection_engine ? ', Welcome-Email' : ' Welcome-Email ' : ''
                    }
                    for this customer
                  </span>
                  : ''
              }
            </div>
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 has-text-weight-bold"> Themes </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="drivers_elements_enabled" value="1" checked={client_details.drivers_elements_enabled === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="0" name="drivers_elements_enabled" checked={client_details.drivers_elements_enabled === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 has-text-weight-bold"> Acknowledgement Email </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="acknowledgement_enabled" value="1" checked={client_details.acknowledgement_enabled === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="0" name="acknowledgement_enabled" checked={client_details.acknowledgement_enabled === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 has-text-weight-bold"> SMS Notification </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="sms_reminders" value="1" checked={client_details.sms_reminders === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="0" name="sms_reminders" checked={client_details.sms_reminders === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 has-text-weight-bold"> WhatsApp Notification </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="whatsapp_enabled" value="1" checked={client_details.whatsapp_enabled === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="0" name="whatsapp_enabled" checked={client_details.whatsapp_enabled === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 has-text-weight-bold"> Missed Milestone </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="reduce_sales_cycle" value="1" checked={client_details.reduce_sales_cycle === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="0" name="reduce_sales_cycle" checked={client_details.reduce_sales_cycle === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
              <span className="marginleft-sm">(Reduce Sales Cycle)</span>
            </div>
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 has-text-weight-bold"> Alert Emails </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="dangreous_word_alert" value="1" checked={client_details.dangreous_word_alert === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="0" name="dangreous_word_alert" checked={client_details.dangreous_word_alert === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 has-text-weight-bold"> Welcome Emails </span>
            <div className="control column is-2">
              <label className="radio">
                <input disabled={client_details.multilingual_enabled} type="radio" name="welcome_emails_enabled" value="1" checked={client_details.welcome_emails_enabled === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input disabled={client_details.multilingual_enabled} type="radio" value="disable" name="welcome_emails_enabled" checked={client_details.welcome_emails_enabled === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
            {
              client_details.multilingual_enabled ?
                <div className="column is-7 has-text-danger is-size-7">
                  Welcome Emails are not supported with new Language support feature, to enable welcome emails please disable language support toggle.
                </div>
                : null
            }
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 pd-up-20 has-text-weight-bold"> Manager Dashboard </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="manager_dashboard_enabled" value="1" checked={client_details.manager_dashboard_enabled === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="0" name="manager_dashboard_enabled" checked={client_details.manager_dashboard_enabled === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 pd-up-20 has-text-weight-bold"> To Start Chat Reminders Limit </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="to_start_reminders_limit" value="1" checked={client_details.to_start_reminders_limit === 1} readOnly />
                &nbsp;&nbsp;1
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="2" name="to_start_reminders_limit" checked={client_details.to_start_reminders_limit === 2} readOnly />
                &nbsp;&nbsp;2
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="3" name="to_start_reminders_limit" checked={client_details.to_start_reminders_limit === 3} readOnly />
                &nbsp;&nbsp;3
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="4" name="to_start_reminders_limit" checked={client_details.to_start_reminders_limit === 4} readOnly />
                &nbsp;&nbsp;4
              </label>
            </div>
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 pd-up-20 has-text-weight-bold"> Incomplete Chat Reminders Limit </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="incomplete_reminders_limit" value="1" checked={client_details.incomplete_reminders_limit === 1} readOnly />
                &nbsp;&nbsp;1
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="2" name="incomplete_reminders_limit" checked={client_details.incomplete_reminders_limit === 2} readOnly />
                &nbsp;&nbsp;2
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="3" name="incomplete_reminders_limit" checked={client_details.incomplete_reminders_limit === 3} readOnly />
                &nbsp;&nbsp;3
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="4" name="incomplete_reminders_limit" checked={client_details.incomplete_reminders_limit === 4} readOnly />
                &nbsp;&nbsp;4
              </label>
            </div>
          </div>
          <div className="columns pd-up-20 ">
            <span className="column is-3 has-text-weight-bold"> Exit Module </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="exit_enable" value="1" checked={client_details.exit_enable === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="0" name="exit_enable" checked={client_details.exit_enable === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 has-text-weight-bold"> Chat Answer Editing </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="chat_editable" value="1" checked={client_details.chat_editable === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="0" name="chat_editable" checked={client_details.chat_editable === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 has-text-weight-bold"> Celebration Emails </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="celebration_emails" value="1" checked={client_details.celebration_emails === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="0" name="celebration_emails" checked={client_details.celebration_emails === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 has-text-weight-bold"> Reachouts </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="disable_stages" value="1" checked={client_details.disable_stages === 0} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="0" name="disable_stages" checked={client_details.disable_stages === 1} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 has-text-weight-bold"> Reminders </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="chat_reminders" value="1" checked={client_details.chat_reminders === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="0" name="chat_reminders" checked={client_details.chat_reminders === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 has-text-weight-bold"> Anonymous Bat </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="enable_anonymous_bat" value="1" checked={client_details.enable_anonymous_bat === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="0" name="enable_anonymous_bat" checked={client_details.enable_anonymous_bat === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 has-text-weight-bold"> Guidelines Disclaimer </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="guidelines_disclaimer_enabled" value="1" checked={client_details.guidelines_disclaimer_enabled === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="0" name="guidelines_disclaimer_enabled" checked={client_details.guidelines_disclaimer_enabled === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 has-text-weight-bold"> Trust Statement </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="trust_statement_editable" value="1" checked={client_details.trust_statement_editable === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="0" name="trust_statement_editable" checked={client_details.trust_statement_editable === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 has-text-weight-bold"> Enable Admin Chats </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="admins_chat_enable" value="1" checked={client_details.admins_chat_enable === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="0" name="admins_chat_enable" checked={client_details.admins_chat_enable === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 has-text-weight-bold"> Advance Demographic Toggle </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="advanced_demographic_enabled" value="1" checked={client_details.advanced_demographic_enabled === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="0" name="advanced_demographic_enabled" checked={client_details.advanced_demographic_enabled === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 has-text-weight-bold"> Enable Glassdoor Feedback </span>
            <div className="control column is-2">
              <label className="radio">
                <input type="radio" name="chat_feedback_enabled" value="1" checked={client_details.chat_feedback_enabled === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="0" name="chat_feedback_enabled" checked={client_details.chat_feedback_enabled === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
            {
              client_details.chat_feedback_enabled && !changed_client_details.chat_feedback_enabled ?
                <div className="column is-7">
                  <input name="chat_feedback_url" value={client_details.chat_feedback_url} className="input" placeholder="Glassdoor link" />
                </div>
                : null

            }
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 has-text-weight-bold"> Master Data Upload </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="master_dataupload_enabled" value="1" checked={client_details.master_dataupload_enabled === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="0" name="master_dataupload_enabled" checked={client_details.master_dataupload_enabled === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 has-text-weight-bold"> Highlight Email Covid Banner </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="enable_covid_banner_for_daily_highlights" value="1" checked={client_details.enable_covid_banner_for_daily_highlights === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="0" name="enable_covid_banner_for_daily_highlights" checked={client_details.enable_covid_banner_for_daily_highlights === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 has-text-weight-bold"> Chat Covid Banner </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="enable_covid_banner_for_chat" value="1" checked={client_details.enable_covid_banner_for_chat === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="0" name="enable_covid_banner_for_chat" checked={client_details.enable_covid_banner_for_chat === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
          </div>
          <div className="columns pd-up-20">
            <span className="column is-3 has-text-weight-bold"> Slack Reachout </span>
            <div className="control column">
              <label className="radio">
                <input type="radio" name="slack_enabled" value="1" checked={client_details.slack_enabled === 1} readOnly />
                &nbsp;&nbsp;Enable
              </label>&nbsp;&nbsp;
              <label className="radio">
                <input type="radio" value="0" name="slack_enabled" checked={client_details.slack_enabled === 0} readOnly />
                &nbsp;&nbsp;Disable
              </label>
            </div>
          </div>
        </div>
        <br />
        <button disabled={is_save_disabled} type="submit" onClick={this.updateClientDetails} className={classnames('button is-primary mg-10', { 'is-loading': saving })}>
          <span className="icon">
            <i className="far fa-save"></i>
          </span>
          <span>Save</span>
        </button>
      </div>
    )
  }
}

Features.propTypes = {
  client_details: PropTypes.object.isRequired,
  getClientDetails: PropTypes.func.isRequired,
  updateClientDetails: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  client_details: state.client.client_details,
})

export default connect(mapStateToProps, { getClientDetails, updateClientDetails })(Features);