import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classnames from 'classnames';
// actions
import { editSegment } from '../../../../../actions/CrisisActions';

class ModuleConfigure extends Component {

    state = {
        saving: false,
        current_segment: {},
        disable_everything: true
    }

    componentDidMount(){
        const { selected_segment } = this.props;
        this.setState({current_segment: selected_segment}, () => this.checkEnableSave());
    }

    componentDidUpdate(prevProps){
        const { selected_segment } = this.props;
        if(JSON.stringify(selected_segment) !== JSON.stringify(prevProps.selected_segment)){
            this.setState({current_segment: selected_segment}, () => this.checkEnableSave());
        }
    }

    checkEnableSave = () => {
        const { current_segment } = this.state;
        // if(current_segment.crisis_name && current_segment.crisis_durations && current_segment.crisis_frequency){
        if(current_segment.crisis_name){
            this.setState({disable_everything: true});
        } else {
            this.setState({disable_everything: false});
        }
    }

    // edit Segment
    // ------------
    editSegment = async (e) => {
        e.preventDefault();
        const { current_segment } = this.state;
        const { client_details } = this.props;
        this.setState({saving: true});
        const response = await this.props.editSegment(client_details.id, current_segment.id, current_segment);
        if(response && response.status === 200){
            this.checkEnableSave();
        }
        this.setState({saving: false});
    }

    onChange = (e) => {
        const { current_segment } = this.state;
        current_segment[e.target.name] = (e.target.name === 'crisis_frequency' ? parseInt(e.target.value) : e.target.value);
        this.setState({current_segment});
    }

    render() {
        const { saving, current_segment, disable_everything } = this.state;
        return (
            <form onSubmit={this.editSegment} >
                <div className="columns">
                    <div className="field column">
                        <label className="label">Crisis Name</label>
                        <div className="control">
                            <input className="input" disabled={disable_everything} onChange={this.onChange} type="text" name="crisis_name" value={current_segment.crisis_name} placeholder="e.g. Covid19" required/>
                        </div>
                    </div>
                    {/* <div className="field column">
                        <label className="label">Crisis Duration</label>
                        <div className="control">
                            <input className="input" disabled={disable_everything} onChange={this.onChange} type="text" name="crisis_durations" value={current_segment.crisis_durations} placeholder="e.g. 6 months" required/>
                        </div>
                    </div>
                    <div className="field column">
                        <label className="label">Crisis Frequency (Days)</label>
                        <div className="control">
                            <input className="input" disabled={disable_everything} onChange={this.onChange} type="number" name="crisis_frequency" value={current_segment.crisis_frequency} placeholder="e.g. 1" required/>
                        </div>
                    </div> */}
                </div>
                <div className="field">
                    <div className="control has-text-right">
                        <button disabled={disable_everything} type="submit" className={classnames('button is-success', {'is-loading': saving})}>Save</button>
                    </div>
                </div>
            </form>
        )
    }
}

// PropTypes
// ---------
ModuleConfigure.propTypes = {
    client_details: PropTypes.object.isRequired,
    selected_segment: PropTypes.object.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    client_details: state.client.client_details,
    editSegment: PropTypes.func.isRequired
})

export default connect(mapStateToProps, { editSegment })(ModuleConfigure);