import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import classnames from 'classnames';
import TextInputgroup from '../../../../generic/TextInputGroup';

const SegmentTable = ({
    segments,
    edit_segment_id,
    edit_segment_text,
    onChangeEditSegment,
    editSegment,
    editing,
    cancelEdit,
    selectEditsegment,
    openDeleteModal,
    loading
}) => {
    return (
        <div className="card">
            <div className="card-content is-paddingless">
                <div className="content">
                <div className="custom-table">
                    <table className="table is-striped">
                    <thead>
                        <tr>
                            <th>S.No.</th>
                            <th>Segment</th>
                            <th>Created On</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    {!loading ? segments.map((segment, index) => (
                        <tr key={index}>
                        <td>{index + 1}</td>
                        <td className={classnames({'is-paddingless': segment.id === edit_segment_id})}>
                        { segment.id === edit_segment_id ?
                        <TextInputgroup
                            name="edit_segment_text"
                            placeholder=""
                            value={edit_segment_text}
                            type="text"
                            onChange={onChangeEditSegment}
                            icon="fas fa-pen"
                            is_small={true}
                            auto_focus={true}
                        />
                        : segment.title
                        }
                        </td>
                        <td>{moment.unix(segment.created).format('DD/MM/YYYY')}</td>
                        <td>
                        { segment.id === edit_segment_id ?
                        <div className="buttons">
                            <button onClick={editSegment.bind(this, segment)} className={classnames('button is-success is-small is-rounded is-outlined',{'is-loading': editing})}>
                                <span>Save</span>
                            </button>
                            <button onClick={cancelEdit} className="button is-danger is-small is-rounded is-outlined">
                                <span>Cancel</span>
                            </button>
                        </div>
                        : 
                        <div className="buttons">
                            <button onClick={selectEditsegment.bind(this, segment)} className="button is-info is-small is-rounded is-outlined">
                                <span>Edit</span>
                            </button>
                            <button onClick={openDeleteModal.bind(this, segment)} className="button is-danger is-small is-rounded is-outlined">
                                <span>Delete</span>
                            </button>
                        </div>
                        }
                        </td>
                        </tr>
                    )) : null}
                    </tbody>
                    </table>
                    {(segments.length === 0 && !loading) ? <p className="no-data has-text-centered">No Data</p> : null}
                    {loading ? <p className="no-data has-text-centered">Loading Data...</p> : null}
                </div>
                </div>
            </div>
      </div>
    )
}

SegmentTable.propTypes = {
    segments: PropTypes.array.isRequired,
    edit_segment_id: PropTypes.number,
    edit_segment_text: PropTypes.string.isRequired,
    onChangeEditSegment: PropTypes.func.isRequired,
    editSegment: PropTypes.func.isRequired,
    editing: PropTypes.bool.isRequired,
    cancelEdit: PropTypes.func.isRequired,
    selectEditsegment: PropTypes.func.isRequired,
    openDeleteModal: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired
};


export default SegmentTable;