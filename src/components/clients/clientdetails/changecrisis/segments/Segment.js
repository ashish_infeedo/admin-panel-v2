import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import ConfirmModal from '../../../../generic/ConfirmModal';
import {looseFocus} from '../../../../../helper/LooseFocus';
// actions
import { createSegments, editSegment, deleteSegment } from '../../../../../actions/CrisisActions';
// components
import SegmentForm from './SegmentForm';
import SegmentTable from './SegmentTable';

class Segment extends Component {

    state = {
        segments: [],
        search: '', 
        new_segments: '', 
        total_new_segments: 0, 
        adding: false, 
        disable_add: true,
        client_id: null,
        edit_segment_id: null, 
        edit_segment_text: '', 
        loading: false, 
        editing: false,
        deleting: false,
        delete_modal: {
            title: 'Delete Segment',
            text: 'Are you sure you want to delete?',
            confirm_text: 'Yes',
            is_active: false
        },
        selected_segment: {}
    }

    // component mounts
    // ----------------
    componentDidMount(){
        const { client_id } = this.props.match.params;
        const { segments } = this.props;
        this.setState({
            client_id: parseInt(client_id),
            segments
        });
    }

    // component updates
    // -----------------
    componentDidUpdate(prevProps){
        const { segments } = this.props;
        if(JSON.stringify(segments) !== JSON.stringify(prevProps.segments)){
            this.setState({segments});
        } 
    }

    // search segments
    // ----------------
    onSearch = () => {

    }

    // Add new Segments
    // ----------------
    onSubmit = async (e) => {
        e.preventDefault();
        const { client_id, new_segments } = this.state;
        this.setState({adding: true});
        const response = await this.props.createSegments(client_id, new_segments);
        if(response && response.status == 200){
            this.props.reloadSegments();
            this.setState({adding: false, new_segments: '', disable_add: true, total_new_segments: 0});
        } else {
            this.setState({adding: false});
        }
    }

    // On Change input
    // ----------------
    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    // on hange segment
    // ----------------
    onChangeSegments = (e) => {
        this.onChange(e);
        const segments = e.target.value;
        if(segments.length > 0) {
          let segments_array = segments.split(',');
          if (segments_array[segments_array.length - 1] === "") {
            segments_array.pop();
          }
          const count = segments_array.length;
          this.setState({total_new_segments: count, disable_add: false});
        } else {
          this.setState({total_new_segments: 0, disable_add: true});
        }
    }

    // edit Segment
    // ------------
    editSegment = async (segment) => {
        const id = segment.id;
        const { edit_segment_text, client_id } = this.state;
        looseFocus();
        this.setState({editing: true});
        segment.title = edit_segment_text;
        const response = await this.props.editSegment(client_id, id, segment);
        if(response && response.status === 200){
            this.props.reloadSegments();
            this.setState({edit_segment_text: '', edit_segment_id: null});
        }
        this.setState({editing: false});
        looseFocus();
    }

    // Cancel Edit
    // -----------
    cancelEdit = () => {
        this.setState({edit_segment_text: '', edit_segment_id: null});
        looseFocus();
    }

    // Select Segment to edit
    // ----------------------
    selectEditsegment = (segment) => {
        this.setState({edit_segment_text: segment.title, edit_segment_id: segment.id});
    }

    // Open Delete Confirm Modal
    // -------------------------
    openDeleteModal = (segment) => {
        const { delete_modal } = this.state;
        delete_modal.title += ` ${segment.title}`;
        delete_modal.is_active = true;
        this.setState({delete_modal: delete_modal, selected_segment: segment});
    }
    
    // Close Delete Confirm Modal
    // -------------------------
    closeDeleteModal = () => {
        const { delete_modal } = this.state;
        delete_modal.is_active = false;
        delete_modal.title = 'Delete Segment';
        this.setState({delete_modal: delete_modal});
    }

    // Confirm Delete segment
    // -------------------------
    confirmDelete = async () => {
        const { id } = this.state.selected_segment;
        const { client_id } = this.state;
        this.setState({deleting: true});
        const response = await this.props.deleteSegment(client_id, id);
        if(response && response.status){
            this.props.reloadSegments();
            this.closeDeleteModal();
        }
        this.setState({deleting: false});
    }

    // Render UI
    // ---------
    render() {
        const { segments, search, new_segments, total_new_segments, adding, disable_add, edit_segment_id, edit_segment_text, loading, editing, delete_modal, deleting } = this.state;
        return (
            <div>
               <SegmentForm
                    search={search}
                    onSearch={this.onSearch}
                    onSubmit={this.onSubmit}
                    onChangeSegments={this.onChangeSegments}
                    new_segments={new_segments}
                    total_new_segments={total_new_segments}
                    adding={adding}
                    disable_add={disable_add}
               />
               <SegmentTable
                    segments={segments}
                    edit_segment_id={edit_segment_id}
                    edit_segment_text={edit_segment_text}
                    onChangeEditSegment={this.onChange}
                    editSegment={this.editSegment}
                    editing={editing}
                    cancelEdit={this.cancelEdit}
                    selectEditsegment={this.selectEditsegment}
                    openDeleteModal={this.openDeleteModal}
                    loading={loading}
               />
               <ConfirmModal 
                    title={delete_modal.title}
                    text={delete_modal.text}
                    confirm_text={delete_modal.confirm_text}
                    onConfirm={this.confirmDelete}
                    onClose={this.closeDeleteModal}
                    is_active={delete_modal.is_active}
                    doing={deleting}
                />
            </div>
        )
    }
}

// PropTypes
// ---------
Segment.propTypes = {
    segments: PropTypes.array.isRequired,
    createSegments: PropTypes.func.isRequired, 
    editSegments: PropTypes.func.isRequired, 
    deleteSegment: PropTypes.func.isRequired,
    reloadSegments: PropTypes.func.isRequired
}
  
export default connect(null, { createSegments, editSegment, deleteSegment })(withRouter(Segment));