import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import TextInputgroup from '../../../../generic/TextInputGroup';

const SegmentForm = ({
    search,
    onSearch,
    onSubmit,
    new_segments,
    onChangeSegments,
    total_new_segments,
    adding,
    disable_add
}) => {
    return (
        <div className="columns">
            {/* <div className="column is-3">
                <TextInputgroup
                    name="search"
                    placeholder="Search Cohorts"
                    value={search}
                    type="text"
                    onChange={onSearch}
                    icon="fas fa-search"
                />
            </div> */}
            <div className="column is-12">
                <form onSubmit={onSubmit} className="columns">
                    <div className="column is-10">
                    <TextInputgroup
                        name="new_segments"
                        placeholder="Add Comma Separated Segments"
                        value={new_segments}
                        type="text"
                        onChange={onChangeSegments}
                        icon="fas fa-users"
                    />
                    </div>
                    <div className="column is-2 pd-up-20">
                        <button type="submit" className={classnames('button is-success is-fullwidth',{'is-loading': adding})} disabled={disable_add}>
                            <span className="icon">
                            <i className="fas fa-plus"></i>
                            </span>
                            <span>Add ({total_new_segments})</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}

SegmentForm.propTypes = {
    onSearch: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onChangeSegments: PropTypes.func.isRequired,
    search: PropTypes.string.isRequired,
    new_segments: PropTypes.string.isRequired,
    total_new_segments: PropTypes.number.isRequired,
    adding: PropTypes.bool.isRequired,
    disable_add: PropTypes.bool.isRequired
};


export default SegmentForm;