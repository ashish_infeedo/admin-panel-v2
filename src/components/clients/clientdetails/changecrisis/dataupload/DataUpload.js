import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { connect } from 'react-redux';

export default class DataUpload extends Component {

    state = {

    }

    render() {
        return (
            <div>
                <div>
                    <h2 className="is-size-6">Data Upload</h2>
                    <p className="help">Download sample file and upload data to trigger </p>
                </div>
            </div>
        )
    }
}

// PropTypes
// ---------
DataUpload.propTypes = {
    client_details: PropTypes.object.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    client_details: state.client.client_details,
})

export default connect(mapStateToProps, {})(DataUpload);