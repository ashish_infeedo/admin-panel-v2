import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { connect } from 'react-redux';
import moment from 'moment';
// components
import ConfirmModal from '../../../../generic/ConfirmModal';
// actions
import { downloadSampleManagerFile, uploadManagerFile, cleanManagerFile, getManagersList, updateEmailSettings } from '../../../../../actions/CrisisActions';

class ManagerDataUpload extends Component {
    
    state = {
        uploading: false, 
        downloading: false,
        cleaning: false,
        managers: [],
        modal_text: {
            title: 'Disable Email Sending',
            text: 'Are you sure you want to stop sending emails?'
        },
        update_modal_active: false, 
        updating: false,
        selected_manager: {}
    }

    componentDidMount() {
        // this.getManagersList();
    }

    uploadManagerFile = async (e) => {
        const { client_details } = this.props;
        const upload_files = Array.from(e.target.files);
        const formData = new FormData();
        formData.append('csv', upload_files[0]);
        this.setState({uploading: true});
        const response = await this.props.uploadManagerFile(client_details.id, formData);
        if(response && response.status === 200){
            this.getManagersList();
        }
        this.setState({uploading: false});
    }

    downloadSampleManagerFile = async () => {
        const { client_details } = this.props;
        this.setState({downloading: true});
        await this.props.downloadSampleManagerFile(client_details.id);
        this.setState({downloading: false});
    }

    cleanManagerFile = async (e) => {
        const { client_details } = this.props;
        const upload_files = Array.from(e.target.files);
        const formData = new FormData();
        formData.append('csv', upload_files[0]);
        this.setState({cleaning: true});
        await this.props.cleanManagerFile(client_details.id, formData);
        this.setState({cleaning: false});
    }

    getManagersList = async () => {
        const { client_details } = this.props;
        this.setState({loading_managers: true});
        const response = await this.props.getManagersList(client_details.id);
        if(response && response.status === 200){
            this.setState({managers: response.data});
        } 
        this.setState({loading_managers: false});
    }

    updateEmailSettings = async () => {
        const { client_details } = this.props;
        const { selected_manager } = this.state;
        this.setState({updating: true});
        const response = await this.props.updateEmailSettings(client_details.id, selected_manager.crisis_manager_id, (selected_manager.email_report === 1 ? 0 : 1));
        if(response && response.status === 200){
            this.closeUpdateModal();
            this.getManagersList();
        }
        this.setState({updating: false});
    }

    openUpdateModal = (manager) => {
        const { modal_text } = this.state;
        if(manager.email_report === 1){
            modal_text.title = `Disable Email Sending to ${manager.name}`;
            modal_text.text = `Are you sure you want to stop sending emails?`;
        } else {
            modal_text.title = `Enable Email Sending to ${manager.name} (${manager.email})`;
            modal_text.text = `Are you sure you want to start sending emails?`;
        }
        this.setState({
            selected_manager: manager,
            modal_text,
            update_modal_active: true
        });
    }

    closeUpdateModal = () => {
        this.setState({
            update_modal_active: false
        });
    }

    render() {
        const { uploading, downloading, cleaning, loading_managers, managers, modal_text, update_modal_active, updating } = this.state; 
        return (
            <div>
                <div>
                    <h2 className="is-size-6">Manager Data Upload</h2>
                </div>
                <article className="message">
                    <div className="message-body modal-sub-header-text">
                        <ol>
                            <li><b>Download the sample file</b> to understand the format.</li>
                            <li><b>Clean the file</b> before uploading to find possible errors.</li>
                            <li><b>Upload the clean file</b> to update manager data.</li>
                        </ol>
                    </div>
                </article>
                <br/>
                <div className="columns">
                    <div className="mg-10 has-text-centered column">
                        <button onClick={this.downloadSampleManagerFile} type="submit" className={classnames('button is-info', {'is-loading': downloading})}>
                            <span className="icon">
                                <i className="fas fa-file-download"></i>
                            </span>
                            <span>Download Sample Manager File</span>
                        </button>
                    </div>
                    <div className="mg-10 has-text-centered column">
                        <button type="submit" onClick={() => {document.getElementById('cleanFile').click();}} className={classnames('button is-primary', {'is-loading': cleaning})}>
                            <span className="icon">
                                <i className="fas fa-file-upload"></i>
                            </span>
                            <span>Clean Manager Data File</span>
                        </button>
                        {cleaning ? null : <input accept=".csv" type="file" id='cleanFile' onChange={this.cleanManagerFile} hidden/>}
                    </div>
                    <div className="mg-10 has-text-centered column">
                        <button type="submit" onClick={() => {document.getElementById('uploadManagerFile').click();}} className={classnames('button is-success', {'is-loading': uploading})}>
                            <span className="icon">
                                <i className="fas fa-file-upload"></i>
                            </span>
                            <span>Upload Manager Data File</span>
                        </button>
                        {uploading ? null : <input accept=".csv" type="file" id='uploadManagerFile' onChange={this.uploadManagerFile} hidden/>}
                    </div>
                </div>
                {/* <div>
                    <hr/>
                    <h2 className="is-size-6 has-text-weight-bold">Last Uploaded Managers</h2><br/>
                    {
                        loading_managers 
                            ? <p className="has-text-centered">Loading...</p>
                            : (managers && managers.length 
                                ? 
                                <div className="has-text-centered">
                                    <table className="table">
                                        <thead>
                                            <tr>
                                                <th>Sno</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Created</th>
                                                <th>Modified</th>
                                                <th>Send Email</th>
                                                <th>Send Email Action</th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                {
                                                    managers.map((manager, index) => (
                                                        <tr key={index}>
                                                            <td>{index + 1}</td>
                                                            <td>{manager.name}</td>
                                                            <td>{manager.email}</td>
                                                            <td>{moment.unix(manager.created).format('DD MMM YYYY')}</td>
                                                            <td>{moment.unix(manager.modified).format('DD MMM YYYY')}</td>
                                                            <td>{manager.email_report === 1 ? <span className="has-text-success">Enabled</span> :  <span className="has-text-danger">Disabled</span> }</td>
                                                            <td>
                                                                <div className="buttons">
                                                                    {manager.email_report === 1 ? 
                                                                        <button onClick={this.openUpdateModal.bind(this, manager)} className="button is-small is-danger is-outlined is-rounded">
                                                                            <span>Disable</span>
                                                                        </button>
                                                                        :<button onClick={this.openUpdateModal.bind(this, manager)} className="button is-small is-success is-outlined is-rounded">
                                                                            <span>Enable</span>
                                                                        </button>
                                                                    }
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    ))
                                                }
                                            </tbody>
                                        </table>
                                </div> 
                                : 
                                <p className="has-text-centered">
                                    No Data Available
                                </p>
                            )
                    }
                </div> */}
                <ConfirmModal
                    text={modal_text.text}
                    title={modal_text.title}
                    confirm_text={"Yes"}
                    doing={updating}
                    is_active={update_modal_active}
                    onConfirm={this.updateEmailSettings}
                    onClose={this.closeUpdateModal}
                />
            </div>
        )
    }
}


// PropTypes
// ---------
ManagerDataUpload.propTypes = {
    client_details: PropTypes.object.isRequired,
    downloadSampleManagerFile: PropTypes.func.isRequired,
    uploadManagerFile: PropTypes.func.isRequired,
    cleanManagerFile: PropTypes.func.isRequired,
    getManagersList: PropTypes.func.isRequired,
    updateEmailSettings: PropTypes.func.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    client_details: state.client.client_details,
})

export default connect(mapStateToProps, { downloadSampleManagerFile, uploadManagerFile, cleanManagerFile, getManagersList, updateEmailSettings })(ManagerDataUpload);