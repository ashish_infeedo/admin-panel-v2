import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { connect } from 'react-redux';
// actions
import { downloadAPFSampleFile, uploadAPFFile, getAPFList } from '../../../../../actions/CrisisActions';

class APFUpload extends Component {

    state = {
        uploading: false, 
        downloading: false,
        articles: [],
        loading_articles: true
    }

    componentDidMount(){
        this.getAPFList();
    }

    getAPFList = async () => {
        const { client_details } = this.props;
        this.setState({loading_articles: true});
        const response = await this.props.getAPFList(client_details.id);
        if(response && response.status === 200){
            this.setState({articles: response.data.data});
        } 
        this.setState({loading_articles: false});
    }

    uploadAPFFile = async (e) => {
        const { client_details } = this.props;
        const upload_files = Array.from(e.target.files);
        const formData = new FormData();
        formData.append('csv', upload_files[0]);
        this.setState({uploading: true});
        await this.props.uploadAPFFile(client_details.id, formData);
        this.getAPFList();
        this.setState({uploading: false});
    }

    downloadAPFSampleFile = async () => {
        const { client_details } = this.props;
        this.setState({downloading: true});
        await this.props.downloadAPFSampleFile(client_details.id);
        this.setState({downloading: false});
    }

    render() {
        const { uploading, downloading, articles, loading_articles } = this.state;
        return (
            <div>
                <div>
                    <h2 className="is-size-6">Action Planning Data Upload</h2>
                </div>
                <article className="message">
                    <div className="message-body modal-sub-header-text">
                        <ol>
                            <li><b>Download the sample file</b> to understand the format.</li>
                            <li><b>Upload QB Data File First in step 3</b> to get driver elements in the APF sample file.</li>
                        </ol>
                    </div>
                </article>
                <br/>
                <div className="columns">
                    <div className="mg-10 has-text-centered column">
                        <button onClick={this.downloadAPFSampleFile} type="submit" className={classnames('button is-info', {'is-loading': downloading})}>
                            <span className="icon">
                                <i className="fas fa-file-download"></i>
                            </span>
                            <span>Download Sample APF File</span>
                        </button>
                    </div>
                    <div className="mg-10 has-text-centered column">
                        <button type="submit" onClick={() => {document.getElementById('apfUploadFile').click();}} className={classnames('button is-success', {'is-loading': uploading})}>
                            <span className="icon">
                                <i className="fas fa-file-upload"></i>
                            </span>
                            <span>Upload APF File</span>
                        </button>
                        {uploading ? null : <input accept=".csv" type="file" id='apfUploadFile' onChange={this.uploadAPFFile} hidden/>}
                    </div>
                </div>
                <hr/>
                <h2 className="is-size-6">Last Uploaded Data</h2>
                {
                    loading_articles 
                        ? <p className="has-text-centered">Loading...</p>
                        : (articles && articles.length 
                            ? 
                            <div className="has-text-centered">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th>Sno</th>
                                            <th>Driver</th>
                                            <th>Element</th>
                                            <th>Article</th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                            {
                                                articles.map((article, index) => (
                                                    <tr key={index}>
                                                        <td>{index + 1}</td>
                                                        <td>{article.driver_name}</td>
                                                        <td>{article.element_name}</td>
                                                        <td><a href={article.link} target="_blank">{article.title}</a></td>
                                                    </tr>
                                                ))
                                            }
                                        </tbody>
                                    </table>
                            </div> 
                            : 
                            <p className="has-text-centered">
                                No Data Available
                            </p>
                        )
                }
            </div>
        )
    }
}

// PropTypes
// ---------
APFUpload.propTypes = {
    client_details: PropTypes.object.isRequired,
    downloadAPFSampleFile: PropTypes.func.isRequired,
    getAPFList: PropTypes.func.isRequired,
    uploadAPFFile: PropTypes.func.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    client_details: state.client.client_details,
})

export default connect(mapStateToProps, { downloadAPFSampleFile, uploadAPFFile, getAPFList })(APFUpload);