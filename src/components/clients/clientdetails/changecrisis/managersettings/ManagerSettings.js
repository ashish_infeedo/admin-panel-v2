import React, { Component } from 'react';
// hoc
import ComponentCard from '../../../../generic/ComponentCard';
// components
import APFUpload from './APFUpload';
import ManagerDataUpload from './ManagerDataUpload';
// HOC wrapper
const ManagerDataUploadCard =  ComponentCard(ManagerDataUpload, 'Manager Data Upload', 1, 'is-link');
const APFUploadCard = ComponentCard(APFUpload, 'APF Upload', 2, 'is-link');



export default class ManagerSettings extends Component {
    render() {
        return (
            <div>
                <ManagerDataUploadCard/>
                <APFUploadCard/>
            </div>
        )
    }
}
