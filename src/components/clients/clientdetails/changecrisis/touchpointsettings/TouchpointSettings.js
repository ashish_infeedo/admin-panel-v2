import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';
// hoc
import ComponentCard from '../../../../generic/ComponentCard';
// actions
import { getAllSurveys } from '../../../../../actions/CrisisActions';
// components
import DropDown from '../../../../generic/Dropdown';
import Emailtemplates from '../emailtemplates/EmailTemplates';
import Peripheral from '../peripheralmessages/Peripheral';
const EmailtemplatesCard = ComponentCard(Emailtemplates, 'Email Templates', 2, 'is-primary');
const PeripheralCard = ComponentCard(Peripheral, 'Peripheral Messages', 3, 'is-primary');

class TouchpointSettings extends Component {

    state = {
        survey_types: [],
        selected_survey: {},
        loading: false
    }

    componentDidMount(){
        this.getAllSurveys();
    }

    componentDidUpdate(prevProps){
        const { selected_segment, check_update } = this.props;
        if(JSON.stringify(selected_segment) !== JSON.stringify(prevProps.selected_segment)){
            this.getAllSurveys();
        }
        if(check_update !== prevProps.check_update){
            this.setState({survey_types: [], selected_survey: {}}, () => this.getAllSurveys());
        }
    }

    onChangeSurvey = (selected_survey) => {
        this.setState({selected_survey});
    }

    getAllSurveys = async () => {
        const { client_details, selected_segment } = this.props;
        this.setState({loading: true});
        const response = await this.props.getAllSurveys(client_details.id, selected_segment.id);
        if(response && response.status){
            this.setState({
                survey_types: (response.data && response.data.length ? response.data.sort((a, b) =>  moment(a.trigger_date, 'DD MMM YYYY').diff(moment(b.trigger_date, 'DD MMM YYYY'))) : []),
                selected_survey: (response.data && response.data.length ? response.data[0] : {})
            })
        }
        this.setState({loading: false});
    }

    render() {
        const { survey_types, selected_survey, loading} = this.state;
        const { selected_segment } = this.props;
        return (
            <div>
                <div>
                    <span className="v-align-sub has-text-weight-bold">Select Reach-out:&nbsp;&nbsp; </span>
                    <DropDown 
                        dropdown_items={survey_types} 
                        selected_item={selected_survey} 
                        onClick={this.onChangeSurvey}
                        identity={'trigger_date'}
                        loading={loading}
                    />
                </div>
                <br/>
                <EmailtemplatesCard
                    disabled={!survey_types.length}
                    selected_segment={selected_segment}
                    selected_survey={selected_survey}
                />
                <PeripheralCard
                    selected_segment={selected_segment}
                    selected_survey={selected_survey}
                    disabled={!survey_types.length}
                />
            </div>
        )
    }
}


// PropTypes
// ---------
TouchpointSettings.propTypes = {
    client_details: PropTypes.object.isRequired,
    getAllSurveys: PropTypes.func.isRequired,
    selected_segment: PropTypes.object.isRequired,
    check_update: PropTypes.number.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    client_details: state.client.client_details,
})

export default connect(mapStateToProps, { getAllSurveys })(TouchpointSettings);