import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classnames from 'classnames';
// acions
import { getEmailtemplates, patchEmailtemplates } from '../../../../../actions/CrisisActions';
// components
import CopyVariable from '../../../../generic/CopyVariable';
import EmailEditor from '../../breadboard/emailtemplates/ReusableEditor';
import ConfirmModal from '../../../../generic/ConfirmModal';
import DropDown from '../../../../generic/Dropdown';

class EmailTemplates extends Component {

    state = {
        variables: [
            {
                id: 'crisis_name',
                value: '{{{crisis_name}}}',
                description: 'Crisis Name'
            },
            {
                id: 'firstname',
                value: '{{{first_name}}}',
                description: 'First Name of Employee'
            },
            {
                id: 'companyname',
                value: '{{{company_name}}}',
                description: 'Company Name'
            },
            {
                id: 'link',
                value: '{{{link}}}',
                description: 'Link to Chat'
            },
            {
                id: 'primary_admin_full_name',
                value: '{{{primary_admin_full_name}}}',
                description: 'Name of Email Sender'
            },
            {
                id: 'designation',
                value: '{{{designation}}}',
                description: 'Designation of Email Sender'
            }
        ],
        emails: [],
        selected_email: {
            subject: '',
            content: ''
        },
        loading: false,
        confirm_active: false,
        saving_email: false,
        modal_text: {
            text: "Are you sure you want to save email template?",
            title: "Save for Current Touchpoint"
        }
    }

    componentDidMount(){
        this.getEmailtemplates();
    }

    componentDidUpdate(prevProps){
        const { selected_survey } = this.props;
        if(JSON.stringify(selected_survey) !== JSON.stringify(prevProps.selected_survey)){
            this.getEmailtemplates();
        }
    }

    getEmailtemplates = async () => {
        const { client_details, selected_segment, selected_survey } = this.props;
        this.setState({loading: true});
        const response = await this.props.getEmailtemplates(client_details.id, selected_segment.id, selected_survey.survey_id);
        if(response && response.status === 200){
            this.setState({
                emails: (response.data && response.data.length ? response.data : []),
                selected_email: (response.data && response.data.length ? response.data[0] : {})
            });
        }
        this.setState({loading: false});
    }

    openConfirmModal = (updated_content, content_changed) => {
        this.setState({confirm_active: true, updated_content});
    }

    closeConfirmModal = () => {
        this.setState({confirm_active: false});
    }

    onSubjectChange = (e) => {
        const { selected_email } = this.state;
        selected_email[e.target.name] = e.target.value;
        this.setState({selected_email});
    }

    onChangeEmail = (selected_email) => {
        this.setState({selected_email});
    }

    onSave = async () => {
        const { client_details, selected_segment, selected_survey } = this.props;
        const { updated_content, selected_email} = this.state;
        selected_email.content = updated_content;
        this.setState({saving_email: true});
        const response = await this.props.patchEmailtemplates(client_details.id, selected_segment.id, selected_survey.survey_id, selected_email.id,  selected_email.subject, selected_email.content);
        if(response && response.status === 200){
            this.closeConfirmModal();
        }
        this.setState({saving_email: false, selected_email});
    }

    render() {
        const { selected_email, variables, emails, loading, modal_text, saving_email, confirm_active } = this.state;
        return (
            <>
            <div>
                <span className="v-align-sub has-text-weight-bold">Select Email Type:&nbsp;&nbsp; </span>
                <DropDown 
                    dropdown_items={emails} 
                    selected_item={selected_email} 
                    onClick={this.onChangeEmail}
                    identity={'type_value'}
                    loading={loading}
                />
            </div>
            <br/>
            {loading
            ? <p className="helper has-text-centered">loading...</p>
            :
            <div className="columns">
                <div className="column">
                    <div className="field">
                        <label className="label">Subject</label>
                        <div className="control">
                            <input className="input" onChange={this.onSubjectChange} type="text" name="subject" value={(selected_email.subject ? selected_email.subject : '')}/>
                        </div>
                    </div>
                    <div>
                        <label className="label">Email Content</label>
                        <EmailEditor
                            onSave={this.openConfirmModal}
                            content={(selected_email.content ? selected_email.content : '')}
                        />
                    </div>
                </div>
                <CopyVariable
                    variables={variables}
                />
            </div>
            }
            <ConfirmModal
                text={modal_text.text}
                title={modal_text.title}
                confirm_text={"Yes"}
                doing={saving_email}
                is_active={confirm_active}
                onConfirm={this.onSave}
                onClose={this.closeConfirmModal}
            />
            </>
        )
    }
}

// PropTypes
// ---------
EmailTemplates.propTypes = {
    client_details: PropTypes.object.isRequired,
    selected_segment: PropTypes.object.isRequired,
    selected_survey: PropTypes.object.isRequired,
    getEmailtemplates: PropTypes.func.isRequired,
    patchEmailtemplates: PropTypes.func.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    client_details: state.client.client_details,
})

export default connect(mapStateToProps, { getEmailtemplates, patchEmailtemplates })(EmailTemplates);