import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// hoc
import ComponentCard from '../../../generic/ComponentCard';
// actions
import { getSegments } from '../../../../actions/CrisisActions';
// components
import DropDown from '../../../generic/Dropdown';
import Segment from './segments/Segment';
import TouchPointSettings from './touchpointsettings/TouchpointSettings';
import ModuleConfigure from './moduleconfigure/ModuleConfigure';
import QuestionBankUpload from './questionbank/QuestionBankUpload';
import ManagerSettings from './managersettings/ManagerSettings';
// HOC wrapper
const SegmentCard = ComponentCard(Segment, 'Segments', 1, 'is-link');
const ModuleConfigureCard =  ComponentCard(ModuleConfigure, 'Change Crisis Config', 2, 'is-link');
const QuestionBankUploadCard = ComponentCard(QuestionBankUpload, 'Question Bank Upload', 3, 'is-link');
const TouchPointSettingsCard = ComponentCard(TouchPointSettings, 'Reach-out Configure', 4, 'is-link');
const ManagerCard = ComponentCard(ManagerSettings, 'Manager Nudges', 5, 'is-link');

class ChangeCrisis extends Component {
    
    
    state = {
        loading_segments: false,
        segments: [],
        selected_segment: {},
        check_update: 0
    }

    // component mounts
    // ----------------
    componentDidMount(){
        const { client_details } = this.props;
        if(client_details && client_details.id){
            this.getSegments();
        }
    }

    // component updates
    // -----------------
    componentDidUpdate(prevProps){
        const { client_details } = this.props;
        if(prevProps.client_details.id !== client_details.id){
            this.getSegments();
        }
    }

    getSegments = async () => {
        const { client_details } = this.props;
        const response = await this.props.getSegments(client_details.id);
        if(response && response.status === 200){
            this.setState({
                segments: (response.data && response.data.length ? response.data : []),
                selected_segment: (response.data && response.data.length ? response.data[0] : {})
            })
        }
    }

    onChangeSegment = (selected_segment) => {
        this.setState({selected_segment});
    }

    incrementUpdate = () => {
        let { check_update } = this.state;
        this.setState({check_update: (check_update + 1)})
    }

    render() {
        const { segments, loading_segments, selected_segment, check_update } = this.state;
        return ( 
            <div>
                <SegmentCard
                    segments={segments}
                    reloadSegments={this.getSegments}
                />
                <br/>
                <div>
                    <span className="v-align-sub has-text-weight-bold">Select Segment:&nbsp;&nbsp; </span>
                    <DropDown 
                        dropdown_items={segments} 
                        selected_item={selected_segment} 
                        onClick={this.onChangeSegment}
                        identity={'title'}
                        loading={loading_segments}
                    />
                </div>
                <br/>
                <ModuleConfigureCard
                    disabled={!segments.length}
                    selected_segment={selected_segment}
                />
                <QuestionBankUploadCard
                    disabled={!segments.length}
                    selected_segment={selected_segment}
                    incrementUpdate={this.incrementUpdate}
                    check_update={check_update}
                />
                <TouchPointSettingsCard
                    disabled={!segments.length}
                    selected_segment={selected_segment}
                    check_update={check_update}
                />
                <br/>
                <ManagerCard
                    disabled={loading_segments}
                />
            </div>
        )
    }
}


// PropTypes
// ---------
ChangeCrisis.propTypes = {
    client_details: PropTypes.object.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    client_details: state.client.client_details,
    getSegments: PropTypes.func.isRequired
})

export default connect(mapStateToProps, { getSegments })(ChangeCrisis);