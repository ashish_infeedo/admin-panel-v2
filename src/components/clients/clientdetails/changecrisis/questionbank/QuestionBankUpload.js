import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { connect } from 'react-redux';
// components
import ReachoutTimeline from './ReachoutTimeline';
// actions
import { getUploadFile, uploadQbFile } from '../../../../../actions/CrisisActions';

class QuestionBankUpload extends Component {

    state = {
        uploading: false, 
        downloading: false,
        selected_segment: {}
    }

    componentDidMount(){
        const { selected_segment } = this.props;
        this.setState({selected_segment});
    }

    componentDidUpdate(prevProps){
        const { selected_segment } = this.props;
        if(JSON.stringify(selected_segment) !== JSON.stringify(prevProps.selected_segment)){
            this.setState({selected_segment});
        }
    }

    uploadQBFile = async (e) => {
        const { selected_segment } = this.state;
        const { client_details } = this.props;
        const upload_files = Array.from(e.target.files);
        const formData = new FormData();
        formData.append('csv', upload_files[0]);
        this.setState({uploading: true});
        const response = await this.props.uploadQbFile(client_details.id, selected_segment.id , formData);
        if(response && response.status === 200){
            this.props.incrementUpdate();
        }
        this.setState({uploading: false});
    }

    getSampleUploadFile = async () => {
        this.setState({downloading: true});
        await this.props.getUploadFile();
        this.setState({downloading: false});
    }

    render() {
        const { uploading, downloading } = this.state;
        const { selected_segment, check_update, incrementUpdate } = this.props;
        return (
            <>
                <div>
                    <h2 className="is-size-6">Question Bank Upload</h2>
                </div>
                <article className="message">
                    <div className="message-body modal-sub-header-text">
                        <ol>
                            <li><b>Download the sample file</b> to understand the format.</li>
                            <li><b>Upload QB Data File</b> to add more reach outs in the <b>selected segment.</b></li>
                            <li>While uploading and launching keep the following points in mind:
                                <ul >
                                    <li><b>Trigger date</b> column date format should be <b>dd-mmm-yyyy</b></li>
                                    <li><b>Reach outs</b> should have at least <b>7 days gap</b> between them (also means reach outs in a segment cannot have a same date)</li>
                                    <li>After successful QB upload, <b>confirm</b> reach outs, their dates, and question bank from the <b>timeline view</b> below</li>
                                    <li><b>Weekly reach outs</b> will have <b>1 reminders</b>, <b>Fortnightly</b> ones will have <b>2 reminders</b> and <b>Monthly</b> ones will have <b>3 reminders</b> (reminders = to start + dropped)</li>
                                    <li>Don’t forget to <b>crosscheck</b> the <b>reach-out and reminder email, and peripheral-messages</b> for each reach out</li>
                                </ul>
                            </li>
                        </ol>
                    </div>
                </article>
                <br/>
                <div className="columns">
                    <div className="mg-10 has-text-centered column">
                        <button onClick={this.getSampleUploadFile} type="submit" className={classnames('button is-info', {'is-loading': downloading})}>
                            <span className="icon">
                                <i className="fas fa-file-download"></i>
                            </span>
                            <span>Download Sample File</span>
                        </button>
                    </div>
                    <div className="mg-10 has-text-centered column">
                        <button type="submit" onClick={() => {document.getElementById('uploadQBFile').click();}} className={classnames('button is-success', {'is-loading': uploading})}>
                            <span className="icon">
                                <i className="fas fa-file-upload"></i>
                            </span>
                            <span>Upload QB Data File</span>
                        </button>
                        {uploading ? null : <input accept=".csv" type="file" id='uploadQBFile' onChange={this.uploadQBFile} hidden/>}
                    </div>
                </div>
                <hr/>
                <ReachoutTimeline
                    selected_segment={selected_segment}
                    check_update={check_update}
                    incrementUpdate={incrementUpdate}
                />
            </>
        )
    }
}

// PropTypes
// ---------
QuestionBankUpload.propTypes = {
    client_details: PropTypes.object.isRequired,
    getUploadFile: PropTypes.func.isRequired,
    selected_segment: PropTypes.object.isRequired,
    check_update: PropTypes.number.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    client_details: state.client.client_details,
})

export default connect(mapStateToProps, { getUploadFile, uploadQbFile })(QuestionBankUpload);