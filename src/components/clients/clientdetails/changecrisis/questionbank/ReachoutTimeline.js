import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { connect } from 'react-redux';
// actions
import { deleteQB, getAllSurveys } from '../../../../../actions/CrisisActions';
// hoc
import ComponentCard from '../../../../generic/ComponentCard';
// components
import QuestionBank from '../questionbank/QuestionBank';
import ConfirmModal from '../../../../generic/ConfirmModal';
const QuestionBankUploadCard = ComponentCard(QuestionBank, '', 3, 'is-link');

class ReachoutTimeline extends Component {

    state = {
        survey_types: [],
        selected_survey: {},
        loading: false,
        action_buttons: [],
        modal_text: {
            title: 'Delete complete reachout?',
            text: 'Are you sure you want to delete this reachout?'
        },
        deleting: false,
        delete_modal_active: false
    }

    componentDidMount(){
        this.getAllSurveys();
        const { action_buttons } = this.state;
        action_buttons.push({
            title: 'Delete',
            color_class: 'is-danger',
            function_call: this.onClickDelete,
            disabled: false
        });
        this.setState({action_buttons});
    }

    componentDidUpdate(prevProps){
        const { selected_segment, check_update } = this.props;
        if(JSON.stringify(selected_segment) !== JSON.stringify(prevProps.selected_segment)){
            this.getAllSurveys();
        }
        if(check_update !== prevProps.check_update){
            this.setState({survey_types: [], selected_survey: {}}, () => this.getAllSurveys());
        }
    }

    getAllSurveys = async () => {
        const { client_details, selected_segment } = this.props;
        this.setState({loading: true});
        const response = await this.props.getAllSurveys(client_details.id, selected_segment.id);
        if(response && response.status){
            this.setState({
                survey_types: (response.data && response.data.length ? response.data : []),
                selected_survey: (response.data && response.data.length ? response.data[0] : {})
            })
        }
        this.setState({loading: false});
    }

    deleteQB = async () => {
        const { client_details, selected_segment } = this.props;
        const { selected_survey } = this.state;
        this.setState({deleting: true});
        const response = await this.props.deleteQB(client_details.id, selected_segment.id, selected_survey.survey_id);
        if(response && response.status){
            this.props.incrementUpdate();
            this.closeDeleteModal();
        }
        this.setState({deleting: false});
    }

    onClickDelete = (selected_survey) => {
        const { modal_text } = this.state;
        modal_text.title = `Delete complete reachout - ${selected_survey.trigger_date}?`
        this.setState({selected_survey, delete_modal_active: true});
    }

    closeDeleteModal = () => {
        this.setState({delete_modal_active: false});
    }

    render() {
        const { loading, survey_types, action_buttons, modal_text, deleting, delete_modal_active } = this.state;
        const { selected_segment } = this.props;
        return (
            <div className="reachout-timeline">
                <div>
                    <h2 className="is-size-6">Reachout List + QB Configure</h2>
                    <p className="help">Click on each touchpoint to edit questions, use the delete button to archive the reachout. Only <b>upcoming reach-outs can be deleted</b> from the timeline.</p>
                </div>
                <br/>
                <div>
                    {
                        loading ? <p className="has-text-centered padding-40">loading questions...</p> 
                        : survey_types.sort((a, b) =>  moment(a.trigger_date, 'DD MMM YYYY').diff(moment(b.trigger_date, 'DD MMM YYYY'))).map((survey, index) => (
                            <QuestionBankUploadCard 
                                key={index} 
                                dynamic_index={index + 1} 
                                dynamic_title={survey.trigger_date}
                                selected_segment={selected_segment}
                                selected_survey={survey}
                                disabled={!survey_types.length}
                                action_buttons={(survey.trigger_date && moment(survey.trigger_date, 'DD MMM YYYY').isAfter(moment()) ? action_buttons : null)}
                                return_value={survey}
                            />
                        ))
                    }
                </div>
                <ConfirmModal
                    text={modal_text.text}
                    title={modal_text.title}
                    confirm_text={"Yes"}
                    doing={deleting}
                    is_active={delete_modal_active}
                    onConfirm={this.deleteQB}
                    onClose={this.closeDeleteModal}
                />
            </div>
        )
    }
}

// PropTypes
// ---------
ReachoutTimeline.propTypes = {
    client_details: PropTypes.object.isRequired,
    selected_segment: PropTypes.object.isRequired,
    check_update: PropTypes.number.isRequired,
    incrementUpdate: PropTypes.func.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    client_details: state.client.client_details,
})

export default connect(mapStateToProps, { deleteQB, getAllSurveys })(ReachoutTimeline);