import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// acions
import { getSpecificQB, deleteQuestion } from '../../../../../actions/CrisisActions';
// components
import QuestionCard from '../../breadboard/questionbank/QuestionCard';
import EditQuestionModal from './EditQuestionModal';
import ConfirmModal from '../../../../generic/ConfirmModal';

class QuestionBank extends Component {

    state = {
        questions: [],
        loading_questions: false,
        open_edit_modal: false,
        delete_modal_active: false,
        selected_question: {},
        modal_text: {
            title: 'Delete Question',
            text: 'Are you sure you want to delete question?'
        }, 
        deleting: false,
        delete_modal_active: false,
    }

    componentDidMount(){
        this.getQuestionbank();
    }

    componentDidUpdate(prevProps){
        const { selected_survey } = this.props;
        if(JSON.stringify(selected_survey) !== JSON.stringify(prevProps.selected_survey)){
            this.getQuestionbank();
        }
    }

    getQuestionbank = async () => {
        const { client_details, selected_segment, selected_survey} = this.props;
        this.setState({loading_questions: true});
        const response = await this.props.getSpecificQB(client_details.id, selected_segment.id, selected_survey.survey_id);
        if(response && response.status){
            this.setState({
                questions: (response.data && response.data.questions ? response.data.questions : [])
            })
        }
        this.setState({loading_questions: false});
    }

    onEditQuestion = (selected_question) => {
        this.setState({selected_question}, () => this.openEditModal());
    }

    openEditModal = () => {
        this.setState({open_edit_modal: true});
    }

    closeEditModal = () => {
        this.setState({open_edit_modal: false, selected_question: {}});
    }

    openDeleteModal = (selected_question) => {
        this.setState({delete_modal_active: true, selected_question});
    }

    closeDeleteModal = () => {
        this.setState({delete_modal_active: false, selected_question: {}});
    }

    deleteQuestion = async () => {
        const { selected_question } = this.state;
        const { client_details, selected_survey,  selected_segment} = this.props;
        this.setState({deleting: true});
        const response = await this.props.deleteQuestion(client_details.id, selected_segment.id, selected_survey.survey_id, selected_question.id);
        if(response && response.status === 200){
            this.closeDeleteModal();
            this.getQuestionbank();
        }
        this.setState({deleting: false});
    }

    render() {
        const { loading_questions, questions, open_edit_modal, selected_question, modal_text, delete_modal_active, deleting } = this.state;
        const { client_details, selected_survey,  selected_segment} = this.props;
        return (
            <div>
                <div>
                {
                    loading_questions 
                    ? <p className="has-text-centered padding-40">loading questions...</p> 
                    : 
                    (
                        questions.length ? questions.map(question => (
                            <QuestionCard
                                key={question.id}
                                question={question}
                                onDelete={this.openDeleteModal}
                                onEdit={this.onEditQuestion}
                                increment_sentiment={3}
                            />
                        )) : <p className="has-text-centered padding-40">No Questions Available</p>
                    )
                }
                </div>
                {open_edit_modal ? 
                    <EditQuestionModal
                        modal_active={open_edit_modal}
                        onClose={this.closeEditModal}
                        question={selected_question}
                        client_id={client_details.id}
                        refreshQuestions={this.getQuestionbank}
                        survey_id={selected_survey.survey_id}
                        segment_id={selected_segment.id}
                    /> 
                : null}
                <ConfirmModal
                    text={modal_text.text}
                    title={modal_text.title}
                    confirm_text={"Yes"}
                    doing={deleting}
                    is_active={delete_modal_active}
                    onConfirm={this.deleteQuestion}
                    onClose={this.closeDeleteModal}
                />
            </div>
        )
    }
}


// PropTypes
// ---------
QuestionBank.propTypes = {
    client_details: PropTypes.object.isRequired,
    selected_segment: PropTypes.object.isRequired,
    selected_survey: PropTypes.object.isRequired,
    getSpecificQB: PropTypes.func.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    client_details: state.client.client_details,
})

export default connect(mapStateToProps, { getSpecificQB, deleteQuestion })(QuestionBank);