import React, { Component } from 'react'

export default class Tabs extends Component {
    render() {
        return (
            <div className="tabs is-centered is-small is-toggle is-toggle-rounded mg-bottom-10">
                <ul>
                    <li className="is-active">
                    <a>
                        <span className="icon is-small"><i className="fas fa-briefcase"></i></span>
                        <span>Tenure</span>
                    </a>
                    </li>
                    <li>
                    <a className="disbaled-tab">
                        <span className="icon is-small"><i className="far fa-clock"></i></span>
                        <span>MTM</span>
                    </a>
                    </li>
                </ul>
            </div>
        )
    }
}
