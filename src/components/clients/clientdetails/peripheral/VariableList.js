import React, { Component } from 'react';
import PropTypes from 'prop-types';

class VariableList extends Component {
    render() {
        const { copyText, variables } = this.props;
        return (
            <div>
                    <div className="column is-12">
                        <article className="message is-info">
                        <div className="message-body">
                        {variables.length ? variables.map((variable) => (
                            <div key={variable.key} className="columns">
                                <div className="column is-3">
                                    <div className="field has-addons is-small">
                                        <div className="control width-200">
                                            <input className="input is-small" type="text" id={variable.key} value={`{{{${variable.key}}}}`} readOnly/>
                                        </div>
                                        <div className="control" onClick={copyText.bind(this, variable.key)}>
                                            <a type="cancel" className="button is-small is-info">
                                            <i className="fas fa-copy"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div className="column is-hidden-mobile">
                                    <p className="is-size-7"><b>Description:</b> {variable.description}</p>
                                    <p className="font-size-10-px"><b>Example:</b> {variable.example}</p>
                                </div>
                            </div>
                        )): null}
                        </div>
                        </article>
                    </div> 
            </div>
        )
    }
}

// PropTypes
// ---------
VariableList.propTypes = {
    copyText: PropTypes.func.isRequired,
    variables: PropTypes.array.isRequired
}

export default VariableList;
