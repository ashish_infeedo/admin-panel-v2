import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const SummaryDetailsModal = ({
    is_active,
    header_text,
    onClose,
    cardData,
    sub_header_text
  }) => {

function abc(){
    let HTML = []
    cardData.forEach((data) => {
        HTML.push(<div className="is-12 pd-up-20" >
                    <div> {data.title} - ( {data.count} ) </div>
                  </div>)
    })
    return HTML;
}
return (
    <div className={classnames('modal', {"is-active" : is_active})}>
        <div onClick= {onClose} className="modal-background"></div>
        <div className="modal-content card">
            <div className="card-content">
                <div className="modal-header-text" >
                    <p> {header_text} </p>
                </div>
                <div className="modal-sub-header-text">
                    <p> {sub_header_text} </p>
                </div>
                {abc()}
            </div> 
        </div>
        <button className="modal-close is-large is-hidden-tablet" aria-label="close"></button>
    </div>
);
};

// Porptypes
// ---------
SummaryDetailsModal.propTypes = {
    is_active: PropTypes.bool.isRequired,
};

// default props
// -------------
SummaryDetailsModal.defaultProps = {
    is_active: false
};

export default SummaryDetailsModal;
