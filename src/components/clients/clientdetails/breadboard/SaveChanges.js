import React from 'react';
import PropTypes from 'prop-types';


const SaveChanges = ({
    onSaveCurrent,
    onSaveForAll,
    disable_save
}) => {
  return (
    <div className="buttons justify-flex-end is-marginless">
        <button onClick={onSaveCurrent} type="submit" className="button is-primary"  disabled={disable_save}>
            <span className="icon">
            <i className="far fa-save"></i>
            </span>
            <span>Save</span>
        </button>
        {/* <button onClick={onSaveForAll} type="submit" className="button is-success" disabled={disable_save}>
            <span className="icon">
            <i className="far fa-save"></i>
            </span>
            <span>Save for all Cohorts</span>
        </button> */}
    </div>
  );
};

SaveChanges.propTypes = {
    onSaveCurrent: PropTypes.func.isRequired,
    onSaveForAll: PropTypes.func.isRequired,
    disable_save: PropTypes.bool.isRequired
};

export default SaveChanges;