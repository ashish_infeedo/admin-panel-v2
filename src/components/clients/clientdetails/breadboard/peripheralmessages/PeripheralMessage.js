import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classnames from 'classnames';
// actions
import { getPeripheralMessages, updatePeripheralMessages } from '../../../../../actions/BreabBoardActions';
// components
import ConfirmModal from '../../../../generic/ConfirmModal';
import VariableList from '../../peripheral/VariableList';
import DropDown from '../../../../generic/Dropdown';

class PeripheralMessage extends Component {

    // initial state
    // -------------
    state = {
        loading: true,
        top_peripheral_messages: [],
        top_peripheral_messages_copy: [],
        bottom_peripheral_messages: [],
        bottom_peripheral_messages_copy: [],
        to_start_button_text: '',
        to_start_button_text_copy: '',
        survey_type: 0,
        variables: [],
        disable_save: true,
        disable_reset: true,
        saving: false,
        confirm_modal: {
            title: 'Peripheral Messages',
            text: 'Are you sure you want to update Peripheral Messages?',
            confirm_text: 'Yes',
            is_active: false
        },
        selected_touch_point: {},
        touch_points: [],
        loading_touch_points: false,
        buckets: [],
        selected_bucket: {},
    }

    componentDidMount(){
        const { selected_bucket } = this.props;
        this.setState({
            touch_points: selected_bucket.touch_points,
            selected_touch_point: selected_bucket.touch_points[0],
            selected_bucket
        }, () => {
            this.getPeripheralMessages();
        });
    }

    // get peripheral messages
    // -----------------------
    getPeripheralMessages = async () => {
        const { client_details, selected_cohort } = this.props;
        const { selected_touch_point } = this.state;
        this.setState({loading: true});
        const response = await this.props.getPeripheralMessages(client_details.id, selected_cohort.id, selected_touch_point.touch_point_key);
        if(response && response.status === 200){
            this.setState({
                loading: false,
                top_peripheral_messages: (response.data && response.data.top_peripheral_messages ? response.data.top_peripheral_messages : []),
                top_peripheral_messages_copy: (response.data && response.data.top_peripheral_messages ? JSON.parse(JSON.stringify(response.data.top_peripheral_messages)) : []),
                bottom_peripheral_messages: (response.data && response.data.bottom_peripheral_messages ? response.data.bottom_peripheral_messages : []),
                bottom_peripheral_messages_copy: (response.data && response.data.bottom_peripheral_messages ? JSON.parse(JSON.stringify(response.data.bottom_peripheral_messages)) : []),
                to_start_button_text: (response.data && response.data.to_start_button_text ? response.data.to_start_button_text : ''),
                to_start_button_text_copy: (response.data && response.data.to_start_button_text ? response.data.to_start_button_text : ''),
                survey_type: (response.data && response.data.survey_type ? response.data.survey_type : null),
                variables: (response.data && response.data.variables ? response.data.variables : [])
            });
        }
    }

    // on input change
    // ---------------
    onChange = e => {
        this.setState({ [e.target.name]: e.target.value }, () => this.checkIfChanges());
    }

    // on text area change
    // -------------------
    onChangeTextArea = (e, index, is_top) => {
        const { top_peripheral_messages, bottom_peripheral_messages } = this.state;
        if(is_top){
            top_peripheral_messages[index].content = e.target.value;
        } else {
            bottom_peripheral_messages[index].content = e.target.value;
        }
        this.setState({ top_peripheral_messages, bottom_peripheral_messages }, () => this.checkIfChanges());
    }
    
    // copy text
    // ---------
    copyText = (id) => {
        const copyText = document.querySelector(`#${id}`);
        copyText.select();
        document.execCommand("copy");
    }

    // check if changes have been made
    // --------------------------------
    checkIfChanges = () => {
        const { top_peripheral_messages, top_peripheral_messages_copy, bottom_peripheral_messages, bottom_peripheral_messages_copy, to_start_button_text, to_start_button_text_copy} = this.state;
        if(to_start_button_text_copy !== to_start_button_text || JSON.stringify(top_peripheral_messages) !== JSON.stringify(top_peripheral_messages_copy) || JSON.stringify(bottom_peripheral_messages) !== JSON.stringify(bottom_peripheral_messages_copy)){
            this.setState({disable_reset: false, disable_save: false});
        } else {
            this.setState({disable_reset: true, disable_save: true});
        }
    }

    // reset all text boxes
    // --------------------
    resetAll = () => {
        const { top_peripheral_messages_copy, bottom_peripheral_messages_copy, to_start_button_text_copy} = this.state;
        this.setState({
            top_peripheral_messages: JSON.parse(JSON.stringify(top_peripheral_messages_copy)),
            bottom_peripheral_messages: JSON.parse(JSON.stringify(bottom_peripheral_messages_copy)),
            to_start_button_text: to_start_button_text_copy,
            disable_reset: true,
            disable_save: true
        });
    }

    // save all changes
    // ----------------
    saveAll = async () => {
        const { bottom_peripheral_messages, top_peripheral_messages, to_start_button_text, survey_type, selected_touch_point } = this.state;
        const { client_details, selected_cohort } = this.props;
        this.setState({saving: true});
        const response = await this.props.updatePeripheralMessages(client_details.id, selected_cohort.id, selected_touch_point.touch_point_key, {bottom_peripheral_messages, top_peripheral_messages, to_start_button_text, survey_type});
        this.setState({saving: false});
        if(response){
            if(response.status === 200){
                this.setState({
                    top_peripheral_messages_copy: JSON.parse(JSON.stringify(top_peripheral_messages)),
                    bottom_peripheral_messages_copy: JSON.parse(JSON.stringify(bottom_peripheral_messages)),
                    to_start_button_text_copy: to_start_button_text,
                    disable_reset: true,
                    disable_save: true
                })
            }
        }
        this.closeModal();
    }

    // close modal
    // -----------
    closeModal = () => {
        const { confirm_modal } = this.state;
        confirm_modal.is_active = false;
        this.setState({confirm_modal});
    }

    // close modal
    // -----------
    openModal = () => {
        const { confirm_modal } = this.state;
        confirm_modal.is_active = true;
        this.setState({confirm_modal});
    }

    changeTouchpoint = (selected_touch_point) => {
        this.setState({selected_touch_point}, () => this.getPeripheralMessages());
    }

    // render UI
    // ---------
    render() {
        const { variables, loading, top_peripheral_messages, bottom_peripheral_messages, to_start_button_text, disable_save, disable_reset, saving, confirm_modal, touch_points, selected_touch_point } = this.state;
        return (
            <div>
                {loading ? <p className="has-text-centered">Loading Data...</p>
                :
                <div>
                    <VariableList copyText={this.copyText} variables={variables}/>
                    <br/>
                    <div className="columns">
                        <div className="column is-6">
                            <h3 className="has-text-centered is-size-6 has-text-weight-bold">Top Peripheral Messages</h3>
                        </div>
                        <div className="column is-6">
                            <h3 className="has-text-centered is-size-6 has-text-weight-bold">Bottom Peripheral Messages</h3>
                        </div>
                    </div>
                    <hr className="is-marginless"/>
                    <br/>
                    {
                        touch_points.length ?
                        <div className="mg-left-2">
                            <span className="v-align-sub has-text-weight-bold is-size-6">Select Touchpoint:&nbsp;&nbsp; </span> 
                            <DropDown 
                                dropdown_items={touch_points} 
                                selected_item={selected_touch_point} 
                                onClick={this.changeTouchpoint}
                                identity={'title'}
                            />
                            <br/><br/>
                        </div>
                        : null
                    }
                    <br/>
                    <div className="columns flex-wrap">
                        <div className="column is-6">
                        {top_peripheral_messages.length 
                            ? 
                            top_peripheral_messages.map((msg, index) => (
                            <div key={index} className="field">
                                <div className="control">
                                    <textarea className="textarea font-size-8" rows="3" onChange={(e) => this.onChangeTextArea(e, index, true)} value={msg.content}></textarea>
                                </div>
                            </div>
                            ))
                            : <p className="has-text-centered">No Messages</p>
                        }
                        </div>
                        <div className="column is-6">
                        {bottom_peripheral_messages.length 
                            ? 
                            bottom_peripheral_messages.map((msg, index) => (
                            <div key={index} className="field">
                                <div className="control">
                                    <textarea className="textarea font-size-8" onChange={(e) => this.onChangeTextArea(e, index, false)} rows="3" value={msg.content}></textarea>
                                </div>
                            </div>
                            ))
                            : <p className="has-text-centered">No Messages</p>
                        }
                        </div>
                        <div className="field column is-12">
                            <label className="label has-text-weight-bold">Edit Start button Text</label>
                            <div className="control">
                                <input className="input" onChange={this.onChange} type="text" name="to_start_button_text" value={to_start_button_text}/>
                            </div>
                        </div>
                        <div className="column is-12 buttons">
                            <button className={classnames('button is-success', {'is-loading': saving})} disabled={disable_save} onClick={this.openModal}>
                                <span className="icon"><i className="fas fa-save"></i></span>
                                <span>Save</span>
                            </button>
                            <button className="button is-info" disabled={disable_reset} onClick={this.resetAll}>
                                <span className="icon"><i className="fas fa-sync"></i></span>
                                <span>Reset</span>
                            </button>
                        </div>
                    </div>
                </div>
                }
                <ConfirmModal 
                    title={confirm_modal.title}
                    text={confirm_modal.text}
                    confirm_text={confirm_modal.confirm_text}
                    onConfirm={this.saveAll}
                    onClose={this.closeModal}
                    is_active={confirm_modal.is_active}
                    doing={saving}
                />
            </div>
        )
    }
}

// PropTypes
// ---------
PeripheralMessage.propTypes = {
    selected_bucket: PropTypes.object.isRequired,
    selected_cohort: PropTypes.object.isRequired,
    getPeripheralMessages: PropTypes.func.isRequired,
    updatePeripheralMessages: PropTypes.func.isRequired,
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    client_details: state.client.client_details, 
})

export default connect(mapStateToProps, { getPeripheralMessages, updatePeripheralMessages })(PeripheralMessage);