import React from 'react';
import PropTypes from 'prop-types';
import Switch from "react-switch";

const GlobalConfig = ({
    welcome_email,
    celebration_email,
    dangerous_words_alert,
    email_spoofing,
    anonymous_bat,
    trust_statement,
    onChange
}) => {
  return (
    <div className="columns column is-paddingless margin-auto is-11 flex-wrap">
        <div className="column is-6 space-toggle pd-10">
            <Switch onChange={onChange.bind(this, 'welcome_email')} checked={(welcome_email ? welcome_email : false)}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={18} width={35}/>
            <span className="is-size-7 toggle-text"><b>Welcome Email</b></span>
        </div> 
        <div className="column is-6 space-toggle pd-10">
            <Switch onChange={onChange.bind(this, 'celebration_email')} checked={(celebration_email ? celebration_email : false)}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={18} width={35}/>
            <span className="is-size-7 toggle-text"><b>Celebration Email</b></span>
        </div>
        <div className="column is-6 space-toggle pd-10">
            <Switch onChange={onChange.bind(this, 'dangerous_words_alert')} checked={(dangerous_words_alert ? dangerous_words_alert : false)}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={18} width={35}/>
            <span className="is-size-7 toggle-text"><b>Dangerous Words Alert Email</b></span>
        </div>
        <div className="column is-6 space-toggle pd-10">
            <Switch onChange={onChange.bind(this, 'email_spoofing')} checked={(email_spoofing ? email_spoofing : false)}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={18} width={35}/>
            <span className="is-size-7 toggle-text"><b>Email Spoofing</b></span>
        </div>
        <div className="column is-6 space-toggle pd-10">
            <Switch onChange={onChange.bind(this, 'anonymous_bat')} checked={(anonymous_bat ? anonymous_bat : false)}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={18} width={35}/>
            <span className="is-size-7 toggle-text"><b>Anonymous bat</b></span>
        </div>
        <div className="column is-6 space-toggle pd-10">
            <Switch onChange={onChange.bind(this, 'trust_statement')} checked={(trust_statement ? trust_statement : false)}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={18} width={35}/>
            <span className="is-size-7 toggle-text"><b>Trust Statement</b></span>
        </div>
    </div>
  );
};

GlobalConfig.propTypes = {
    welcome_email: PropTypes.bool.isRequired,
    celebration_email: PropTypes.bool.isRequired,
    dangerous_words_alert: PropTypes.bool.isRequired,
    email_spoofing: PropTypes.bool.isRequired,
    anonymous_bat: PropTypes.bool.isRequired,
    trust_statement: PropTypes.bool.isRequired,
    onChange:  PropTypes.func.isRequired
};

export default GlobalConfig;