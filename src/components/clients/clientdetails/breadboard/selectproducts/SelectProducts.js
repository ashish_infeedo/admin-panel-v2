import React, { Component } from 'react';
import Switch from "react-switch";
import arrayMove from 'array-move';
import {SortableContainer, SortableElement} from 'react-sortable-hoc';
import GlobalConfig from './GlobalConfig';
import SaveChanges from '../SaveChanges';
import ConfirmModal from '../../../../generic/ConfirmModal';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// actions
import { patchBuckets, triggerQBcreation, triggerEmailTemplateCreation, tenureTouchpointMigration } from '../../../../../actions/BreabBoardActions';

class SelectProducts extends Component {
    
    // initilal state
    // --------------
    state = {
        buckets: [],
        expand_touchpoints: {},
        saving: false,
        global_config: {
            welcome_email: false,
            celebration_email: false,
            dangerous_words_alert: false,
            email_spoofing: false,
            anonymous_bat: false,
            trust_statement: false
        },
        confirm_active: false, 
        disable_save: false,
        save_for_all: false,
        modal_text: {
            text: "Are you sure you want to apply for all cohorts?",
            title: "Save for all Cohorts"
        }
    }

    // component mounts
    // ----------------
    componentDidMount(){
        const { buckets, global_config } = this.props;
        this.setState({buckets: JSON.parse(JSON.stringify(buckets)), global_config: JSON.parse(JSON.stringify(global_config))}, () => this.checkEnableSave());
    }

    // component Updates
    // -----------------
    componentDidUpdate(prevProps){
        const { buckets, global_config } = this.props;
        if(JSON.stringify(buckets) !== JSON.stringify(prevProps.buckets) || JSON.stringify(global_config) !== JSON.stringify(prevProps.global_config)){
            this.setState({buckets: JSON.parse(JSON.stringify(buckets)), global_config: JSON.parse(JSON.stringify(global_config))}, () => this.checkEnableSave());
        }
    }

    // Patch product buckets
    // ---------------------
    patchBuckets = async () => {
        const { buckets, global_config, save_for_all } = this.state;
        const { client_details, selected_cohort, unique_id } = this.props;
        this.setState({saving: true});
        const response = await this.props.patchBuckets(client_details.id, selected_cohort.id, buckets, global_config, unique_id, save_for_all);
        if(response && response.status === 200){
            this.closeModal();
            this.props.refreshBuckets(true);
            // this.triggerQBcreation();
            this.triggerEmailTemplateCreation();
            this.checkTenureEnabled();
        }
        this.setState({saving: false});
    }

    // Trigger QB copy in backend for all cohorts
    // ------------------------------------------
    triggerQBcreation = async () => {
        const { buckets } = this.state;
        const { client_details } = this.props;
        const buckets_enabled = []
        buckets.forEach(bucket => {
            if(bucket.enabled){
                buckets_enabled.push(bucket.bucket_key);
            }
        })
        await this.props.triggerQBcreation(client_details.id, buckets_enabled.join());

    }

    // trigger Email templates copy in backend for all cohorts
    // -------------------------------------------------------
    triggerEmailTemplateCreation = async () => {
        const { buckets } = this.state;
        const { client_details, selected_cohort } = this.props;
        const buckets_enabled = []
        buckets.forEach(bucket => {
            if(bucket.enabled){
                buckets_enabled.push(bucket.bucket_key);
            }
        })
        await this.props.triggerEmailTemplateCreation(client_details.id, selected_cohort.id, buckets_enabled.join());
    }

    // check tenure enabled
    // ------------------------------
    checkTenureEnabled = async () => {
        const { buckets } = this.state;
        
        let tenure_enabled = false;
        buckets.forEach(bucket => {
            if(bucket.bucket_key === 'tenure' && bucket.enabled === true){
                tenure_enabled = true;
            }
        })
        if(tenure_enabled){
            this.tenureTouchpointMigration();
        }
    }

    // trigger Tenure touchpoint copy
    // ------------------------------
    tenureTouchpointMigration = async () => {
        const { client_details, selected_cohort } = this.props;
        await this.props.tenureTouchpointMigration(client_details.id, selected_cohort.id);
        this.props.refreshBuckets(true);
    }

    // Enable/Dsiable Product
    // ----------------------
    toggleProduct = (index) => {
        const { buckets } = this.state;
        buckets[index].enabled = !buckets[index].enabled
        if(!buckets[index].enabled){
            this.deselectSelectAllSubBuckets(index, false);
        } else {
            this.deselectSelectAllSubBuckets(index, true);
        }
        this.setState({buckets}, () => this.checkEnableSave());
    }

    // check enable save
    // -----------------
    checkEnableSave = () => {
        const { buckets, global_config } = this.state;
        if(JSON.stringify(buckets) === JSON.stringify(this.props.buckets) && JSON.stringify(global_config) === JSON.stringify(this.props.global_config)){
            this.setState({disable_save: true});
        } else {
            this.setState({disable_save: false});
        }
    }

    // select/deselect all sub-buckets/touchpoints
    // -------------------------------------------
    deselectSelectAllSubBuckets = (index, select = true) => {
        const { buckets } = this.state;
        buckets[index].touchpoints.forEach(sub_bucket => {
            sub_bucket.enabled = select;
        })
        this.setState({buckets});
    }

    // check/uncheck sub-buckets/touchpoints
    // -------------------------------------------
    checkUncheckSubBucket = (index, sub_index) => {
        const { buckets } = this.state;
        buckets[index].touchpoints[sub_index].enabled = !buckets[index].touchpoints[sub_index].enabled;
        this.setState({buckets}, () => this.checkEnableSave());
    }

    // on Drag Drop End
    // ----------------
    onSortEnd = ({oldIndex, newIndex}) => {
        const { buckets } = this.state;
        const swap_precedence = buckets[oldIndex].precedence;
        buckets[oldIndex].precedence = buckets[newIndex].precedence;
        buckets[newIndex].precedence = swap_precedence;
        this.setState({
            buckets: arrayMove(buckets, oldIndex, newIndex),
        }, () => this.checkEnableSave());
    };

    // Expand touchpoints
    // ------------------
    expandTouchpoints = (id) => {
        const { expand_touchpoints } = this.state;
        expand_touchpoints[id] = true;
        this.setState({expand_touchpoints});
    }

    // Collapse touchpoints
    // --------------------
    collapseTouchpoints = (id) => {
        const { expand_touchpoints } = this.state;
        expand_touchpoints[id] = false;
        this.setState({expand_touchpoints});
    }

    // Toggle Global Config keys
    // -------------------------
    toggleGlobalConfig = (key) => {
        const{ global_config } = this.state;
        global_config[key] = !global_config[key];
        this.setState({global_config}, () => this.checkEnableSave());
    }

    // Open Save All Modal
    // -------------------
    openSaveAllModal = () => {
        let { modal_text } = this.state;
        modal_text = {
            text: "Are you sure you want to apply for all cohorts?",
            title: "Save for all Cohorts"
        }
        this.setState({modal_text, confirm_active: true, save_for_all: true});
    }

    // Open Save Current Modal
    // -----------------------
    openSaveCurrentModal = () => {
        let { modal_text } = this.state;
        modal_text = {
            text: "Are you sure you want to apply for current cohorts?",
            title: "Save for Current Cohorts"
        }
        this.setState({modal_text, confirm_active: true, save_for_all: false});
    }

    // close Save Modal
    // ----------------
    closeModal = () => {
        this.setState({confirm_active: false, save_for_all: false});
    }


    // Render UI
    // ---------
    render() {
        const { buckets, expand_touchpoints, saving, modal_text, global_config, confirm_active, disable_save } = this.state;
        const { loading } = this.props;

        // Sortable Item HOC
        // -----------------
        const SortableItem = SortableElement(({bucket, row_index}) => 
            <div key={bucket.bucket_key} className="content space-toggle pd-10 precedence-box">
                <span className="precedene-buttons cursor-cross">
                    <i className="fas fa-arrows-alt-v"></i>
                </span>
                <Switch onChange={this.toggleProduct.bind(this, row_index)} checked={bucket.enabled} disabled={bucket.title === 'EXIT' || bucket.title === 'OHA'}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={18} width={35}/>
                <span className="is-size-7 toggle-text">
                    <b> {row_index + 1}. {bucket.title} 
                        {bucket.touchpoints.length 
                            ? <span> ({bucket.touchpoints.length})</span> 
                            : null
                        }
                    </b>
                </span>
                {bucket.touchpoints.length 
                ? <span className="padding-left-5 has-text-grey is-hoverable">
                    {expand_touchpoints[bucket.bucket_key] 
                    ? <i onClick={this.collapseTouchpoints.bind(this, bucket.bucket_key)} className="far fa-minus-square"></i> 
                    : <i onClick={this.expandTouchpoints.bind(this, bucket.bucket_key)} className="far fa-plus-square"></i>}
                </span>
                : null}
                {
                    bucket.touchpoints && bucket.touchpoints.length && expand_touchpoints[bucket.bucket_key]
                    ? bucket.touchpoints.map((sub_bucket, sub_index) => (
                        <div key={sub_index} className="product-checkbox">
                            <label className="checkbox">
                            <input type="checkbox" onChange={this.checkUncheckSubBucket.bind(this, row_index, sub_index)} checked={sub_bucket.enabled}/>
                                {sub_bucket.title}
                            </label>
                        </div>
                    ))
                    : null
                }
            </div>
        );

        // Sortable List HOC
        // -----------------
        const SortableList = SortableContainer(({buckets}) => {
            return (
                <ul>
                {buckets.map((bucket, index) => (
                    <SortableItem key={`item-${bucket.bucket_key}`} index={index} row_index={index} bucket={bucket} />
                ))}
                </ul>
            );
        });

        // Return HTML
        // -----------
        return (
            <div>
            {loading 
                ? <p className="has-text-centered padding-40">loading...</p> 
                : 
                <div>
                    <div className="pd-10">
                        <h2 className="is-size-6">Select Product and Set Precedence</h2>
                        <p className="help">Drag and drop products to set precedence with product at top with highest precedence</p>
                    </div>
                    <article className="message">
                        <div className="message-body modal-sub-header-text">
                            <ol>
                                <li>Set Product precedence to send reachouts from different products and <b>resolve conflicts based on priority</b></li>
                                <li><b>Note:</b> Changes in product precedence will only resolve overlaps occurring due to future data uploads.</li>
                            </ol>
                        </div>
                    </article>
                    <SortableList buckets={buckets} onSortEnd={this.onSortEnd} distance={1}/>
                    {/* <div className="pd-10">
                        <h2 className="is-size-6">Global Config</h2>
                        <p className="help mg-bottom-10">These configurations are applicable to all the products selected above</p>
                    </div> */}
                    {/* <GlobalConfig
                        welcome_email={global_config.welcome_email}
                        celebration_email={global_config.celebration_email}
                        dangerous_words_alert={global_config.dangerous_words_alert}
                        email_spoofing={global_config.email_spoofing}
                        anonymous_bat={global_config.anonymous_bat}
                        trust_statement={global_config.trust_statement}
                        onChange={this.toggleGlobalConfig}
                    /> */}
                    {/* <p className="has-text-centered padding-40">Coming Soon</p> */}
                    <br/>
                    <SaveChanges 
                        onSaveCurrent={this.openSaveCurrentModal} 
                        onSaveForAll={this.openSaveAllModal} 
                        disable_save={disable_save}
                    />
                </div>
            }
            <ConfirmModal
                text={modal_text.text}
                title={modal_text.title}
                confirm_text={"Yes"}
                doing={saving}
                is_active={confirm_active}
                onConfirm={this.patchBuckets}
                onClose={this.closeModal}
            />
            </div>
        )
    }
}

// PropTypes
// ---------
SelectProducts.propTypes = {
    loading: PropTypes.bool.isRequired,
    buckets: PropTypes.array.isRequired,
    global_config: PropTypes.object.isRequired,
    selected_cohort: PropTypes.object.isRequired,
    patchBuckets: PropTypes.func.isRequired,
    refreshBuckets: PropTypes.func.isRequired,
    unique_id: PropTypes.string.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    client_details: state.client.client_details, 
})

export default connect(mapStateToProps, { patchBuckets, triggerQBcreation, triggerEmailTemplateCreation, tenureTouchpointMigration })(SelectProducts);