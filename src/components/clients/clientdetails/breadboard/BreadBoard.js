import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// actions
import { getCohorts } from '../../../../actions/ClientsActions';
// components
import DropDown from '../../../generic/Dropdown';
import Cohorts from '../../clientdetails/Cohorts';
import Users from '../../clientdetails/users/Users';
import MomentSettings from './individualsettings/MomentSettingsMTM';
import EmailTemplates from './emailtemplates/EmailTemplates';
import QuestionBank from './questionbank/QuestionBank';
import DataSync from './datasync/DataSync';
import BreadBoardToggle from './BreadBoardToggle';
import PeripheralMessage from './peripheralmessages/PeripheralMessage';
import Monitoring from './monitor/Monitoring';
// hoc
import ComponentCard from '../../../generic/ComponentCard';
// cards
const MonitoringCard = ComponentCard(Monitoring, 'Monitor Reach-outs', 1, 'is-primary');
const CohortsCard = ComponentCard(Cohorts, 'Cohorts', 2, 'is-primary');
const UsersCard = ComponentCard(Users, 'Users', 3, 'is-primary');
const QuestionBankCard = ComponentCard(QuestionBank, 'MTM Question Bank Configure', 1);
const MomentSettingsCard = ComponentCard(MomentSettings, 'MTM Settings', 2);
const EmailConfigureCard = ComponentCard(EmailTemplates, 'MTM Email Templates', 3);
const PeripheralMessageCard = ComponentCard(PeripheralMessage, 'MTM Peripheral Messages', 4);
const IntegrationCard = ComponentCard(DataSync, 'MTM Data Sync', 5);

class BreadBoard extends Component {

    // initial State
    // -------------
    state = {
        buckets: [],
        selected_bucket: 
            {
                bucket_key: "mtm",
                common_config: {},
                created: "",
                enabled: true,
                modified: "",
                precedence: 1,
                title: "MTM",
                touch_points: [
                    {
                        title: "Manager Change",
                        touch_point_key: "MANAGER_CHANGE",

                    },
                    {
                        title: "Role Change",
                        touch_point_key: "ROLE_CHANGE",

                    },
                    {
                        title: "Project Change",
                        touch_point_key: "PROJECT_CHANGE",

                    },
                    {
                        title: "Location Change",
                        touch_point_key: "LOCATION_CHANGE",

                    },
                    {
                        title: "Appraisal",
                        touch_point_key: "APPRAISAL",

                    }
                ]
            },
        global_config: {
            welcome_email: false,
            celebration_email: false,
            dangerous_words_alert: false,
            email_spoofing: false,
            anonymous_bat: false,
            trust_statement: false
        },
        selected_cohort: {},
        loading_cohorts: true
    }

    // component mounts
    // ----------------
    componentDidMount(){
        const { client_details } = this.props;
        if(client_details && client_details.id){
            this.getCohorts();
        }
    }

    // component updates
    // -----------------
    componentDidUpdate(prevProps){
        const { client_details } = this.props;
        if(prevProps.client_details.id !== client_details.id){
            this.getCohorts();
        }
    }

    // GET cohorts
    // -----------
    getCohorts = async () => {
        const { client_details } = this.props;
        const response = await this.props.getCohorts(client_details.id);
        this.setState({loading_cohorts: true});
        if(response.length){
            const selected_cohort = (response.length ? response[0] : {});
            this.setState({selected_cohort}, () => this.props.getClientDetails());
        } 
        this.setState({loading_cohorts: false});
    }


    // On Cohort Change
    // ----------------
    cohortChange = (selected_cohort) => {
        this.setState({
            selected_cohort
        }, ()=> {
            this.props.getClientDetails();
        });
    }

    // Render UI
    // ---------
    render() {
        const { selected_cohort, buckets, selected_bucket, loading_cohorts } = this.state;
        const { cohorts, client_details } = this.props;
        return (
            <div>
                <BreadBoardToggle/>
                <MonitoringCard/>
                <CohortsCard/>
                <UsersCard/>
                <br/>
                {/* 1. Select Cohort */}
                <div>
                    <span className="v-align-sub has-text-weight-bold">Select Cohort:&nbsp;&nbsp; </span>
                    <DropDown 
                        dropdown_items={cohorts} 
                        selected_item={selected_cohort} 
                        onClick={this.cohortChange}
                        identity={'title'}
                        loading={loading_cohorts}
                    />
                </div>
                <br/>
                {/* 2. cohorts */}
                    <div>
                        <QuestionBankCard
                            selected_bucket={selected_bucket}
                            selected_cohort={selected_cohort}
                            disabled={client_details && client_details.activate_mtm ? false : true}
                        />
                        <MomentSettingsCard
                            selected_bucket={selected_bucket}
                            selected_cohort={selected_cohort}
                            disabled={client_details && client_details.activate_mtm ? false : true}
                            client_details={client_details} 
                        />
                        <EmailConfigureCard
                            selected_bucket={selected_bucket}
                            selected_cohort={selected_cohort}
                            disabled={client_details && client_details.activate_mtm ? false : true}
                        />
                        <PeripheralMessageCard
                            disabled={client_details && client_details.activate_mtm ? false : true}
                            selected_cohort={selected_cohort}
                            selected_bucket={selected_bucket}
                        />
                        <IntegrationCard
                            disabled={client_details && client_details.activate_mtm ? false : true}
                            selected_cohort={selected_cohort}
                            selected_bucket={selected_bucket}
                        />
                    </div>
            </div>
        )
    }
}

// PropTypes
// ---------
BreadBoard.propTypes = {
    client_details: PropTypes.object.isRequired,
    getCohorts: PropTypes.func.isRequired,
    getClientDetails: PropTypes.func.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    client_details: state.client.client_details,
    cohorts: state.cohort.cohorts
})

export default connect(mapStateToProps, { getCohorts })(BreadBoard);