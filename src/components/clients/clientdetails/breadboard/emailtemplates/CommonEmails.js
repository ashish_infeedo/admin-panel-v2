import React, { Component } from 'react';
// components
import CopyVariables from '../../../../generic/CopyVariable';
import EmailEditor from './ReusableEditor';

export default class CommonEmails extends Component {
    
    state = {
        
    }

    render() {
        const { variables } = this.props;
        return (
            <div>
                {
                    true ? <p className="has-text-centered padding-40 has-text-grey">Not Available in Breadboard Yet</p>
                    :
                    <div>
                        <div className="tabs is-toggle is-centered">
                            <ul>
                                <li className="is-active">
                                    <a>
                                        <span className="icon is-small"><i className="fas fa-paper-plane" aria-hidden="true"></i></span>
                                        <span>Welcome Email</span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span className="icon is-small"><i className="fas fa-birthday-cake" aria-hidden="true"></i></span>
                                        <span>Celebration Email</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div className="columns">
                            <CopyVariables variables={variables}/>
                            <div className="column">
                                <div className="field">
                                    <label className="label">Subject</label>
                                    <div className="control">
                                        <input className="input" type="text" placeholder="e.g Alex Smith"/>
                                    </div>
                                </div>
                                <div>
                                    <label className="label">Email Content</label>
                                    <EmailEditor
                                        onSave={() => {}}
                                        content={''}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </div>
        )
    }
}
