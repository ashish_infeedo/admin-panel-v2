import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { connect } from 'react-redux';
// components
import ProductWiseEmails from './ProductWiseEmails';
import CommonEmails from './CommonEmails';

class EmailTemplates extends Component {
    
    state = {
        variables: [
            {
                id: 'firstname',
                value: '{{{firstname}}}',
                description: 'First Name of Employee'
            },
            {
                id: 'companyname',
                value: '{{{company_name}}}',
                description: 'Company Name'
            },
            {
                id: 'touchpoint',
                value: '{{{period}}}',
                description: 'Touchpoint Value (e.g. 1 year, Manager Change)'
            },
            {
                id: 'link',
                value: '{{{link}}}',
                description: 'Link to Chat'
            },
            {
                id: 'primary_admin_full_name',
                value: '{{{primary_admin_full_name}}}',
                description: 'Name of Email Sender'
            },
            {
                id: 'designation',
                value: '{{{designation}}}',
                description: 'Designation of Email Sender'
            }
        ]
    }

    render() {
        const { variables } = this.state;
        const { selected_bucket, selected_cohort } = this.props;
        return (
            <div>
                <div>
                    <div className="pd-10">
                        <h2 className="is-size-6">Email Templates</h2>
                        <p className="help">Select and configure email templates for common and product wise emails</p>
                    </div>
                    <ProductWiseEmails
                        selected_cohort={selected_cohort}
                        selected_bucket={selected_bucket}
                        variables={variables}
                    />
                </div>
            </div>
        )
    }
}

// PropTypes
// ---------
EmailTemplates.propTypes = {
    selected_bucket: PropTypes.object.isRequired,
    selected_cohort: PropTypes.object.isRequired,
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    client_details: state.client.client_details
})

export default connect(mapStateToProps, {})(EmailTemplates);