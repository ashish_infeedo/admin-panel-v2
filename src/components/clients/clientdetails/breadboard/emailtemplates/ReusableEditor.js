import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import classnames from 'classnames';

export default class ReusableEditor extends Component {

    state = {
        editorState: EditorState.createEmpty(),
        ContentState: {},
        editor_content: '',
        disable_save: false
    }

    componentDidMount() {
        this.setEditorContent();
    }

    componentDidUpdate(prevProps){
        const { content } = this.props;
        if(content !== prevProps.content){
            this.setEditorContent();
        }
    }

    setEditorContent = () => {
        const { content } = this.props;
        const contentBlock = htmlToDraft(content);
        if (contentBlock) {
            const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
            const editorState = EditorState.createWithContent(contentState);
            this.setState({
                editorState, 
                contentState,
                editor_content: draftToHtml(convertToRaw(editorState.getCurrentContent()))
            });
        }
    }


    // On editor state change
    // ----------------------
    onEditorStateChange = (editorState) => {
        this.setState({
            editorState,
            editor_content: draftToHtml(convertToRaw(editorState.getCurrentContent()))
        });
    }

    validateContent = () => {
        const { editor_content, editorState } = this.state;
        if(editor_content === '' || editorState.getCurrentContent().getPlainText().trim().length === 0){
            return false;
        } 
        return true;
    }

    onSave = () => {
        const { editor_content } = this.state;
        const valid = this.validateContent();
        this.props.onSave(editor_content, valid);
    }

    render() {
        const { editorState, contentState, disable_save } = this.state;
        const { loading } = this.props;
        return (
            <div>
                 <div className="is-light">
                    <Editor
                        initialContentState={contentState}
                        editorState={editorState}
                        wrapperClassName="demo-wrapper"
                        editorClassName="demo-editor"
                        onEditorStateChange={this.onEditorStateChange}
                    />
                </div>
                <br/>
                <button onClick={this.onSave} type="submit" className={classnames('button is-primary is-pulled-right', {'is-loading': loading})}  disabled={disable_save}>
                    <span className="icon">
                    <i className="far fa-save"></i>
                    </span>
                    <span>Save</span>
                </button>
            </div>
        )
    }
}

// PropTypes
// ---------
ReusableEditor.propTypes = {
    content: PropTypes.string.isRequired,
    onSave: PropTypes.func.isRequired,
    loading: PropTypes.bool
}

ReusableEditor.defaultProps = {
    loading: false
};