import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import EmailEditor from './ReusableEditor';
import { connect } from 'react-redux';
import DropDown from '../../../../generic/Dropdown';
import ConfirmModal from '../../../../generic/ConfirmModal';
import { getBucketEmails, patchEmail } from '../../../../../actions/BreabBoardActions';
// components
import CopyVariables from '../../../../generic/CopyVariable'

class ProductWiseEmails extends Component {
    
    state = {
        selected_bucket: {},
        selected_email_group: [],
        emails: {},
        selected_email: {},
        saving_email: false,
        tab_active: 1,
        loading_emails: false,
        confirm_active: false,
        updated_content: '',
        modal_text: {
            text: "Are you sure you want to save email template?",
            title: "Save for Current Cohort"
        }
    }

    componentDidMount(){
        const { selected_bucket } = this.props;
        this.setState({
            touch_points: selected_bucket.touch_points,
            selected_touch_point: selected_bucket.touch_points[0],
            selected_bucket
        }, () => {
            this.getBucketEmails();
        })
    }

    getBucketEmails = async () => {
        const { selected_cohort, client_details } = this.props;
        const { selected_bucket } = this.state;
        this.setState({loading_emails: true});
        const response = await this.props.getBucketEmails(client_details.id, selected_cohort.id,  (selected_bucket.bucket_key ? selected_bucket.bucket_key.toLowerCase() : null));
        if(response && response.status === 200){
            this.setState({emails: response.data.data}, () => this.selectInitalEmailAndGroup());
        }
        this.setState({loading_emails: false});
    }

    selectInitalEmailAndGroup = () => {
        const { emails, tab_active } = this.state;
        if(tab_active === 1){
            this.setState({
                selected_email_group: emails.reachout, 
                selected_email: (emails.reachout && emails.reachout.length ? emails.reachout[0] : {})
            });
        } else if(tab_active === 2){
            this.setState({
                selected_email_group: emails.reminder, 
                selected_email: (emails.reminder && emails.reminder.length ? emails.reminder[0] : {})
            });
        } else if(tab_active === 3){
            this.setState({
                selected_email_group: emails.notification, 
                selected_email: (emails.notification && emails.notification.length ? emails.notification[0] : {})
            });
        }
    } 

    selectTab = (selected_bucket) => {
        this.setState({selected_bucket}, () => this.getBucketEmails());
    }

    onSubjectChange = (e) => {
        const { selected_email } = this.state;
        selected_email[e.target.name] = e.target.value;
        this.setState({selected_email});
    }

    onSave = async () => {
        const { selected_cohort, client_details } = this.props;
        const { selected_bucket, updated_content, selected_email} = this.state;
        selected_email.content = updated_content;
        this.setState({saving_email: true});
        const response = await this.props.patchEmail(client_details.id, selected_cohort.id, (selected_bucket.bucket_key ? selected_bucket.bucket_key.toLowerCase() : null), selected_email);
        if(response && response.status === 200){
            this.closeConfirmModal();
        }
        this.setState({saving_email: false, selected_email});
    }

    openConfirmModal = (updated_content) => {
        this.setState({confirm_active: true, updated_content});
    }

    closeConfirmModal = () => {
        this.setState({confirm_active: false});
    }

    switchTab = (tab_active) => {
        this.setState({tab_active}, () => this.selectInitalEmailAndGroup());
    }

    changeTouchpoint = (selected_email) => {
        this.setState({selected_email});
    }

    render() {
        const { selected_email, saving_email, tab_active, selected_email_group, loading_emails, modal_text, confirm_active} = this.state;
        const { variables } = this.props;
        return (
            <div>

                {loading_emails ? 
                    <p className="has-text-centered padding-40">loading...</p> 
                :<div>
                    <div className="columns">
                        <div className="column">
                            <div className="tabs">
                                <ul className="no-margin-left-right">
                                    <li className={classnames('', {'is-active': tab_active === 1})} ><a onClick={this.switchTab.bind(this, 1)}>Reach-outs</a></li>
                                    <li className={classnames('', {'is-active': tab_active === 2})} onClick={this.switchTab.bind(this, 2)}><a>Reminders</a></li>
                                    <li className={classnames('', {'is-active': tab_active === 3})} onClick={this.switchTab.bind(this, 3)}><a>Notification</a></li>
                                </ul>
                            </div>
                            {
                                selected_email_group && selected_email_group.length ? 
                                <div>
                                    <div className="has-text-left">
                                        <label className="label">
                                            {tab_active === 1 ? 'Select Reachout Email' : null}
                                            {tab_active === 2 ? 'Select Reminder Email' : null}
                                            {tab_active === 3 ? 'Select Notification Email' : null}
                                        </label> 
                                        {selected_email_group ? 
                                        <DropDown 
                                            dropdown_items={selected_email_group} 
                                            selected_item={selected_email} 
                                            onClick={this.changeTouchpoint}
                                            identity={'touchpoint_key'}
                                        />
                                        : null}
                                    </div>
                                    <br/>
                                    <div className="field">
                                        <label className="label">Subject</label>
                                        <div className="control">
                                            <input className="input" onChange={this.onSubjectChange} type="text" name="subject" value={(selected_email.subject ? selected_email.subject : '')}/>
                                        </div>
                                    </div>
                                    <div>
                                        <label className="label">Email Content</label>
                                        <EmailEditor
                                            onSave={this.openConfirmModal}
                                            content={(selected_email.content ? selected_email.content : '')}
                                        />
                                    </div>
                                </div>
                                : <p className="has-text-centered padding-40">No Data Avialable</p> 
                            }
                        </div>
                        <CopyVariables variables={variables}/>
                    </div>
                </div>}
                <ConfirmModal
                    text={modal_text.text}
                    title={modal_text.title}
                    confirm_text={"Yes"}
                    doing={saving_email}
                    is_active={confirm_active}
                    onConfirm={this.onSave}
                    onClose={this.closeConfirmModal}
                />
            </div>
        )
    }
}

// PropTypes
// ---------
ProductWiseEmails.propTypes = {
    selected_bucket: PropTypes.object.isRequired,
    selected_cohort: PropTypes.object.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    client_details: state.client.client_details
})

export default connect(mapStateToProps, { getBucketEmails, patchEmail })(ProductWiseEmails);