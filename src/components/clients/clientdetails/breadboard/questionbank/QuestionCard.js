import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';


const SaveChanges = ({
    question,
    onEdit,
    onDelete,
    increment_sentiment
}) => {
  return (
    <div className="card question-card">
        <div className="card-content border-grey">
            <div className="mg-bottom-10">
                <span className="tag is-light is-capitalized">{question.type}</span>
                <span className="buttons is-pulled-right">
                    {question.options && question.options.length && question.options[0].type === "image" ? null : <button onClick={onEdit.bind(this, question)} className="button is-info is-outlined is-small">Edit</button>}
                    {question.options && question.options.length && question.options[0].type === "image" ? null : <button onClick={onDelete.bind(this, question)} className="button is-danger is-outlined is-small">Delete</button>}
                </span>
            </div>
            <h5>{question.question_index ? <span>{question.question_index}. </span> : null}{question.content}</h5>
            {question.options && question.options.length ? 
                <div>
                    <p className="is-size-7 has-text-weight-bold mg-top-bottom-25 has-text-grey">OPTIONS:</p>
                    <div className="options">
                        {question.options.map((option, index) => (
                            <React.Fragment key={index}>
                            { option.type === 'image' 
                                ? <img width="60px" src={`/assets/images/${option.content}`} alt="icon" className="mg-right-10 mg-bottom-10"/>
                                : <p>
                                    <span className="sentiment-wrapper">
                                        <span className={classnames('sentiment-box s-red', {'sentiment-box-active': parseInt(option.sentiment_score) === (-2 + increment_sentiment)})}>1</span>
                                        <span className={classnames('sentiment-box s-red-light', {'sentiment-box-active': parseInt(option.sentiment_score) === (-1 + increment_sentiment)})}>2</span>
                                        <span className={classnames('sentiment-box s-yellow', {'sentiment-box-active': parseInt(option.sentiment_score) === (0 + increment_sentiment)})}>3</span>
                                        <span className={classnames('sentiment-box s-green-light', {'sentiment-box-active': parseInt(option.sentiment_score) === (1 + increment_sentiment)})}>4</span>
                                        <span className={classnames('sentiment-box s-green', {'sentiment-box-active': parseInt(option.sentiment_score) === (2 + increment_sentiment)})}>5</span>
                                    </span>
                                    {option.content} 
                                </p>
                            }
                            </React.Fragment>    
                            )
                        )}
                    </div>
                </div> 
            : null }
            {question.question_index === 1 ? null 
            :   <div className="columns">
                    <div className="column">
                        <p className="is-size-7 has-text-weight-bold mg-top-bottom-25 has-text-grey">DRIVER:</p>
                        <span className="tag is-link is-light is-medium">{(question.employee_driver ? question.employee_driver : 'Not Available')}</span>
                    </div>
                    <div className="column">
                        <p className="is-size-7 has-text-weight-bold mg-top-bottom-25 has-text-grey">ELEMENT:</p>
                        <span className="tag is-link is-light is-medium">{(question.engagement_element ? question.engagement_element : 'Not Available')}</span>
                    </div>
                </div>
            }
        </div>
    </div>
  );
};

SaveChanges.propTypes = {
    question: PropTypes.object.isRequired,
    onEdit: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    increment_sentiment: PropTypes.number
};

SaveChanges.defaultProps = {
    increment_sentiment: 0
};

export default SaveChanges;