import React, { Component } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NotificationManager } from 'react-notifications';
// components
import DropDown from '../../../../generic/Dropdown';
// actions 
import { getDrivers, getElements } from '../../../../../actions/CrisisActions';
import { patchQuestion } from '../../../../../actions/BreabBoardActions';

class EditQuestionModal extends Component {
    
    // initial state
    // -------------
    state = {
        question: {},
        question_copy: {},
        selected_driver: {},
        drivers: [],
        elements: [],
        selected_element: {},
        loading_drivers: false,
        loading_elements: false,
        patching: false,
        disable_patch: true
    }

    // Component Mounts
    // ----------------
    componentDidMount(){
        const { question } = this.props;
        this.getDrivers();
        this.setState({
            question: JSON.parse(JSON.stringify(question)),
            question_copy: JSON.parse(JSON.stringify(question))
        });
    }

    // component updates
    // -----------------
    componentDidUpdate(prevProps){
        
    }

        // get Drivers
    // -----------
    getDrivers = async () => {
        const { client_id } = this.props;
        this.setState({loading_drivers: true});
        const response = await this.props.getDrivers(client_id);
        if(response && response.status === 200){
            const drivers = response.data;
            this.setState({
                drivers
            }, () => {
                this.getSelectedDriver();
            });
        }
        this.setState({loading_drivers: false});
    }

    // get selected driver
    // -------------------
    getSelectedDriver = () => {
        const { drivers, question } = this.state;
        let selected_driver = {};
        drivers.forEach(driver => {
            if(driver.name === question.employee_driver){
                selected_driver = driver;
            }
        })
        this.setState({selected_driver}, () => this.getElements());
    }

    // get elements
    // ------------
    getElements = async () => {
        const { client_id } = this.props;
        const { selected_driver } = this.state;
        if(!selected_driver.id){
            return;
        }
        this.setState({loading_elements: true});
        const response = await this.props.getElements(client_id);
        if(response && response.status === 200){
            const elements = response.data
            this.setState({elements: (elements.length ? elements : [])}, () => this.getSelectedElement());
        }
        this.setState({loading_elements: false});
    }

    // get selecetd element
    // --------------------
    getSelectedElement = () => {
        const { elements, question } = this.state;
        let selected_element = {};
        elements.forEach(element => {
            if(element.name === question.engagement_element){
                selected_element = element;
            }
        })
        this.setState({selected_element});
    }

    // Patch Question
    // --------------
    patchQuestion = async (e) => {
        e.preventDefault();
        const { question, selected_element } = this.state;
        const { client_id, survey_id, selected_cohort } = this.props;
        if(Object.keys(selected_element).length === 0 && selected_element.constructor === Object){
            NotificationManager.info("Please Select Element");
            return;
        }
        this.setState({patching: true});
        const response = await this.props.patchQuestion(client_id, survey_id, selected_cohort.id, question);
        if(response && response.status === 200){
            this.props.refreshQuestions();
            this.props.onClose();
        } else {

        }
        this.setState({patching: false});
    }

    // Change Sentiment
    // ----------------
    changeSentiment = (index, value) => {
        const { question } = this.state;
        question.options[index].sentiment_score = value;
        this.setState({question}, () => this.checkEnablePatch());
    }

    // On Question content Change
    // --------------------------
    onChangeQuestion = (e) => {
        const { question } = this.state;
        question.content = e.target.value;
        this.setState({question}, () => this.checkEnablePatch());
    }

    // On option content Change
    // --------------------------
    onChangeOption = (e, index) => {
        const { question } = this.state;
        question.options[index].content = e.target.value;
        this.setState({question}, () => this.checkEnablePatch());
    }

    // On Driver Change
    // ----------------
    onDriverChange = (selected_driver) => {
        const { question, question_copy } = this.state;
        question.employee_driver = selected_driver.name;
        if(question.employee_driver !== question_copy.employee_driver){
            question.isdriverEdited = 1;
        } else {
            delete question.isdriverEdited;
        }
        this.setState({question, selected_driver}, () => {
            this.checkEnablePatch();
            this.getElements();
        });
    }

    // On Element Change
    // -----------------
    onElementChange = (selected_element) => {
        const { question, question_copy } = this.state;
        question.engagement_element = selected_element.name;
        if(question.employee_driver !== question_copy.employee_driver){
            question.isElementEdited = 1;
        } else {
            delete question.isElementEdited;
        }
        this.setState({question, selected_element}, () => this.checkEnablePatch());
    }

    // check enable
    // ------------
    checkEnablePatch = () => {
        const { question, question_copy } = this.state;
        if(JSON.stringify(question) === JSON.stringify(question_copy)){
            this.setState({disable_patch: true});
        } else {
            this.setState({disable_patch: false});
        }
    }

    // Render UI
    // ---------
    render() {
        const { question, drivers, selected_driver, elements, selected_element, patching, disable_patch } = this.state;
        const { modal_active, onClose } = this.props;
        return (
            <div className={classnames('modal', {'is-active': modal_active})}>
                <div className="modal-background"></div>
                <div className="modal-card radius-access">
                    <header className="modal-card-head">
                        <p className="modal-card-title is-marginless">Edit Question</p>
                        <button onClick={onClose} className="delete" aria-label="close"></button>
                    </header>
                    <section className="modal-card-body">
                        <form onSubmit={this.patchQuestion}>
                            <div className="field">
                                <label className="label">Question</label>
                                <div className="control">
                                    <input onChange={this.onChangeQuestion} className="input" type="text" name="content" value={(question.content ? question.content : '')} required/>
                                </div>
                            </div>
                            {question.type === 'radio' ?
                            <div>
                                {
                                    question.options.map((option, index) => (
                                        <div key={index} className="control">
                                            <label className="label">Option {index +  1}</label>
                                            <input onChange={(e) => this.onChangeOption(e, index)} className="input" type="text" name="content" value={(option.content ? option.content : '')} required/>
                                            {/* <div className="sentiment-wrapper mg-senti">
                                                <span onClick={this.changeSentiment.bind(this, index, -2)} className={classnames('sentiment-box is-hoverable s-red', {'sentiment-box-active': parseInt(option.sentiment_score) === -2})}>1</span>
                                                <span onClick={this.changeSentiment.bind(this, index, -1)} className={classnames('sentiment-box is-hoverable s-red-light', {'sentiment-box-active': parseInt(option.sentiment_score) === -1})}>2</span>
                                                <span onClick={this.changeSentiment.bind(this, index, 0)} className={classnames('sentiment-box is-hoverable s-yellow', {'sentiment-box-active': parseInt(option.sentiment_score) === 0})}>3</span>
                                                <span onClick={this.changeSentiment.bind(this, index, 1)} className={classnames('sentiment-box is-hoverable s-green-light', {'sentiment-box-active': parseInt(option.sentiment_score) === 1})}>4</span>
                                                <span onClick={this.changeSentiment.bind(this, index, 2)} className={classnames('sentiment-box is-hoverable s-green', {'sentiment-box-active': parseInt(option.sentiment_score) === 2})}>5</span>
                                            </div> */}
                                            <div className="sentiment-wrapper mg-senti">
                                                <span onClick={this.changeSentiment.bind(this, index, 1)} className={classnames('sentiment-box is-hoverable s-red', {'sentiment-box-active': parseInt(option.sentiment_score) === 1})}>1</span>
                                                <span onClick={this.changeSentiment.bind(this, index, 2)} className={classnames('sentiment-box is-hoverable s-red-light', {'sentiment-box-active': parseInt(option.sentiment_score) === 2})}>2</span>
                                                <span onClick={this.changeSentiment.bind(this, index, 3)} className={classnames('sentiment-box is-hoverable s-yellow', {'sentiment-box-active': parseInt(option.sentiment_score) === 3})}>3</span>
                                                <span onClick={this.changeSentiment.bind(this, index, 4)} className={classnames('sentiment-box is-hoverable s-green-light', {'sentiment-box-active': parseInt(option.sentiment_score) === 4})}>4</span>
                                                <span onClick={this.changeSentiment.bind(this, index, 5)} className={classnames('sentiment-box is-hoverable s-green', {'sentiment-box-active': parseInt(option.sentiment_score) === 5})}>5</span>
                                            </div>
                                        </div>
                                    ))
                                }
                            </div> : null}
                            <article className="message is-danger">
                                <div className="message-body is-size-7">
                                    <strong>Note:</strong> Don't Change Driver Element unless super important as it will cause the question to be archived and a new question will be created for this client only which will lead to loss in mapping from the combined question bank.
                                </div>
                            </article>
                            <div className="columns">
                                <div className="field column">
                                    <label className="label">Driver</label>
                                    <DropDown 
                                        dropdown_items={drivers} 
                                        selected_item={selected_driver} 
                                        onClick={this.onDriverChange}
                                        identity={'name'}
                                        dropdown_classes={'is-up'}
                                    />
                                </div>
                                <div className="field column">
                                    <label className="label">Element</label>
                                    <DropDown 
                                        dropdown_items={elements} 
                                        selected_item={selected_element} 
                                        onClick={this.onElementChange}
                                        identity={'name'}
                                        dropdown_classes={'is-up'}
                                    />
                                </div>
                            </div>
                            <br/>
                            <button type="submit" className={classnames('button is-success', {'is-loading': patching})} disabled={disable_patch}>Save changes</button>
                        </form>
                    </section>
                </div>
            </div>
        )
    }
}

EditQuestionModal.propTypes = {
    modal_active: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    question: PropTypes.object.isRequired,
    client_id: PropTypes.number.isRequired,
    refreshQuestions: PropTypes.func.isRequired,
    survey_id: PropTypes.number.isRequired,
    selected_cohort: PropTypes.object.isRequired
};


export default connect(null, { getDrivers, getElements, patchQuestion })(EditQuestionModal);