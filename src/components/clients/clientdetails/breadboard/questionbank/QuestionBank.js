import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { connect } from 'react-redux';
// components
import DropDown from '../../../../generic/Dropdown';
import QuestionCard from './QuestionCard';
import EditQuestionModal from './EditQuestionModal';
import ConfirmModal from '../../../../generic/ConfirmModal';
// actions
import { getQuestionBank, deleteQuestion, getSampleQuestionBankFile, uploadQbFileMtm } from '../../../../../actions/BreabBoardActions';

class QuestionBank extends Component {

    state = {
        selected_bucket: {},
        selected_touch_point: {},
        selected_question: {},
        touch_points: [],
        loading_questions: false,
        questions: [],
        open_edit_modal: false,
        modal_text: {
            title: 'Delete Question',
            text: 'Are you sure you want to delete question?'
        }, 
        deleting: false,
        delete_modal_active: false,
        survey_id: null,
        downloading: false,
        uploading: false
    }

    componentDidMount(){
        const { selected_bucket } = this.props;
        this.setState({
            touch_points: selected_bucket.touch_points,
            selected_touch_point: selected_bucket.touch_points[0],
            selected_bucket
        }, () => {
            this.getQuestionBank();
        })
    }

    getQuestionBank = async () => {
        const { selected_cohort, client_details } = this.props;
        const { selected_touch_point, selected_bucket } = this.state;
        if(selected_bucket.bucket_key !== 'mtm'){
            this.setState({
                questions: [],
                survey_id: null
            });
        } else {
            this.setState({loading_questions: true});
            const response = await this.props.getQuestionBank(client_details.id, selected_cohort.id, selected_touch_point.touch_point_key);
            if(response && response.status === 200){
                this.setState({
                    questions: (response.data && response.data.data.question_query_data ? response.data.data.question_query_data : []),
                    survey_id: (response.data.data.survey_id ? response.data.data.survey_id : null)
                });
            } else {
                this.setState({questions: []});
            }
            this.setState({loading_questions: false});
        }
    }

    changeTouchpoint = (selected_touch_point) => {
        this.setState({selected_touch_point}, () => this.getQuestionBank());
    }

    onEditQuestion = (selected_question) => {
        this.setState({selected_question}, () => this.openEditModal());
    }

    openEditModal = () => {
        this.setState({open_edit_modal: true});
    }

    closeEditModal = () => {
        this.setState({open_edit_modal: false, selected_question: {}});
    }

    openDeleteModal = (selected_question) => {
        this.setState({delete_modal_active: true, selected_question});
    }

    closeDeleteModal = () => {
        this.setState({delete_modal_active: false, selected_question: {}});
    }

    deleteQuestion = async () => {
        const { selected_question, survey_id } = this.state;
        const { client_details, selected_cohort } = this.props;
        this.setState({deleting: true});
        const response = await this.props.deleteQuestion(client_details.id, selected_question.id, selected_cohort.id ,survey_id);
        if(response && response.status === 200){
            this.closeDeleteModal();
            this.getQuestionBank();
        }
        this.setState({deleting: false});
    }

    uploadQBFile = async (e) => {
        const { selected_cohort, client_details } = this.props;
        const upload_files = Array.from(e.target.files);
        const formData = new FormData();
        formData.append('csv', upload_files[0]);
        this.setState({uploading: true});
        const response = await this.props.uploadQbFileMtm(client_details.id, selected_cohort.id , formData);
        if(response && response.status === 200){
            this.getQuestionBank();
        } else {
            this.getQuestionBank();
        }
        this.setState({uploading: false});
    }

    getSampleUploadFile = async () => {
        this.setState({downloading: true});
        await this.props.getSampleQuestionBankFile();
        this.setState({downloading: false});
    }


    render() {
        const { selected_touch_point, loading_questions, touch_points, questions, open_edit_modal, selected_question, modal_text, deleting, delete_modal_active, downloading, uploading, survey_id } = this.state;
        const { client_details, selected_cohort } = this.props;
        return (
            <div>
                <div>
                    <div className="pd-10">
                        <h2 className="is-size-6">Upload Question Bank</h2>
                        <p className="help">Upload Question bank for selected cohort</p>
                    </div>
                    <div className="columns">
                        <div className="mg-10 has-text-centered column">
                            <button onClick={this.getSampleUploadFile} type="submit" className={classnames('button is-info', {'is-loading': downloading})}>
                                <span className="icon">
                                    <i className="fas fa-file-download"></i>
                                </span>
                                <span>Download Sample File</span>
                            </button>
                        </div>
                        <div className="mg-10 has-text-centered column">
                            <button type="submit" onClick={() => {document.getElementById('uploadQBFileMTM').click();}} className={classnames('button is-success', {'is-loading': uploading})}>
                                <span className="icon">
                                    <i className="fas fa-file-upload"></i>
                                </span>
                                <span>Upload QB Data File</span>
                            </button>
                            {uploading ? null : <input accept=".csv" type="file" id='uploadQBFileMTM' onChange={this.uploadQBFile} hidden/>}
                        </div>
                    </div>
                    <hr/>
                    <div className="pd-10">
                        <h2 className="is-size-6">Update Uploaded QB</h2>
                        <p className="help">Edit, Update, Delete Uploaded questions</p>
                    </div>
                    <br/>
                        <div>
                            {
                                touch_points && touch_points.length ?
                                <div className="mg-left-2">
                                    <span className="v-align-sub has-text-weight-bold is-size-6">Select Touchpoint:&nbsp;&nbsp; </span> 
                                    <DropDown 
                                        dropdown_items={touch_points} 
                                        selected_item={selected_touch_point} 
                                        onClick={this.changeTouchpoint}
                                        identity={'title'}
                                    />
                                    <br/><br/>
                                </div>
                                : null
                            }
                            {
                                loading_questions 
                                ? <p className="has-text-centered padding-40">loading questions...</p> 
                                : 
                                (
                                    questions.length ? questions.map(question => (
                                        <QuestionCard
                                            key={question.id}
                                            question={question}
                                            onDelete={this.openDeleteModal}
                                            onEdit={this.onEditQuestion}
                                            increment_sentiment={3}
                                        />
                                    )) : <p className="has-text-centered padding-40">No Questions Available</p>
                                )
                            }
                        </div>
                </div>
                {open_edit_modal ? 
                    <EditQuestionModal
                        modal_active={open_edit_modal}
                        onClose={this.closeEditModal}
                        question={selected_question}
                        client_id={client_details.id}
                        refreshQuestions={this.getQuestionBank}
                        survey_id={survey_id}
                        selected_cohort={selected_cohort}
                    /> 
                : null}
                <ConfirmModal
                    text={modal_text.text}
                    title={modal_text.title}
                    confirm_text={"Yes"}
                    doing={deleting}
                    is_active={delete_modal_active}
                    onConfirm={this.deleteQuestion}
                    onClose={this.closeDeleteModal}
                />
            </div>
        )
    }
}

// PropTypes
// ---------
QuestionBank.propTypes = {
    selected_cohort: PropTypes.object.isRequired,
    getQuestionBank: PropTypes.func.isRequired,
    getSampleQuestionBankFile: PropTypes.func.isRequired,
    uploadQbFileMtm: PropTypes.func.isRequired,
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    client_details: state.client.client_details
})

export default connect(mapStateToProps, { getQuestionBank, deleteQuestion, getSampleQuestionBankFile, uploadQbFileMtm})(QuestionBank);