import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Switch from "react-switch";
import SaveChanges from './SaveChanges';
import ConfirmModal from '../../../generic/ConfirmModal';
// actions
import { getBreadboardStatus, patchBreadboardStatus } from '../../../../actions/BreabBoardActions';

class BreadBoardToggle extends Component {

    state = {
        features: {},
        reduce_sales_cycle: false,
        activate_projection_engine: false,
        loading: true,
        modal_text: {
            text: "Are you sure you want to apply for all cohorts?",
            title: "Save for all Cohorts"
        },
        saving: false,
        confirm_active: false,
        disable_save: true,
        activate_pulse: false,
        crisis_enable: false,
        activate_mtm: false
    }

    // component mounts
    // ----------------
    componentDidMount(){
        const { client_details } = this.props;
        if(client_details && client_details.id){
            this.getBreadboardStatus();
        }
    }

    // component updates
    // -----------------
    componentDidUpdate(prevProps){
        const { client_details } = this.props;
        if(prevProps.client_details.id !== client_details.id){
            this.getBreadboardStatus();
        }
    }

    getBreadboardStatus = async () => {
        const { client_details } = this.props;
        this.setState({loading: true});
        const response = await this.props.getBreadboardStatus(client_details.id);
        if(response && response.status === 200){
            this.setState({
                features: (response.data ? response.data : {}),
                reduce_sales_cycle: (response.data.reduce_sales_cycle ? response.data.reduce_sales_cycle : 0),
                activate_projection_engine: (response.data.activate_projection_engine ? response.data.activate_projection_engine : 0),
                activate_pulse: (response.data.activate_pulse ? response.data.activate_pulse : 0),
                crisis_enable: (response.data.crisis_enable ? response.data.crisis_enable : 0),
                activate_mtm: (response.data.activate_mtm ? response.data.activate_mtm : 0)
            });
        } 
        this.setState({loading: false});
    }

    onChangeRSC = (value) => {
        let { reduce_sales_cycle } = this.state;
        reduce_sales_cycle = (value ? 1 : 0);
        this.setState({reduce_sales_cycle}, () => this.checkSave());
    }

    onChangeBB = (value) => {
        let { activate_projection_engine } = this.state;
        activate_projection_engine = (value ? 1 : 0);
        this.setState({activate_projection_engine}, () => this.checkSave());
    }

    onChangePulse = (value) => {
        let { activate_pulse } = this.state;
        activate_pulse = (value ? 1 : 0);
        this.setState({activate_pulse}, () => this.checkSave());
    }

    onChangeMTM = (value) => {
        let { activate_mtm } = this.state;
        activate_mtm = (value ? 1 : 0);
        this.setState({activate_mtm}, () => this.checkSave());
    }

    updateBBSettings = async () => {
        const { client_details } = this.props;
        const { reduce_sales_cycle, activate_projection_engine, activate_pulse, activate_mtm } = this.state;
        this.setState({saving: true});
        const response = await this.props.patchBreadboardStatus(client_details.id, {reduce_sales_cycle, activate_projection_engine, activate_pulse, activate_mtm});
        if(response && response.status === 200){
            this.closeModal();
            this.getBreadboardStatus();
        } 
        this.setState({saving: false, disable_save: true});
    }

    openSaveModal= () => {
        let { modal_text } = this.state;
        modal_text = {
            text: "Are you sure you want to save global settings?",
            title: "Save for Current Client"
        }
        this.setState({modal_text, confirm_active: true});
    }

    closeModal = () => {
        this.setState({confirm_active: false});
    }

    checkSave = () => {
        const { features, reduce_sales_cycle, activate_projection_engine, activate_pulse, activate_mtm } = this.state;
        if(reduce_sales_cycle !== features.reduce_sales_cycle || features.activate_projection_engine !== activate_projection_engine || features.activate_pulse !== activate_pulse || features.activate_mtm !== activate_mtm){
            this.setState({disable_save: false});
        } else {
            this.setState({disable_save: true});
        }
    }

    render() {
        const { loading, modal_text, saving, confirm_active, disable_save, reduce_sales_cycle, activate_projection_engine, activate_pulse, crisis_enable, activate_mtm } = this.state;
        const {client_details} = this.props
        return (
            <div className="card mg-bottom-10">
                <div className="card-content">
                    <div className="pd-10">
                        <h2 className="is-size-6 has-text-weight-bold">Global Settings</h2>
                        <p className="help">Client wise setting</p>
                    </div>
                    {loading 
                    ? <p className="has-text-centered">loading...</p>
                    : 
                        <div>
                            <div className="space-toggle mg-right-10">
                                <Switch disabled={client_details.multilingual_enabled ? true : false} onChange={this.onChangeBB} checked={(activate_projection_engine === 1 ? true : false)}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={18} width={35}/>
                                <span className="is-size-7 toggle-text"><b>Breadboard toggle <br/>
                                {
                                    client_details.multilingual_enabled ? 
                                    <>
                                    <span className="is-size-7 has-text-danger">In order to switch this feature ON you'll have to disable Langguage support for this customer</span><br/>
                                    </>
                                    : ''
                                }
                                <i className="has-text-weight-normal">(Enable/Disable breadboard architecture which supports reach-outs from different products simultaneously based on priority)</i></b></span>
                            </div>
                            <div className="space-toggle mg-right-10">
                                <Switch onChange={this.onChangePulse} checked={(activate_pulse === 1 ? true : false)}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={18} width={35}/>
                                <span className="is-size-7 toggle-text"><b>Custom Check-in <br/><i className="has-text-weight-normal">(Enable/Disable Custom Check-In Product)</i></b></span>
                            </div>
                            <div className="space-toggle mg-right-10">
                                <Switch onChange={this.onChangeMTM} checked={(activate_mtm === 1 ? true : false)}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={18} width={35}/>
                                <span className="is-size-7 toggle-text"><b>MTM <br/><i className="has-text-weight-normal">(Enable/Disable Custom MTM Product)</i></b></span>
                            </div>
                            <div className="space-toggle mg-right-10">
                                <Switch onChange={this.onChangeRSC} checked={(reduce_sales_cycle === 1 ? true : false)}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={18} width={35}/>
                                <span className="is-size-7 toggle-text"><b>Reduce Sales Cycle <br/><i className="has-text-weight-normal">(Enable/Disable Reduce Sales Cycle for last 3 months)</i></b></span>
                            </div>
                            <hr/>
                            <div className="space-toggle mg-right-10">
                                <Switch onChange={() => {}} disabled={true} checked={(crisis_enable === 1 ? true : false)}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={18} width={35}/>
                                <span className="is-size-7 toggle-text"><b>Crisis Enabled <br/><i className="has-text-weight-normal">(To see if crisis is enabled, to enable/disable contact tech team)</i></b></span>
                            </div>
                            <hr/>
                        </div>
                    }
                    <SaveChanges 
                        onSaveCurrent={this.openSaveModal} 
                        onSaveForAll={this.openSaveModal} 
                        disable_save={disable_save}
                    />
                    <ConfirmModal
                        text={modal_text.text}
                        title={modal_text.title}
                        confirm_text={"Yes"}
                        doing={saving}
                        is_active={confirm_active}
                        onConfirm={this.updateBBSettings}
                        onClose={this.closeModal}
                    />
                </div>
            </div>
        )
    }
}

// PropTypes
// ---------
BreadBoardToggle.propTypes = {
    client_details: PropTypes.object.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    client_details: state.client.client_details
})

export default connect(mapStateToProps, { getBreadboardStatus, patchBreadboardStatus })(BreadBoardToggle);