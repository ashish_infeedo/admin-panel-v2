import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import DatePicker from 'react-date-picker';
import moment from 'moment';
import { withRouter } from 'react-router-dom';
import { getMonitoringStats } from '../../../../../actions/BreabBoardActions';

class Monitoring extends Component {

    state = {
        client_id: null,
        start_date: moment().subtract(30,'d').startOf('day').toDate(),
        end_date: moment().toDate(),
        loading: false,
        projections: []
    }

    componentDidMount(){
        const { client_id } = this.props.match.params;
        this.setState({client_id}, () => this.getMonitoringStats());
    }

    getMonitoringStats = async () => {
        const { client_id, start_date, end_date } = this.state;
        this.setState({loading: true});
        const response = await this.props.getMonitoringStats(client_id, moment(start_date).unix(), moment(end_date).unix());
        if(response && response.status === 200){
            this.setState({projections: response.data ? response.data : []})
        }
        this.setState({loading: false});
    }

    onChangeStart = (start_date) => {
        this.setState({start_date}, () => this.getMonitoringStats());
    }

    onChangeEnd = (end_date) => {
        this.setState({end_date}, () => this.getMonitoringStats());
    }

    render() {
        const { start_date, end_date, projections, loading } = this.state;
        return (
            <div className="monitoring-reachouts">
                <div className="columns">
                    <div className="column is-4">
                        <b>Start Date</b>
                        <br/>
                        <DatePicker onChange={this.onChangeStart} value={start_date} maxDate={new Date()}/> 
                    </div>
                    <div className="column is-4">
                        <b>End Date</b>
                        <br/>
                        <DatePicker onChange={this.onChangeEnd} value={end_date} maxDate={new Date()}/>  
                    </div>
                </div>
                <div className="card-content is-paddingless">
                    <div className="content">
                        <div className="custom-table">
                            <table className="table is-striped">
                                <thead>
                                    <tr>
                                    <th>S.No.</th>
                                    <th>Date</th>
                                    <th>Total Chats</th>
                                    <th>Manifested</th>
                                    <th>To Start</th>
                                    <th>Dropped</th>
                                    <th>Completed</th>
                                    <th>On Hold</th>
                                    <th>Errors</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {!loading ? projections.map((reachout, index) => (
                                <tr key={index}>
                                    <td>{index + 1}</td>
                                    <td><b>{moment(reachout.scheduled_for).format('DD MMM YYYY')}</b></td>
                                    <td>{reachout.total_projections}</td>
                                    <td>{reachout.manifested}</td>
                                    <td>{reachout.to_start}</td>
                                    <td>{reachout.dropped}</td>
                                    <td>{reachout.completed}</td>
                                    <td>{reachout.on_hold}</td>
                                    <td>{reachout.error}</td>
                                </tr>
                                )): null}
                                </tbody>
                            </table>
                            {(projections.length === 0 && !loading) ? <p className="no-data has-text-centered">No Data</p> : null}
                            {loading ? <p className="no-data has-text-centered">Loading Data...</p> : null}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

Monitoring.propTypes = {
    getMonitoringStats: PropTypes.func.isRequired,
}
  
export default connect(null, {getMonitoringStats})(withRouter(Monitoring));