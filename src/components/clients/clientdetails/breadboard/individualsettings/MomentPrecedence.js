import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import arrayMove from 'array-move';
import {SortableContainer, SortableElement} from 'react-sortable-hoc';
import SaveChanges from '../SaveChanges';
import ConfirmModal from '../../../../generic/ConfirmModal';
// actions
import { patchMomentsPrecedence } from '../../../../../actions/BreabBoardActions';

class MomentPrecedence extends Component {

    state = {
        touchpoints: [],
        disable_save: true, 
        confirm_active: false, 
        modal_text: {

        }, 
        saving: false
    }

    componentDidMount(){
        const { touchpoints } = this.props;
        if(touchpoints.length){
            this.setState({touchpoints: JSON.parse(JSON.stringify(touchpoints))});
        }
    }

    componentDidUpdate(prevProps){
        const { touchpoints } = this.props;
        if(JSON.stringify(touchpoints) !== JSON.stringify(prevProps.touchpoints)){
            this.setState({touchpoints: JSON.parse(JSON.stringify(touchpoints))});
        }
    }

    patchMomentsPrecedence = async () => {
        const { touchpoints } = this.state;
        const { client_id, selected_cohort_id, bucket_key, bucket_id, unique_id } = this.props;
        this.setState({saving: true});
        const response = await this.props.patchMomentsPrecedence(client_id, selected_cohort_id, bucket_key, bucket_id, touchpoints, unique_id)
        if(response && response.status === 200){
            this.closeModal();
            this.props.refreshBucket(true);
        }
        this.setState({saving: false, disable_save: true});
    }

    // Open Save All Modal
    // -------------------
    openSaveAllModal = () => {
        let { modal_text } = this.state;
        modal_text = {
            text: "Are you sure you want to apply for all cohorts?",
            title: "Save for all Cohorts"
        }
        this.setState({modal_text, confirm_active: true, save_for_all: true});
    }

    // Open Save Current Modal
    // -----------------------
    openSaveCurrentModal = () => {
        let { modal_text } = this.state;
        modal_text = {
            text: "Are you sure you want to apply for current cohorts?",
            title: "Save for Current Cohorts"
        }
        this.setState({modal_text, confirm_active: true, save_for_all: false});
    }

    // close Save Modal
    // ----------------
    closeModal = () => {
        this.setState({confirm_active: false, save_for_all: false});
    }

    // on Drag Drop End
    // ----------------
    onSortEnd = ({oldIndex, newIndex}) => {
        const { touchpoints } = this.state;
        const swap_precedence = touchpoints[oldIndex].precedence;
        touchpoints[oldIndex].precedence = touchpoints[newIndex].precedence;
        touchpoints[newIndex].precedence = swap_precedence;
        this.setState({
            touchpoints: arrayMove(touchpoints, oldIndex, newIndex),
        }, () => this.checkEnableSave());
    };

    checkEnableSave = () => {
        const { touchpoints } = this.state;
        if(JSON.stringify(touchpoints) !== JSON.stringify(this.props.touchpoints)){
            this.setState({disable_save: false});
        } else {
            this.setState({disable_save: true});
        }
    }

    render() {

        const { touchpoints, disable_save, confirm_active, modal_text, saving } = this.state;

        // Sortable Item HOC
        // -----------------
        const SortableItem = SortableElement(({touchpoint, row_index}) => 
            <div key={touchpoint.touchpoint_key} className="content space-toggle pd-10 precedence-box">
                <span className="precedene-buttons cursor-cross">
                    <i className="fas fa-arrows-alt-v"></i>
                </span>
                {touchpoint.title} 
                {touchpoint.enabled ? <span className="is-size-7 has-text-success"> (enabled)</span> : <span className="is-size-7 has-text-danger"> (disabled)</span>}
            </div>
        );

        // Sortable List HOC
        // -----------------
        const SortableList = SortableContainer(({touchpoints}) => {
            return (
                <ul>
                {touchpoints.map((touchpoint, index) => (
                    <SortableItem key={`item-${touchpoint.touchpoint_key}`} index={index} row_index={index} touchpoint={touchpoint} />
                ))}
                </ul>
            );
        });

        return (
            <div>
                <div>
                    <h2 className="is-size-6">Moments Precedence</h2>
                    <p className="help">Customize moments and combinations precedence for MTM with moment at top with highest precedence</p>
                </div>
                <article className="message">
                    <div className="message-body modal-sub-header-text">
                        <ol>
                            <li>Set Moment precedence to send reachouts from different moments and <b>resolve conflicts based on priority</b></li>
                            <li><b>Note:</b> Changes in moment precedence will only reflect in case of overlaps occurring due to future data uploads.</li>
                        </ol>
                    </div>
                </article>
                <SortableList touchpoints={touchpoints} onSortEnd={this.onSortEnd} distance={1}/>
                <SaveChanges 
                    onSaveCurrent={this.openSaveCurrentModal} 
                    onSaveForAll={this.openSaveAllModal} 
                    disable_save={disable_save}
                />
                <ConfirmModal
                    text={modal_text.text}
                    title={modal_text.title}
                    confirm_text={"Yes"}
                    doing={saving}
                    is_active={confirm_active}
                    onConfirm={this.patchMomentsPrecedence}
                    onClose={this.closeModal}
                />
            </div>
        )
    }
}

MomentPrecedence.propTypes = {
    touchpoints: PropTypes.array.isRequired,
    client_id: PropTypes.number.isRequired,
    selected_cohort_id: PropTypes.number.isRequired,
    bucket_id: PropTypes.string.isRequired,
    unique_id: PropTypes.string.isRequired,
    bucket_key: PropTypes.string.isRequired,
    refreshBucket: PropTypes.func.isRequired
};

export default connect(null, { patchMomentsPrecedence })(MomentPrecedence);