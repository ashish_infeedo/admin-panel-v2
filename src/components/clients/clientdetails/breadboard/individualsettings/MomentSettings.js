import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classnames from 'classnames';
import SaveChanges from '../SaveChanges';
import ConfirmModal from '../../../../generic/ConfirmModal';
// actions
import { patchSelectedMomentSettings } from '../../../../../actions/BreabBoardActions';

class MomentSettings extends Component {
    
    state = {
        selected_touchpoint: {
            notification_delay: 0,
            gestation_period: 0,
            blocking_period: 0,
            action_fields: [],
            date_field: '',
            action_field_relation: ''
        },
        modal_text: {}, 
        confirm_active: false,
        disable_save: true,
        saving: false,
        selected_index: 0,
        local_relationships: [
            'BOOLEAN',
            'DATE'
        ],
        global_relationships: [
            'AND',
            'OR',
        ],
        new_field: []
    }

    // component mounts
    // ----------------
    componentDidMount(){
        this.selectFirstEnabledTab();
    }

    // Component Updates
    // -----------------
    componentDidUpdate(prevProps){
        const { touchpoints } = this.props;
        if(JSON.stringify(touchpoints) !== JSON.stringify(prevProps.touchpoints)){
            this.selectFirstEnabledTab();
        }
    }

    // Patch Momemt Settings
    // ---------------------
    patchSelectedMomentSettings = async () => {
        const { selected_touchpoint } = this.state;
        const { client_id, bucket_id, bucket_key, selected_cohort_id, unique_id } = this.props;
        this.setState({saving: true});
        const response = await this.props.patchSelectedMomentSettings(client_id, selected_cohort_id, bucket_key, bucket_id, selected_touchpoint, unique_id);
        if(response && response.status === 200){
            this.closeModal();
            this.props.refreshBucket(true);
        }
        this.setState({saving: false, disable_save: true});
    }

    // checkEnableSave button
    // ----------------------
    checkEnableSave = () => {
        const { selected_index, selected_touchpoint } = this.state;
        const { touchpoints } = this.props;
        if(
            (selected_touchpoint.notification_delay !== touchpoints[selected_index].notification_delay
            ||  selected_touchpoint.blocking_period !== touchpoints[selected_index].blocking_period
            ||  selected_touchpoint.gestation_period !== touchpoints[selected_index].gestation_period
            ||  selected_touchpoint.date_field !== touchpoints[selected_index].date_field
            ||  JSON.stringify(selected_touchpoint.action_fields) !== JSON.stringify(touchpoints[selected_index].action_fields))
            && selected_touchpoint.notification_delay 
            && selected_touchpoint.blocking_period 
            && selected_touchpoint.gestation_period 
        ){
            let action_configured_properly = true;
            selected_touchpoint.action_fields.forEach(action_field => {
                if(!action_field.fields.length || !action_field.relation.length){
                    action_configured_properly = false;
                }
            })
            if(action_configured_properly){
                this.setState({disable_save: false});
            } else {
                this.setState({disable_save: true});
            }
        } else {
            this.setState({disable_save: true});
        }
    }

    // select first enabled tab
    // ------------------------
    selectFirstEnabledTab = () => {
        const { selected_touchpoint } = this.state;
        const { touchpoints } = this.props;
        if(selected_touchpoint && selected_touchpoint.title){
            return;
        }
        let selected_touchpoint_local = {};
        if(touchpoints && touchpoints.length){
            for(let i = 0; i< touchpoints.length; i++){
                if(touchpoints[i].enabled && !touchpoints[i].combination){
                    selected_touchpoint_local = JSON.parse(JSON.stringify(touchpoints[i]));
                    break;
                }
            }
            this.setState({selected_touchpoint: selected_touchpoint_local}, () => this.createNewFieldArray());
        }
    }

    // Select Tab on Click
    // -------------------
    selectTab = (selected_touchpoint, selected_index) => {
        if(selected_touchpoint.enabled){
            this.setState({selected_touchpoint: JSON.parse(JSON.stringify(selected_touchpoint)), selected_index}, () => this.createNewFieldArray());
        }
    }

    // Create New Action Fields inputs array 
    // -------------------------------------
    createNewFieldArray = () => {
        const { selected_touchpoint, new_field } = this.state;
        selected_touchpoint.action_fields.forEach((field) => {
            new_field.push({new_field_text: ''});
        })
        this.setState({new_field});
        this.checkEnableSave()
    }

    // Open Save All Modal
    // -------------------
    openSaveAllModal = () => {
        let { modal_text } = this.state;
        modal_text = {
            text: "Are you sure you want to apply for all cohorts?",
            title: "Save for all Cohorts"
        }
        this.setState({modal_text, confirm_active: true, saving: true});
    }

    // Open Save Current Modal
    // -----------------------
    openSaveCurrentModal = () => {
        let { modal_text } = this.state;
        modal_text = {
            text: "Are you sure you want to apply for current cohorts?",
            title: "Save for Current Cohorts"
        }
        this.setState({modal_text, confirm_active: true, saving: false});
    }

    // close Save Modal
    // ----------------
    closeModal = () => {
        this.setState({confirm_active: false, saving: false});
    }

    // On Input Change
    // ---------------
    onChange = (e) => {
        const { selected_touchpoint } = this.state;
        if(!e.target.value){
            selected_touchpoint[e.target.name] = '';
        } else {
            if(e.target.name === 'notification_delay' || e.target.name === 'gestation_period' || e.target.name === 'blocking_period'){
                selected_touchpoint[e.target.name] = parseInt(e.target.value);
            } else {
                selected_touchpoint[e.target.name] = e.target.value;
            }
        }
        this.setState({selected_touchpoint}, () => this.checkEnableSave());
    }

    // On Change Action Fields
    // -----------------------
    onChangeField = (e, index) => {
        const { new_field } = this.state;
        new_field[index].new_field_text = (e.target.value ? e.target.value : '');
        this.setState({new_field});
    }

    // Add new Action Field Group
    // --------------------------
    addNewFieldGroup = () => {
        const { selected_touchpoint, new_field } = this.state;
        selected_touchpoint.action_fields.push({
            fields: [],
            relation: ''
        });
        new_field.push({new_field_text: ''});
        this.setState({selected_touchpoint, new_field});
    }

    // Delete Action Field Group
    // -------------------------
    deleteFieldGroup = (index) => {
        const { selected_touchpoint, new_field } = this.state;
        selected_touchpoint.action_fields.splice(index, 1);
        new_field.splice(index, 1);
        this.setState({selected_touchpoint, new_field}, () => this.checkEnableSave());
    }

    // Select Relation of fields
    // -------------------------
    selectRelation = (relation, index) => {
        const { selected_touchpoint } = this.state;
        selected_touchpoint.action_fields[index].relation = relation;
        this.setState({selected_touchpoint}, () => this.checkEnableSave());
    }

    // Select Global Relation of groups
    // --------------------------------
    selectGlobalRelation = (relation) => {
        const { selected_touchpoint } = this.state;
        selected_touchpoint.action_field_relation = relation;
        this.setState({selected_touchpoint}, () => this.checkEnableSave());
    }

    // Add action field in a group
    // ---------------------------
    addField = (index) => {
        const { new_field, selected_touchpoint } = this.state;
        selected_touchpoint.action_fields[index].fields.push(new_field[index].new_field_text);
        new_field[index].new_field_text = '';
        this.setState({selected_touchpoint, new_field}, () => this.checkEnableSave());
    }

    // delete action field from group
    // ------------------------------
    deleteField = (index, sub_index) => {
        const { selected_touchpoint } = this.state;
        selected_touchpoint.action_fields[index].fields.splice(sub_index, 1);
        this.setState({selected_touchpoint}, () => this.checkEnableSave());
    }
    
    // Render UI
    // ---------
    render() {
        const { selected_touchpoint, disable_save, modal_text, confirm_active, saving, local_relationships, global_relationships, new_field } = this.state;
        const { touchpoints } = this.props;
        return (
            <div>
                <div className="tabs">
                    <ul>
                        {
                            touchpoints.map((touchpoint, index) => (
                               ( touchpoint.combination 
                                    ? null
                                    :
                                    <li onClick={this.selectTab.bind(this, touchpoint, index)} key={touchpoint.touchpoint_key} className={classnames('', {'is-active': selected_touchpoint.touchpoint_key === touchpoint.touchpoint_key})}>
                                        <a className={classnames('', {'disable-anchor': !touchpoint.enabled})}>{touchpoint.title}</a>
                                    </li>
                               )
                            ))
                        }
                    </ul>
                </div>
                <div className="mg-left-2">
                    <div className="columns flex-wrap">
                        <div className="field column">
                            <label className="label">Notification Period (days)</label>
                            <div className="control">
                                <input onChange={this.onChange} name="notification_delay" className="input" type="number" value={selected_touchpoint.notification_delay}/>
                            </div>
                        </div>
                        <div className="field column">
                            <label className="label">Gestation Period (days)</label>
                            <div className="control">
                                <input onChange={this.onChange} name="gestation_period" className="input" type="number" value={selected_touchpoint.gestation_period}/>
                            </div>
                        </div>
                        <div className="field column">
                            <label className="label">Blocking Period (days)</label>
                            <div className="control">
                                <input onChange={this.onChange} name="blocking_period" className="input" type="number" value={selected_touchpoint.blocking_period}/>
                            </div>
                        </div>
                        {/* <div className="field column">
                            <label className="label">Date Field</label>
                            <div className="control">
                                <input onChange={this.onChange} name="date_field" className="input" type="string" value={(selected_touchpoint.date_field ? selected_touchpoint.date_field : '')}/>
                            </div>
                        </div> */}
                    </div>
                    <hr/>
                    <div>
                        <h2 className="is-size-6">Action Fields</h2>
                        <p className="help">Configure action fields to trigger touchpoint</p>
                        <div>
                            {
                                selected_touchpoint.action_fields.map((action_field, index) => (
                                <div key={index}>
                                    <h2 className="is-size-6">Group { index +1 }</h2>
                                    {selected_touchpoint.action_fields[index].fields && selected_touchpoint.action_fields[index].fields.length === 1 ? null 
                                    : <div className="field has-addons is-small">
                                        <div className="control">
                                            <input className="input is-small" name="amber" onChange={(e) => this.onChangeField(e, index)} value={(new_field[index] ? new_field[index].new_field_text : '')} type="text" placeholder="Add Action Field"/>
                                        </div>
                                        <div className="control">
                                            <button onClick={this.addField.bind(this, index)} className="button is-small is-outlined is-success">
                                            Add
                                            </button>
                                        </div>
                                    </div>}
                                    <ul className="mg-left-10">
                                        {action_field.fields.map((field, inner_index) => (
                                            <li key={inner_index} className="is-size-7 mg-10">
                                                {field} &nbsp;&nbsp;
                                                <i className="far fa-trash-alt is-hoverable" onClick={this.deleteField.bind(this, index, inner_index)}></i>
                                            </li>
                                        ))}
                                    </ul>  
                                    <div>
                                        <p className="is-size-7 has-text-weight-bold mg-top-bottom-25 has-text-grey">RELATIONSHIP:</p>
                                        <div className="columns">
                                            <div className="column tags are-small">
                                                {
                                                    local_relationships.map((relation, inner_index) => (
                                                        <span 
                                                            onClick={this.selectRelation.bind(this, relation, index)}
                                                            key={inner_index} 
                                                            className={classnames('tag is-hoverable', {
                                                                'is-link': action_field.relation === relation
                                                            }
                                                        )}>
                                                        {relation}
                                                        </span>
                                                    ))
                                                }
                                            </div>
                                            <div className="column has-text-right">
                                                <button onClick={this.deleteFieldGroup.bind(this, index)} className="button is-small is-pulled-right is-outlined is-danger">
                                                    Delete
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                </div>
                                ))
                            }
                            {selected_touchpoint.action_fields && selected_touchpoint.action_fields.length > 1 ? 
                                <div>
                                    <h2 className="is-size-6">Global Relation</h2>
                                    <p className="help">Select Relationship between groups above</p>
                                    <span className="tags are-small">
                                        {
                                            global_relationships.map((relation, index) => (
                                                <span 
                                                    onClick={this.selectGlobalRelation.bind(this, relation)}
                                                    key={index} 
                                                    className={classnames('tag is-hoverable', {
                                                        'is-primary': relation === selected_touchpoint.action_field_relation
                                                    }
                                                )}>
                                                {relation}
                                                </span>
                                            ))
                                        }
                                    </span>
                                </div> 
                            : null}
                            <br/>
                            <button onClick={this.addNewFieldGroup} className="button">
                                <span className="icon is-small">
                                    <i className="fas fa-plus"></i>
                                </span>
                            </button>
                            <br/>
                        </div>
                    </div>
                    <SaveChanges 
                        onSaveCurrent={this.openSaveCurrentModal} 
                        onSaveForAll={this.openSaveAllModal} 
                        disable_save={disable_save}
                    />
                    <ConfirmModal
                        text={modal_text.text}
                        title={modal_text.title}
                        confirm_text={"Yes"}
                        doing={saving}
                        is_active={confirm_active}
                        onConfirm={this.patchSelectedMomentSettings}
                        onClose={this.closeModal}
                    />
                </div>
            </div>
        )
    }
}

MomentSettings.propTypes = {
    touchpoints: PropTypes.array.isRequired,
    client_id: PropTypes.number.isRequired,
    selected_cohort_id: PropTypes.number.isRequired,
    bucket_id: PropTypes.string.isRequired,
    unique_id: PropTypes.string.isRequired,
    bucket_key: PropTypes.string.isRequired,
    refreshBucket: PropTypes.func.isRequired
};

export default connect(null, { patchSelectedMomentSettings })(MomentSettings);