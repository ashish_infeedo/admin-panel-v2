import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { connect } from 'react-redux';
import Switch from "react-switch";
import { IsJsonString } from '../../../../../helper/DataChecks';
// actions
import { getTouchPointConfig, patchTouchPointConfig } from '../../../../../actions/BreabBoardActions';

class MomentSettingsMTM extends Component {

    state = {
        touch_points: [],
        selected_touch_point: {},
        loading_settings: true,
        touch_point_settings: {
            action_field: {
                field: '',
                relation: 'DATE'
            },
            gestation_period: 0,
            notification_delay: 0,
            notification_email: 0,
            title: "",
        },
        disable_save: false,
        saving_fields: false
    }

    componentDidMount(){
        const { selected_bucket } = this.props;
        if(selected_bucket && selected_bucket.touch_points){
            this.setState({
                touch_points: selected_bucket.touch_points,
                selected_touch_point: selected_bucket.touch_points[0]
            }, () => {
                this.getTouchPointConfig();
            });
        }
    }


    getTouchPointConfig = async () => {
        const { selected_bucket, selected_cohort, client_details } = this.props;
        const { selected_touch_point } = this.state;
        this.setState({
            loading_settings: true
        });
        const response = await this.props.getTouchPointConfig(client_details.id, selected_cohort.id, selected_bucket.bucket_key, selected_touch_point.touch_point_key);
        if(response && response.status === 200){
            const default_object = {
                action_field: {
                    field: '',
                    relation: 'DATE'
                },
                gestation_period: 0,
                notification_delay: 0,
                notification_email: 0,
                survey_id: 2,
                title: "",
            }
            const latest_settings = response.data && response.data.touchpoint ? response.data.touchpoint : default_object;
            const touch_point_settings = {
                action_field: latest_settings.action_field && IsJsonString(latest_settings.action_field) ? JSON.parse(latest_settings.action_field) : default_object.action_field,
                gestation_period: latest_settings.gestation_period,
                notification_delay: latest_settings.notification_delay,
                notification_email: latest_settings.notification_email,
                title: latest_settings.title,
            }
            this.setState({
                touch_point_settings
            }, () => {
                this.checkEnableSave();
            });
        }
        this.setState({
            loading_settings: false
        });
    }

    selectTab = (selected_touch_point) => {
        this.setState({
            selected_touch_point
        }, () => this.getTouchPointConfig())
    }

    onChange = (e) => {
        const { touch_point_settings } = this.state;
        if(e.target.name === 'action_field'){
            if(!e.target.value){
                touch_point_settings.action_field.field = '';
            } else {
                touch_point_settings.action_field.field = e.target.value;
            }
        } else {
            if(!e.target.value){
                touch_point_settings[e.target.name] = '';
            } else {
                if(e.target.name === 'notification_delay' || e.target.name === 'gestation_period'){
                    touch_point_settings[e.target.name] = parseInt(e.target.value);
                } else {
                    touch_point_settings[e.target.name] = e.target.value;
                }
            }
        }
        this.setState({touch_point_settings}, () => {
            this.checkEnableSave()
        });
    }

    checkEnableSave = () => {
        const { touch_point_settings } = this.state;
        if((touch_point_settings.notification_email && !touch_point_settings.notification_delay) || !touch_point_settings.gestation_period || !touch_point_settings.action_field.field || !touch_point_settings.action_field.relation){
            this.setState({disable_save: true});
        } else {
            this.setState({disable_save: false});
        }
    }

    onSave = async () => {
        const { selected_bucket, selected_cohort, client_details } = this.props;
        const { selected_touch_point, touch_point_settings } = this.state;
        this.setState({
            saving_fields: true
        })
        await this.props.patchTouchPointConfig(client_details.id, selected_cohort.id, selected_bucket.bucket_key, selected_touch_point.touch_point_key, touch_point_settings);
        this.setState({
            saving_fields: false
        })
    }

    onChangeToggle = (key) => {
        const { touch_point_settings } = this.state;
        touch_point_settings.notification_email = key ? 1 : 0;
        if(!key){
            touch_point_settings.notification_delay = 0;
        }
        this.setState({touch_point_settings}, () => this.checkEnableSave());
    }

    selectRelation = (relation) => {
        const { touch_point_settings } = this.state;
        touch_point_settings.action_field.relation = relation;
        this.setState({
            touch_point_settings
        });
    }

    render() {
        const { touch_points, selected_touch_point, loading_settings, touch_point_settings, disable_save, saving_fields } = this.state;
        return (
            <div>
                <div className="pd-10">
                    <h2 className="is-size-6">Moment Settings</h2>
                    <p className="help">Select individual moments to configure settings</p>
                </div>  
                <div className="tabs">
                    <ul className="is-marginless">
                        {
                            touch_points.map((touch_point, index) => (
                                <li onClick={this.selectTab.bind(this, touch_point)} key={index} className={classnames('', {'is-active': selected_touch_point.touch_point_key === touch_point.touch_point_key})}>
                                    <a>{touch_point.title}</a>
                                </li>
 
                            ))
                        }
                    </ul>
                </div>
                {
                    loading_settings 
                    ? 
                        <p className="has-text-centered padding-40">loading...</p> 
                    :
                        <div>
                            <div className="column is-6 space-toggle pd-10">
                                <Switch onChange={this.onChangeToggle} checked={touch_point_settings.notification_email ? true : false}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={18} width={35}/>
                                <span className="is-size-7 toggle-text"><b>Notification Email</b></span>
                            </div>
                            <div className="columns flex-wrap pd-10">
                                <div className="field column">
                                    <label className="label">Notification Period (days)</label>
                                    <div className="control">
                                        <input onChange={this.onChange} disabled={!touch_point_settings.notification_email} name="notification_delay" className="input" type="number" value={touch_point_settings.notification_delay}/>
                                    </div>
                                </div>
                                <div className="field column">
                                    <label className="label">Gestation Period (days)</label>
                                    <div className="control">
                                        <input onChange={this.onChange} name="gestation_period" className="input" type="number" value={touch_point_settings.gestation_period}/>
                                    </div>
                                </div>
                                <div className="field column">
                                    <label className="label">Action Field (CSV Column name)</label>
                                    <div className="control">
                                        <input onChange={this.onChange} name="action_field" className="input" type="text" value={touch_point_settings.action_field.field}/>
                                    </div>
                                    <div className="columns">
                                        <div className="column tags are-small mg-top-10">
                                            <span 
                                                onClick={this.selectRelation.bind(this, 'DATE')}
                                                className={classnames('tag is-hoverable', {
                                                    'is-link': touch_point_settings.action_field.relation === 'DATE'
                                                }
                                            )}>
                                            DATE
                                            </span>
                                            <span 
                                                onClick={this.selectRelation.bind(this, 'BOOLEAN')}
                                                className={classnames('tag is-hoverable', {
                                                    'is-link': touch_point_settings.action_field.relation === 'BOOLEAN'
                                                }
                                            )}>
                                            BOOLEAN
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="has-text-right">
                                <button onClick={this.onSave} type="submit" className={classnames("button is-primary", {'is-loading': saving_fields})}  disabled={disable_save}>
                                    <span className="icon">
                                    <i className="far fa-save"></i>
                                    </span>
                                    <span>Save</span>
                                </button>
                            </div>
                        </div>
                }
            </div>
        )
    }
}
 

MomentSettingsMTM.propTypes = {
    selected_bucket: PropTypes.object.isRequired,
    selected_cohort: PropTypes.object.isRequired,
    client_details:  PropTypes.object.isRequired,
    getTouchPointConfig: PropTypes.func.isRequired,
    patchTouchPointConfig: PropTypes.func.isRequired
};

export default connect(null, { getTouchPointConfig, patchTouchPointConfig })(MomentSettingsMTM);