import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Switch from "react-switch";
import SaveChanges from '../SaveChanges';
import { NotificationManager } from 'react-notifications';
import ConfirmModal from '../../../../generic/ConfirmModal';
import { patchBucketCommonConfig } from '../../../../../actions/BreabBoardActions';

class ChatSettings extends Component {

    state = {
        common_config: {
            reminders: {},
            reduce_sales_cycle: {on: false, period: 0},
            notification_email: false,
            chat_editable: false,
            eligible_roles: [2, 3],
            paused: false,
            reminders: {admin: [], amber: []}
        },
        roles: {
            1: 'Admin',
            2: 'Sub-Admin',
            3: 'Member',
            4: 'Manager'
        },
        disable_save: true,
        modal_text: {
            text: "Are you sure you want to apply for all cohorts?",
            title: "Save for all Cohorts"
        },
        reminder_values: {
            admin: 0,
            amber: 0
        },
        confirm_active: false,
        saving: false
    }

    componentDidMount(){
        const { common_config } = this.props;
        this.setState({common_config: JSON.parse(JSON.stringify(common_config))});
    }

    componentDidUpdate(prevProps){
        const { common_config } = this.props;
        if(JSON.stringify(prevProps.common_config) !== JSON.stringify(common_config)){
            this.setState({common_config: JSON.parse(JSON.stringify(common_config))});
        }
    }

    checkEnableSave = () => {
        const { common_config } = this.state;
        if(JSON.stringify(common_config) === JSON.stringify(this.props.common_config)){
            this.setState({disable_save: true});
        } else {
            this.setState({disable_save: false});
        }
    }

    patchBucketCommonConfig = async () => {
        const { common_config, save_for_all } = this.state;
        const { client_id, selected_cohort_id, bucket_id, unique_id, bucket_key } = this.props;
        this.setState({saving: true});
        const response = await this.props.patchBucketCommonConfig(client_id, selected_cohort_id, bucket_key, bucket_id, common_config, unique_id, save_for_all);
        if(response && response.status === 200){
            this.closeModal();
            this.props.refreshBucket(true);
        }
        this.setState({saving: false, disable_save: true});
    }

    onChange = (key) => {
        const { common_config } = this.state;
        common_config[key] = !common_config[key];
        this.setState({common_config}, () => this.checkEnableSave());
    }

    onChangeRSC = (key) => {
        const { common_config } = this.state;
        common_config.reduce_sales_cycle[key] = !common_config.reduce_sales_cycle[key];
        this.setState({common_config}, () => this.checkEnableSave());
    }

    selectDeselectRole = (role) => {
        const { common_config } = this.state;
        const index = common_config.eligible_roles.indexOf(role);
        if(index > -1) {
            common_config.eligible_roles.splice(index, 1);
        } else {
            common_config.eligible_roles.push(role);
        }
        this.setState({common_config}, () => this.checkEnableSave());
    }

    // Open Save All Modal
    // -------------------
    openSaveAllModal = () => {
        let { modal_text } = this.state;
        modal_text = {
            text: "Are you sure you want to apply for all cohorts?",
            title: "Save for all Cohorts"
        }
        this.setState({modal_text, confirm_active: true, save_for_all: true});
    }

    // Open Save Current Modal
    // -----------------------
    openSaveCurrentModal = () => {
        let { modal_text } = this.state;
        modal_text = {
            text: "Are you sure you want to apply for current cohorts?",
            title: "Save for Current Cohorts"
        }
        this.setState({modal_text, confirm_active: true, save_for_all: false});
    }

    // close Save Modal
    // ----------------
    closeModal = () => {
        this.setState({confirm_active: false, save_for_all: false});
    }

    onInputChnage = (e) => {
        const { common_config } = this.state;
        common_config.reduce_sales_cycle.period = e.target.value;
        this.setState({common_config}, () => this.checkEnableSave());
    }

    deleteReminder = (type, index) => {
        const { common_config } = this.state;

        if (index > -1) {
            common_config.reminders[type].splice(index, 1);
        }

        common_config.reminders[type].sort((a, b) => a - b);
        this.setState({common_config}, () => this.checkEnableSave());
    }

    addReminder = (type) => {
        const { common_config, reminder_values } = this.state;
        if(!reminder_values[type]){
            NotificationManager.info("Reminder value should be > 1")
            return
        }
        if(common_config.reminders[type].includes(reminder_values[type])){
            NotificationManager.info("Reminder for same value already set");
            return
        }
        common_config.reminders[type].push(reminder_values[type]);
        common_config.reminders[type].sort((a, b) => a - b);
        this.setState({common_config}, () => this.checkEnableSave());
    }

    onChangeReminder = (e) => {
        const { reminder_values } = this.state;
        reminder_values[e.target.name] = parseInt(e.target.value);
        this.setState({reminder_values});
    }

    render() {
        const { roles, common_config, disable_save, confirm_active, modal_text, saving, reminder_values } = this.state;
        return (
            <div className="pd-10">
                <div>
                    <h2 className="is-size-6">Chat Settings</h2>
                    <p className="help">Customize chat feature settings</p>
                </div>
                <p className="is-size-7 has-text-weight-bold mg-top-bottom-25 has-text-grey">FEATURES:</p>
                <div className="columns flex-wrap">
                     {/* <div className="column is-6 space-toggle pd-10">
                        <Switch onChange={this.onChange.bind(this, 'paused')} checked={common_config.paused}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={18} width={35}/>
                        <span className="is-size-7 toggle-text"><b>Pause Reachouts</b></span>
                    </div> */}
                    <div className="column is-6 space-toggle pd-10">
                        <Switch onChange={this.onChange.bind(this, 'notification_email')} checked={common_config.notification_email}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={18} width={35}/>
                        <span className="is-size-7 toggle-text"><b>Notification Email</b></span>
                    </div>
                    {/* <div className="column is-6 space-toggle pd-10">
                        <Switch onChange={this.onChange.bind(this, 'chat_editable')} checked={common_config.chat_editable}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={18} width={35}/>
                        <span className="is-size-7 toggle-text"><b>Chat Editable</b></span>
                    </div> */}
                    {/* <div className="column is-6 space-toggle pd-10">
                        <Switch onChange={this.onChangeRSC.bind(this, 'on')} checked={common_config.reduce_sales_cycle.on}  uncheckedIcon={false} checkedIcon={false} onColor={'#23d160'} height={18} width={35}/>
                        <span className="is-size-7 toggle-text"><b>Reduce Sales Cycle with <span>Months to Cover &nbsp;&nbsp;
                            <input type="text" placeholder="Text input" onChange={this.onInputChnage} value={common_config.reduce_sales_cycle.period} disabled/> 
                        </span></b></span>
                    </div> */}
                </div>
                {/* <div>
                    <p className="is-size-7 has-text-weight-bold mg-top-bottom-25 has-text-grey">ELIGIBLE FOR:</p>
                    <div className="tags are-small">
                        {
                            Object.entries(roles).map(([key, value]) => (
                                <span 
                                    onClick={this.selectDeselectRole.bind(this, parseInt(key))} 
                                    key={key} 
                                    className={classnames('tag is-hoverable', {
                                        'is-link': common_config.eligible_roles.includes(parseInt(key))
                                    }
                                )}>
                                {value}
                                </span>
                            ))
                        }
                    </div>
                </div> */}
                {/* <br/>
                <div>
                    <p className="is-size-7 has-text-weight-bold mg-top-bottom-25 has-text-grey">REMINDER SETTINGS:</p>
                    <div className="columns">
                        <div className="column">
                            <p className="is-size-7 has-text-weight-bold">1. Admin Reminders ({common_config.reminders.admin.length}):</p>
                            <div className="field has-addons is-small">
                                <div className="control">
                                    <input className="input is-small" name="admin" onChange={this.onChangeReminder} type="number" min="1" value={reminder_values.admin} placeholder="Add Admin Reminder"/>
                                </div>
                                <div className="control">
                                    <button onClick={this.addReminder.bind(this, 'admin')} className="button is-small is-outlined is-success">
                                    Add (days)
                                    </button>
                                </div>
                            </div>
                            <div className="content">
                                {
                                    common_config.reminders.admin.length ? 
                                    <ul className="mg-left-10">
                                        {common_config.reminders.admin.map((interval, index) => (
                                            <li key={index}  className="is-size-7 mg-10">
                                                {interval} day(s) from reachout &nbsp;&nbsp;
                                                <i onClick={this.deleteReminder.bind(this, 'admin', index)} className="far fa-trash-alt is-hoverable"></i>
                                            </li>
                                        ))}
                                    </ul>   
                                    : <p className="help">No Reminders Set</p>
                                }
                            </div>
                        </div>
                        <div className="column">
                            <p className="is-size-7 has-text-weight-bold">2. Amber Reminders ({common_config.reminders.amber.length}):</p>
                            <div className="field has-addons is-small">
                                <div className="control">
                                    <input className="input is-small" name="amber" onChange={this.onChangeReminder} type="number" min="1" value={reminder_values.amber} placeholder="Add Amber Reminder"/>
                                </div>
                                <div className="control">
                                    <button onClick={this.addReminder.bind(this, 'amber')} className="button is-small is-outlined is-success">
                                    Add (days)
                                    </button>
                                </div>
                            </div>
                            <div className="content">
                                {
                                    common_config.reminders.amber.length ? 
                                    <ul className="mg-left-10">
                                        {common_config.reminders.amber.map((interval, index) => (
                                            <li key={index} className="is-size-7 mg-10">
                                                {interval} day(s) from reachout &nbsp;&nbsp;
                                                <i onClick={this.deleteReminder.bind(this, 'amber', index)} className="far fa-trash-alt is-hoverable"></i>
                                            </li>
                                        ))}
                                    </ul>   
                                    : <p className="help">No Reminders Set</p>
                                }
                            </div>
                        </div>
                    </div>
                </div> */}
                <br/>
                <SaveChanges 
                    onSaveCurrent={this.openSaveCurrentModal} 
                    onSaveForAll={this.openSaveAllModal} 
                    disable_save={disable_save}
                />
                <ConfirmModal
                    text={modal_text.text}
                    title={modal_text.title}
                    confirm_text={"Yes"}
                    doing={saving}
                    is_active={confirm_active}
                    onConfirm={this.patchBucketCommonConfig}
                    onClose={this.closeModal}
                />
            </div>
        )
    }
}

ChatSettings.propTypes = {
    common_config: PropTypes.object.isRequired,
    client_id: PropTypes.number.isRequired,
    selected_cohort_id: PropTypes.number.isRequired,
    bucket_id: PropTypes.string.isRequired,
    unique_id: PropTypes.string.isRequired,
    bucket_key: PropTypes.string.isRequired,
    refreshBucket: PropTypes.func.isRequired
};

export default connect(null, { patchBucketCommonConfig })(ChatSettings);