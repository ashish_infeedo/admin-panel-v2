import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { connect } from 'react-redux';
// action
import { getUploadFile, uploadMTMFile } from '../../../../../actions/BreabBoardActions';

class DataSync extends Component {

    state = {
        buckets: [],
        touch_points: [],
        selected_touch_points: [],
        downloading: false,
        uploading: false,
        use_employee_code: 0
    }

    componentDidMount(){
        const { selected_bucket } = this.props;
        this.setState({
            touch_points: selected_bucket.touch_points,
            selected_touch_point: selected_bucket.touch_points[0],
            selected_bucket
        });
    }

    getSampleUploadFile = async () => {
        const { client_details, selected_cohort } = this.props;
        const { selected_touch_points, use_employee_code } = this.state;
        if(selected_touch_points.length === 0){
            return
        }
        this.setState({downloading: true});
        await this.props.getUploadFile(client_details.id, selected_cohort.id, 'mtm', selected_touch_points.join(), use_employee_code);
        this.setState({downloading: false});
    }

    uploadMTMFile = async (e) => {
        const { use_employee_code } = this.state;
        const { client_details, selected_cohort } = this.props;
        const upload_files = Array.from(e.target.files);
        const formData = new FormData();
        formData.append('csv', upload_files[0]);
        this.setState({uploading: true});
        await this.props.uploadMTMFile(client_details.id, selected_cohort.id, 'mtm', formData, use_employee_code);
        this.setState({uploading: false});
    }

    checkUncheckTouchpoint = (touch_point_key) => {
        let { selected_touch_points } = this.state;
        selected_touch_points = [];
        selected_touch_points.push(touch_point_key);
        this.setState({selected_touch_points});
    }

    useEmployeeCode = (use_employee_code) => {
        this.setState({use_employee_code});
    }

    render() {
        const { touch_points, selected_touch_points, downloading, uploading, use_employee_code } = this.state;
        return (
            <div>
                <div>
                    <h2 className="is-size-6">Data Upload</h2>
                    <p className="help">Select touchpoints below to download a combination sample file for selected touchpoints</p>
                </div>
                <br/>
                <article className="message">
                    <div className="message-body modal-sub-header-text">
                        <ol>
                            <li><b>Date format</b> for upload is <code>DD-MMM-YYYY</code> e.g. <b>20-JUL-2020</b></li>
                            <li><b>Boolean format</b> for upload is <code>TRUE/FALSE</code></li>
                        </ol>
                    </div>
                </article>
                <div>
                    <p className="is-size-7 has-text-weight-bold mg-top-bottom-25 has-text-grey">SELECT UNIQUE KEY FOR DOWNLOAD & UPLOAD:</p>
                    <div className="tags are-small">
                        <span 
                            onClick={this.useEmployeeCode.bind(this, 0)} 
                            className={classnames('tag is-hoverable', {
                                'is-link': use_employee_code === 0
                            }
                        )}>
                        Email
                        </span>
                        <span 
                            onClick={this.useEmployeeCode.bind(this, 1)} 
                            className={classnames('tag is-hoverable', {
                                'is-link': use_employee_code === 1
                            }
                        )}>
                        Employee Code
                        </span>
                    </div>
                </div>
                <hr/>
                <div className="mg-10 has-text-centered">
                {
                    touch_points.map(touch_point => (
                        <label key={touch_point.touch_point_key} className="radio mg-10">
                            <input onChange={this.checkUncheckTouchpoint.bind(this, touch_point.touch_point_key)} checked={selected_touch_points.includes(touch_point.touch_point_key)} type="radio"/>
                            &nbsp;&nbsp;{touch_point.title}
                        </label>
                    ))
                }
                </div>
                <div className="mg-10 has-text-centered">
                    <button onClick={this.getSampleUploadFile} type="submit" className={classnames('button is-info', {'is-loading': downloading})} disabled={!selected_touch_points.length}>
                        <span className="icon">
                            <i className="fas fa-file-download"></i>
                        </span>
                        <span>Download Sample File</span>
                    </button>
                </div>
                <hr/>
                <div className="mg-10 has-text-centered">
                    <button type="submit" onClick={() => {document.getElementById('uploadMTMData').click();}} className={classnames('button is-success', {'is-loading': uploading})}>
                        <span className="icon">
                            <i className="fas fa-file-upload"></i>
                        </span>
                        <span>Upload MTM Data File</span>
                    </button>
                    {uploading ? null : <input accept=".csv" type="file" id='uploadMTMData' onChange={this.uploadMTMFile} hidden/>}
                </div>
            </div>
        )
    }
}

// PropTypes
// ---------
DataSync.propTypes = {
    getUploadFile: PropTypes.func.isRequired,
    selected_bucket: PropTypes.object.isRequired,
    selected_cohort: PropTypes.object.isRequired,
    uploadMTMFile: PropTypes.func.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    client_details: state.client.client_details,
})

export default connect(mapStateToProps, { getUploadFile, uploadMTMFile })(DataSync);