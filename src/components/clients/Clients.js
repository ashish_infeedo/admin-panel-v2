import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import ReactTooltip from 'react-tooltip';
// components
import Pagination from '../generic/Pagination';
import TextInputgroup from '../generic/TextInputGroup';
import AddClient from './AddClient';
// actions
import { getClients } from '../../actions/ClientsActions';
import { exportCsv } from '../../actions/ExportCsv';
import { ScrollToTop } from '../../helper/Scroll';
import { looseFocus } from '../../helper/LooseFocus';


class Clients extends Component {

    state ={
        payment_status:{
            1: 'On Trial',
            2: 'Amber Mini',
            3: 'Amber Pro',
            4: 'End Trial'
        } ,
        current_page: 1,
        search: '',
        loading: true,
        exporting_all: {},
        exporting_enabled: {},
        exporting_disabled: {},
        show_add_new_clinet: false,
        sort_by: 'created',
        sort_order: 'desc',
        sorting: false
    }

    componentDidMount() {
        this.getClients(this.state.current_page, this.state.search);
    }

    // get Clients 
    // -----------
    getClients = async (page, search, loading_flag = false) => {
        const { sort_by, sort_order } = this.state;
        if(loading_flag){
            this.setState({sorting: true});
        }
        await this.props.getClients(page, search, sort_by, sort_order);
        this.setState({loading: false, sorting: false});
    }

    // Pagination Methods
    // ------------------
    onNext = () => {
        let { current_page } = this.state;
        current_page += 1; 
        this.setState({current_page: current_page});
        this.getClients(current_page, '');
        ScrollToTop();
        
    }

    onPrevious = () => {
        let { current_page } = this.state;
        current_page -= 1;
        this.setState({current_page: current_page});
        this.getClients(current_page, '');
        ScrollToTop();
    }

    jumpToLast = () => {
        const { total_pages, search } = this.props;
        this.setState({current_page: total_pages});
        this.getClients(total_pages, search);
        ScrollToTop();
    }

    // Search
    // ------
    onSearch = e => {
        this.setState({ [e.target.name]: e.target.value });
        const search = e.target.value;
        if(search.length > 2) {
            this.setState({current_page: 1});
            this.getClients(1, search);
        } else if(!search.length) {
            this.setState({current_page: 1});
            this.getClients(1, '');
        }
    };

    exportCsv = async (client_id, enabled) => {
        const { exporting_all, exporting_disabled, exporting_enabled } = this.state;
        looseFocus();
        if(enabled === 1){
            exporting_enabled[client_id] = true;
            this.setState({exporting_enabled});
        } else if(enabled === 2){
            exporting_disabled[client_id] = true;
            this.setState({exporting_disabled});
        } else {
            exporting_all[client_id] = true;
            this.setState({exporting_all});
        }
        await this.props.exportCsv(client_id, enabled);
        if(enabled === 1){
            exporting_enabled[client_id] = false;
            this.setState({exporting_enabled});
        } else if(enabled === 2){
            exporting_disabled[client_id] = false;
            this.setState({exporting_disabled});
        } else {
            exporting_all[client_id] = false;
            this.setState({exporting_all});
        }
    }

    openAddClient = () => {
        this.setState({show_add_new_clinet: true});  
    }

    closeAddClient = () => {
        this.setState({show_add_new_clinet: false});  
    }

    changeSortOrder = () => {
        const { sort_order, search } = this.state;
        if(sort_order === 'asc'){
            this.setState({sort_order: 'desc'}, () => this.getClients(1, search, true));
        } else {
            this.setState({sort_order: 'asc'}, () => this.getClients(1, search, true));
        }
        this.setState({current_page: 1});
    }

    changeSortType = (sort_by_selected) => {
        const { search, sort_by } = this.state;
        if(sort_by ===  sort_by_selected){
            this.changeSortOrder();
            return false;
        }
        if(sort_by_selected === 'title' || sort_by_selected === 'created' || sort_by_selected === 'subscription_end_date'){
            this.setState({sort_by: sort_by_selected}, () => this.getClients(1, search, true))
        }
        this.setState({current_page: 1});
    }

    // Render
    // ------
    render() {
        const { clients, total_pages, page_size } = this.props;
        const { loading, payment_status, current_page, search, exporting_all, exporting_disabled, exporting_enabled, show_add_new_clinet, sort_by, sort_order, sorting } = this.state;
        return (
        <div>
            <div className="columns">
                <div className="column is-11">
                    <TextInputgroup
                        name="search"
                        placeholder="Search Client"
                        value={search}
                        type="text"
                        onChange={this.onSearch}
                        icon="fas fa-search"
                    />
                </div>
                <div onClick={this.openAddClient} className="column is-12-mobile is-1 pd-up-20 has-text-right"><button className="button is-success is-fullwidth"><i className="fas fa-user-plus"></i></button></div>
            </div>
            <div className="card">
                <header className="card-header">
                    <p className="card-header-title">
                    Clients
                    </p>
                </header>
                <div className="card-content is-paddingless">
                    <div className="content">
                        <div className="custom-table">
                            <table className="table is-striped">
                                <thead>
                                    <tr>
                                    <th>S.No.</th>
                                    <th className="width-200"><span className="cursor-pointer" onClick={this.changeSortType.bind(this, 'title')}><u>Title</u></span>
                                        {sort_by === 'title' ? 
                                            (sorting ? 
                                                <button className="button is-small is-white is-loading"></button>
                                            : <span>{sort_order === 'asc' ? 
                                                <span onClick={this.changeSortOrder} className="cursor-pointer">&nbsp;&nbsp;<i className="fas fa-arrow-up"></i></span>
                                                :<span onClick={this.changeSortOrder} className="cursor-pointer">&nbsp;&nbsp;<i className="fas fa-arrow-down"></i></span>
                                              }</span>
                                            )
                                          :null
                                        }
                                    </th>
                                    <th className="width-200">
                                        <span className="cursor-pointer" onClick={this.changeSortType.bind(this, 'created')}><u>Added On</u></span>
                                        {sort_by === 'created' ? 
                                            (sorting ? 
                                                <button className="button is-small is-white is-loading"></button>
                                            : <span>{sort_order === 'asc' ? 
                                                <span onClick={this.changeSortOrder} className="cursor-pointer">&nbsp;<i className="fas fa-arrow-up"></i></span>
                                                :<span onClick={this.changeSortOrder} className="cursor-pointer">&nbsp;<i className="fas fa-arrow-down"></i></span>
                                              }</span>
                                            )
                                          :null
                                        }
                                    </th>
                                    <th className="width-200">
                                        <span className="cursor-pointer" onClick={this.changeSortType.bind(this, 'subscription_end_date')}><u>Sub End Date</u></span>
                                        {sort_by === 'subscription_end_date' ? 
                                            (sorting ? 
                                                <button className="button is-small is-white is-loading"></button>
                                            : <span>{sort_order === 'asc' ? 
                                                <span onClick={this.changeSortOrder} className="cursor-pointer">&nbsp;<i className="fas fa-arrow-up"></i></span>
                                                :<span onClick={this.changeSortOrder} className="cursor-pointer">&nbsp;<i className="fas fa-arrow-down"></i></span>
                                              }</span>
                                            )
                                          :null
                                        }
                                    </th>
                                    <th className="width-200">Language Support</th>
                                    <th className="width-200">Payment Status</th>
                                    <th className="width-100px"><span data-tip="Reduce Sales Cycle">RSC</span></th>
                                    <th className="width-100px">Unique Key</th>
                                    <th className="width-200">Integration</th>
                                    <th>Export User data</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {!loading ? clients.map((client, index) => (
                                <tr key={client.id}>
                                    <td>{index + 1 + (current_page - 1) * (page_size)}</td>
                                    <td>
                                    <Link to={`/admin-panel/clients/${client.id}/basic`}>
                                        <b>{client.title}</b>
                                    </Link>
                                    </td>
                                    <td>{moment(client.created * 1000).format("DD MMM YYYY")}</td>
                                    <td>{moment(client.subscription_end_date * 1000).format("DD MMM YYYY")}</td>
                                    <td>{client.multilingual_enabled ? 'Enabled' : 'Disabled'}</td>
                                    <td>{payment_status[client.status]}</td>
                                    <td><span className={classnames('tag is-small', {'is-success': client.reduce_sales_cycle === 1, 'is-danger': client.reduce_sales_cycle === 0})}>{client.reduce_sales_cycle === 1 ? "On" : 'Off'}</span></td>
                                    <td><small><b>{client.unique_identifier}</b></small></td>
                                    <td>
                                        <small>Type: <b>{client.integration_type} ({client.integration_status})</b></small>
                                    </td>
                                    <td>
                                    <div className="buttons">
                                        <button onClick={this.exportCsv.bind(this, client.id, null)} className={classnames('button is-info is-small is-rounded is-outlined', {'is-loading': exporting_all[client.id]})}>
                                            <span>All</span>
                                        </button>
                                        <button onClick={this.exportCsv.bind(this, client.id, 1)} className={classnames('button is-info is-small is-rounded is-outlined', {'is-loading': exporting_enabled[client.id]})}>
                                            <span>Enabled</span>
                                        </button>
                                        <button onClick={this.exportCsv.bind(this, client.id, 2)} className={classnames('button is-info is-small is-rounded is-outlined', {'is-loading': exporting_disabled[client.id]})}>
                                            <span>Disabled</span>
                                        </button>
                                    </div>
                                    </td>
                                    </tr>
                                )) : null}
                                </tbody>
                            </table>
                            {(clients.length === 0 && !loading) ? <p className="no-data has-text-centered">No Data</p> : null}
                            {loading ? <p className="no-data has-text-centered">Loading Data...</p> : null}
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            {loading
                ? null 
                : <Pagination onNext={this.onNext} onPrevious={this.onPrevious} jumpToLast={this.jumpToLast} current_page={current_page} total_pages={total_pages}/>
            }
            <AddClient is_active={show_add_new_clinet} onClose={this.closeAddClient}/>
            <ReactTooltip place="top" effect="solid"/>
        </div>
        )
    }
}

// PropTypes
// ---------
Clients.propTypes = {
    clients: PropTypes.array.isRequired,
    getClients: PropTypes.func.isRequired,
    exportCsv: PropTypes.func.isRequired,
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    clients: state.client.clients,
    total_pages: state.client.total_pages,
    page_size: state.client.page_size
})

export default connect(mapStateToProps, {getClients, exportCsv})(Clients);