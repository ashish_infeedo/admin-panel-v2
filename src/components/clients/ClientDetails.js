import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {Route} from 'react-router-dom';
import classnames from 'classnames';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ReactTooltip from 'react-tooltip';
// components
import Access from './clientdetails/access/Access';
import Basic from './clientdetails/Basic';
import ActivateFeatures from './clientdetails/activatefeatures/ActivateFeatures'
import BreadBoard from './clientdetails/breadboard/BreadBoard';
import ChangeCrisis from './clientdetails/changecrisis/ChangeCrisis'
import Billing from './clientdetails/Billing';
import Cohorts from './clientdetails/Cohorts';
import Departments from './clientdetails/Departments';
import Emails from './clientdetails/emails/Emails';
import Lifecycle from './clientdetails/Lifecycle'
import Features from './clientdetails/Features';
import Locations from './clientdetails/Locations';
import Summary from './clientdetails/Summary';
import Users from './clientdetails/users/Users';
import PeripheralMessages from './clientdetails/peripheral/PeripheralMessages';
import Banner from '../banner_config/ClientBanner';
import DBUploadLogs from './clientdetails/DBUploadLogs';
// actions
import { getClientDetails, clearClientDetails } from '../../actions/ClientsActions';

class ClientDetails extends Component {

    state = {
        client_id: '',
        selected_tab: '',
        selected_tab_index: 0,
        tabs: [
            {key: 'basic', value: 'Basic Details', component: Basic, enabled: true},
            {key: 'billing', value: 'Billing', component: Billing, enabled: true},
            {key: 'cohorts', value: 'Cohorts', component: Cohorts, enabled: true},
            {key: 'departments', value: 'Departments', component: Departments, enabled: true},
            {key: 'locations', value: 'Locations', component: Locations, enabled: true},
            {key: 'users', value: 'Users', component: Users, enabled: true},
            {key: 'lifecycle', value: 'Lifecycle', component: Lifecycle, enabled: true},
            {key: 'emails', value: 'Emails', component: Emails, enabled: true},
            {key: 'access', value: 'Access', component: Access, enabled: true},
            {key: 'peripheral', value: 'Peripheral Messages', component: PeripheralMessages, enabled: true},
            {key: 'features', value: 'Features', component: Features, enabled: true}, 
            {key: 'summary', value: 'Summary', component: Summary, enabled: true},
            {key: 'DB Upload Logs', value: 'DB Upload', component: DBUploadLogs, enabled: true},
            {key: 'banner_configs', value: 'Banner Config', component: Banner, enabled: true},
            {key: 'activatefeatures', value: 'Dynamic Features', component: ActivateFeatures, enabled: true}
        ],
        indices: {
            'basic': 0,
            'billing': 1,
            'cohorts': 2,
            'departments': 3,
            'locations': 4,
            'users': 5,
            'lifecycle': 6,
            'emails': 7,
            'access': 8,
            'features': 9,
            'summary': 10,
            'dbuploadlogs':11
        },
        loading: true,
        tab_active: 0 // 0 - tenure, 1 - breadboard, 2 - crisis
    }
    
    componentDidMount() {
        this.getURLParameters();
        this.getClientDetails();
    }

    componentWillUnmount() {
        this.props.clearClientDetails();
    }

    componentDidUpdate() {
        this.getURLParameters();
    }

    getClientDetails = async () => {
        const { client_id } = this.props.match.params;
        const response = await this.props.getClientDetails(client_id);
        this.setState({loading: false});
    }

    getURLParameters() {
        const { client_id, selected_tab } = this.props.match.params;
        const { indices } = this.state;
        if(this.state.selected_tab !== selected_tab){
            const index = indices[selected_tab]
            this.setState({selected_tab_index: index});
            this.setState({client_id: client_id, selected_tab: selected_tab});
        }
    }

    goBack = () => {
        const { client_id, selected_tab_index, tabs } = this.state;
        if(selected_tab_index !== 0){
            const next = selected_tab_index - 1;
            this.setState({selected_tab_index: next});
            this.props.history.push(`/admin-panel/clients/${client_id}/${tabs[next].key}`)
        }
    }

    goNext = () => {
        const { client_id, selected_tab_index, tabs } = this.state;
        if(selected_tab_index !== 10){
            const next = selected_tab_index + 1;
            this.setState({selected_tab_index: next});
            this.props.history.push(`/admin-panel/clients/${client_id}/${tabs[next].key}`)
        }
    }

    toggleBreadboard = (tab_active) => {
        this.setState({tab_active});
    }

    render() {
        const {client_id, selected_tab, loading, tabs, tab_active} = this.state;
        const { title } = this.props.client_details;
        return (
        <div>
            {/* breadboard tabs */}
            <div className="tabs is-toggle is-toggle-rounded is-centered">
                <ul>
                    <li className={classnames('', {'is-active': tab_active === 0})} onClick={this.toggleBreadboard.bind(this, 0)}>
                        <a className="min-width-145">
                            <span className="icon is-small"><i className="far fa-calendar-alt"></i></span>
                            <span>Tenure</span>
                        </a>
                    </li>
                    <li className={classnames('', {'is-active': tab_active === 1})} onClick={this.toggleBreadboard.bind(this, 1)}>
                        <a className="min-width-145">
                            <span className="icon is-small"><i className="fas fa-bread-slice"></i></span>
                            <span>Breadboard</span>
                        </a>
                    </li>
                    <li className={classnames('', {'is-active': tab_active === 2})} onClick={this.toggleBreadboard.bind(this, 2)}>
                        <a className="min-width-145">
                            <span className="icon is-small"><i className="fas fa-bomb"></i></span>
                            <span>Change Crisis</span>
                        </a>
                    </li>
                </ul>
            </div>
            {/* Selected Client */}
            <div><h2 className="has-text-weight-semibold is-size-5">&nbsp;&nbsp;{loading ? 'Fetching..' : title}</h2><br/></div>
            {tab_active === 2 ? <ChangeCrisis/> : null }
            {tab_active === 1 ? 
                <BreadBoard
                    getClientDetails={this.getClientDetails}
                /> 
            : null }
            {tab_active === 0 ? <div>
                    <div className="tabs is-small">
                        <ul>
                            {tabs.map((tab) => (
                                <li data-tip={tab.hover} key={tab.key} className={classnames({'is-active': selected_tab === tab.key })}><Link className={classnames('', {'disable-anchor': !tab.enabled})} to={`/admin-panel/clients/${client_id}/${tab.key}`}>{tab.value}</Link></li>
                            ))}
                        </ul>
                    </div>
                    <div>
                    {tabs.map((tab) => (
                    <Route  key={tab.key} path={`/admin-panel/clients/:client_id/${tab.key}`} component={tab.component}/>
                    ))}
                    </div>
                </div>
            : null }
            <div>

            </div>
            <br/><br/>
            <ReactTooltip place="top" effect="solid"/>
        </div>
        )
    }
}


// PropTypes
// ---------
ClientDetails.propTypes = {
    client_details: PropTypes.object.isRequired,
    getClientDetails: PropTypes.func.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    client_details: state.client.client_details,
})

export default connect(mapStateToProps, {getClientDetails, clearClientDetails})(ClientDetails);