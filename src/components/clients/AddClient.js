import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { withRouter } from "react-router";
// components
import TextInputgroup from '../../components/generic/TextInputGroup';
// actions
import { createNewClient } from '../../actions/ClientsActions';

class AddClient extends Component {

    state = {
        id: '',
        title: '',
        title_original: '',
        email_domain: '',
        email_domain_original: '',
        disable_save: true,
        saving: false
    }

    createNewClient = async (e) => {
        e.preventDefault();
        const { title, email_domain } = this.state; 
        const payload = {title: title, email_domain: email_domain};
        this.setState({saving: true});
        const response = await this.props.createNewClient(payload);
        this.setState({title_original: title, email_domain_original: email_domain, disable_save: true, saving: false});
        if(response.status === 200){
            this.props.history.push(`/admin-panel/clients/${response.data.client_id}/basic`)
        }
    }

    onChange = e => {
        this.setState({ [e.target.name]: e.target.value }, () => this.checkIfValuesChanged());
    }

    checkIfValuesChanged = () => {
        const { title, email_domain, title_original, email_domain_original } = this.state;
        if( title !== title_original && email_domain !== email_domain_original){
            this.setState({disable_save: false});
        } else {
            this.setState({disable_save: true});
        }
    }

    render() {
        const { title, email_domain, disable_save, saving } = this.state;
        const { is_active } = this.props;
        return (
                <div className={classnames('modal', {'is-active': is_active})}>
                    <div className="modal-background"></div>
                    <div className="modal-card">
                        <header className="modal-card-head">
                            <p className="modal-card-title">Add Client</p>
                        <button className="delete" onClick={this.props.onClose} aria-label="close"></button>
                        </header>
                        <section className="modal-card-body radius-access">
                        <form onSubmit={this.createNewClient}>
                            <label className="label">Client Name</label>
                            <TextInputgroup
                                name="title"
                                placeholder="Client Name"
                                value={title || ''}
                                type="text"
                                onChange={this.onChange}
                                icon="fas fa-user-circle"
                                required={true}
                            />
                            <label className="label">Primary Email Domain</label>
                            <TextInputgroup
                                name="email_domain"
                                placeholder="Email Domain"
                                value={email_domain || ''}
                                type="text"
                                onChange={this.onChange}
                                icon="fas fa-envelope"
                                required={true}
                            />
                            <button disabled={disable_save} type="submit" className={classnames('button is-primary is-pulled-right', {'is-loading': saving})}>
                                <span className="icon">
                                    <i className="far fa-save"></i>
                                </span>
                                <span>Add</span>
                            </button>
                            <button onClick={this.props.onClose} type="button" className="button is-white is-pulled-right">
                                <span>Cancel</span>
                            </button>
                        </form>
                        </section>
                    </div>
                </div>
        )
    }
}


// PropTypes
// ---------
AddClient.propTypes = {
    createNewClient: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired,
    is_active: PropTypes.bool.isRequired
  }
  
// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
})

export default connect(mapStateToProps, {createNewClient})(withRouter(AddClient));