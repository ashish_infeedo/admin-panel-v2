import React, { Component } from 'react';
import DatePicker from 'react-date-picker';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';
// components
import TextInputgroup from '../generic/TextInputGroup';
// actions
import { getReachouts } from '../../actions/ReachoutActions';

class Reachout extends Component {

    state = {
        date: moment().startOf('day').toDate(),
        search: '',
        all_reachouts: [],
        loading: true
    }

    componentDidMount() {
        const { date } = this.state;
        this.getReachouts(date);
    }

    getReachouts = async (date) => {
        const unix = this.getUnixStamp(date);
        await this.props.getReachouts(unix);
        this.setState({all_reachouts: this.props.reachouts});
        this.setState({loading: false});
    }

    getUnixStamp = (date) => {
        return moment(date).unix();
    }

    // Search
    // ------
    onSearch = e => {
        this.setState({ [e.target.name]: e.target.value });
        const search = e.target.value;
        if(search.length > 0) {
            const results = this.props.reachouts.filter(response => response.title.toLowerCase().includes(search.toLowerCase()));
            this.setState({all_reachouts: results});
        } else if(!search.length) {
            this.setState({all_reachouts: this.props.reachouts});
        }
    };
    
    // On change
    // ---------
    onChange = (date) => {
        this.setState({ date: date });
        this.getReachouts(date);
    }

    render() {
        const { search, all_reachouts, loading } = this.state;
        return (
        <div>
            <div className="columns">
                <div className="column is-10">
                    <TextInputgroup
                        name="search"
                        placeholder="Search Client"
                        value={search}
                        type="text"
                        onChange={this.onSearch}
                        icon="fas fa-search"
                    />
                </div>
                <div className="column pd-up-20">
                <DatePicker onChange={this.onChange} value={this.state.date} /> 
                </div>
            </div>
            <div className="card">
                <header className="card-header">
                    <p className="card-header-title">
                    Reachouts
                    </p>
                </header>
                <div className="card-content is-paddingless">
                    <div className="content">
                        <div className="custom-table">
                            <table className="table is-striped">
                                <thead>
                                    <tr>
                                    <th>S.No.</th>
                                    <th>Client</th>
                                    <th>Reachouts</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {!loading ? all_reachouts.map((reminder, index) => (
                                <tr key={index}>
                                    <td>{index + 1}</td>
                                    <td><b>{reminder.title}</b></td>
                                    <td>{reminder.reachouts}</td>
                                </tr>
                                )): null}
                                </tbody>
                            </table>
                            {(all_reachouts.length === 0 && !loading) ? <p className="no-data has-text-centered">No Data</p> : null}
                            {loading ? <p className="no-data has-text-centered">Loading Data...</p> : null}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}
// PropTypes
// ---------
Reachout.propTypes = {
    reachouts: PropTypes.array.isRequired,
    getReachouts: PropTypes.func.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    reachouts: state.reachout.reachouts,
})

export default connect(mapStateToProps, {getReachouts})(Reachout);