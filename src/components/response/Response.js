import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// components
import TextInputgroup from '../generic/TextInputGroup';
import Dropdown from '../generic/Dropdown';
// actions
import { getResponses } from '../../actions/ResponseActions';


class Response extends Component {

    state ={
        survey_type: 1,
        employee_status: 'Current Employees',
        search: '',
        all_responses: [],
        survey_types: [
            {
                key: 1,
                value: 'Tenure'
            },
            {
                key: 2,
                value: 'MTM'
            }
        ],
        all_employee_status: [
            {
                key: 'Ex-Employees',
                value: 'Ex-Employees'
            },
            {
                key: 'Current Employees',
                value: 'Current Employees'
            }
        ],
        group_by_options: [
            {
                key: 'Clients',
                value: 'clients'
            },
            {
                key: 'Cohorts',
                value: 'cohorts'
            }
        ],
        selected_survey: {key: 1,value: 'Tenure'},
        selected_employee: { key: 'Current Employees',value: 'Current Employees'},
        selected_group_by: { key: 'Clients',value: 'clients'},
        loading: true
    }

    componentDidMount() {
        const {survey_type, employee_status, selected_group_by } = this.state;
        this.getResponses(survey_type, employee_status, selected_group_by.value);
    }

    // Get Responses
    // ---------------
    getResponses = async (survey_type, employee_status, group_by) => {
        await this.props.getResponses(survey_type, employee_status, group_by);
        this.setState({loading: false});
        this.setState({all_responses: this.props.responses});
    }

    // Search
    // ------
    onSearch = e => {
        this.setState({ [e.target.name]: e.target.value });
        const search = e.target.value;
        if(search.length > 0) {
            const results = this.props.responses.filter(response => response.client_title.toLowerCase().includes(search.toLowerCase()));
            this.setState({all_responses: results});
        } else if(!search.length) {
            this.setState({all_responses: this.props.responses});
        }
    };

    // Select Survey type
    // --------------------
    selectSurvey = (selected) => {
        const { employee_status, selected_group_by } = this.state;
        this.setState({survey_type: selected.key, selected_survey: selected});
        this.getResponses(selected.key, employee_status, selected_group_by.value);
    }

    // Select Company
    // --------------
    selectEmployeeType = (status) => {
        const { survey_type, selected_group_by } = this.state;
        this.setState({employee_status: status.key, selected_employee: status});
        this.getResponses(survey_type, status.key, selected_group_by.value);
    }

    // Select Group By
    // --------------
    selectGroupBy = (option) => {
        const { survey_type, employee_status, selected_group_by } = this.state;
        this.setState({selected_group_by: option});
        this.getResponses(survey_type, employee_status, option.value);
    }

    // Render
    // ------
    render() {
        const { loading, all_responses, selected_survey, selected_employee, all_employee_status, survey_types, search, group_by_options, selected_group_by } = this.state;
        return (
        <div>
            <div className="columns">
                <div className="column is-7">
                    <TextInputgroup
                        name="search"
                        placeholder="Search Client"
                        value={search}
                        type="text"
                        onChange={this.onSearch}
                        icon="fas fa-search"
                    />
                </div>
                <div className="column pd-up-20">
                    <Dropdown selected_item={selected_employee} dropdown_items={all_employee_status} onClick={this.selectEmployeeType}/>
                </div>
                <div className="column pd-up-20">
                    <Dropdown selected_item={selected_survey} dropdown_items={survey_types} onClick={this.selectSurvey}/>
                </div>
                <div className="column pd-up-20">
                    <Dropdown selected_item={selected_group_by} dropdown_items={group_by_options} onClick={this.selectGroupBy}/>
                </div>
            </div>
            <div className="card">
                <header className="card-header">
                    <p className="card-header-title">
                    Responses
                    </p>
                </header>
                <div className="card-content is-paddingless">
                    <div className="content">
                        <div className="custom-table">
                            <table className="table is-striped">
                                <thead>
                                    <tr>
                                    <th>S.No.</th>
                                    <th>Client</th>
                                    <th>Response (%)</th>
                                    <th>Responses Count</th>
                                    <th>Total Chat Sent</th>
                                    <th>Answered Count</th>
                                    <th>Dropped Count</th>
                                    <th>To Start Count</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {!loading ? all_responses.map((response, index) => (
                                <tr key={index}>
                                    <td>{index + 1}</td>
                                    <td><b>{response.client_title} {selected_group_by.value === 'cohorts' ? `(${response.cohort_title})` : ''}</b></td>
                                    <td>{response.response_rate}</td>
                                    <td>{response.response_count}</td>
                                    <td>{response.total_chat_sent_count}</td>
                                    <td>{response.answered_count}</td>
                                    <td>{response.dropped_count}</td>
                                    <td>{response.to_start_count}</td>
                                </tr>
                                )): null}
                                </tbody>
                            </table>
                            {(all_responses.length === 0 && !loading) ? <p className="no-data has-text-centered">No Data</p> : null}
                            {loading ? <p className="no-data has-text-centered">Loading Data...</p> : null}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}

// PropTypes
// ---------
Response.propTypes = {
    responses: PropTypes.array.isRequired,
    getResponses: PropTypes.func.isRequired
}

// Map Redux state to props
// ------------------------
const mapStateToProps = (state) => ({
    responses: state.response.responses,
})

export default connect(mapStateToProps, {getResponses})(Response);