import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';

const initialState = {};

const middleware = [thunk];

const host = window.location.hostname;

let store = {};

if(host === 'localhost' && window.__REDUX_DEVTOOLS_EXTENSION__){
    // Dev - redux dev tools
    store = createStore(rootReducer, initialState, compose(applyMiddleware(...middleware), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()));
} else {
    // Prod
    store = createStore(rootReducer, initialState, compose(applyMiddleware(...middleware)));
}

export default store;