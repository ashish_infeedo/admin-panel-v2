import _axios from '../helper/Axios';
import { STORE_REMINDERS } from './Types';
import handleException from '../helper/errorHandling';


export const getReminders = (date) => async dispatch => {
    try {
        const response = await _axios.get(`/v1/clients/reminders?date=${date}`);
        dispatch({ type: STORE_REMINDERS, payload: response.data || []});
    } catch (error) {
        handleException(error);
    }
};