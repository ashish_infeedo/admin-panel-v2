import _axios from '../helper/Axios';
import { STORE_REACHOUTS } from './Types';
import handleException from '../helper/errorHandling';


export const getReachouts = (date) => async dispatch => {
    try {
        const response = await _axios.get(`/v1/clients/reachouts?date=${date}`);
        dispatch({ type: STORE_REACHOUTS, payload: response.data || []});
    } catch (error) {
        handleException(error);
    }
};