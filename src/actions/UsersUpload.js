import _axios from '../helper/Axios';
import { NotificationManager } from 'react-notifications';
import handleException from '../helper/errorHandling';

export const uploadUserCsv = (client_id, file) => async () => {
    try {
        const response = await _axios.post(`/v1/clients/${client_id}/upload_csv`, file, { headers: {'Content-Type': 'multipart/form-data' }});
        NotificationManager.success(`${response.data.insert_count} records inserted and ${response.data.update_count} records updated.`)
        return response.data;
    } catch (error) {
        handleException(error);
    }
};

export const cleanUserCsv = (client_id, file) => async () => {
    try {
        const response = await _axios.post(`/v1/clients/${client_id}/clean_csv`, file, { headers: {'Content-Type': 'multipart/form-data' }});
        NotificationManager.success(`Success`)
        return response.data;
    } catch (error) {
        handleException(error);
    }
};

export const deleteUserCsv = (client_id, file) => async () => {
    try {
        const response = await _axios.delete(`/v1/god/users/${client_id}`, { headers: {'Content-Type': 'multipart/form-data' }, data: file});
        NotificationManager.success(`${response.data.success} user(s) deleted`)
        return response.data;
    } catch (error) {
        handleException(error);
    }
};