import _axios from '../helper/Axios';
import { NotificationManager } from 'react-notifications';
import handleException from '../helper/errorHandling';

export const exportCsv = (client_id, enabled) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/export_csv`, {params: {enabled}});
        NotificationManager.success(response.data || "Success")
    } catch (error) {
        handleException(error);
    }
};