import _axios from '../helper/Axios';
import { NotificationManager } from 'react-notifications';
import handleException from '../helper/errorHandling';

export const getWildDomains = () => async dispatch => {
    try {
        const response = await _axios.get('/v1/wild_domains');
        return response;
    } catch (error) {
        handleException(error);
    }
};

export const saveWildDomains = (wild_domains) => async dispatch => {
    try {
        const response = await _axios.post('/v1/wild_domains',{wild_domains: wild_domains});
        NotificationManager.success('Domains Updated');
        return response;
    } catch (error) {
        handleException(error, 'Failed to save wild domains');
    } 
};
