import _axios from '../helper/Axios';
import { STORE_GOD_REPORTS } from './Types';
import handleException from '../helper/errorHandling';

export const getGodReports = (page, response_status, search, company) => async dispatch => {
    try {
        const response = await _axios.get('/v1/reports/god/data', {params: {page: page, response_status:response_status, search: search, company: company}});
        dispatch({ type: STORE_GOD_REPORTS, payload: response.data});
    } catch (error) {
        handleException(error);
    }
};

export const getChat = (session_id) => async () => {
    try {
        const response = await _axios.get(`/v1/reports/god/transcript/${session_id}`);
        return response.data;
    } catch (error) {
        handleException(error);
    }
};