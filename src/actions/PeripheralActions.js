import _axios from '../helper/Axios';
import { NotificationManager } from 'react-notifications';
import handleException from '../helper/errorHandling';

export const getPeripheralMessages = (client_id, cohort_id, language_id) => async () => {
    try {
        const response = await _axios.get(`v1/clients/${client_id}/peripheral_messages?language_id=${language_id}&cohort_id=${cohort_id}`)
        return response;
    } catch (error) {
        handleException(error);
    }
};

export const updatePeripheralMessages = (client_id, language_id, payload) => async () => {
    try {
        const response = await _axios.put(`v1/clients/${client_id}/peripheral_messages?language_id=${language_id}`, payload)
        NotificationManager.success('Peripheral Messages Updated');
        return response;
    } catch (error) {
        handleException(error);
    }
};