import _axios from "../helper/Axios";
import handleException from "../helper/errorHandling";
import { NotificationManager } from "react-notifications";

// get features data
//----------------------------------------------------------------------------------------
export const getAllFeatures = client_id => async () => {
  try {
    const response = await _axios.get(`/v1/clients/${client_id}/client_params`);
    if (response && response.status === 200 && response.data.data) {
      return response.data.data;
    } else {
      return [];
    }
  } catch (error) {
    NotificationManager.error("Failed to get Features");
    handleException(error, "Failed to get Features");
  }
};

//create a new feature
//----------------------------------------------------------------------------------------
export const addFeature = (client_id, payload) => async () => {
  try {
    const response = await _axios.post(`/v1/clients/${client_id}/create_client_params?key=${payload.key}&value=${payload.value}&cohort_id=${payload.id}`, payload);
    NotificationManager.success("Feature Added");
    return response;
  } catch (error) {
    NotificationManager.error("Failed to create feature");
    handleException(error, "Failed to create feature");
  }
};

//update a existing feature
//----------------------------------------------------------------------------------------
export const updateFeature = (client_id, payload) => async () => {
  try {
    const response = await _axios.patch(`/v1/clients/${client_id}/update_client_params?key=${payload.key}&value=${payload.value}&id=${payload.id}`);
    if (response && response.status === 200) {
      NotificationManager.success("Feature Updated");
    }
    return response;
  } catch (error) {
    NotificationManager.error("Failed to update feature");
    handleException(error, "Failed to update feature");
  }
};

//delete a existing feature
//----------------------------------------------------------------------------------------
export const deleteExistingFeature = (client_id, id) => async () => {
  try {
    const response = await _axios.patch(`/v1/clients/${client_id}/deactivate_client_params?id=${id}`);
    NotificationManager.success("Feature Deleted");
    return response;
  } catch (error) {
    NotificationManager.error("Failed to delete feature");
    handleException(error, "Failed to delete feature");
  }
};
