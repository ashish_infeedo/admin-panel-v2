import _axios from '../helper/Axios';
import handleException from '../helper/errorHandling';
import { BUCKETS_PATCHED, STORE_CLIENT_DETAILS } from './Types';
import { NotificationManager } from 'react-notifications';

export const patchBuckets = (client_id, cohort_id, buckets, global_config, _id, save_for_all) => async (dispatch) => {
    const local_bucket = JSON.parse(JSON.stringify(buckets));
    local_bucket.forEach(bucket => {
        delete bucket.common_config;
        bucket.touchpoints.forEach((touchpoint, index) => {
            const local_touchpoint = {
                bucket_key: touchpoint.bucket_key,
                created: touchpoint.created,
                modified: touchpoint.modified,
                precedence: touchpoint.precedence,
                title: touchpoint.title,
                touchpoint_key: touchpoint.touchpoint_key,
                _id: touchpoint._id,
                enabled: touchpoint.enabled
            }
            bucket.touchpoints[index] = local_touchpoint;
        }) 
    })
    try {
        const response = await _axios.patch(`/v1/clients/${client_id}/cohorts/${cohort_id}/breadboard/buckets`, {buckets: local_bucket, global_config, _id, save_for_all});
        NotificationManager.success("Saved Successfully");
        dispatch({ type: BUCKETS_PATCHED, payload: {}});
        return response;
    } catch (error) {
        handleException(error, 'Failed to save modules');
    }
};

export const getTouchPointConfig = (client_id, cohort_id, module_key, touchpoint_key) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/cohorts/${cohort_id}/breadboard/buckets/${module_key}/touchpoint_config`, {params: {touchpoint_key}});
        return response;
    } catch (error) {
        handleException(error, 'Failed to get config');
    }
}

export const patchTouchPointConfig = (client_id, cohort_id, module_key, touchpoint_key, touchpoint) => async () => {
    try {
        touchpoint.touchpoint_key = touchpoint_key;
        touchpoint.bucket_key = module_key;
        const response = await _axios.patch(`/v1/clients/${client_id}/cohorts/${cohort_id}/breadboard/buckets/${module_key}/touchpoint_config?touchpoint_key=${touchpoint_key}`, {touchpoint});
        NotificationManager.success("Saved Successfully");
        return response;
    } catch (error) {
        handleException(error, 'Failed to get config');
    }
}

export const patchBucketCommonConfig = (client_id, cohort_id, module_key, bucket_id, common_config, unique_id, save_for_all) => async () => {
    try {
        const response = await _axios.patch(`/v1/clients/${client_id}/cohorts/${cohort_id}/breadboard/buckets/${module_key}/common_config`, {
            _id: unique_id, 
            bucket: {
                _id: bucket_id,
                common_config
            }, 
            save_for_all
        });
        NotificationManager.success("Saved Successfully");
        return response;
    } catch (error) {
        handleException(error, 'Failed to save config');
    }
};

export const patchMomentsPrecedence = (client_id, cohort_id, module_key, bucket_id, touchpoints, unique_id) => async () => {
    try {
        const response = await _axios.patch(`/v1/clients/${client_id}/cohorts/${cohort_id}/breadboard/buckets/${module_key}/touchpoint_precedence`, {
            _id: unique_id, 
            bucket: {
                _id: bucket_id,
                touchpoints
            }
        });
        NotificationManager.success("Saved Successfully");
        return response;
    } catch (error) {
        handleException(error, 'Failed to save config');
    }
};

export const patchSelectedMomentSettings = (client_id, cohort_id, module_key, bucket_id, touchpoint, unique_id) => async () => {
    try {
        const response = await _axios.patch(`/v1/clients/${client_id}/cohorts/${cohort_id}/breadboard/buckets/${module_key}/touchpoint_config`, {
            _id: unique_id, 
            bucket: {
                _id: bucket_id,
                touchpoint
            }
        });
        NotificationManager.success("Saved Successfully");
        return response;
    } catch (error) {
        handleException(error, 'Failed to save config');
    }
};

export const getSampleQuestionBankFile = () => async () => {
    try {
        const response = await _axios.get(`/v1/clients/breadboard/download_sample_file`,
        {
            responseType: 'arraybuffer',
        });
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', `Sample_MTM_File.csv`); 
        document.body.appendChild(link);
        link.click();
        return response;
    } catch (error) {
        handleException(error, 'Failed to Sample QB File');
    }
}

export const uploadQbFileMtm = (client_id, cohort_id, file) => async () => {
    try {
        const response = await _axios.post(`/v1/clients/${client_id}/cohorts/${cohort_id}/breadboard/upload_question_banks`, file, { headers: {'Content-Type': 'multipart/form-data' }});
        NotificationManager.success("Uploaded Successfully");
        return response;
    } catch (error) {
        if(error.response.status === 400){
            NotificationManager.success("Uploaded Successfully");
            return;
        }
        if(error && error.response && error.response.data && error.response.data.status === "error"){
            if(error.response.data.error_details && error.response.data.error_details.length){
                NotificationManager.error(JSON.stringify(error.response.data.error_details), 'Upload Failed', 8000);
            } else {
                NotificationManager.error("The uploaded file is not valid");
            }
            return;
        }
        handleException(error, 'Failed to upload File');
    }
}

export const getQuestionBank = (client_id, cohort_id, event) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/cohorts/${cohort_id}/breadboard/question_bank`, {params: {event}});
        return response;
    } catch (error) {
        if(error.response && error.response.data && error.response.data.message === 'Survey does not exist.'){
            NotificationManager.info("QB not uploaded for selected moment");
            return;
        }
        handleException(error, 'Failed to get QB');
    }
}

export const patchQuestion = (client_id, survey_id, cohort_id, question) => async () => {
    try {
        const response = await _axios.patch(`v1/clients/${client_id}/cohorts/${cohort_id}/breadboard/surveys/${survey_id}/questions/${question.id}`, {question});
        NotificationManager.success("Saved Successfully");
        return response;
    } catch (error) {
        handleException(error, 'Failed to get QB');
    }
}

export const deleteQuestion = (client_id, question_id, cohort_id, survey_id) => async () => {
    try {
        const response = await _axios.delete(`/v1/clients/${client_id}/cohorts/${cohort_id}/breadboard/surveys/${survey_id}/questions/${question_id}/delete`);
        NotificationManager.success("Deleted Successfully");
        return response;
    } catch (error) {
        handleException(error, 'Failed to Delete Question');
    }
}

export const triggerQBcreation = (client_id, module_key) => async () => {
    try {
        const mtm_enabled = module_key.includes('mtm')
        if(!mtm_enabled){
            return
        }
        const response = await _axios.post(`/v1/clients/${client_id}/question_bank/copy_master?module_bucket=MTM`);
        return response;
    } catch (error) {
        handleException(error, 'Failed to create QB');
    }
}

export const triggerEmailTemplateCreation = (client_id, cohort_id, module_key) => async () => {
    try {
        const mtm_enabled = module_key.includes('mtm')
        if(!mtm_enabled){
            return
        }
        const response = await _axios.post(`/v1/clients/${client_id}/cohorts/${cohort_id}/breadboard/email_templates?module_bucket=MTM`);
        return response;
    } catch (error) {
        if(error.response && error.response.data && error.response.data.message === "Email Exists"){
            return;
        }
        handleException(error, 'Failed to create Email Templates');
    }
}

export const getBucketEmails = (client_id, cohort_id, bucket_key ) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/cohorts/${cohort_id}/breadboard/buckets/${bucket_key}/email_templates`);
        return response;
    } catch (error) {
        handleException(error, 'Failed to get QB');
    }
}

export const patchEmail = (client_id, cohort_id, bucket_key, email ) => async () => {
    try {
        const payload = {
            template_id: email.id,
            subject: email.subject,
            content: email.content
        }
        const response = await _axios.patch(`/v1/clients/${client_id}/cohorts/${cohort_id}/breadboard/buckets/${bucket_key}/email_templates`, payload);
        NotificationManager.success("Saved Successfully");
        return response;
    } catch (error) {
        handleException(error, 'Failed to get QB');
    }
}


export const getUploadFile = (client_id, cohort_id, bucket_key, touchpoint, use_employee_code ) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/cohorts/${cohort_id}/breadboard/buckets/${bucket_key}/upload_template?touchpoint=${touchpoint}&use_employee_code=${use_employee_code}`, 
        {
            responseType: 'arraybuffer',
        });
        const file_name = touchpoint.toLowerCase().replace(',', '_').concat('_', client_id);
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', `${file_name}.csv`); 
        document.body.appendChild(link);
        link.click();
    } catch (error) {
        var enc = new TextDecoder("utf-8");
        error.response.data = JSON.parse(enc.decode(error.response.data));
        handleException(error, 'Failed to get File');
    }
}


export const uploadMTMFile = (client_id, cohort_id, bucket_key, file, use_employee_code) => async () => {
    try {
        const response = await _axios.post(`/v1/clients/${client_id}/cohorts/${cohort_id}/breadboard/buckets/${bucket_key}/upload?use_employee_code=${use_employee_code}`, file, { headers: {'Content-Type': 'multipart/form-data' }});
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', `uploaded_file_${client_id}.csv`); 
        document.body.appendChild(link);
        link.click();
    } catch (error) {
        if(!error.response){
            NotificationManager.success("File contains more than 500 users, the file processing will take some time. you will receive an email once processing is complete.");
            return;
        }
        handleException(error, 'Failed to upload File');
    }
}

export const tenureTouchpointMigration =  (client_id, cohort_id) =>  async () => {
    try {
        const response = await _axios.post(`/v1/clients/${client_id}/cohorts/${cohort_id}/breadboard/import_tenure_touchpoints`);
        return response;
    } catch (error) {
        handleException(error, 'Failed to import touchpoints');
    }
}


export const getBreadboardStatus =  (client_id) =>  async dispatch => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}`);
        dispatch({ type: STORE_CLIENT_DETAILS, payload: response.data});
        return response;
    } catch (error) {
        handleException(error, 'Failed to import touchpoints');
    }
}

export const patchBreadboardStatus =  (client_id, payload) =>  async () => {
    try {
        const response = await _axios.patch(`/v1/clients/${client_id}`, payload);
        NotificationManager.success("Saved Successfully");
        return response;
    } catch (error) {
        handleException(error, 'Failed to import touchpoints');
    }
}


export const getPeripheralMessages = (client_id, cohort_id, event) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/cohorts/${cohort_id}/breadboard/peripheral_messages?survey_type=2&event=${event}`)
        return response;
    } catch (error) {
        handleException(error);
    }
};

export const updatePeripheralMessages = (client_id, cohort_id, event, payload) => async () => {
    try {
        const response = await _axios.patch(`/v1/clients/${client_id}/cohorts/${cohort_id}/breadboard/peripheral_messages?survey_type=2&event=${event}`, payload)
        NotificationManager.success('Peripheral Messages Updated');
        return response;
    } catch (error) {
        handleException(error);
    }
};

export const getMonitoringStats = (client_id, start_date, end_date) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/breadboard/triggers/internal_use/monitoring`, {params: {client_id, start_date, end_date}})
        return response;
    } catch (error) {
        handleException(error);
    }
};