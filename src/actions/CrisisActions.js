import _axios from '../helper/Axios';
import handleException from '../helper/errorHandling';
import { NotificationManager } from 'react-notifications';

export const getSegments = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/crisis_management/clients/${client_id}/segments`);
        return response;
    } catch (error) {
        handleException(error, 'Failed to get segments');
    }
};

export const createSegments = (client_id, segments) => async () => {
    try {
        const response = await _axios.post(`/v1/crisis_management/clients/${client_id}/segments`, {titles: segments});
        
        return response;
    } catch (error) {
        handleException(error, 'Failed to create segments');
    }
};

export const editSegment = (client_id, segment_id, segment) => async () => {
    try {
        const response = await _axios.patch(`/v1/crisis_management/clients/${client_id}/segments/${segment_id}`, segment);
        NotificationManager.success("Saved Successfully");
        return response;
    } catch (error) {
        handleException(error, 'Failed to patch segment');
    }
};

export const deleteSegment = (client_id, segment_id) => async () => {
    try {
        const response = await _axios.delete(`/v1/crisis_management/clients/${client_id}/segments/${segment_id}`);
        NotificationManager.success("Deleted Successfully");
        return response;
    } catch (error) {
        handleException(error, 'Failed to delete segment');
    }
};

export const getUploadFile = () => async () => {
    try {
        const response = await _axios.get(`/v1/crisis_management/download_sample_file`, 
        {
            responseType: 'arraybuffer',
        });
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', `Sample_Crisis_File.csv`); 
        document.body.appendChild(link);
        link.click();
    } catch (error) {
        handleException(error, 'Failed to get File');
    }
}

export const uploadQbFile = (client_id, segment_id, file) => async () => {
    try {
        const response = await _axios.post(`/v1/crisis_management/clients/${client_id}/segments/${segment_id}/upload_question_banks`, file, { headers: {'Content-Type': 'multipart/form-data' }});
        if(response.data && response.data.error_details && response.data.error_details.length){
            NotificationManager.info(`${JSON.stringify(response.data.error_details)}`, 'Partial Success', 8000);
        } else {
            NotificationManager.success("Uploaded Successfully");
        }
        return response;
    } catch (error) {
        if(error && error.response && error.response.data && error.response.data.status === "error"){
            if(error.response.data.error_details && error.response.data.error_details.length){
                NotificationManager.error(JSON.stringify(error.response.data.error_details), 'Upload Failed', 8000);
            } else {
                NotificationManager.error("The uploaded file is not valid");
            }
            return;
        }
        handleException(error, 'Failed to upload File');
    }
}

export const getAllSurveys  = (client_id, segment_id) => async () => {
    try {
        const response = await _axios.get(`/v1/crisis_management/clients/${client_id}/segments/${segment_id}/surveys`);
        return response;
    } catch (error) {
        handleException(error, 'Failed to get surveys');
    }
};

export const getSpecificQB  = (client_id, segment_id, survey_id) => async () => {
    try {
        const response = await _axios.get(`/v1/crisis_management/clients/${client_id}/segments/${segment_id}/surveys/${survey_id}`);
        return response;
    } catch (error) {
        handleException(error, 'Failed to get QB');
    }
};

export const patchQuestion = (client_id, segment_id, survey_id, question_id, question) => async () => {
    try {
        const response = await _axios.patch(`/v1/crisis_management/clients/${client_id}/segments/${segment_id}/surveys/${survey_id}/questions/${question_id}`, {question});
        NotificationManager.success("Saved Successfully");
        return response;
    } catch (error) {
        handleException(error, 'Failed to patch segment');
    }
};

export const deleteQuestion = (client_id, segment_id, survey_id, question_id) => async () => {
    try {
        const response = await _axios.patch(`/v1/crisis_management/clients/${client_id}/segments/${segment_id}/surveys/${survey_id}/questions/${question_id}/delete`);
        NotificationManager.success("Saved Successfully");
        return response;
    } catch (error) {
        handleException(error, 'Failed to patch segment');
    }
};

export const getDrivers  = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/employee_drivers`);
        return response;
    } catch (error) {
        handleException(error, 'Failed to get Drivers');
    }
};

export const getElements  = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/engagement_elements`);
        return response;
    } catch (error) {
        handleException(error, 'Failed to get Elements');
    }
};


export const getPeripheralMessages  = (client_id, segment_id, survey_id) => async () => {
    try {
        const response = await _axios.get(`/v1/crisis_management/clients/${client_id}/segments/${segment_id}/surveys/${survey_id}/peripheral_messages`);
        return response;
    } catch (error) {
        handleException(error, 'Failed to get Peripheral Msgs');
    }
};


export const patchPeripheralMessages  = (client_id, segment_id, survey_id, payload) => async () => {
    try {
        const response = await _axios.patch(`/v1/crisis_management/clients/${client_id}/segments/${segment_id}/surveys/${survey_id}/peripheral_messages`, payload);
        NotificationManager.success("Saved Successfully");
        return response;
    } catch (error) {
        handleException(error, 'Failed to patch Peripheral Msgs');
    }
};


export const getEmailtemplates  = (client_id, segment_id, survey_id) => async () => {
    try {
        const response = await _axios.get(`/v1/crisis_management/clients/${client_id}/segments/${segment_id}/surveys/${survey_id}/email_templates`);
        return response;
    } catch (error) {
        handleException(error, 'Failed to get Emails');
    }
};

export const patchEmailtemplates  = (client_id, segment_id, survey_id, email_id, subject, content) => async () => {
    try {
        const response = await _axios.patch(`/v1/crisis_management/clients/${client_id}/segments/${segment_id}/surveys/${survey_id}/email_templates/${email_id}`, {subject, content});
        NotificationManager.success("Saved Successfully");
        return response;
    } catch (error) {
        handleException(error, 'Failed to get Emails');
    }
};


export const deleteQB  = (client_id, segment_id, survey_id) => async () => {
    try {
        const response = await _axios.delete(`/v1/crisis_management/clients/${client_id}/segments/${segment_id}/surveys/${survey_id}`);
        NotificationManager.success("Deleted Successfully");
        return response;
    } catch (error) {
        handleException(error, 'Failed to get QB');
    }
};


export const downloadSampleManagerFile = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/crisis_management/clients/${client_id}/manager_persona/download_sample_file`, 
        {
            responseType: 'arraybuffer',
        });
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', `Sample_Manager_File.csv`); 
        document.body.appendChild(link);
        link.click();
    } catch (error) {
        handleException(error, 'Failed to get File');
    }
}

export const cleanManagerFile = (client_id, file) => async () => {
    try {
        const response = await _axios.post(`/v1/crisis_management/clients/${client_id}/manager_persona/clean_csv`, file, { headers: {'Content-Type': 'multipart/form-data' }});
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', `Clean_Manager_File.csv`); 
        document.body.appendChild(link);
        link.click();
        NotificationManager.info("Check cleaned file for errors if any");
    } catch (error) {
        handleException(error, 'Failed to get File');
    }
}

export const uploadManagerFile = (client_id, file) => async () => {
    try {
        const response = await _axios.post(`/v1/crisis_management/clients/${client_id}/manager_persona/upload_csv`, file, { headers: {'Content-Type': 'multipart/form-data' }});
        NotificationManager.success("Managers list updated");
        return response;
    } catch (error) {
        handleException(error, 'Failed to get File');
    }
}


export const downloadAPFSampleFile = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/crisis_management/clients/${client_id}/apf_driver_element`, 
        {
            responseType: 'arraybuffer',
        });
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', `Sample_APF_File.csv`); 
        document.body.appendChild(link);
        link.click();
    } catch (error) {
        handleException(error, 'Failed to get File');
    }
}

export const uploadAPFFile = (client_id, file) => async () => {
    try {
        const response = await _axios.post(`/v1/crisis_management/clients/${client_id}/upload_apf`, file, { headers: {'Content-Type': 'multipart/form-data' }});
        NotificationManager.success("APF links updated");
        return response;
    } catch (error) {
        handleException(error, 'Failed to update APF, check all values are present in CSV');
    }
}

export const getAPFList  = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/crisis_management/clients/${client_id}/apf_articles`);
        return response;
    } catch (error) {
        handleException(error, 'Failed to get Emails');
    }
};

export const getManagersList  = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/crisis_management/clients/${client_id}/manager_persona/managers`);
        return response;
    } catch (error) {
        handleException(error, 'Failed to get Emails');
    }
};

export const updateEmailSettings  = (client_id, manager_id, enabled) => async () => {
    try {
        const response = await _axios.patch(`/v1/crisis_management/clients/${client_id}/manager_persona/${manager_id}/email_report?email_report=${enabled}`);
        return response;
    } catch (error) {
        handleException(error, 'Failed to get Emails');
    }
};
