import _axios from '../helper/Axios';
import { STORE_RESPONSES } from './Types';
import handleException from '../helper/errorHandling';


export const getResponses = (survey_type, employee_status, group_by, is_admin_panel = true) => async dispatch => {
    try {
        const response = await _axios.get('/v1/clients/responses', {params: {survey_type: survey_type, employee_status: employee_status, group_by: group_by, is_admin_panel }});
        dispatch({ type: STORE_RESPONSES, payload: response.data || []});
    } catch (error) {
        handleException(error);
    }
};

export const getChatFeedback = () => async () => {
    try {
        const response = await _axios.get('/v1/clients/chat_feedback');
        return response;
    } catch (error) {
        handleException(error);
    }
};