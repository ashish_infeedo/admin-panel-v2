import _axios from '../helper/Axios';
import { NotificationManager } from 'react-notifications';
import handleException from '../helper/errorHandling';

export const verifyLogin = (credentials) => async () => {
    try {
        const response = await _axios.post('/v1/authenticate', credentials)
        sessionStorage.setItem('token', response.data.token);
        sessionStorage.setItem('profile', JSON.stringify(response.data.profile));
        NotificationManager.success('Login Success')
    } catch (error) {
        handleException(error);
    }
};

export const logout = () => () => {
    sessionStorage.clear();
    NotificationManager.success('Logout Success')
}