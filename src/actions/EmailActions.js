import _axios from '../helper/Axios';
import { NotificationManager } from 'react-notifications';
import handleException from '../helper/errorHandling';


export const sendDemoEmail = (survey_id, emails) => async () => {
    try {
        await _axios.post(`/v1/generateDemoEmails`, {emails: emails, survey_id: survey_id});
        NotificationManager.success('Demo Email sent');
    } catch (error) {
        handleException(error);
    }
};

export const getCohorts = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/cohorts`);
        return response;
    } catch (error) {
        handleException(error);
    }
};

export const  getTemplates = (params, client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/customised_emails?language_id=${params.language_id}${params.cohort_id !== undefined ? `&cohort_id=${params.cohort_id}&` : ''}${params.welcome_email ? `&welcome_mail=true`:''}`);
        return response;
    } catch (error) {
        handleException(error);
    }
};

export const updateTemplate = (template_type, client_id, mail_id, cohort_id = '', language_id, params, type)  => async () => {
    try {
        let response = {}
        if (template_type === 'email') {
            response = await _axios.patch(`/v1/clients/${client_id}/customised_emails/${mail_id}?cohort_id=${cohort_id}&language_id=${language_id}`, params);
        } else {
            response = await _axios.put(`/v1/clients/${client_id}/slack_templates?type=${type}&cohort_id=${cohort_id}&language_id=${language_id}`, params);
        }
        if(response && response.status === 200)
        NotificationManager.success("Email Updated");
    } catch (error) {
        handleException(error);
        NotificationManager.error("Something went wrong");
    }
}

export const getCustomEmailSenders = () => async () => {
    try {
        const response = await _axios.get(`/v1/custom_email`);
        return response.data;
    } catch (error) {
        handleException(error);
    }
};

export const sendCustomEmails = (payload) => async () => {
    try {
        const response = await _axios.post(`/v1/custom_email`, payload);
        NotificationManager.success(`Emails sent to ${response.data.count} users`);
    } catch (error) {
        handleException(error);
    }
};

export const uploadSignature = (file) => async () => {
    try {
        const response = await _axios.post(`/v1/clients/0/upload_signature_image`, file, { headers: {'Content-Type': 'multipart/form-data' }});
        return response.data;
    } catch (error) {
        handleException(error);
    }
};


export const getSlackTemplate = (client_id, type, language_id) => async () => {
    try {
        const response = await _axios.get(`v1/clients/${client_id}/slack_templates?type=${type}&language_id=${language_id}`);
        return response;
    } catch (error) {
        handleException(error);
    }
};