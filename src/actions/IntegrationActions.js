import _axios from '../helper/Axios';
import handleException from '../helper/errorHandling';

export const getIntegrationDetails = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/integration_service`)
        return response;
    } catch (error) {
        handleException(error);
    }
};


export const testSFTPConnection = (client_id, host, port, username, password, type = 0) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/integration_service/check_connection`, {params: {host, port, username, password, type}})
        return response;
    } catch (error) {
        handleException(error, 'Credentials Incorrect', true);
    }
};

export const saveSFTPConnection = (client_id, host, port, username, password, directory, type = 0) => async () => {
    try {
        const response = await _axios.post(`/v1/clients/${client_id}/integration_service`, {host, port, username, password, type, directory})
        return response;
    } catch (error) {
        handleException(error);
    }
};


export const getAmberFields = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/integration_service/amber_fields`);
        return response;
    } catch (error) {
        handleException(error);
    }
};

export const getClientFields = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/integration_service/client_fields`);
        return response;
    } catch (error) {
        handleException(error);
    }
};