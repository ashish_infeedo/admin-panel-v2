import _axios from '../helper/Axios';
import { NotificationManager } from 'react-notifications';
import handleException from '../helper/errorHandling';
import { 
    SAVE_ACCESS_EMAILS,
    SAVE_ACCESS_FILE_URL,
    SAVE_DEMOGRAPHIC_ACCESS,
    STORE_ACCESS_GROUPS
} from './Types';

export const getSubAdmins = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/access_controls`);
        return response.data;
    } catch (error) {
        handleException(error, 'Failed to get sub-admins');
    }
}

export const revokeAdmin =  (client_id, admin_id) => async () => {
    try {
        const response = await _axios.patch(`/v1/clients/${client_id}/access_controls/${admin_id}`, {});
        NotificationManager.success(`Access Revoked`);
        return response.data;
    } catch (error) {
        handleException(error, 'Failed to revoke admin');
    }
}

export const revokeSuperAdmin =  (admin_id) => async () => {
    try {
        const response = await _axios.patch(`/v1/clients/admin/${admin_id}/is_admin/0`);
        NotificationManager.success(`Group Access Revoked`);
        return response.data;
    } catch (error) {
        handleException(error, 'Failed to revoke admin');
    }
}

export const sendResetPasswordEmail =  (admin_id, admin_name) => async () => {
    try {
        const response = await _axios.post(`/v1/user/${admin_id}/token_email`);
        NotificationManager.success(`Reset Password Link sent to ${admin_name}`);
        return response.data;
    } catch (error) {
        handleException(error, 'Failed to revoke admin');
    }
}

export const getMemberUsers = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/member_users`);
        return response.data;
    } catch (error) {
        handleException(error, 'Failed to get memebers');
    }
}

export const getIAMPermssions = (admin_id) => async () => {
    try {
        const response = await _axios.get(`/v1/god/users/${admin_id}/access_controls`);
        return response.data;
    } catch (error) {
        handleException(error, 'Failed to get permissions');
    }
}

export const getManagers = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/manager_access`);
        return response.data;
    } catch (error) {
        handleException(error, 'Failed to get managers');
    }
}

export const getManagerEligibility = (client_id, admin_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/users/${admin_id}/manager_eligibility`);
        return response.data;
    } catch (error) {
        handleException(error, 'Failed to get manager eligibility');
    }
}

export const deleteManagerAccess = (client_id, manager_access_id) => async () => {
    try {
        const response = await _axios.delete(`/v1/clients/${client_id}/manager_access/${manager_access_id}`);
        NotificationManager.success('Access revoked for selected Manager');
        return response;
    } catch (error) {
        handleException(error, 'Failed to delete manager access');
    }
}

export const getDriverElements = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/drivers_and_elements`);
        return response.data;
    } catch (error) {
        handleException(error, 'Failed to get driver elements');
    }
}


export const setManagerAccess = (client_id, user_id, driver_elements) => async () => {
    try {
        const response = await _axios.post(`/v1/clients/${client_id}/manager_access`, {user_id, driver_elements});
        return response.data;
    } catch (error) {
        handleException(error, 'Failed to give manager access');
    }
}

export const getManagerAccess = (client_id, manager_access_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/manager_access/${manager_access_id}`);
        return response.data;
    } catch (error) {
        handleException(error, 'Failed to get manager access');
    }
}

export const getAccessGroups = (client_id) => async (dispatch)=> {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/user_roles`);
        if(response && response.status === 200){
            dispatch({ type: STORE_ACCESS_GROUPS, payload: response.data});
        }
        return response;
    } catch (error) {
        handleException(error, 'Failed to get manager access');
    }
}

export const deleteAccessGroups = (client_id, access_id) => async ()=> {
    try {
        const response = await _axios.delete(`/v1/clients/${client_id}/user_roles/${access_id}`);
        return response;
    } catch (error) {
        handleException(error, 'Failed to get manager access');
    }
}

// export const editAccessGroups = (client_id) => async ()=> {
//     try {
//         const response = await _axios.get(`/v1/clients/${client_id}/user_roles`);
//         return response;
//     } catch (error) {
//         handleException(error, 'Failed to get manager access');
//     }
// }

export const saveAccessGroups = (client_id, data, is_edit_request) => async ()=> {
    try {
        let response = {}
        if(is_edit_request) {
            response = await _axios.patch(`/v1/clients/${client_id}/user_roles/${data.id}`, data);
        } else {
            response = await _axios.post(`/v1/clients/${client_id}/user_roles`, data);
        }
        return response;
    } catch (error) {
        handleException(error, 'Failed to get manager access');
    }
}

export const searchEmailIds = (keyword, client_id) => async (dispatch) => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/users_roles/users?search=${keyword}`);
        return response;
    } catch (error) {
        handleException(error);
    }
}

export const storeAccessEmails = (emails) => async (dispatch) => {
    try {
        dispatch({ type: SAVE_ACCESS_EMAILS, payload: emails});
    } catch (error) {
        handleException(error);
    }
}

export const storeAccessFileURL = (file_url) => async (dispatch) => {
    try {
        dispatch({ type: SAVE_ACCESS_FILE_URL, payload: file_url});
    } catch (error) {
        handleException(error);
    }
}

export const accessUploadCleanFile = (file, client_id) => async (dispatch) => {
    try {
        let url = `/v1/clients/${client_id}/user_roles/clean_file`;
        const response = await _axios.post(url,file);
        if(response && response.status === 200){
            NotificationManager.success('File uploaded successfully');
        }
        return response;
    } catch (error) {
        handleException(error);
    }
}

export const getUserCategoriesForDemographics = (type, client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/${type}`);
        return response;
    } catch (error) {
        handleException(error);
    }
}

export const getCategoriesForDemographics = (type, client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/${type}`);
        return response;
    } catch (error) {
        handleException(error);
    }
}

export const saveDemographicAccessData = (access_data) => async (dispatch) => {
    try {
        dispatch({ type: SAVE_DEMOGRAPHIC_ACCESS, payload: access_data});
    } catch (error) {
        handleException(error);
    }
}

export const saveAdminAccess = (file_path, emails, access_groups, default_view, client_id) => async () => {
    try {
        let response = {};
        if(default_view){
            response = await _axios.post(`/v1/clients/${client_id}/user_roles/all_admins`, {file_path, emails, access_groups, default_view});
        }
        else{
            response = await _axios.post(`/v1/clients/${client_id}/user_roles/all_admins`, {file_path, emails, access_groups});
        }
        if(response && response.status === 200){
            NotificationManager.success(`Saved successfully.`)
        }
        return response;
    } catch (error) {
        handleException(error);
    }
}

export const editAdminAccess = (admin_id, access_groups, default_view, client_id) => async () => {
    try {
        let response = {};
        if(default_view){
            response = await _axios.patch(`/v1/clients/${client_id}/user_roles/all_admins/${admin_id}`, { access_groups, default_view});
        }
        else{
            response = await _axios.patch(`/v1/clients/${client_id}/user_roles/all_admins/${admin_id}`, { access_groups});
        }
        if(response && response.status === 200){
            NotificationManager.success(`Saved successfully.`)
        }
        return response;
    } catch (error) {
        handleException(error);
    }
}

export const deleteAdminAccess = (admin_id, client_id) => async () => {
    try {
        const response = await _axios.delete(`/v1/clients/${client_id}/user_roles/all_admins/${admin_id}`);
        if(response && response.status === 200){
            NotificationManager.success(`Deleted successfully.`)
        }
        return response;
    } catch (error) {
        handleException(error);
    }
}