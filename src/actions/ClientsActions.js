import _axios from '../helper/Axios';
import { 
    STORE_CLIENTS, 
    STORE_CLIENTS_LIST, 
    STORE_CLIENT_DETAILS, 
    CLEAR_CLIENT_DETAILS, 
    UPDATE_CLIENT_DETAILS, 
    STORE_BILLING_CONTACTS, 
    STORE_ADMINS,
    STORE_COHORTS
} from './Types';
import { NotificationManager } from 'react-notifications';
import handleException from '../helper/errorHandling';

// Clients
// -------
export const getClients = (page, search, sort_by, sort_order) => async dispatch => {
    try {
        const response = await _axios.get('/v1/clients', {params: {page: page, search: search, sort_by, sort_order}});
        dispatch({ type: STORE_CLIENTS, payload: response.data});
    } catch (error) {
        handleException(error, 'Failed to get clients');
    }
};


export const getClientsList = () => async dispatch => {
    try {
        const response = await _axios.get('/v1/clients');
        dispatch({ type: STORE_CLIENTS_LIST, payload: response.data});
    } catch (error) {
        handleException(error, 'Failed to get clients list');
    }
}


export const getClientDetails = (client_id) => async dispatch => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}`);
        dispatch({ type: STORE_CLIENT_DETAILS, payload: response.data});
        return response;
    } catch (error) {
        handleException(error, 'Failed to get clients details');
    }
}

export const clearClientDetails = () =>  dispatch => {
    dispatch({ type: CLEAR_CLIENT_DETAILS, payload: null});
}

export const updateClientDetails = (client_id, payload) => async dispatch => {
    try {
        await _axios.patch(`/v1/clients/${client_id}`, payload);
        dispatch({ type: UPDATE_CLIENT_DETAILS, payload: payload});
        NotificationManager.success( 'Details Updated');
    } catch (error) {
        handleException(error, 'Failed to update clients details');
    }
}

export const createNewClient = (payload) => async dispatch => {
    try {
        const response = await _axios.post(`/v1/clients`, payload);
        dispatch({ type: UPDATE_CLIENT_DETAILS, payload: payload});
        NotificationManager.success( 'New Client Created');
        return response;
    } catch (error) {
        handleException(error, 'Failed to create client');
    }
}

// Billing
// -------
export const getBillingContacts = (client_id) => async dispatch => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/billing_contacts`);
        dispatch({ type: STORE_BILLING_CONTACTS, payload: (response.data ? response.data : {} )});
        
    } catch (error) {
        handleException(error, 'Failed to get billing contacts');
    }
}

export const updateContactPersonDetails = (client_id, user_id, payload) => async dispatch => {
    try {
        await _axios.patch(`/v1/clients/${client_id}/billing_contacts/${user_id}`, payload);
        NotificationManager.success( 'Contact Person Details Updated');
    } catch (error) {
        handleException(error, 'Failed to update billing contacts');
    }
}

export const getAdmins = (client_id) => async dispatch => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/user_roles/all_admins`);
        dispatch({ type: STORE_ADMINS, payload: response.data});
        return response.data;
    } catch (error) {
        handleException(error, 'Failed to get admins');
    }
}

// Cohorts
// -------
export const getCohorts = (client_id) => async (dispatch) => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/cohorts`);
        dispatch({ type: STORE_COHORTS, payload: response.data});
        return response.data || [];
    } catch (error) {
        handleException(error, 'Failed to get cohorts');
    }
}

export const addCohorts = (client_id, cohort_string) => async () => {
    try {
        const response = await _axios.post(`/v1/clients/${client_id}/cohorts`, {cohort_string: cohort_string});
        NotificationManager.success(response.data);
    } catch (error) {
        handleException(error, 'Failed to add cohorts');
    }
}

export const editCohort = (client_id, cohort_id, cohort_name) => async () => {
    try {
        const response = await _axios.patch(`/v1/clients/${client_id}/cohorts/${cohort_id}`, {cohort_name: cohort_name});
        NotificationManager.success(response.data);
    } catch (error) {
        handleException(error, 'Failed to edit cohorts');
    }
}

export const deleteCohort = (cohort_id) => async () => {
    try {
        const response = await _axios.delete(`/v1/cohorts/${cohort_id}`);
        NotificationManager.success(response.data);
    } catch (error) {
        handleException(error, 'Failed to delete cohorts');
    }
}

// Sub Departments
// ---------------
export const getSubDepartments = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/sub_departments`);
        return response.data || [];
    } catch (error) {
        handleException(error, 'Failed to get sub-departments');
    }
}

// Departments
// -----------
export const getDepartments = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/departments`);
        return response.data || [];
    } catch (error) {
        handleException(error, 'Failed to get departments');
    }
}

export const addDepartments = (client_id, department_string) => async () => {
    try {
        const response = await _axios.post(`/v1/clients/${client_id}/departments`, {department_string: department_string});
        NotificationManager.success(response.data);
    } catch (error) {
        handleException(error, 'Failed to add departments');
    }
}

export const editDepartment = (department_id, department_name) => async () => {
    try {
        const response = await _axios.patch(`/v1/departments/${department_id}`, {title: department_name});
        NotificationManager.success(response.data);
    } catch (error) {
        handleException(error, 'Failed to edit departments');
    }
}

export const deleteDepartment = (department_id) => async () => {
    try {
        const response = await _axios.delete(`/v1/departments/${department_id}`);
        NotificationManager.success(response.data);
    } catch (error) {
        handleException(error, 'Failed to delete departments');
    }
}

// Locations
// ---------
export const getLocations = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/locations`);
        return response.data || [];
    } catch (error) {
        handleException(error, 'Failed to get locations');
    }
}

export const addLocations = (client_id, location_string) => async () => {
    try {
        const response = await _axios.post(`/v1/clients/${client_id}/locations`, {location_string: location_string});
        NotificationManager.success(response.data);
    } catch (error) {
        handleException(error, 'Failed to add locations');
    }
}

export const editLocation = (location_id, location_name) => async () => {
    try {
        const response = await _axios.patch(`/v1/locations/${location_id}`, {title: location_name});
        NotificationManager.success(response.data);
    } catch (error) {
        handleException(error, 'Failed to edit location');
    }
}

export const deleteLocation = (location_id) => async () => {
    try {
        const response = await _axios.delete(`/v1/locations/${location_id}`);
        NotificationManager.success(response.data);
    } catch (error) {
        handleException(error, 'Failed to delete location');
    }
}

// Users
// -----
export const getUsers = (client_id, page, search) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/users`, {params: {page: page, search: search}});
        return response.data;
    } catch (error) {
        handleException(error, 'Failed to get users');
    }
};

export const changeUserStatus = (client_id, users, enabled) => async () => {
    try {
        const response = await _axios.patch(`/v1/clients/${client_id}/change_users_status`, {users: users, enabled: enabled});
        NotificationManager.success(`Users Updated : ${response.data.total_users_updated}`);
        if(response.data.total_users_not_updated !== 0) {
            NotificationManager.warning(`Users Not Updated : ${response.data.total_users_not_updated}`);
        }
    } catch (error) {
        handleException(error, 'Failed to change user status');
    }
}

export const makePrimaryAdmin = (client_id, user_id) => async () => {
    try {
        await _axios.patch(`/v1/clients/${client_id}/primary_admin/${user_id}`, {id: user_id});
        NotificationManager.success(`Primary Admin Updated`);
    } catch (error) {
        handleException(error, 'Failed to make primary admin');
    }
}


// Surveys
// -----------
export const getSurveys = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/surveys`);
        return response.data;
    } catch (error) {
        handleException(error, 'Failed to get surveys');
    }
};

export const getLaunchCheckList = (client_id) => async dispatch => {
    try {
        const response = await _axios.get(`/v1/checklists/launch/${client_id}`);
        return response;
    } catch (error) {
        handleException(error);
    }
};

export const sendTriggerChat = (client_id) => async dispatch => {
    try {
        const response = await _axios.get(`/test_sendEmail/${client_id}`);
        NotificationManager.success(response.data + '. Please check emails.');
    } catch (error) {
        handleException(error);
    }
};

export const getBusiness = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/business`);
        return response.data || [];
    } catch (error) {
        handleException(error);
    }
}

export const getFunctions = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/functions`);
        return response.data || [];
    } catch (error) {
        handleException(error);
    }
}

export const getManagers = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/managers`);
        return response.data || [];
    } catch (error) {
        handleException(error);
    }
}

export const getTeams = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/teams`);
        return response.data || [];
    } catch (error) {
        handleException(error);
    }
}

export const getHRBPs = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/hrbps`);
        return response.data || [];
    } catch (error) {
        handleException(error);
    }
}

export const getLteams = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/l_teams`);
        return response.data || [];
    } catch (error) {
        handleException(error);
    }
}

export const getCities = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/city`);
        return response.data || [];
    } catch (error) {
        handleException(error);
    }
}

export const getCountry = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/country`);
        return response.data || [];
    } catch (error) {
        handleException(error);
    }
}

export const getVertical = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/vertical`);
        return response.data || [];
    } catch (error) {
        handleException(error);
    }
}

export const getEmployeeType = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/employee_type`);
        return response.data || [];
    } catch (error) {
        handleException(error);
    }
}

export const getSubFunction = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/sub_function`);
        return response.data || [];
    } catch (error) {
        handleException(error);
    }
}

export const getNationality = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/nationality`);
        return response.data || [];
    } catch (error) {
        handleException(error);
    }
}

export const getRegions = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/region`);
        return response.data || [];
    } catch (error) {
        handleException(error);
    }
}

export const getDivisions= (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/division`);
        return response.data || [];
    } catch (error) {
        handleException(error);
    }
}

export const getAdminFeatures = (client_id, admin_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/users/${admin_id}/access_features`);
        return response.data;
    } catch (error) {
        handleException(error);
    }
}

export const setAdminFeatures = (client_id, admin_id, anonymous_bat, access_id) => async () => {
    try {
        const payload = {
            features: {
                anonymous_bat: anonymous_bat
            },
            access_id: access_id
        }
        await _axios.post(`/v1/clients/${client_id}/users/${admin_id}/access_features`, payload);
        return true;
    } catch (error) {
        handleException(error);
    }
}


export const checkUsersInIAM = (client_id, payload) => async () => {
    try {
        const response = await _axios.post(`/v1/clients/${client_id}/check_users_in_iam`, payload);
        return response;
    } catch (error) {
        handleException(error);
    }
}

export const giveAccess = (client_id, admin_id, payload) => async () => {
    try {
        const response = await _axios.post(`/v1/clients/${client_id}/users/${admin_id}/access_controls`, payload);
        NotificationManager.success('Permissions granted for the selected user');
        return response.data;
    } catch (error) {
        handleException(error);
    }
}

export const editAccess = (client_id, admin_id, payload) => async () => {
    try {
        const response = await _axios.patch(`/v1/clients/${client_id}/access_controls/${admin_id}`, payload);
        NotificationManager.success('Permissions granted for the selected user');
        return response.data;
    } catch (error) {
        handleException(error);
    }
}

export const makeAdmin = (admin_id) => async () => {
    try {
        const response = await _axios.patch(`/v1/clients/admin/${admin_id}/is_admin/1`);
        NotificationManager.success('Permissions granted for the selected user');
        return response.data;
    } catch (error) {
        handleException(error);
    }
}

export const getDBUploadLogs = (client_id) => async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/enabled_users_history`);
        return response.data;
    } catch (error) {
        handleException(error);
    }
}