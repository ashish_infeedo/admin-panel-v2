import _axios from '../helper/Axios';
import handleException from '../helper/errorHandling';

export const getDrivers = () => async () => {
    try {
        const response = await _axios.get('/v1/apf/drivers');
        return response;
    } catch (error) {
        handleException(error, 'Failed to get Drivers');
    }
};

export const getElements = (driver_id) => async () => {
    try {
        const response = await _axios.get(`/v1/apf/drivers/${driver_id}/elements`);
        return response;
    } catch (error) {
        handleException(error, 'Failed to get Elements');
    }
};

export const getAllArticles = (driver_id) => async () => {
    try {
        const response = await _axios.get(`/v1/apf/drivers/${driver_id}/articles`);
        return response;
    } catch (error) {
        handleException(error, 'Failed to get Articles');
    }
};

export const createArticle = (driver_id, article) => async () => {
    try {
        const response = await _axios.post(`/v1/clients/apf/drivers/${driver_id}/article`, article);
        return response;
    } catch (error) {
        handleException(error, 'Failed to create article');
    }
};

export const editArticle = (driver_id, article_id, article) => async () => {
    try {
        const response = await _axios.patch(`/v1/clients/apf/drivers/${driver_id}/article/${article_id}`, article);
        return response;
    } catch (error) {
        handleException(error, 'Failed to create article');
    }
};

export const deleteArticle = (driver_id, article_id) => async () => {
    try {
        const response = await _axios.delete(`/v1/clients/apf/drivers/${driver_id}/article/${article_id}`);
        return response;
    } catch (error) {
        handleException(error, 'Failed to delete article');
    }
};

export const getLinkMetaData = (link) => async () => {
    try {
        const response = await _axios.post(`/v1/clients/apf/fetchlink`, link);
        return response;
    } catch (error) {
        handleException(error, 'Failed to get data from link');
    }
};