import _axios from '../helper/Axios';
import _axios_pe from '../helper/AxiosPe';
import { 
    // STORE_USER_DETAILS,
    // CLEAR_USER_DETAILS
} from './Types';
import handleException from '../helper/errorHandling';

// Clients
// -------
export const getUserDetails = (client_id, user_id) => async dispatch => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/users/${user_id}`);
        // dispatch({ type: STORE_USER_DETAILS, payload: response.data});
        return response
    } catch (error) {
        handleException(error);
    }
}


export const uploadUserProfileImage = (client_id, file) => async dispatch => {
    try{
        const response = await _axios.post(`/v1/clients/${client_id}/upload_image`, file, { headers: {'Content-Type': 'multipart/form-data' }})
        return response
    } catch (error) {
        handleException(error);
    }
};
export const removeUserProfileImage = (client_id, user_id) => async dispatch => {
    try{
        const response = await _axios.delete(`/v1/clients/${client_id}/users/${user_id}/upload_image`)
        return response
    } catch (error) {
        handleException(error, 'Failed to remove image');
    }
};
export const updateUserProfile = (client_id, user_id, payload) => async dispatch => {
    try{
        const response = await _axios.patch(`/v1/clients/${client_id}/users/${user_id}`, payload)
        return response
    } catch (error) {
        handleException(error);
    }
};

export const getUserProjections =  (client_id, user_id) =>  async () => {
    try {
        const response = await _axios.get(`/v1/clients/${client_id}/breadboard/users/${user_id}/projections`);
        return response;
    } catch (error) {
        handleException(error, 'Failed to get projections');
    }
}



