import _axios from "../helper/Axios";
import handleException from "../helper/errorHandling";
import { NotificationManager } from "react-notifications";

// get banners data
//----------------------------------------------------------------------------------------
export const getBanners = client_id => async () => {
  try {
    const response = await _axios.get(`/v1/clients/${client_id}/banners`);
    if (response && response.status === 200 && response.data.data) {
      return response.data.data;
    } else {
      return [];
    }
  } catch (error) {
    NotificationManager.error("Failed to get Banners");
    handleException(error, "Failed to get Banners");
  }
};

//create a new banner
//----------------------------------------------------------------------------------------
export const createBanners = (client_id, payload) => async () => {
  try {
    const response = await _axios.post(`/v1/clients/${client_id}/banners`, payload);
    NotificationManager.success("Banner Created");
    return response;
  } catch (error) {
    NotificationManager.error("Failed to create Banner");
    handleException(error, "Failed to create Banner");
  }
};

//update a existing banner
//----------------------------------------------------------------------------------------
export const updateBanners = (client_id, banner_id, payload) => async () => {
  try {
    const response = await _axios.patch(`/v1/clients/${client_id}/banners/${banner_id}`, payload);
    if (response && response.status === 200) {
      NotificationManager.success("Banner Updated");
    }
    return response;
  } catch (error) {
    NotificationManager.error("Failed to update Banner");
    handleException(error, "Failed to update Banner");
  }
};

//delete a existing banner
//----------------------------------------------------------------------------------------
export const deleteExistingBanner = (client_id, banner_id) => async () => {
  try {
    const response = await _axios.delete(`/v1/clients/${client_id}/banners/${banner_id}`);
    NotificationManager.success("Banner Deleted");
    return response;
  } catch (error) {
    NotificationManager.error("Failed to delete Banner");
    handleException(error, "Failed to delete Banner");
  }
};
