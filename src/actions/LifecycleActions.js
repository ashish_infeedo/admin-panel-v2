import _axios from '../helper/Axios';
import handleException from '../helper/errorHandling';

export const getCohorts = async(client_id) => {
    // https://api.amber.fidento.com/v1/clients/179/cohorts
 try{
    const response = await _axios.get(`v1/clients/${client_id}/cohorts`);
      return response.data;
    } catch (error) {
      handleException(error);
    }
}

export const getLifecycleData = async(client_id) => {
  // https://api.amber.fidento.com/v1/clients/179/cohorts
  const requests = [
     _axios.get(`v1/clients/${client_id}/cohorts`),
     _axios.get(`v1/clients/${client_id}/employee_drivers`),
     _axios.get(`v1/clients/${client_id}/engagement_elements`),
  ]
try{
  const response = await Promise.all(requests);
    return response;
  } catch (error) {
    handleException(error);
  }
}

export const uploadStages = async(client_id, language_id, stages) => {
  try {
    const response = await _axios.post(`v1/reports/god/clients/${client_id}/stages/${language_id}`, {stages})
    return response
  } catch (error) {
    handleException(error);
  }
}


export const getTouchpointsList = async (client_id, cohort) => {
  try {
    const response = await _axios.get(`v1/clients/${client_id}/surveys`)
    return response
  } catch (error) {
    handleException(error);
  }
}

export const fetchLanguages = async (client_id, stage) => {
  try {
    const response = await _axios.get(`v1/clients/${client_id}/surveys/${stage}/languages`)
    return response;
  }catch (error) {
    handleException(error)
  }
} 

export const fetchStage = async (client_id, stage_id, language_id) => {
  try {
    const response = await _axios.get(`v1/clients/${client_id}/surveys/${stage_id}/${language_id}`)
    return response;
  }catch (error) {
    handleException(error)
  }
} 

export const updateStages = async(client_id, stage_id, language_id, stage) => {
  try {
    const response = await _axios.patch(`v1/clients/${client_id}/surveys/${stage_id}/${language_id}`, {...stage})
    return response;
  } catch (error) {
    handleException(error)
  }
}

export const archiveStage = async (client_id, stage_id) => {
  try {
    const response  = await _axios.patch(`v1/clients/${client_id}/surveys/${stage_id}/archive`, {archive: 1})
    return response
  } catch (error) {
    handleException(error)
  }
}

export const downloadQB = async (client_id, cohort_id) => {
  try {
    const response  = await _axios.get(`v1/clients/${client_id}/getQuestionBank?cohort_id=${cohort_id}&type=1`)
    console.log(response)
    return response
  } catch (error) {
    handleException(error)
  }
}